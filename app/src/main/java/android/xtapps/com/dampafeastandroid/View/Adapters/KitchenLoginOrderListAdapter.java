package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.Kitchenorder;
import android.xtapps.com.dampafeastandroid.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class KitchenLoginOrderListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<ImageModel> data;
    //  private List<AllProductsApiResponse.ProductList> mProductLists;
    private static LayoutInflater inflater=null;
    private  ViewHolder holder = null;
    HashMap<String, String> song;
    List<Kitchenorder> mProductList;
    String str;
    ItemClickListener mItemClickListner;

    public KitchenLoginOrderListAdapter(Activity a,  List<Kitchenorder> mStoreList,ItemClickListener itemClickListner) {
        activity = a;
        mProductList=mStoreList;
        mItemClickListner = itemClickListner;
        //itemClickListner = clickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return  mProductList.size();
        //return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
        TextView tv_items,tv_status_prep,tv_status_pending,tv_status_cmplte,tv_status_img,tv_image;
        ImageView img_recy_item;
        ListView listview;
        RatingBar recy_rating_bar;
        LinearLayout ll_outer;
        TextView tv_tablename,tv_staff_name,tv_time,tv_comment;
        LinearLayout l_layout_table_item;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Kitchenorder object = mProductList.get(position);
        final Kitchenorder item = mProductList.get(position);
        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.kitchen_login_order_list_item, null);
            holder = new ViewHolder();

            holder.l_layout_table_item = vi.findViewById(R.id.ll_table_item);

            holder.tv_tablename= vi.findViewById(R.id.tv_table_name);
            holder.tv_staff_name= vi.findViewById(R.id.tv_staff_name);
            holder.tv_comment= vi.findViewById(R.id.tv_comment);

            holder.tv_time= vi.findViewById(R.id.tv_time);
            holder.tv_items= vi.findViewById(R.id.tv_items);
            holder.tv_status_prep= vi.findViewById(R.id.tv_status_prep);
            holder.tv_status_pending= vi.findViewById(R.id.tv_status_pending);
            holder.tv_status_cmplte= vi.findViewById(R.id.tv_status_cmplte);
            holder.tv_status_img= vi.findViewById(R.id.tv_status_img);
            holder.tv_image= vi.findViewById(R.id.tv_image);

          /*  str = item.getTable_info().getUpdated_at();
            String[] splited = str.split("\\s+");*/
            holder.tv_tablename.setText(item.getTable_info().getTable_name());
            //holder.tv_staff_name.setText(item.getTable_info().getWaiter_name());
            holder.tv_staff_name.setText(item.getWaiter_name());

            holder.tv_comment.setText(item.getComment());
           // holder.tv_time.setText(splited[1]);
            //holder.tv_time.setText(  run(item.getOrder_time()));

            holder.tv_time.setText(  run(item.getOrder_time()));
           //run(item.getOrder_time(),item.getMenuItemsLists().size());
            holder.tv_items.setText(String.valueOf(item.getMenuItemsLists().size()));
            holder.tv_status_prep.setText(item.getPreparation_items_count());
            holder.tv_status_pending.setText(item.getPending_items_count());
            holder.tv_status_cmplte.setText(item.getCompleted_items_count());


            if(item.getOrder_type().equals("dine_in"))
            {

                holder.tv_image.setBackgroundResource(R.drawable.dine_in_black);
            }
            else
            if(item.getOrder_type().equals("delivery"))
            {
                holder.tv_image.setBackgroundResource(R.drawable.delivery_black);
            }
            else
            if(item.getOrder_type().equals("takeaway"))
            {
                holder.tv_image.setBackgroundResource(R.drawable.take_away_black);
            }

            if(item.getCompleted_items_count().equals(String.valueOf(item.getMenuItemsLists().size())))
            {
                holder.tv_status_img.setBackgroundResource(R.drawable.order_checked);
                holder.l_layout_table_item.setBackgroundResource(R.color.completed_order);
            }

            else  if(item.getPreparation_items_count().equals("0"))
            {
                holder.tv_status_img.setBackgroundResource(R.drawable.new_order_);
                holder.l_layout_table_item.setBackgroundResource(R.color.pending_order);
               // vibrate();
            }
            else
            {
                holder.tv_status_img.setBackgroundResource(R.drawable.hourglass_icon);
                holder.l_layout_table_item.setBackgroundResource(R.color.preparing_order);
            }

            if((run(item.getOrder_time(),item.getMenuItemsLists().size()).equals("1"))
                    && !item.getCompleted_items_count().equals(String.valueOf(item.getMenuItemsLists().size())))
            {
                holder.tv_status_img.setBackgroundResource(R.drawable.hourglass_icon);
                holder.l_layout_table_item.setBackgroundResource(R.color.preparing_order);
            }
            if(AppSession.getInstance().getKitchen_newflag()==1)
            {
                vibrate();
                AppSession.getInstance().setKitchen_newflag(0);
            }
            else
            {

            }
            if(item.getUpdated().equals("1"))
            {
                holder.l_layout_table_item.setBackgroundResource(R.color.updated_order);
            }
            else
            {

            }
            if(AppSession.getInstance().getCompleteOrderList()==1)
            {
                holder.tv_status_img.setBackgroundResource(R.drawable.order_checked);
                holder.l_layout_table_item.setBackgroundResource(R.color.completed_order);
                holder.tv_time.setText("");
               // holder.l_layout_table_item.setEnabled(false);
            }
            else
            {

            }



            holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    mItemClickListner.onItemSelected("select_order", position, item);
                    notifyDataSetChanged();


                }
            });










            vi.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)vi.getTag();
        }


        return vi;
    }

    public String run(String time, int size)
    {
        String dateString = null;
        try
        {

            //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            try {
                Date date1 = simpleDateFormat.parse(time);
                Date currentTime = Calendar.getInstance().getTime();
                //Date date2 = simpleDateFormat.parse("18:00:00");

                dateString=  printDifference(date1, currentTime,size);

            } catch (ParseException e) {
                e.printStackTrace();
            }



        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return dateString;
    }

    //1 minute = 60 seconds
//1 hour = 60 x 60 = 3600
//1 day = 3600 x 24 = 86400
    public String printDifference(Date startDate, Date endDate,int size) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        long delay= (size*4);

        if(elapsedHours>0)
        {
            String aaaa="1";
            return aaaa;
        }
        else {
            if(elapsedMinutes>delay)
            {
                String aaaa="1";
                return aaaa;
            }
            else {
                String aaaa="0";
                return aaaa;
            }
        }



        //String aaaa=String.format("%02d", elapsedHours)+":"+String.format("%02d", elapsedMinutes)+":"+String.format("%02d", elapsedSeconds);
        //return aaaa;
    }

    public String run(String time)
    {
        String dateString = null;
        try
        {

            //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            try {
                Date date1 = simpleDateFormat.parse(time);
                Date currentTime = Calendar.getInstance().getTime();
                //Date date2 = simpleDateFormat.parse("18:00:00");

                dateString=  printDifference(date1, currentTime);

            } catch (ParseException e) {
                e.printStackTrace();
            }



        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return dateString;
    }

    public String printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;



        System.out.printf(
                "%02d days, %02d hours, %02d minutes, %02d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if(elapsedHours>0)
        {
            String aaaa=String.format("%d hrs", elapsedHours)+" "+String.format("%02d +mins", elapsedMinutes);
            return aaaa;
        }
        else {
            String aaaa=String.format("%02d +mins", elapsedMinutes);
            return aaaa;
        }


        //String aaaa=String.format("%02d", elapsedHours)+":"+String.format("%02d", elapsedMinutes)+":"+String.format("%02d", elapsedSeconds);
        //return aaaa;
    }
    private void vibrate() {
        Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(activity, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
