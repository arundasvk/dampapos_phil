package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
public class CustomAdapter  extends RecyclerView.Adapter<CustomAdapter .MyView> {

    private Context context;
    private ArrayList personNames;
    private static LayoutInflater inflater=null;



    public CustomAdapter(Context context, ArrayList personNames) {
       this.context=context;
        this.personNames=personNames;
    }

    public class MyView extends RecyclerView.ViewHolder{

        TextView name,distance,txt_posted,tv_recy_item;
        ImageView iv_catimg;
        LinearLayout grid_container;

        public MyView(View itemView) {
            super(itemView);


           /*name = (TextView) itemView.findViewById(R.id.txt_product_name);
           distance = (TextView) itemView.findViewById(R.id.txt_distance);
           txt_posted = (TextView) itemView.findViewById(R.id.txt_posted);*/
            name = itemView.findViewById(R.id.name);
            // grid_container = (LinearLayout) itemView.findViewById(R.id.grid_container);
        }
    }
    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_activity_recy_row, parent, false);

        return new MyView(itemView);
    }
    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

       /* holder.name.setText("Product Name");
        holder.distance.setText("With in 2 km");
        holder.txt_posted.setText("posted 2 days ago");*/

        holder.name.setText((CharSequence) personNames.get(position));


    }
    @Override
    public int getItemCount() {
        return personNames.size();
    }








}
