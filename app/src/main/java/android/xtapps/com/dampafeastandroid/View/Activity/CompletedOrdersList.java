package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.LogOutTimerUtil;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.KitchenOrderListResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.Kitchenorder;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuItemsList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ReportResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.KitchenLoginOrderListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.KitchenOrderListDetailsadapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

public class CompletedOrdersList  extends AppCompatActivity implements onApiFinish,LogOutTimerUtil.LogOutListener {
    ProgressBar simpleProgressBar;
    private String TAG = CompletedOrdersList.class.getSimpleName();
    private Context mContext;
    ImageView nav_back;
    private String  FROM_KITCHEN_LOGIN_ORDER="kitchen_login_orderlist";
    ArrayList<Kitchenorder> K_login_Orderlist = new ArrayList<Kitchenorder>();
    ArrayList<MenuItemsList> K_login_Orderlist_details = new ArrayList<MenuItemsList>();
    KitchenLoginOrderListAdapter adapter2;
    KitchenOrderListDetailsadapter adapter3;
    private GridView gridview;
    ItemClickListener mItemClickListner;
    private String OrderId="";
    private ListView listview,listview2;
    LinearLayout rl_container;
    ImageView img_close;
    TextView txt_complete_all;
    RelativeLayout toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kitchen_order_list);
        nav_back = (ImageView) findViewById(R.id.nav_back);
        mContext=this;
        listview = findViewById(R.id.listview);
        listview2 = findViewById(R.id.listview2);
        rl_container = findViewById(R.id.rl_container);
        gridview = findViewById(R.id.gridview);
        img_close = findViewById(R.id.img_close);
        txt_complete_all = findViewById(R.id.txt_complete_all);
        toolbar = findViewById(R.id.toolbar);
        simpleProgressBar = findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);
        simpleProgressBar.setVisibility(View.VISIBLE);
        gridview.setVisibility(View.VISIBLE);
        listview.setVisibility(View.GONE);
        txt_complete_all.setVisibility(View.GONE);
        toolbar.setVisibility(View.VISIBLE);
        initCallBackLisrners();
        loadOrderDataList();

        rl_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rl_container.setVisibility(View.GONE);


            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rl_container.setVisibility(View.GONE);


            }
        });

        nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setCompleteOrderList(0);
                finish();
            }
        });
    }



    @Override
    public void onSuccess(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Gson gson = new Gson();
        KitchenOrderListResponse menuResponse = gson.fromJson(response, KitchenOrderListResponse.class);
        if (tag.equals(FROM_KITCHEN_LOGIN_ORDER)) {
            if (mContext!=null) {

                K_login_Orderlist.clear();
                K_login_Orderlist=menuResponse.getCompletedOrders();
                if(K_login_Orderlist!=null)
                {
                    if(AppSession.getInstance().getPrev_size()<(K_login_Orderlist.size()))
                    {
                        AppSession.getInstance().setKitchen_newflag(1);
                        AppSession.getInstance().setPrev_size(K_login_Orderlist.size());
                    }
                    else
                    {
                        AppSession.getInstance().setKitchen_newflag(0);
                        AppSession.getInstance().setPrev_size(K_login_Orderlist.size());
                    }
                    adapter2 = new KitchenLoginOrderListAdapter(CompletedOrdersList.this,K_login_Orderlist,mItemClickListner);
                    gridview.setAdapter(adapter2);
                }

            }

            }

            else {
                Toast.makeText(getApplicationContext(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }




    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        ReportResponse menuResponse = gson.fromJson(response, ReportResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }




    @Override
    protected void onStart() {
        super.onStart();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "OnStart () &&& Starting timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "User interacting with screen");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "onResume()");
    }

    /**
     * Performing idle time logout
     */
    @Override
    public void doLogout() {
        logout();
        // write your stuff here
    }
    private void logout() {
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setTick("logout");
        Intent i=new Intent(CompletedOrdersList.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }

    private void loadOrderDataList() {
        Log.d(TAG, "####################################################loadOrderDataList###########################");

        String URL = AppSession.getInstance().getRegUrl()+ API.KITCHEN_LOGIN_ORDER;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /* values.put("action", "kitchenOrderList");*/
        values.put("action",  API.ACTION_KITCHEN_LOGIN_ORDER);
        //values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id",AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("user_type", AppSession.getInstance().getUser_Type());

        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletedOrdersList.this, values);
        objCallWebServiceTask.execute(URL,FROM_KITCHEN_LOGIN_ORDER, "POST");

    }
    private void initCallBackLisrners() {


        mItemClickListner = new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onItemSelected(String tag, int position, Object response) {

              if (tag.equals("select_order")) {
                    K_login_Orderlist_details = K_login_Orderlist.get(position).getMenuItemsLists();
                    AppSession.getInstance().setOrderType(K_login_Orderlist.get(position).getOrder_type());
                    OrderId=K_login_Orderlist.get(position).getOrder_id();
                    if(K_login_Orderlist_details.size()>0)
                    {
                        //list_popup();
                        rl_container.setVisibility(View.VISIBLE);
                        for(int i=0;i<K_login_Orderlist_details.size();i++)
                        {
                            K_login_Orderlist_details.get(i).setTable_name(K_login_Orderlist.get(position).getTable_info().getTable_name());

                            adapter3 = new KitchenOrderListDetailsadapter(CompletedOrdersList.this,K_login_Orderlist_details,mItemClickListner);
                            listview2.setAdapter(adapter3);
                        }

                    }


                }

            }
        };


    }

    @Override public void onBackPressed() {
        AppSession.getInstance().setCompleteOrderList(0);
        finish();
     }
}
