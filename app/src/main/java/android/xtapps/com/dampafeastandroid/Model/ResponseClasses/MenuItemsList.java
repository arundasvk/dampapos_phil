package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MenuItemsList {
    @SerializedName("menu_id")
    @Expose
    private String menuId;


    private String table_name;

    @SerializedName("menu_name")
    @Expose
    private String menuName;
    @SerializedName("secondary_name")
    @Expose
    private String menuName2;

    @SerializedName("menu_description")
    @Expose
    private String menuDescription;

    @SerializedName("location_name")
    @Expose
    private String locationName;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("minimum_qty")
    @Expose
    private String minimumQty;

    @SerializedName("menu_priority")
    @Expose
    private String menuPriority;

    @SerializedName("mealtime_name")
    @Expose
    private String mealTimeName;

    @SerializedName("menu_price")
    @Expose
    private String menuPrice;

    @SerializedName("order_menu_id")
    @Expose
    private String order_menu_id;

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("menu_type")
    @Expose
    private String menu_type;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("menu_status_id")
    @Expose
    private String menu_status_id;

    @SerializedName("status_name")
    @Expose
    private String status_name;

    @SerializedName("status_comment")
    @Expose
    private String status_comment;

    @SerializedName("options")
    @Expose
    private ArrayList<MenuOptionList> options;

    @SerializedName("menu_options")
    @Expose
    private ArrayList<MenuOptionList> menuOptions;

    int state;
    int selected;


    public ArrayList<MenuOptionList> getMenuOptions() {
        return menuOptions;
    }

    public void setMenuOptions(ArrayList<MenuOptionList> menuOptions) {
        this.menuOptions = menuOptions;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }


    public String getMenuName2() {
        return menuName2;
    }

    public void setMenuName2(String menuName2) {
        this.menuName2 = menuName2;
    }




    public String getMenuDescription() {
        return menuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMinimumQty() {
        return minimumQty;
    }

    public void setMinimumQty(String minimumQty) {
        this.minimumQty = minimumQty;
    }

    public String getMenuPriority() {
        return menuPriority;
    }

    public void setMenuPriority(String menuPriority) {
        this.menuPriority = menuPriority;
    }

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public String getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(String menuPrice) {
        this.menuPrice = menuPrice;
    }
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getOrder_menu_id() {
        return order_menu_id;
    }

    public void setOrder_menu_id(String order_menu_id) {
        this.order_menu_id = order_menu_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMenu_status_id() {
        return menu_status_id;
    }

    public void setMenu_status_id(String menu_status_id) {
        this.menu_status_id = menu_status_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getStatus_comment() {
        return status_comment;
    }

    public void setStatus_comment(String status_comment) {
        this.status_comment = status_comment;
    }

    public ArrayList<MenuOptionList> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<MenuOptionList> options) {
        this.options = options;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }
}
