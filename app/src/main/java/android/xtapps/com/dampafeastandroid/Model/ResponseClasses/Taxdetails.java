package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Taxdetails {
    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("vat_amount")
    @Expose
    private String vat_amount;

    @SerializedName("tax_exemption")
    @Expose
    private String tax_exemption;

    @SerializedName("discount_amount")
    @Expose
    private String discount_amount;

    @SerializedName("service_charge")
    @Expose
    private String service_charge;

    @SerializedName("payable_amount")
    @Expose
    private String payable_amount;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mobile_number")
    @Expose
    private String mobile_number;

    @SerializedName("no_of_pax")
    @Expose
    private String no_of_pax;

    @SerializedName("senior_citizen")
    @Expose
    private String senior_citizen;

    @SerializedName("solo_parent")
    @Expose
    private String solo_parent;

    @SerializedName("disabled")
    @Expose
    private String disabled;

    @SerializedName("coupon_discount")
    @Expose
    private String coupon_discount;

    @SerializedName("vatable_sales")
    @Expose
    private String vatable_sales;

    @SerializedName("non_vatable_sales")
    @Expose
    private String non_vatable_sales;

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getVat_amount() {
        return vat_amount;
    }

    public void setVat_amount(String vat_amount) {
        this.vat_amount = vat_amount;
    }

    public String getTax_exemption() {
        return tax_exemption;
    }

    public void setTax_exemption(String tax_exemption) {
        this.tax_exemption = tax_exemption;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getService_charge() {
        return service_charge;
    }

    public void setService_charge(String service_charge) {
        this.service_charge = service_charge;
    }

    public String getPayable_amount() {
        return payable_amount;
    }

    public void setPayable_amount(String payable_amount) {
        this.payable_amount = payable_amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getNo_of_pax() {
        return no_of_pax;
    }

    public void setNo_of_pax(String no_of_pax) {
        this.no_of_pax = no_of_pax;
    }

    public String getSenior_citizen() {
        return senior_citizen;
    }

    public void setSenior_citizen(String senior_citizen) {
        this.senior_citizen = senior_citizen;
    }

    public String getSolo_parent() {
        return solo_parent;
    }

    public void setSolo_parent(String solo_parent) {
        this.solo_parent = solo_parent;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getVatable_sales() {
        return vatable_sales;
    }

    public void setVatable_sales(String vatable_sales) {
        this.vatable_sales = vatable_sales;
    }

    public String getNon_vatable_sales() {
        return non_vatable_sales;
    }

    public void setNon_vatable_sales(String non_vatable_sales) {
        this.non_vatable_sales = non_vatable_sales;
    }
}
