package android.xtapps.com.dampafeastandroid.DB;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class DbBaseProvider extends ContentProvider {

    private DbHelper mDbHelper;
    private static final UriMatcher uriMatcher;
    private SQLiteDatabase mSqldb;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);


        uriMatcher.addURI(DBConstants.AUTHORITY,
                DBConstants.CART_TABLE, DBConstants.CART);

        uriMatcher.addURI(DBConstants.AUTHORITY,
                DBConstants.MODIFIER_TABLE, DBConstants.MODIFIER);

        uriMatcher.addURI(DBConstants.AUTHORITY,
                DBConstants.TAX_TABLE, DBConstants.TAX);


    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count;
        mSqldb = mDbHelper.getWritableDatabase();
        String databasetable;


        databasetable = uri.getPathSegments().get(0);
        count = mSqldb.delete(databasetable, selection, selectionArgs);
       /* switch (uriMatcher.match(uri)) {
            case DBConstants.CART:
                databasetable = uri.getPathSegments().get(0);
                count = mSqldb.delete(databasetable, selection, selectionArgs);
                break
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }*/
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        mSqldb = mDbHelper.getWritableDatabase();
        long rowID;
        Uri retUri;
        switch (uriMatcher.match(uri)) {


            case DBConstants.CART:

                rowID = mSqldb.insert(DBConstants.CART_TABLE, "",
                        values);
                Log.e("done", "db");
                // ---if added successfully---
                if (rowID > 0) {
                    retUri = ContentUris.withAppendedId(
                            DBConstants.CART_CONTENT_URI, rowID);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return retUri;
                }

                break;

            case DBConstants.MODIFIER:

                rowID = mSqldb.insert(DBConstants.MODIFIER_TABLE, "",
                        values);
                Log.e("done", "db");
                // ---if added successfully---
                if (rowID > 0) {
                    retUri = ContentUris.withAppendedId(
                            DBConstants.CART_CONTENT_URI2, rowID);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return retUri;
                }

                break;

            case DBConstants.TAX:

                rowID = mSqldb.insert(DBConstants.TAX_TABLE, "",
                        values);
                Log.e("done", "db");
                // ---if added successfully---
                if (rowID > 0) {
                    retUri = ContentUris.withAppendedId(
                            DBConstants.CART_CONTENT_URI3, rowID);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return retUri;
                }

                break;


            default:
                throw new SQLException("Failed to insert row into " + uri);


        }
        return null;

    }


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int uriType = 0;
        int insertCount = 0;
        try {

            uriType = uriMatcher.match(uri);
            SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();

            switch (uriType) {

                case DBConstants.CART:
                    try {
                        sqlDB.beginTransaction();
                        for (ContentValues value : values) {
                            long id = sqlDB.insert(DBConstants.CART_TABLE, null, value);
                            if (id > 0)
                                insertCount++;
                        }
                        sqlDB.setTransactionSuccessful();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        sqlDB.endTransaction();
                    }
                    break;

                case DBConstants.MODIFIER:
                    try {
                        sqlDB.beginTransaction();
                        for (ContentValues value : values) {
                            long id = sqlDB.insert(DBConstants.MODIFIER_TABLE, null, value);
                            if (id > 0)
                                insertCount++;
                        }
                        sqlDB.setTransactionSuccessful();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        sqlDB.endTransaction();
                    }
                    break;
                case DBConstants.TAX:
                    try {
                        sqlDB.beginTransaction();
                        for (ContentValues value : values) {
                            long id = sqlDB.insert(DBConstants.TAX_TABLE, null, value);
                            if (id > 0)
                                insertCount++;
                        }
                        sqlDB.setTransactionSuccessful();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        sqlDB.endTransaction();
                    }
                    break;


            }

        } catch (Exception e) {

        }

        return insertCount;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        Log.v("TABLES/oncreate", "CREATEDDDDDDDDDDDDDD" + context);
        mDbHelper = new DbHelper(context);
        // return false;

        /*
         *
         *
         *
         * Create a write able database which will trigger its creation if it
         * doesn't already exist.
         */
        mSqldb = mDbHelper.getWritableDatabase();
        return (mSqldb != null);

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int choice = uriMatcher.match(uri);

        String tableName;
        Cursor cursor;

        mSqldb = mDbHelper.getReadableDatabase();
        tableName = uri.getLastPathSegment();
        queryBuilder.setTables(tableName);

        cursor = queryBuilder.query(mSqldb, projection, selection,
                selectionArgs, null, null, sortOrder);
        if (cursor != null)
            cursor.setNotificationUri(getContext().getContentResolver(),
                    uri);


        /*switch (choice) {


            case DBConstants.CART:
                mSqldb = mDbHelper.getReadableDatabase();
                tableName = uri.getLastPathSegment();
                queryBuilder.setTables(tableName);

                cursor = queryBuilder.query(mSqldb, projection, selection,
                        selectionArgs, null, null, sortOrder);
                if (cursor != null)
                    cursor.setNotificationUri(getContext().getContentResolver(),
                            uri);
                break;


            default:
                throw new IllegalArgumentException("UnKnown URI " + uri);

        }*/
        return cursor;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        mSqldb = mDbHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {

            case DBConstants.CART:
                count = mSqldb.update(DBConstants.CART_TABLE, values, selection, selectionArgs);
                break;

            case DBConstants.MODIFIER:
                count = mSqldb.update(DBConstants.MODIFIER_TABLE, values, selection, selectionArgs);
                break;

            case DBConstants.TAX:
                count = mSqldb.update(DBConstants.TAX_TABLE, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    // Helper Class to operate on DB
    public static class DbHelper extends SQLiteOpenHelper {
        private final Context mContext;

        public DbHelper(Context context) {
            super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
            this.mContext = context;

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    /*        Log.d(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

            // NOTE: This switch statement is designed to handle cascading database
            // updates, starting at the current version and falling through to all
            // future upgrade cases. Only use "break;" when you want to drop and
            // recreate the entire database.
            int version = oldVersion;

            switch (version) {
                case VER_LAUNCH:
                    // Version 22 added column for session feedback URL.
                    db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
                            + SessionsColumns.SESSION_FEEDBACK_URL + " TEXT");
                    version = VER_SESSION_FEEDBACK_URL;

                case VER_SESSION_FEEDBACK_URL:
                    // Version 23 added columns for session official notes URL and slug.
                    db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
                            + SessionsColumns.SESSION_NOTES_URL + " TEXT");
                    db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
                            + SessionsColumns.SESSION_SLUG + " TEXT");
                    version = VER_SESSION_NOTES_URL_SLUG;
            }

            Log.d(TAG, "after upgrade logic, at version " + version);
            if (version != DATABASE_VERSION) {
                Log.w(TAG, "Destroying old data during upgrade");

                db.execSQL("DROP TABLE IF EXISTS " + Tables.BLOCKS);

                // ... delete all your tables ...

                onCreate(db);
            }*/
            db.execSQL("DROP TABLE IF EXISTS " + DBConstants.CART_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DBConstants.MODIFIER_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DBConstants.TAX_TABLE);
            db.execSQL(DBConstants.SQL_CREATE_CART_TABLE);
            db.execSQL(DBConstants.SQL_CREATE_MODIFIER_TABLE);
            db.execSQL(DBConstants.SQL_CREATE_TAX_TABLE);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DBConstants.SQL_CREATE_CART_TABLE);

            db.execSQL(DBConstants.SQL_CREATE_MODIFIER_TABLE);

            db.execSQL(DBConstants.SQL_CREATE_TAX_TABLE);
        }

    }

}
