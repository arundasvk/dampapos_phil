package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.DB.ModifierItems;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModifierSubListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<ImageModel> data;
    //  private List<AllProductsApiResponse.ProductList> mProductLists;
    private static LayoutInflater inflater=null;
    private  ViewHolder holder = null;
    HashMap<String, String> song;
    List<OptionValuesList> mProductList;



    public ModifierSubListAdapter(Activity a, List<OptionValuesList> mStoreList) {
        activity = a;
        mProductList=mStoreList;
        //itemClickListner = clickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return  mProductList.size();
        //return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
        TextView tv_recy_name;
        ImageView img_recy_item;
        ListView listview;
        RatingBar recy_rating_bar;
        LinearLayout ll_outer;
        TextView tv_tablename,tv_staff_name;
        LinearLayout l_layout_table_item;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        OptionValuesList object = mProductList.get(position);

        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.sub_menu_list_row, null);
            holder = new ViewHolder();

            holder.l_layout_table_item = (LinearLayout) vi.findViewById(R.id.l_layout_table_item);

            holder.tv_tablename= (TextView) vi.findViewById(R.id.tv_tablename);
            final OptionValuesList item = mProductList.get(position);
            holder.tv_tablename.setText(item.getValue());

            List<ModifierItems> modifiers = DBMethodsCart.newInstance(activity).getModifierById2(item.getMenu_id(),item.getOption_value_id());
            if(modifiers!=null&&modifiers.size()>0)
            {
                object.setState(1);
                holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
            }
            else
            {
                object.setState(0);
                holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);
            }

            vi.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)vi.getTag();
        }

   /*     holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
            int check=1;
            @Override
            public void onClick(View view) {
                if(check==1)
                {
                  holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
                 check=0;
                }
                else
                {
                    holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);
                    check=1;
                }


            }});*/

        if (object.getState() == 1) {

            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);



        }else
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);

        }
        return vi;
    }




}