package android.xtapps.com.dampafeastandroid.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class DBMethodsCart {
    private static Context mContext;

    private static DBMethodsCart newInstance = new DBMethodsCart();

    public static DBMethodsCart newInstance(Context c) {
        mContext = c;
        if (newInstance == null)
            newInstance = new DBMethodsCart();
        return newInstance;
    }

    //********************************insert to cart****************************************
  /*  public void insertCartItems(String menu_id,String menu_name, String menu_description, String location_name, String location,   String menu_priority,String mealtime_name,String menu_price,
                                int minimum_qty,int menu_qty,int menu_qty_X_menu_price) {
        ContentValues cv = new ContentValues();

        cv.put(DBConstants.CART_MENU_ID, menu_id);
        cv.put(DBConstants.CART_MENU_NAME, menu_name);
        cv.put(DBConstants.CART_MENU_DESC, menu_description);
        cv.put(DBConstants.CART_MENU_LOCATION_NAME, location_name);
        cv.put(DBConstants.CART_MENU_LOCATION, location);
        cv.put(DBConstants.CART_MENU_PRIORITY, menu_priority);
        cv.put(DBConstants.CART_MENU_MEALTIME_NAME, mealtime_name);
        cv.put(DBConstants.CART_MENU_PRICE, menu_price);
        cv.put(DBConstants.CART_MENU_MIN_QTY, minimum_qty);
        cv.put(DBConstants.CART_MENU_QTY, menu_qty);
        cv.put(DBConstants.CART_MENU_QTY_X_MENU_PRICE, menu_qty_X_menu_price);


        mContext.getContentResolver().insert(DBConstants.CART_CONTENT_URI, cv);
    }*/
    public void insertCartItems(String menu_id,String order_menu_id,String category_name,String menu_name,String menu_name2,String order_id, String menu_description, String location_name, String location,   String menu_priority,String mealtime_name,String menu_price,
                                int minimum_qty,int menu_qty,int prev_quantity,double menu_qty_X_menu_price,String required,String comment,String menu_option_id,String menu_option_value_id,String option_value_id,String option_id,String value,String new_price,String price,String status,
                                Double discount,Double discount_percent,String discount_comment) {
        ContentValues cv = new ContentValues();

        cv.put(DBConstants.CART_MENU_ID, menu_id);
        cv.put(DBConstants.CART_ORDER_MENU_ID, order_menu_id);
        cv.put(DBConstants.CART_CATEGORY_NAME, category_name);
        cv.put(DBConstants.CART_MENU_NAME, menu_name);
        cv.put(DBConstants.CART_MENU_NAME2, menu_name2);
        cv.put(DBConstants.CART_ORDER_ID, order_id);
        cv.put(DBConstants.CART_MENU_DESC, menu_description);
        cv.put(DBConstants.CART_MENU_LOCATION_NAME, location_name);
        cv.put(DBConstants.CART_MENU_LOCATION, location);
        cv.put(DBConstants.CART_MENU_PRIORITY, menu_priority);
        cv.put(DBConstants.CART_MENU_MEALTIME_NAME, mealtime_name);
        cv.put(DBConstants.CART_MENU_PRICE, menu_price);
        cv.put(DBConstants.CART_MENU_MIN_QTY, minimum_qty);
        cv.put(DBConstants.CART_MENU_QTY, menu_qty);
        cv.put(DBConstants.CART_MENU_prev_quantity, prev_quantity);
        cv.put(DBConstants.CART_MENU_QTY_X_MENU_PRICE, menu_qty_X_menu_price);

        cv.put(DBConstants.CART_MENU_option_Required, required);
        cv.put(DBConstants.CART_MENU_option_comment, comment);
        cv.put(DBConstants.CART_MENU_option_menu_option_id, menu_option_id);
        cv.put(DBConstants.CART_MENU_option_menu_option_value_id, menu_option_value_id);
        cv.put(DBConstants.CART_MENU_option_option_value_id, option_value_id);
        cv.put(DBConstants.CART_MENU_option_option_id, option_id);
        cv.put(DBConstants.CART_MENU_option_value, value);
        cv.put(DBConstants.CART_MENU_option_new_price, new_price);
        cv.put(DBConstants.CART_MENU_option_price, price);
        cv.put(DBConstants.CART_MENU_ORDER_STATUS, status);
        cv.put(DBConstants.CART_MENU_DISCOUNT, discount);
        cv.put(DBConstants.CART_MENU_DISCOUNT_PERCENT, discount_percent);
        cv.put(DBConstants.CART_MENU_DISCOUNT_COMMENT, discount_comment);

        mContext.getContentResolver().insert(DBConstants.CART_CONTENT_URI, cv);
    }
    //***********************************get cart items**********************************************
/*    public List<CartItem> getCartItems() {

        List<CartItem> items = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int menu_qty_X_menu_price = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));


                CartItem item = new CartItem(menu_id,menu_name,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,menu_qty_X_menu_price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;

    }*/
    public List<CartItem> getCartItems() {

        List<CartItem> items = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.CART_INDEX));

                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String category_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_CATEGORY_NAME));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int prev_quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_prev_quantity));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));

                String required = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_Required));

                String comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_comment));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_id));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_price));

                String status = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ORDER_STATUS));
                Double discount = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT));
                Double discount_percent = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_PERCENT));
                String discount_comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_COMMENT));

                CartItem item = new CartItem(index,menu_id,order_menu_id,category_name,menu_name,menu_name2,order_id,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,prev_quantity,menu_qty_X_menu_price,required,comment,menu_option_id,menu_option_value_id,option_value_id,option_id,value,new_price,price,status,
                        discount,discount_percent,discount_comment);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;

    }
    //**********************************get item by product id from cart************************
  /*  public List<CartItem> getProductById(String productIds) {
        List<CartItem> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_MENU_ID + "='"
                        + productIds + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int menu_qty_X_menu_price = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));


                CartItem item = new CartItem(menu_id,menu_name,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,menu_qty_X_menu_price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }*/
    public List<CartItem> getProductById(String productIds) {
        List<CartItem> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_MENU_ID + "='"
                        + productIds + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.CART_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String category_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_CATEGORY_NAME));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int prev_quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_prev_quantity));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));

                String required = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_Required));
                String comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_comment));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_id));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_price));
                String status = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ORDER_STATUS));
                Double discount = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT));
                Double discount_percent = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_PERCENT));

                String discount_comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_COMMENT));


                CartItem item = new CartItem(index,menu_id,order_menu_id,category_name,menu_name,menu_name2,order_id,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,prev_quantity,menu_qty_X_menu_price,required,comment,menu_option_id,menu_option_value_id,option_value_id,option_id,value,new_price,price,status,
                        discount,discount_percent,discount_comment);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }

    public List<CartItem> getProductByOrderId(String productIds) {
        List<CartItem> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_ORDER_ID + "='"
                        + productIds + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.CART_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String category_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_CATEGORY_NAME));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int prev_quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_prev_quantity));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));

                String required = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_Required));
                String comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_comment));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_id));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_price));
                String status = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ORDER_STATUS));
                Double discount = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT));
                Double discount_percent = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_PERCENT));

                String discount_comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_COMMENT));


                CartItem item = new CartItem(index,menu_id,order_menu_id,category_name,menu_name,menu_name2,order_id,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,prev_quantity,menu_qty_X_menu_price,required,comment,menu_option_id,menu_option_value_id,option_value_id,option_id,value,new_price,price,status,
                        discount,discount_percent,discount_comment);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }
    // get item by status
    public List<CartItem> getByOrderStatus(String Status) {
        List<CartItem> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_MENU_ORDER_STATUS + "='"
                        + Status + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.CART_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String category_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_CATEGORY_NAME));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int prev_quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_prev_quantity));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));

                String required = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_Required));
                String comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_comment));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_id));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_price));
                String status = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ORDER_STATUS));
                Double discount = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT));
                Double discount_percent = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_PERCENT));

                String discount_comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_COMMENT));



                CartItem item = new CartItem(index,menu_id,order_menu_id,category_name,menu_name,menu_name2,order_id,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,prev_quantity,menu_qty_X_menu_price,required,comment,menu_option_id,menu_option_value_id,option_value_id,option_id,value,new_price,price,status,
                        discount,discount_percent,discount_comment);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }

    // get item by discount status
    public List<CartItem> getByOrderDiscount(String Status) {
        List<CartItem> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_MENU_DISCOUNT_COMMENT + "='"
                        + Status + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.CART_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String category_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_CATEGORY_NAME));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                String menu_description = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DESC));
                String location_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION_NAME));
                String location = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_LOCATION));
                String menu_priority = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRIORITY));
                String mealtime_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_MEALTIME_NAME));
                String menu_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_PRICE));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                int prev_quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_prev_quantity));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));

                String required = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_Required));
                String comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_comment));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_option_id));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_option_price));
                String status = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ORDER_STATUS));
                Double discount = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT));
                Double discount_percent = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_PERCENT));

                String discount_comment = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_DISCOUNT_COMMENT));



                CartItem item = new CartItem(index,menu_id,order_menu_id,category_name,menu_name,menu_name2,order_id,menu_description,location_name,location,menu_priority,mealtime_name,menu_price,
                        minimum_qty,menu_qty,prev_quantity,menu_qty_X_menu_price,required,comment,menu_option_id,menu_option_value_id,option_value_id,option_id,value,new_price,price,status,
                        discount,discount_percent,discount_comment);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }

    // get item name * min qty
    public List<CartItem> getItemname_qty() {

        List<CartItem> items = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_ID));
                String order_menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME));
                String menu_name2 = cursor.getString(cursor.getColumnIndex(DBConstants.CART_MENU_NAME2));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.CART_ORDER_ID));
                int minimum_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_MIN_QTY));
                int menu_qty = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
                double menu_qty_X_menu_price = cursor.getDouble(cursor.getColumnIndex(DBConstants.CART_MENU_QTY_X_MENU_PRICE));


                CartItem item = new CartItem(menu_id,order_menu_id,menu_name,menu_name2,
                        minimum_qty,menu_qty,menu_qty_X_menu_price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;

    }
    //*************************************get count***********************************
    public int getCartItemsCount() {

        List<CartItem> items = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, null, null, null);

        return cursor.getCount();

    }
    //**********************************remove from cart**************************
    public void removeFromCart(String productId) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI, DBConstants.CART_MENU_ID + "='"
                + productId + "'", null);

    }
    public void removeFromCartByOrderMenuId(String OrderMenuId) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI, DBConstants.CART_ORDER_MENU_ID + "='"
                + OrderMenuId + "'", null);

    }
    //*************************************clear cart**********************************
    public void clearCart(){
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI, null, null);
    }
    //**********************************update cart*****************************************
    public void updateCart(String menuId,String ordermenuId, String status,String prev_quantity, String orderId) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.CART_MENU_ORDER_STATUS, status);
        cv.put(DBConstants.CART_ORDER_ID, orderId);
        cv.put(DBConstants.CART_ORDER_MENU_ID, ordermenuId);
        cv.put(DBConstants.CART_MENU_prev_quantity, prev_quantity);


        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_MENU_ID + "='"
                + menuId + "'", null);
    }

    public void updateCartDiscount(String menuId, Double discount,Double discount_percent, String orderId,String discount_comment) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.CART_MENU_DISCOUNT_PERCENT, discount_percent);
        cv.put(DBConstants.CART_MENU_DISCOUNT, discount);
        cv.put(DBConstants.CART_MENU_DISCOUNT_COMMENT, discount_comment);
        cv.put(DBConstants.CART_ORDER_ID, orderId);



        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_ORDER_MENU_ID + "='"
                + menuId + "'", null);
    }

    public void updateCartDiscount(String discount_comment, String orderId) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.CART_MENU_DISCOUNT_COMMENT, discount_comment);
        cv.put(DBConstants.CART_ORDER_ID, orderId);



        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_ORDER_ID + "='"
                + orderId + "'", null);
    }

    public void updateCartDiscount(String discount_comment) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.CART_MENU_DISCOUNT_COMMENT, discount_comment);




        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_MENU_DISCOUNT_COMMENT + "='"
                + "" + "'", null);
    }

    public int updateCart(boolean isAdd, String productId,Double price,String status,String discount) {

        int quantity = 0;

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI, null, DBConstants.CART_ORDER_MENU_ID + "='"
                        + productId + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            quantity = cursor.getInt(cursor.getColumnIndex(DBConstants.CART_MENU_QTY));
            if (isAdd)
                ++quantity;
            else
                --quantity;

            if (quantity > 0) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.CART_MENU_QTY, quantity);
                cv.put(DBConstants.CART_MENU_QTY_X_MENU_PRICE, price);
                cv.put(DBConstants.CART_MENU_ORDER_STATUS, status);
                cv.put(DBConstants.CART_MENU_DISCOUNT, discount);
                mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_ORDER_MENU_ID + "='"
                        + productId + "'", null);
            }
        }

        return quantity;
    }

    public void updateCart(String menuId, int quantity,double price,String Menu_option_id,String Menu_option_value_id,
                           String Option_value_id,String Option_id,String Value,String New_price,String Price, String Comment,String status ) {
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.CART_MENU_QTY, quantity);
        cv.put(DBConstants.CART_MENU_QTY_X_MENU_PRICE, price);
        cv.put(DBConstants.CART_MENU_option_comment, Comment);
        cv.put(DBConstants.CART_MENU_option_menu_option_value_id, Menu_option_value_id);
        cv.put(DBConstants.CART_MENU_option_option_value_id, Option_value_id);
        cv.put(DBConstants.CART_MENU_option_option_id, Option_id);
        cv.put(DBConstants.CART_MENU_option_value, Value);
        cv.put(DBConstants.CART_MENU_option_new_price, New_price);
        cv.put(DBConstants.CART_MENU_option_price, Price);
        cv.put(DBConstants.CART_MENU_option_menu_option_id, Menu_option_id);
        cv.put(DBConstants.CART_MENU_ORDER_STATUS, status);


        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI, cv, DBConstants.CART_ORDER_MENU_ID + "='"
                + menuId + "'", null);
    }




    /**************************************modifier table*************************************************/
    public void insertModifierItems(String menu_id,String menu_name,String menu_option_id,String menu_option_value_id,String option_value_id,String option_id,String option_name,String display_type,String value,String new_price,String price) {
        ContentValues cv = new ContentValues();

        cv.put(DBConstants.MODIFIER_MENU_ID, menu_id);
        cv.put(DBConstants.MODIFIER_MENU_NAME, menu_name);
        cv.put(DBConstants.MODIFIER_MENU_option_menu_option_id, menu_option_id);
        cv.put(DBConstants.MODIFIER_menu_option_value_id, menu_option_value_id);
        cv.put(DBConstants.MODIFIER_option_option_value_id, option_value_id);
        cv.put(DBConstants.MODIFIER_option_option_id, option_id);
        cv.put(DBConstants.MODIFIER_option_option_name, option_name);
        cv.put(DBConstants.MODIFIER_option_display_type, display_type);
        cv.put(DBConstants.MODIFIER_option_value, value);
        cv.put(DBConstants.MODIFIER_option_new_price, new_price);
        cv.put(DBConstants.MODIFIER_option_price, price);




        mContext.getContentResolver().insert(DBConstants.CART_CONTENT_URI2, cv);
    }

    public void updateModifierItems(String menu_id,String menu_option_id,String menu_option_value_id,String option_value_id,String option_id,String option_name,String display_type,String value,String new_price,String price) {
        ContentValues cv = new ContentValues();

        cv.put(DBConstants.MODIFIER_MENU_ID, menu_id);
        cv.put(DBConstants.MODIFIER_MENU_option_menu_option_id, menu_option_id);
        cv.put(DBConstants.MODIFIER_menu_option_value_id, menu_option_value_id);
        cv.put(DBConstants.MODIFIER_option_option_value_id, option_value_id);
        cv.put(DBConstants.MODIFIER_option_option_id, option_id);
        cv.put(DBConstants.MODIFIER_option_option_name, option_name);
        cv.put(DBConstants.MODIFIER_option_display_type, display_type);
        cv.put(DBConstants.MODIFIER_option_value, value);
        cv.put(DBConstants.MODIFIER_option_new_price, new_price);
        cv.put(DBConstants.MODIFIER_option_price, price);




        mContext.getContentResolver().update(DBConstants.CART_CONTENT_URI2, cv, DBConstants.MODIFIER_MENU_ID + "='"
                + menu_id + "'", null);
    }


    public List<ModifierItems> getModifierById(String menuoption_id,String optionvalue_id) {
        List<ModifierItems> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI2, null, DBConstants.MODIFIER_MENU_option_menu_option_id + "='"
                        + menuoption_id + "'"
                        +" and "
                        +DBConstants.MODIFIER_menu_option_value_id + "='"
                        + optionvalue_id + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_NAME));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_id));
                String option_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_name));
                String display_type = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_display_type));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_price));



                ModifierItems item = new ModifierItems(index,menu_id,menu_name,menu_option_id,menu_option_value_id,option_value_id,option_id,option_name,display_type,value,new_price,price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }
    public List<ModifierItems> getModifierById2(String menuoption_id,String optionvalue_id) {
        List<ModifierItems> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI2, null, DBConstants.MODIFIER_MENU_ID + "='"
                        + menuoption_id + "'"
                        +" and "
                        +DBConstants.MODIFIER_option_option_value_id + "='"
                        + optionvalue_id + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_NAME));
                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_id));
                String option_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_name));
                String display_type = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_display_type));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_price));



                ModifierItems item = new ModifierItems(index,menu_id,menu_name,menu_option_id,menu_option_value_id,option_value_id,option_id,option_name,display_type,value,new_price,price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }


    public List<ModifierItems> getModifierByMenuId(String menuoption_id) {
        List<ModifierItems> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI2, null, DBConstants.MODIFIER_MENU_ID + "='"
                        + menuoption_id + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_INDEX));
                String menu_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_ID));
                String menu_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_NAME));

                String menu_option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_MENU_option_menu_option_id));
                String menu_option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_menu_option_value_id));
                String option_value_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_value_id));
                String option_id = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_id));
                String option_name = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_option_name));
                String display_type = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_display_type));
                String value = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_value));
                String new_price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_new_price));
                String price = cursor.getString(cursor.getColumnIndex(DBConstants.MODIFIER_option_price));



                ModifierItems item = new ModifierItems(index,menu_id,menu_name,menu_option_id,menu_option_value_id,option_value_id,option_id,option_name,display_type,value,new_price,price);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }

    //***********************************************delete********************************************************
    public void clearModifier(){
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI2, null, null);
    }
    //***************************************remove ************************************************
    public void removeFromModifier(String productId) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI2, DBConstants.CART_MENU_ID + "='"
                + productId + "'", null);

    }
    public void removeFromModifierByOrderMenuId(String productId) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI2, DBConstants.CART_MENU_ID + "='"
                + productId + "'", null);

    }

    public void removeFromModifier2(String productId,String optionid) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI2, DBConstants.CART_MENU_ID + "='"
                + productId + "'"
                +" and "
                +DBConstants.MODIFIER_option_option_value_id + "='"
                + optionid + "'", null);

    }
    //*****************************************************TAX_TABLE******************************************************

    public void insertTaxItems(String order_id,String vat_amount,String tax_exemption,String discount_amount,String service_charge,String payable_amount) {
        ContentValues cv = new ContentValues();

        cv.put(DBConstants.TAX_ORDER_ID, order_id);
        cv.put(DBConstants.TAX_VAT_AMOUNT, vat_amount);
        cv.put(DBConstants.TAX_EXEMPTION, tax_exemption);
        cv.put(DBConstants.TAX_DISCOUNT_AMOUNT, discount_amount);
        cv.put(DBConstants.TAX_SERVICE_CAHARGE, service_charge);
        cv.put(DBConstants.TAX_PAYABLE_AMOUNT, payable_amount);





        mContext.getContentResolver().insert(DBConstants.CART_CONTENT_URI3, cv);
    }


    public List<TaxItems> getTaxItemsByOrderId(String orderid) {
        List<TaxItems> items = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(
                DBConstants.CART_CONTENT_URI3, null, DBConstants.TAX_ORDER_ID + "='"
                        + orderid + "'", null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String index = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_INDEX));
                String order_id = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_ORDER_ID));
                String vat_amount = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_VAT_AMOUNT));

                String tax_exemption = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_EXEMPTION));
                String discount_amount = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_DISCOUNT_AMOUNT));
                String service_charge = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_SERVICE_CAHARGE));
                String payable_amount = cursor.getString(cursor.getColumnIndex(DBConstants.TAX_PAYABLE_AMOUNT));


                TaxItems item = new TaxItems(index,order_id,vat_amount,tax_exemption,discount_amount,service_charge,payable_amount);
                items.add(item);

            } while (cursor.moveToNext());
            cursor.close();

        }

        return items;
    }

    public void clearTaxItems(){
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI3, null, null);
    }
    //***************************************remove ************************************************
    public void removeTaxItems(String productId) {
        mContext.getContentResolver().delete(DBConstants.CART_CONTENT_URI3, DBConstants.TAX_ORDER_ID + "='"
                + productId + "'", null);

    }



    }