package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.OrderlistAdapter;

import java.util.ArrayList;

public class OrdersFragment extends Fragment {
    Context mContext;
    private  AlertDialog.Builder dialogBuilder;
    private  AlertDialog alertDialog;
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{
            R.drawable.ic_menu_camera,R.drawable.ic_menu_gallery,R.drawable.ic_menu_send,
            R.drawable.ic_menu_share,R.drawable.ic_menu_slideshow,R.drawable.ic_menu_manage
            ,R.drawable.ic_menu_slideshow};
    private ListView listview;
    OrderlistAdapter adapter;
    public static OrdersFragment newInstance() {
        OrdersFragment fragment = new OrdersFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.order_fragment, container, false);
        listview = view.findViewById(R.id.listview);
        mContext = getActivity();
        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();

        adapter = new OrderlistAdapter(getActivity(),imageModelArrayList);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


               // Toast.makeText(getActivity(),"order", Toast.LENGTH_SHORT).show();

       alert();



            }
        });

        return view;
    }

    private void alert() {
        dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.NewDialog);
        // LayoutInflater inflater = mContext.getLayoutInflater();
       // LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.order_complete_popup, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();

        // TextView mText=(TextView)dialogView.findViewById(R.id.mText) ;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);

        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();



        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    // To dismiss the fragment when the back-button is pressed.
                    alertDialog.hide();
                    return true;
                }


                // Otherwise, do nothing else
                else return false;
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){


                /****cleanup code****/
            }});
    }

    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 7; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawablee(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }
}
