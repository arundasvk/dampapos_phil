package android.xtapps.com.dampafeastandroid.Webservices.backgroundtask;

import android.content.Context;
import android.os.AsyncTask;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Webservices.controller.AppLog;
import android.xtapps.com.dampafeastandroid.Webservices.network.MultipartUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Created by align3 on 27/7/15.
 */
public class CallMultipartWebServiceTask extends AsyncTask<String,Void,String> {

    onApiFinish listener;
    Context mcontext;
    private String TAG="CallWebServiceTask";
    HashMap<String,String> values;
    String from;

    public CallMultipartWebServiceTask(Context mcontext, HashMap<String, String> values){
        this.listener=(onApiFinish)mcontext;
        this.mcontext=mcontext;
        this.values=values;

    }
    public CallMultipartWebServiceTask(Context mContext, onApiFinish listener, HashMap<String, String> values) {
        this.listener = listener;
        this.mcontext = mContext;
        this.values=values;


    }
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... params) {
        String charset = "UTF-8";
        String url = params[0];
        this.from=params[1];
        String result=null;
        try {
            MultipartUtility multipart = new MultipartUtility(url, charset);
/*
            multipart.addHeaderField("User-Agent", "CodeJava");
            multipart.addHeaderField("Test-Header", "Header-Value");*/

            if (values != null) {
                Iterator entries = values.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry entry = (Map.Entry) entries.next();
                    multipart.addFormField(entry.getKey().toString(), entry.getValue().toString());
                    // entries.remove(); // avoids a ConcurrentModificationException
                }
            }
  /*          multipart.addFormField("description", "Cool Pictures");
            multipart.addFormField("keywords", "Java,upload,Spring");*/
            if(params[2]!=null) {
                File f = new File(params[2]);

                multipart.addFilePart("file", f);
            }


            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");

            for (String line : response) {
                result=line;
                AppLog.logString("result"+result);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return result;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
       AppLog.logString("WebService 123  response  "+response);
        if(response!=null){

            try {
                JSONObject json = new JSONObject(response);
                if(json.getString("response_code").equalsIgnoreCase("200")){

                    listener.onSuccess(from,response);

                }
                else{
                    listener.onFailed(from, response);
                }
            }catch(JSONException je){
                listener.onJsonError(from,response);
            }

        }else{
            listener.onServerError(from,response);
        }
    }
}
