package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("tab_token")
    @Expose
    private String tab_token;

    @SerializedName("tab_profile")
    @Expose
    private String tab_profile;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("currency_symbol")
    @Expose
    private String currency_symbol;

    @SerializedName("currency_name")
    @Expose
    private String currency_name;

    @SerializedName("currency_decimal")
    @Expose
    private String currency_decimal;

    @SerializedName("restaurant_name")
    @Expose
    private String restaurant_name;

    @SerializedName("restaurant_address")
    @Expose
    private String restaurant_address;

    @SerializedName("restaurant_city")
    @Expose
    private String restaurant_city;

    @SerializedName("telephone")
    @Expose
    private String telephone;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("tax_number")
    @Expose
    private String tax_number;

    @SerializedName("tax_name")
    @Expose
    private String tax_name;

    @SerializedName("tax_type")
    @Expose
    private String tax_type;

    @SerializedName("tax_value")
    @Expose
    private String tax_value;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTab_token() {
        return tab_token;
    }

    public void setTab_token(String tab_token) {
        this.tab_token = tab_token;
    }

    public String getTab_profile() {
        return tab_profile;
    }

    public void setTab_profile(String tab_profile) {
        this.tab_profile = tab_profile;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_address() {
        return restaurant_address;
    }

    public void setRestaurant_address(String restaurant_address) {
        this.restaurant_address = restaurant_address;
    }

    public String getRestaurant_city() {
        return restaurant_city;
    }

    public void setRestaurant_city(String restaurant_city) {
        this.restaurant_city = restaurant_city;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTax_number() {
        return tax_number;
    }

    public void setTax_number(String tax_number) {
        this.tax_number = tax_number;
    }

    public String getTax_name() {
        return tax_name;
    }

    public void setTax_name(String tax_name) {
        this.tax_name = tax_name;
    }

    public String getTax_type() {
        return tax_type;
    }

    public void setTax_type(String tax_type) {
        this.tax_type = tax_type;
    }

    public String getTax_value() {
        return tax_value;
    }

    public void setTax_value(String tax_value) {
        this.tax_value = tax_value;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    public String getCurrency_name() {
        return currency_name;
    }

    public void setCurrency_name(String currency_name) {
        this.currency_name = currency_name;
    }

    public String getCurrency_decimal() {
        return currency_decimal;
    }

    public void setCurrency_decimal(String currency_decimal) {
        this.currency_decimal = currency_decimal;
    }
}
