package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.RegisterResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 7/17/2018.
 */

public class RegistrationActivity extends RootActivity implements onApiFinish {
    private String select="",id,location_name,selected_id="",SetBy="",REG_URL="",selected_name="",selected_add1="",selected_add2="",selected_image="";
    private TextView tv_register,tv_reg,tv_tab_id_value;
    private EditText edt_url,edt_setby,edt_password;
    private String FROM_RESTAURANT_Registration = "restaurant_registation";
    private Context mContext;
    ProgressBar simpleProgressBar;
    private ConnectionDetector objDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);

        mContext=this;
        objDetector = new ConnectionDetector(getApplicationContext());

        simpleProgressBar = findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);

        tv_register= findViewById(R.id.tv_register);
        tv_reg= findViewById(R.id.tv_reg);
        tv_tab_id_value= findViewById(R.id.tv_tab_id_value);

        edt_url= findViewById(R.id.edt_url);
        edt_setby= findViewById(R.id.edt_setby);
        edt_password= findViewById(R.id.edt_password);
        final SearchableSpinner spinner1 = findViewById(R.id.spinner);

        tv_tab_id_value.setText(AppSession.getInstance().getAndroid_id());

        Intent intent = getIntent();
      //  HashMap<String, String> map1 = (HashMap<String, String>) intent.getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
        final ArrayList<HashMap<String, String>> map1 =(ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                REG_URL = edt_url.getText().toString().trim();
                AppSession.getInstance().setRegUrl(REG_URL);
                  SetBy =  edt_setby.getText().toString().trim();
                String  password = edt_password.getText().toString().trim();
                if (edt_url.getText().toString().trim().length() != 0||edt_setby.getText().toString().trim().length() != 0 ){

                    if (edt_url.getText().toString().length()==0){
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.url_valid),Toast.LENGTH_LONG).show();

                    }
                    else if (edt_setby.getText().toString().length() == 0) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.setby_valid),Toast.LENGTH_LONG).show();

                    }

                    else {
                        if (!ConnectionDetector.isConnectingToInternet()) {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_internet),Toast.LENGTH_LONG).show();


                        }
                        else
                        {
                            simpleProgressBar.setVisibility(View.VISIBLE);
                            loadData();
                        }


                    }

                }

                else {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.field_valid),Toast.LENGTH_LONG).show();



                }



            }
        });
        // Initializing an ArrayAdapter
        List<String> itemList = new ArrayList<String>();

        /* Iterate through your original collection, in this case defined with an Integer key and String value. */
        for (int i=0;i<map1.size();i++) {
             id = map1.get(i).get(SplashScreenActivity.LOCAION_ID);
             location_name = map1.get(i).get(SplashScreenActivity.LOCAION_NAME);
            itemList.add(location_name);
  /* Build the StringWithTag List using these keys and values. */

        }

        spinner1.setTitle(getResources().getString(R.string.spinner_title));
        String[] types1 = getResources().getStringArray(R.array.select_service);
        final List<String> genderType1 = new ArrayList<>(Arrays.asList(types1));

       /* Set your ArrayAdapter with the StringWithTag, and when each entry is shown in the Spinner, .toString() is called. */
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, itemList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position >= 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);
                   selected_id = map1.get(position).get(SplashScreenActivity.LOCAION_ID);
                    selected_name = map1.get(position).get(SplashScreenActivity.LOCAION_NAME);
                    selected_add1 = map1.get(position).get(SplashScreenActivity.LOCAION_ADD1);
                    selected_add2 = map1.get(position).get(SplashScreenActivity.LOCAION_ADD2);
                    selected_image= map1.get(position).get(SplashScreenActivity.LOCAION_IMAGE);
                    AppSession.getInstance().setLocation_id(selected_id);
                    AppSession.getInstance().setLocationbill_Name(selected_name);
                    AppSession.getInstance().setLocationbill_adress1(selected_add1);
                    AppSession.getInstance().setLocationbill_adress2(selected_add2);
                    AppSession.getInstance().setLocationbill_image(selected_image);

             /*  Toast.makeText(getApplicationContext(),selected_id+selected_name+selected_add1+selected_add2, Toast.LENGTH_LONG).show();*/

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

    }


    @Override
    public void onSuccess(String tag, String response) {

        if (tag.equals(FROM_RESTAURANT_Registration)) {

            Gson gson = new Gson();
            RegisterResponse restaurantLocationResponse = gson.fromJson(response, RegisterResponse.class);

            if (restaurantLocationResponse.getStatus().equals("status")) {
                simpleProgressBar.setVisibility(View.GONE);
                AppSession.getInstance().setisFirstTimeLoader(true);
                
                String message = restaurantLocationResponse.getMessage();

                String tabToken = restaurantLocationResponse.getTab_token();
                AppSession.getInstance().setTabToken(restaurantLocationResponse.getTab_token());
                AppSession.getInstance().setTabProfile(restaurantLocationResponse.getTab_profile());
                AppSession.getInstance().setRestraunt_Name(restaurantLocationResponse.getRestaurant_name());
                AppSession.getInstance().setRestraunt_Address(restaurantLocationResponse.getRestaurant_address());
                AppSession.getInstance().setRestraunt_City(restaurantLocationResponse.getRestaurant_city());
                AppSession.getInstance().setRestraunt_Country(restaurantLocationResponse.getCountry());
                AppSession.getInstance().setRestraunt_Currency(restaurantLocationResponse.getCurrency());
                AppSession.getInstance().setRestraunt_Telephone(restaurantLocationResponse.getTelephone());

                AppSession.getInstance().setRestraunt_tax_name(restaurantLocationResponse.getTax_name());
                AppSession.getInstance().setRestraunt_tax_number(restaurantLocationResponse.getTax_number());
                AppSession.getInstance().setRestraunt_tax_value(restaurantLocationResponse.getTax_value());
                AppSession.getInstance().setRestraunt_tax_type(restaurantLocationResponse.getTax_type());




                Intent intent=new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();

            }

            else {
                Toast.makeText(getApplicationContext(),restaurantLocationResponse.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        RegisterResponse menuResponse = gson.fromJson(response, RegisterResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();

    }



    private void loadData() {

        String URL = Constants.API.REGISTERATION;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

       /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /*values.put("tab_id", "123123123");
        values.put("location","11");
        values.put("set_by", "arun");
        values.put("register_type", "development");
        values.put("url", "url: http://18.220.183.40/dampafeast_local/applicationapi/stafflogin");*/

       /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        values.put("tab_id", AppSession.getInstance().getAndroid_id());
       /* values.put("register_type", "development");*/
        values.put("register_type", Constants.API.ACTION_REGISTERATION);
        values.put("location",selected_id);

        values.put("set_by", SetBy);
        values.put("url",REG_URL);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(RegistrationActivity.this, values);
        objCallWebServiceTask.execute(URL,FROM_RESTAURANT_Registration, "POST");

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(this);super.onBackPressed();
            return;
        }this.doubleBackToExitPressedOnce = true;
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);}


    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if(view instanceof EditText){
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}
