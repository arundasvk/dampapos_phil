package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MenuOptionList {

    @SerializedName("menu_option_id")
    @Expose
    private String menu_option_id;

    @SerializedName("menu_option_value_id")
    @Expose
    private String menu_option_value_id;

    @SerializedName("option_value_id")
    @Expose
    private String option_value_id;

    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("option_id")
    @Expose
    private String option_id;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("default_value_id")
    @Expose
    private String default_value_id;

    @SerializedName("required")
    @Expose
    private String required;

    @SerializedName("option_name")
    @Expose
    private String option_name;

    @SerializedName("option_values")
    @Expose
    private ArrayList<OptionValuesList> optionValues;

    public String getMenu_option_id() {
        return menu_option_id;
    }

    public void setMenu_option_id(String menu_option_id) {
        this.menu_option_id = menu_option_id;
    }

    public String getDefault_value_id() {
        return default_value_id;
    }

    public void setDefault_value_id(String default_value_id) {
        this.default_value_id = default_value_id;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public ArrayList<OptionValuesList> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(ArrayList<OptionValuesList> optionValues) {
        this.optionValues = optionValues;
    }

    public String getMenu_option_value_id() {
        return menu_option_value_id;
    }

    public void setMenu_option_value_id(String menu_option_value_id) {
        this.menu_option_value_id = menu_option_value_id;
    }

    public String getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(String option_value_id) {
        this.option_value_id = option_value_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}