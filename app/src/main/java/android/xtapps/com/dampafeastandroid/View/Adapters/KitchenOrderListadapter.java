package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.Kitchenorder;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;

public class KitchenOrderListadapter extends BaseAdapter {

    ItemClickListener mItemClickListner;
    customButtonListener customListner;
    public interface customButtonListener {
        public void onButtonClickListner(int position);
    }

    public void setCustomButtonListner(customButtonListener listener) {
        this.customListner = listener;
    }





    int row_index=-1;
           int row_indexx=-1;
    String Clicked="";
    private Activity activity;
    ArrayList<Kitchenorder>  data;
    private static LayoutInflater inflater=null;
    private ViewHolder holder = null;
    String Modifiers="";

    /*ArrayList<FavotateProductlistitemResponce> FavorateProductList;*/


    public KitchenOrderListadapter(Activity a, ArrayList<Kitchenorder> d, ItemClickListener itemClickListner) {
        activity = a;
        data=d;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    class ViewHolder {
        TextView small_tv_status,name,description,oredrid,quantity,tv_table,small_tv_table,small_name,small_description,small_quantity,small_oredrid;
        ImageView iv_catimg,orderchange,orderchanged;
        RelativeLayout itemm,rl_small_row;
        LinearLayout ll_big_row;
        Switch simpleSwitchh;
        TextView description2;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final Kitchenorder item = data.get(position);


        View vi=convertView;
        if(convertView==null) {

            vi = inflater.inflate(R.layout.activity_order_row_new, null);
            holder = new ViewHolder();


           holder.name = (TextView) vi.findViewById(R.id.product_name);
            holder.small_name = (TextView) vi.findViewById(R.id.product_namee);
            holder.description = (TextView) vi.findViewById(R.id.product_description);
            holder.description2 = (TextView) vi.findViewById(R.id.product_description2);
            holder.small_description = (TextView) vi.findViewById(R.id.product_descriptionn);
            //holder.oredrid = (TextView) vi.findViewById(R.id.oredrid);
            //holder.small_oredrid = (TextView) vi.findViewById(R.id.oredridd);
            holder.tv_table = (TextView) vi.findViewById(R.id.tv_table);
            holder.small_tv_table = (TextView) vi.findViewById(R.id.product_table);

            holder.orderchange = (ImageView) vi.findViewById(R.id.orderchange);
            holder.orderchanged = (ImageView) vi.findViewById(R.id.orderchanged);
            holder.ll_big_row = (LinearLayout) vi.findViewById(R.id.ll_big_row);
            holder.rl_small_row = (RelativeLayout) vi.findViewById(R.id.rl_small_row);
            holder.simpleSwitchh = (Switch) vi.findViewById(R.id.simpleSwitchh);



            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

            if(item.getOptionValuesLists().size()>0)
        {
            String previous="";
            Modifiers="";
            for(int i=0;i<item.getOptionValuesLists().size();i++)
            {
              /*  Modifiers=Modifiers+" ,"+item.getOptionValuesLists().get(i).getValue();*/
                if(previous.equals("")){
                    Modifiers=item.getOptionValuesLists().get(i).getOption_name()+" : "+item.getOptionValuesLists().get(i).getValue()+", ";

                }else if(previous.equals(item.getOptionValuesLists().get(i).getOption_name())){
                    Modifiers=Modifiers+item.getOptionValuesLists().get(i).getValue()+", ";
                }else{
                    Modifiers=Modifiers+"\n"+item.getOptionValuesLists().get(i).getOption_name()+" : "+item.getOptionValuesLists().get(i).getValue()+" ,";
                }

                previous=item.getOptionValuesLists().get(i).getOption_name();
            }
            holder.description2.setVisibility(View.VISIBLE);
            holder.description2.setText(Modifiers);
        }
        else
        {
            holder.description2.setVisibility(View.GONE);
           /* holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(item.getComment());*/
        }
        if(item.getComment().equals("")||item.getComment()==null)
        {
            holder.description.setVisibility(View.GONE);
        }
        else
        {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText("Comments : "+item.getComment());
        }
        holder.name.setText(item.getName()+" X "+item.getQuantity());
        holder.small_name.setText(item.getName()+" X "+item.getQuantity());

       // holder.description.setText(item.getComment());
      //  holder.small_description.setText(item.getComment());
        //holder.small_oredrid.setText(item.getOrder_id());
        //holder.oredrid.setText(item.getOrder_id());
        if(item.getTable_name()!=null) {
            holder.tv_table.setText(" for table: " + item.getTable_name());
            holder.small_tv_table.setText(" for table: " + item.getTable_name());
        }else{
            holder.tv_table.setText("");
            holder.small_tv_table.setText("");
        }
       // holder.productimage.setBackgroundResource(R.drawable.dine_in);

        if(item.getMenu_status_id().equals("13"))
        {
           item.setState(1);
            holder.orderchange.setVisibility(View.VISIBLE);
            //holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            holder.tv_table.setText(item.getTable_name());

        }

        else if(item.getMenu_status_id().equals("14"))
        {
           item.setState(2);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.tv_table.setText(item.getTable_name());

        }
        else if(item.getMenu_status_id().equals("19")||item.getMenu_status_id().equals("20"))
        {
            item.setState(3);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.tv_table.setText(item.getTable_name());

        }else if(item.getMenu_status_id().equals("12"))
        {

            item.setState(0);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(true);
            holder.simpleSwitchh.setChecked(false);
           // Toast.makeText(activity,item.getMenu_status_id(), Toast.LENGTH_SHORT).show();

        }

        if (item.getState() == 1) {
           /* holder.orderchange.setVisibility(View.VISIBLE);
            holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.orderchange.setVisibility(View.VISIBLE);
            //holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            holder.tv_table.setText(item.getTable_name());

        }
        else if(item.getState() == 2)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.tv_table.setText(item.getTable_name());
        }
        else if(item.getState() == 3)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.tv_table.setText(item.getTable_name());
        }

        else if(item.getState() == 0)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(true);
            holder.simpleSwitchh.setChecked(false);
        }


        holder.orderchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                row_index=position;
                mItemClickListner.onItemSelected("tick_button", position, item);
                notifyDataSetChanged();
               // Toast.makeText(activity,"order", Toast.LENGTH_SHORT).show();

            }
        });

        holder.simpleSwitchh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                    row_indexx=position;
                    mItemClickListner.onItemSelected("slide_button", position, item);
                 notifyDataSetChanged();


            }
        });

       if(row_index==position){
            item.setMenu_status_id("14");
            item.setMenu_status_name("Delivery");
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
           notifyDataSetChanged();

        }

        if(row_indexx==position){
            item.setMenu_status_id("13");
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            notifyDataSetChanged();

        }
        if(!AppSession.getInstance().getUser_Type().equals("KITCHEN")
                &&!AppSession.getInstance().getUser_Type().equals("BAR"))
        {
            holder.simpleSwitchh.setVisibility(View.INVISIBLE);
            holder.orderchange.setEnabled(false);
        }


        return vi;
    }



}
