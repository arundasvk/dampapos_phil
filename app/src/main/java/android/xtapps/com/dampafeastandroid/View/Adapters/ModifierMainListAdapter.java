package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuOptionList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ModifierDataList;
import android.xtapps.com.dampafeastandroid.R;

import java.util.List;

public class ModifierMainListAdapter extends RecyclerView.Adapter<ModifierMainListAdapter.MyView>{

    List<ModifierDataList> mProductList;
    private Activity activity;
    int row_index;
    public ModifierMainListAdapter(FragmentActivity activity, List<ModifierDataList> mStoreList) {
        this.mProductList = mStoreList;
        this.activity=activity;
    }

    public class MyView extends RecyclerView.ViewHolder{

        TextView tv_recy_name;
        ImageView img_recy_item;
        ListView listview;
        RatingBar recy_rating_bar;
        LinearLayout ll_outer;

        TextView tv_tablename,tv_staff_name;
        LinearLayout l_layout_table_item;
        public MyView(View itemView) {
            super(itemView);

            /*img_recy_item = (ImageView) itemView.findViewById(R.id.colorView);
            tv_recy_name = (TextView) itemView.findViewById(R.id.tvItemName);*/
            tv_tablename = itemView.findViewById(R.id.tv_tablename);
            tv_staff_name = itemView.findViewById(R.id.tv_staff_name);
            ll_outer = itemView.findViewById(R.id.l_layout_table_item);
        }
    }
    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_grid_row, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {



        final ModifierDataList item = mProductList.get(position);

        holder.tv_tablename.setText(item.getoption_name());

        holder.ll_outer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;
                notifyDataSetChanged();/* holder.l_layout_table_item.setBackgroundColor(Color.parseColor("#567845"));*/}});
        if(row_index==position)
        {
            holder.ll_outer.setBackgroundResource(R.drawable.table_green_bg);
        }else
        {
            holder.ll_outer.setBackgroundResource(R.drawable.gridview_item_shape);
        }



    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
