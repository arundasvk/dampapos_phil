package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.DataList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuInfoResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuItemsList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuOptionList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderInfo;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableContinueResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableList2;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableList3;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableListResponse2;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Activity.CartActivity;
import android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity;
import android.xtapps.com.dampafeastandroid.View.Adapters.FloorListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.MainMenuListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.SubScreenGridViewAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.TableGridviewAdapter2;
import android.xtapps.com.dampafeastandroid.View.Adapters.TakeAwayGridviewAdapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;

public class AllInOneFragment extends Fragment {
    public static AllInOneFragment newInstance() {
        AllInOneFragment fragment = new AllInOneFragment();
        return fragment;
    }
    public static final String TAG = "AllInOneFragment";
    private TextView tv_item_name,tv_add_cartt;
    private  LinearLayout dine,take,delvery,ll_add_cart,ll_rightside_frame;
    private EditText dinein_edt_guest_name,deinin_edt_number,dinein_guest_count,dinein_edt_senior_guest
            ,dinein_edt_solo_guest,dinein_edt_disabled
            ,takeaway_edt_guest_name,takeaway_edt_guest_number,
            delivery_edt_guest_name,delivery_edt_number,
            edt_one,edt_two,edt_three,edt_four,edt_five;
    Context mContext;
    View view;
    int cartcount;
    int row_index=-1;
    int position;
    boolean is_refresh_table=false;
    private ScheduledExecutorService scheduleTaskExecutor;
    onApiFinish mOnApiFinish;
    private String FROM_TABLE = "restaurant_table";
    private String FROM_MENU = "restaurant_menu";
    private String FROM_CONTINUE = "continue_order";
    private String FROM_KITCHEN_LOGIN_takeaway = "takeaway_orders";
    private String FROM_KITCHEN_LOGIN_delivery = "delivery_orders";
 /*   List<TableList> mProductLists;*/
    FloorListAdapter floor_adapter;
    List<TableList3> mProductLists;
    List<TableList2> floor_names;
    List<DataList> mProductLists2;
    List<MenuItemsList> mProductLists3;
    List<MenuOptionList> mProductLists4;
    List<TableContinueResponse> mContinueLists;
    List<OptionValuesList> mContinueLists2;
    List<OrderInfo> mContinueLists_details;
   /* TableGridviewAdapter lasthrAdapter;*/
   TableGridviewAdapter2 lasthrAdapter;
    MainMenuListAdapter lasthrAdapter2;
    SubScreenGridViewAdapter lasthrAdapter3;
    TakeAwayGridviewAdapter lasthr_takeaway_Adapter;
    private GridView gridview,gridview_take,gridview_del,rv_submenu;
    RecyclerView recyclerView,rv_floors;
    List<CartItem> cartItems;
    String req="";
    private String selected_items;
    Double Total=0.00;
    ProgressBar simpleProgressBar;
    Spinner spinner;
    String no_guest;
    DecimalFormat df2;
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    TextView tv_toolbar_title;
    private ConnectionDetector objDetector;
    String category_name ="";
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    ListView list;
    TextView tv_new_order_takeaway,tv_new_order_del,tv_new_order_takeaway_text,tv_new_order_del_text;
    ItemClickListener mItemClickListner;
    String pax_valid="0",paxflag="";
    String senior="0",solo="0",disabled="0";
    String decimal="#.";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objDetector = new ConnectionDetector(getContext());
        mContext = getActivity();
        view =  inflater.inflate(R.layout.mainlayoutmain2, container, false);

        tv_toolbar_title =(TextView) getActivity().findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.dash_board));
        gridview = (GridView) view.findViewById(R.id.gridview);
        gridview_take = (GridView) view.findViewById(R.id.gridview_take);
        gridview_del = (GridView) view.findViewById(R.id.gridview_del);
        rv_submenu = view.findViewById(R.id.rv_submenu);
        // list=getActivity().findViewById(R.id.listview);
        //  list.setVisibility(View.GONE);
        tv_new_order_takeaway = (TextView) view.findViewById(R.id.tv_new_order_takeaway);
        tv_new_order_del = (TextView) view.findViewById(R.id.tv_new_order_del);
        tv_new_order_del_text = (TextView) view.findViewById(R.id.tv_new_order_del_text);
        tv_new_order_takeaway_text = (TextView) view.findViewById(R.id.tv_new_order_takeaway_text);
        recyclerView = view.findViewById(R.id.rvAnimals);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        rv_floors = view.findViewById(R.id.rv_floors);
        rv_floors.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManagaerOne
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_floors.setLayoutManager(horizontalLayoutManagaerOne);

        dine =(LinearLayout)view.findViewById(R.id.dine);
        take =(LinearLayout)view.findViewById(R.id.take);
        delvery =(LinearLayout)view.findViewById(R.id.delvery);
        ll_rightside_frame =(LinearLayout)view.findViewById(R.id.ll_rightside_frame);

        if (DBMethodsCart.newInstance(mContext).getCartItemsCount()<=0 &&
                AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            ll_rightside_frame.setVisibility(View.GONE);
        }
        if(AppSession.getInstance().getTable_ID().equals("")
                &&
                AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            ll_rightside_frame.setVisibility(View.GONE);
        }
        else
        {
            ll_rightside_frame.setVisibility(View.VISIBLE);
        }



       // df2 = new DecimalFormat(AppSession.getInstance().getRestraunt_Decimal_Format());

        dinein_edt_guest_name = (EditText) view.findViewById(R.id.dinein_edt_guest_name);
        deinin_edt_number = (EditText) view.findViewById(R.id.dinein_edt_number);
        dinein_guest_count = (EditText) view.findViewById(R.id.dinein_guest_count);
        dinein_edt_senior_guest = (EditText) view.findViewById(R.id.dinein_edt_senior_guest);
        dinein_edt_solo_guest = (EditText) view.findViewById(R.id.dinein_edt_solo_guest);
        dinein_edt_disabled = (EditText) view.findViewById(R.id.dinein_edt_disabled);
        takeaway_edt_guest_name= (EditText) view.findViewById(R.id.takeaway_edt_guest_name);
        takeaway_edt_guest_number= (EditText) view.findViewById(R.id.takeaway_edt_guest_number);
        delivery_edt_guest_name= (EditText) view.findViewById(R.id.delivery_edt_guest_name);
        delivery_edt_number= (EditText) view.findViewById(R.id.delivery_edt_number);
        edt_one = (EditText) view.findViewById(R.id.edt_one);
        edt_two = (EditText) view.findViewById(R.id.edt_two);
        edt_three = (EditText) view.findViewById(R.id.edt_three);
        edt_four = (EditText) view.findViewById(R.id.edt_four);
        edt_five = (EditText) view.findViewById(R.id.edt_five);

        tv_item_name =(TextView)view.findViewById(R.id.tv_item_name);
        tv_add_cartt=(TextView)view.findViewById(R.id.tv_add_cart);
        ll_add_cart=(LinearLayout)view.findViewById(R.id.ll_add_cart);

        simpleProgressBar = (ProgressBar)view.findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);
        simpleProgressBar.setVisibility(View.VISIBLE);
        spinner = (Spinner) view.findViewById(R.id.spinner);

        sharedpreferencesData = this.getActivity().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

        setTextPreference();

        initCallBackLisrners();
        initCallBackListner();

        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            dine.setVisibility(View.VISIBLE);
            take.setVisibility(View.GONE);
            delvery.setVisibility(View.GONE);
        }
        else
        if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            dine.setVisibility(View.GONE);
            take.setVisibility(View.VISIBLE);
            delvery.setVisibility(View.GONE);
        }
        else
        if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            dine.setVisibility(View.GONE);
            take.setVisibility(View.GONE);
            delvery.setVisibility(View.VISIBLE);
        }
        else
        {
            dine.setVisibility(View.VISIBLE);
            take.setVisibility(View.GONE);
            delvery.setVisibility(View.GONE);
        }
        tv_new_order_takeaway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.clear();
                editor1.commit();
                Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                getActivity().startActivity(myIntent);
                AppSession.getInstance().setTable_ID("");
                AppSession.getInstance().setOrderNumber("");
                AppSession.getInstance().setOrderTotal("0");
                AppSession.getInstance().setOrder_id_delivery("");
                AppSession.getInstance().setOrder_idTake_delivery("");
                AppSession.getInstance().setTable_Name("");
                AppSession.getInstance().setContinue_position(5000);
                DBMethodsCart.newInstance(mContext).clearCart();
                DBMethodsCart.newInstance(mContext).clearModifier();
                bottomBarSettext();
            }
        });
        tv_new_order_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.clear();
                editor1.commit();
                Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                getActivity().startActivity(myIntent);
                AppSession.getInstance().setTable_ID("");
                AppSession.getInstance().setOrderNumber("");
                AppSession.getInstance().setOrderTotal("0");
                AppSession.getInstance().setOrder_id_delivery("");
                AppSession.getInstance().setOrder_idTake_delivery("");
                AppSession.getInstance().setTable_Name("");
                AppSession.getInstance().setContinue_position(5000);
                DBMethodsCart.newInstance(mContext).clearCart();
                DBMethodsCart.newInstance(mContext).clearModifier();
                bottomBarSettext();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener( getActivity(),
                recyclerView, new ItemClickListener() {

            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
             /*   Toast.makeText(getActivity(), "Single Click on position        :"+position,
                        Toast.LENGTH_SHORT).show();*/
                mProductLists3 = mProductLists2.get(position).getMenuItemsLists();
                category_name = mProductLists2.get(position).getCategoryName();
                if(mProductLists3!=null&&mProductLists3.size()>0)
                {


                    lasthrAdapter3 = new SubScreenGridViewAdapter(getActivity(), mProductLists3,mItemClickListner);
                    rv_submenu.setAdapter(lasthrAdapter3);
                    lasthrAdapter3.notifyDataSetChanged();




                    rv_submenu.setVisibility(View.VISIBLE);

                }
                else
                {
                    rv_submenu.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
               /* Toast.makeText( getActivity(), "Long press on position :"+position,
                        Toast.LENGTH_LONG).show();*/
            }

            @Override
            public void onItemSelected(String item, int position, Object response) {

            }

        }));

        rv_floors.addOnItemTouchListener(new RecyclerTouchListener( getActivity(),
                rv_floors, new ItemClickListener() {

            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
             /*   Toast.makeText(getActivity(), "Single Click on position        :"+position,
                        Toast.LENGTH_SHORT).show();*/
               /* mProductLists3 = mProductLists2.get(position).getMenuItemsLists();
                category_name = mProductLists2.get(position).getCategoryName();*/
                AppSession.getInstance().setFloor(position);
                mProductLists = floor_names.get(position).getInfo();
                if(mProductLists!=null&&mProductLists.size()>0)
                {

/*
                    lasthrAdapter3 = new SubScreenGridViewAdapter(getActivity(), mProductLists3,mItemClickListner);
                    rv_submenu.setAdapter(lasthrAdapter3);
                    lasthrAdapter3.notifyDataSetChanged();*/
                    lasthrAdapter = new TableGridviewAdapter2(getActivity(), mProductLists,mItemClickListner);
                    gridview.setAdapter(lasthrAdapter);
                    // gridview_take.setAdapter(lasthrAdapter);
                    lasthrAdapter.notifyDataSetChanged();



                    gridview.setVisibility(View.VISIBLE);

                }
                else
                {
                    gridview.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
               /* Toast.makeText( getActivity(), "Long press on position :"+position,
                        Toast.LENGTH_LONG).show();*/
            }

            @Override
            public void onItemSelected(String item, int position, Object response) {

            }

        }));




        rv_submenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {






            }
        });

        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!sharedpreferencesData.getString("dinein_guest_count","").equals(dinein_guest_count.getText().toString().trim())
                        ||!sharedpreferencesData.getString("dinein_edt_senior_guest","0").equals(dinein_edt_senior_guest.getText().toString().trim())
                        ||!sharedpreferencesData.getString("dinein_edt_solo_guest","0").equals(dinein_edt_solo_guest.getText().toString().trim())
                        ||!sharedpreferencesData.getString("dinein_edt_disabled","0").equals(dinein_edt_disabled.getText().toString().trim()))
                {
                    AppSession.getInstance().setUpdatedFlag_sssss("true");
                    setPreference();
                }
                else
                {
                    AppSession.getInstance().setUpdatedFlag_sssss("");
                }
                if (DBMethodsCart.newInstance(mContext).getCartItemsCount()>0)
                {


                    if(!AppSession.getInstance().getTable_ID().equals("")||
                            !AppSession.getInstance().getServiceType().equals("dine_in"))
                    {
                        setPreference();
                        if(AppSession.getInstance().getServiceType().equals("dine_in"))
                        {
                            if(dinein_guest_count.getText().length()==0)
                            {
                                pax_valid="0";
                                paxflag="false";
                            }
                            else
                            {
                                pax_valid=dinein_guest_count.getText().toString();
                                paxflag="true";
                            }
                            if(paxflag.equals("false"))
                            {
                                Toast.makeText(mContext,getResources().getString(R.string.valid_no_pax), Toast.LENGTH_SHORT).show();

                            }
                            else if(Integer.valueOf(senior)>Integer.valueOf(pax_valid))
                            {
                                Toast.makeText(mContext,getResources().getString(R.string.s_citizen_no_pax), Toast.LENGTH_SHORT).show();
                            }
                            else if(Integer.valueOf(solo)>Integer.valueOf(pax_valid))
                            {
                                Toast.makeText(mContext,getResources().getString(R.string.s_parent_no_pax), Toast.LENGTH_SHORT).show();
                            }
                            else if(Integer.valueOf(disabled)>Integer.valueOf(pax_valid))
                            {
                                Toast.makeText(mContext,getResources().getString(R.string.disabled_no_pax), Toast.LENGTH_SHORT).show();
                            }
                            else if(Integer.valueOf(disabled)+Integer.valueOf(solo)
                                    +Integer.valueOf(senior)>Integer.valueOf(pax_valid))
                            {
                                Toast.makeText(mContext,getResources().getString(R.string.total_no_pax), Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                AppSession.getInstance().setTABLE_ChangeFLAG("added_cart");
                                AppSession.getInstance().getOrderNumber();

                                Intent i = new Intent(getActivity(), CartActivity.class);
                                startActivity(i);
                                // Toast.makeText(mContext,"count"+AppSession.getInstance().getGuest_Count(), Toast.LENGTH_SHORT).show();

                            }
                        }


                        else
                        {
                            AppSession.getInstance().setTABLE_ChangeFLAG("added_cart");
                            AppSession.getInstance().getOrderNumber();

                            Intent i = new Intent(getActivity(), CartActivity.class);
                            startActivity(i);
                            // Toast.makeText(mContext,"count"+AppSession.getInstance().getGuest_Count(), Toast.LENGTH_SHORT).show();

                        }



                    }
                    else
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.select_table), Toast.LENGTH_SHORT).show();

                    }
                }
                else
                {
                    Toast.makeText(mContext,getResources().getString(R.string.no_items), Toast.LENGTH_SHORT).show();
                }

            }
        });


        String[] types = getResources().getStringArray(R.array.guestCount);
        final List<String> nationalityType = new ArrayList<>(Arrays.asList(types));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_text, nationalityType) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {

                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
//        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//        spinner.setAdapter(spinnerArrayAdapter);
//        if (!AppSession.getInstance().getGuest_Count().equals("")) {
//            int spinnerPosition = spinnerArrayAdapter.getPosition(AppSession.getInstance().getGuest_Count());
//            spinner.setSelection(spinnerPosition);
//        }
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
//
//                no_guest = (String) parent.getItemAtPosition(position);
//                // If user change the default selection
//                // First item is disable and it is used for hint
//                if (position > 0) {
//                    // Notify the selected item text
//
//                    String spinnerValue = spinner.getSelectedItem().toString();
//                    AppSession.getInstance().setGuest_Count(spinnerValue);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

       /* new Handler().postDelayed(new Runnable() {



            @Override
            public void run() {
                callme();

            }
        }, 2000);*/
       /* if (!objDetector.isConnectingToInternet()) {
            View view = inflater.inflate(R.layout.activity_network_connection, null, false);
            simpleProgressBar = (ProgressBar)view.findViewById(R.id.progressSpinner);
            simpleProgressBar.setScaleY(3f);
            simpleProgressBar.setScaleX(3f);
            simpleProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {



                @Override
                public void run() {

                    simpleProgressBar.setVisibility(View.GONE);

                }
            }, 3000);
            return view;
        }*/


        /*ScheduledExecutorService scheduler =
                Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleAtFixedRate
                (new Runnable() {
                    public void run() {
                        Toast.makeText(mContext, "Its been 5 seconds", Toast.LENGTH_SHORT).show();
                    }
                }, 0, 1, TimeUnit.MINUTES);*/

       /* is_refresh_table=false;
        //startTimer();
        settime();*/
        return view;
    }

    private void settime() {
        final Handler handler = new Handler();
        Timer    timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {

                            callme();
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 20000);
    }


    private void callme() {
       /* simpleProgressBar.setVisibility(View.VISIBLE);*/
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            //loadData();

            loadData2();
        }
        else
        if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {

            loadDataTakeaway();
        }
        else
        if(AppSession.getInstance().getServiceType().equals("delivery"))
        {

            loadDataDelivery();
        }

    }




    private void ContinueOrder() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.CONTINUE_TABLE_ORDER;
        /*current_page= page++;*/HashMap values = new HashMap<>();
       /* values.put("action", "continueTableOrder");*/
        values.put("action", Constants.API.ACTION_CONTINUE_TABLE_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_CONTINUE , "POST");
    }

    private void initCallBackListner() {
        mOnApiFinish = new onApiFinish() {
            @Override
            public void onSuccess(String tag, String response) {


               /* if (tag.equals(FROM_TABLE)) {

                    Gson gson = new Gson();
                    TableListResponse tableResponse = gson.fromJson(response, TableListResponse.class);
                    if (tableResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        mProductLists = tableResponse.getData();

                        if (getActivity()!=null) {
                            lasthrAdapter = new TableGridviewAdapter(getActivity(), mProductLists,mItemClickListner);
                            gridview.setAdapter(lasthrAdapter);
                            // gridview_take.setAdapter(lasthrAdapter);
                            lasthrAdapter.notifyDataSetChanged();
                        }
                        if(!is_refresh_table)
                        {
                            loadDataCategories();
                        }

                    }

                    else {
                        Toast.makeText(mContext,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }*/
                if (tag.equals(FROM_TABLE)) {

                    Gson gson = new Gson();
                    TableListResponse2 tableResponse = gson.fromJson(response, TableListResponse2.class);
                    if (tableResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        mProductLists = tableResponse.getData().get(AppSession.getInstance().getFloor()).getInfo();
                        floor_names=tableResponse.getData();
                        if (getActivity()!=null) {
                            lasthrAdapter = new TableGridviewAdapter2(getActivity(), mProductLists,mItemClickListner);
                            gridview.setAdapter(lasthrAdapter);
                            // gridview_take.setAdapter(lasthrAdapter);
                            lasthrAdapter.notifyDataSetChanged();
                            floor_adapter = new FloorListAdapter(getActivity(), floor_names,mItemClickListner);
                            rv_floors.setAdapter(floor_adapter);
                            floor_adapter.notifyDataSetChanged();
                        }
                        if(!is_refresh_table)
                        {
                            loadDataCategories();
                        }

                    }

                    else {
                        Toast.makeText(mContext,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
                if (tag.equals(FROM_MENU)) {


                    Gson gson = new Gson();
                    MenuInfoResponse menuResponse = gson.fromJson(response, MenuInfoResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        is_refresh_table=true;
                        simpleProgressBar.setVisibility(View.GONE);
                        mProductLists2 = menuResponse.getData();
                        if(menuResponse.getData().size()>0)
                        {
                            if (getActivity()!=null) {
                                lasthrAdapter2 = new MainMenuListAdapter(getActivity(), mProductLists2);
                                recyclerView.setAdapter(lasthrAdapter2);
                                lasthrAdapter2.notifyDataSetChanged();

                                   /* for(int i=0;i<mProductLists.size();i++)
                                    {*/
                                mProductLists3 = menuResponse.getData().get(0).getMenuItemsLists();
                                category_name = menuResponse.getData().get(0).getCategoryName();
                                if(mProductLists2.size()>0)
                                {
                                    lasthrAdapter3 = new SubScreenGridViewAdapter(getActivity(), mProductLists3,mItemClickListner);
                                    rv_submenu.setAdapter(lasthrAdapter3);
                                    lasthrAdapter3.notifyDataSetChanged();



                                }
                                /// }


                            }
                        }
                        else
                        {
                            Toast.makeText(mContext, menuResponse.getData().toString(), Toast.LENGTH_SHORT).show();

                        }

                    }

                    else {
                        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                if (tag.equals(FROM_KITCHEN_LOGIN_takeaway)) {

                    Gson gson = new Gson();
                    TableContinueResponse tableResponse2 = gson.fromJson(response, TableContinueResponse.class);
                    if (tableResponse2.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        mContinueLists_details = tableResponse2.getData();
                        if(mContinueLists_details.size()==0)
                        {
                            tv_new_order_takeaway_text.setVisibility(View.GONE);
                            tv_new_order_takeaway.setVisibility(View.GONE);

                        }
                        if (getActivity()!=null) {
                            lasthr_takeaway_Adapter = new TakeAwayGridviewAdapter(getActivity(), mContinueLists_details);
                            gridview_take.setAdapter(lasthr_takeaway_Adapter);
                            // gridview_take.setAdapter(lasthrAdapter);
                            lasthr_takeaway_Adapter.notifyDataSetChanged();
                        }
                        if(!is_refresh_table)
                        {
                            loadDataCategories();
                        }


                    }

                    else {
                        Toast.makeText(mContext,tableResponse2.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
                if (tag.equals(FROM_KITCHEN_LOGIN_delivery)) {

                    Gson gson = new Gson();
                    TableContinueResponse tableResponse2 = gson.fromJson(response, TableContinueResponse.class);
                    if (tableResponse2.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        mContinueLists_details = tableResponse2.getData();
                        if(mContinueLists_details.size()==0)
                        {
                            tv_new_order_del.setVisibility(View.GONE);
                            tv_new_order_del_text.setVisibility(View.GONE);
                        }
                        if (getActivity()!=null) {
                            lasthr_takeaway_Adapter = new TakeAwayGridviewAdapter(getActivity(), mContinueLists_details);
                            gridview_del.setAdapter(lasthr_takeaway_Adapter);
                            // gridview_take.setAdapter(lasthrAdapter);
                            lasthr_takeaway_Adapter.notifyDataSetChanged();
                        }
                        if(!is_refresh_table)
                        {
                            loadDataCategories();
                        }


                    }

                    else {
                        Toast.makeText(mContext,tableResponse2.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
                if (tag.equals(FROM_CONTINUE)) {

                    Gson gson = new Gson();
                    TableContinueResponse tableResponse = gson.fromJson(response, TableContinueResponse.class);
                    if (tableResponse.getStatus().equals("success")) {

                  /*      DBMethodsCart.newInstance(activity).insertCartItems(
                                mProductLists2.get(i).getMenuId().toString(),
                                mProductLists2.get(i).getMenuName().toString(),
                                "",
                                "",
                                mProductLists2.get(i).getLocation().toString(),
                                mProductLists2.get(i).getMenuPriority().toString(),
                                "",
                                mProductLists2.get(i).getMenuPrice().toString(),
                                Integer.valueOf(mProductLists2.get(i).getMinimumQty().toString()),
                                Integer.valueOf(mProductLists2.get(i).getMinimumQty().toString()),
                                (Double.valueOf(mProductLists2.get(i).getMenuPrice().toString())*
                                        Double.valueOf(mProductLists2.get(i).getMinimumQty().toString())),
                                req,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "");*/
                        /*DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();*/

                        mContinueLists_details = tableResponse.getData();
                        for (int i=0;i<mContinueLists_details.size();i++)
                        {
                            AppSession.getInstance().setOrderTotal(mContinueLists_details.get(i).getOrder_total().toString());
                            AppSession.getInstance().setOrderNumber(mContinueLists_details.get(i).getOrder_id().toString());
                            AppSession.getInstance().setOrder_status(mContinueLists_details.get(i).getOrder_status().toString());
                            AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                            for(int j=0;j<mContinueLists_details.get(i).getCart().size();j++)
                            {
                                // DecimalFormat df2 = new DecimalFormat("###.###");
                                double f = Double.valueOf( mContinueLists_details.get(i).getCart().get(j).getPrice().toString());
                               String order_m_id= mContinueLists_details.get(i).getCart().get(j).getOrder_menu_id().toString();
                               if(order_m_id==null||order_m_id.equals(""))
                               {
                                   order_m_id= mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString();
                               }
                                 //String ft =df2.format(f);
                               /* String ft = String.format("%.3f", f);*/
                                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                                DBMethodsCart.newInstance(mContext).insertCartItems(
                                        mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString(),
                                        order_m_id,
                                        "",
                                        mContinueLists_details.get(i).getCart().get(j).getName().toString(),
                                        mContinueLists_details.get(i).getCart().get(j).getName2().toString(),
                                        mContinueLists_details.get(i).getOrder_id().toString(),
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        ft,
                                        1,
                                        Integer.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString()),
                                        Integer.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString()),
                                        (Double.valueOf(ft)*
                                                Double.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString())),
                                        "2",
                                        mContinueLists_details.get(i).getCart().get(j).getComment().toString(),
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "order_sent",
                                         Double.valueOf( mContinueLists_details.get(i).getCart().get(j).getDiscount_value().toString()),
                                         Double.valueOf( mContinueLists_details.get(i).getCart().get(j).getDiscount().toString()),
                                         mContinueLists_details.get(i).getCart().get(j).getDiscount_comment().toString());


                                mContinueLists2=mContinueLists_details.get(i).getCart().get(j).getOptionValues();
                                if(mContinueLists2.size()>0 && mContinueLists2 !=null)
                                { for(int k =0;k<mContinueLists2.size();k++)
                                {
                                    DBMethodsCart.newInstance(mContext).insertModifierItems( mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString(),
                                            mContinueLists2.get(k).getOption_name(),
                                            mContinueLists_details.get(i).getCart().get(j).getOrder_menu_id().toString(),
                                            mContinueLists2.get(k).getMenu_option_value_id(),
                                            mContinueLists2.get(k).getOption_value_id(),
                                            mContinueLists2.get(k).getOption_id(),
                                            mContinueLists2.get(k).getOption_name(),
                                            mContinueLists2.get(k).getDisplay_type(),
                                            mContinueLists2.get(k).getValue(),
                                            mContinueLists2.get(k).getNew_price(),
                                            mContinueLists2.get(k).getPrice()); } }
                            }
                            if(AppSession.getInstance().getServiceType().equals("dine_in"))
                            {
                                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                                editor1.putString("order_comment", mContinueLists_details.get(i).getComment().toString());

                                editor1.putString("dinein_GuestName", mContinueLists_details.get(i).getCustomer().getName());
                                editor1.putString("dinein_GuestNumber", mContinueLists_details.get(i).getCustomer().getMobile_number());
                                editor1.putString("dinein_guest_count", mContinueLists_details.get(i).getCustomer().getNo_of_pax());
                                editor1.putString("dinein_edt_senior_guest", mContinueLists_details.get(i).getCustomer().getSenior_citizen());
                                editor1.putString("dinein_edt_solo_guest", mContinueLists_details.get(i).getCustomer().getSolo_parent());
                                editor1.putString("dinein_edt_disabled", mContinueLists_details.get(i).getCustomer().getDisabled());
                                editor1.commit();
                            }
                            else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                            {
                                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                                editor1.putString("order_comment", mContinueLists_details.get(i).getComment().toString());
                                editor1.putString("takeaway_GuestName", mContinueLists_details.get(i).getCustomer().getName());
                                editor1.putString("takeaway_GuestNumber", mContinueLists_details.get(i).getCustomer().getMobile_number());
                                editor1.commit();
                            }
                            else if(AppSession.getInstance().getServiceType().equals("delivery"))
                            {
                                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                                editor1.putString("order_comment", mContinueLists_details.get(i).getComment().toString());
                                editor1.putString("delivery_GuestName", mContinueLists_details.get(i).getCustomer().getName());
                                editor1.putString("delivery_GuestNumber", mContinueLists_details.get(i).getCustomer().getMobile_number());
                                editor1.commit();
                            }

                        }
                        AppSession.getInstance().setTABLE_ChangeFLAG("added_cart");
                        lasthrAdapter.notifyDataSetChanged();

                        //  Toast.makeText(activity,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        if (! getActivity().isFinishing()) {
                            Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                            getActivity().startActivity(myIntent);
                            // bottomBarSettext();
                        }

                    }

                    else {
                        Toast.makeText(mContext,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onFailed(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Gson gson = new Gson();
                TableContinueResponse tableResponse = gson.fromJson(response, TableContinueResponse.class);
                Toast.makeText(mContext,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onJsonError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServerError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                //Toast.makeText(mContext,"server error", Toast.LENGTH_SHORT).show();
                Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();

            }

        };



    }

    private void loadDataCategories() {
        String URL = AppSession.getInstance().getRegUrl()+Constants.API.MENU_LIST;
        /*current_page= page++;*/
        HashMap<String, String> values = new HashMap<>();
       /* values.put("action", "MenuInfo");*/
        values.put("action", Constants.API.ACTION_MENU_LIST);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        //values.put("staff_id", "11");
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_MENU , "POST");
    }

    @Override
    public void onResume() {
        super.onResume();
       /* simpleProgressBar.setVisibility(View.VISIBLE);
        bottomBarSettext();
        loadData();*/
        bottomBarSettext();
    }
    @Override
    public void onStart() {
        super.onStart();
        is_refresh_table=false;
        Log.d(TAG, "####################################################on start###########################");
        stoptimertask(view);
        startTimer();
       // settime();
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "####################################################on stop###########################");

        stoptimertask(view);
    }
    private void loadData() {

        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.TABLE_LIST;
        HashMap<String, String> values = new HashMap<>();
       /* values.put("action", "TableInfo");*/
        values.put("action",Constants.API.ACTION_TABLE_LIST);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext, mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_TABLE , "POST");
    }
    private void loadData2() {
        Log.d(TAG, "####################################################loadDataDelivery###########################");

        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.TABLE_LIST2;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("action", "TableInfo");*/
        values.put("action",Constants.API.ACTION_TABLE_LIST);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext, mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_TABLE , "POST");
    }
    private void loadDataTakeaway() {
        Log.d(TAG, "####################################################loadDataDelivery###########################");


        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.KITCHEN_LOGIN_takeaway;
            /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", Constants.API.ACTION_KITCHEN_LOGIN_takeaway);
        // values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        //values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_type", AppSession.getInstance().getServiceType());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_KITCHEN_LOGIN_takeaway , "POST");

    }
    private void loadDataDelivery() {
        Log.d(TAG, "####################################################loadDataDelivery###########################");

        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.KITCHEN_LOGIN_takeaway;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", Constants.API.ACTION_KITCHEN_LOGIN_takeaway);
        // values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        //values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_type", AppSession.getInstance().getServiceType());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_KITCHEN_LOGIN_delivery , "POST");
    }
    private void loadDataTableCountinue() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.CONTINUE_TABLE_ORDER;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", Constants.API.ACTION_CONTINUE_TABLE_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_CONTINUE , "POST");
    }
    private void bottomBarSettext() {
        selected_items="";
        Total=Double.valueOf(AppSession.getInstance().getRestraunt_Decimal_Zero());
        cartItems =DBMethodsCart.newInstance(getActivity()).getItemname_qty( );
        for(int i=0;i<cartItems.size();i++)
        {
            selected_items=selected_items+cartItems.get(i).getMenu_name()+" X "+
                    cartItems.get(i).getMenu_qty()+", ";
            Total=Total+cartItems.get(i).getMenu_qty_X_menu_price();
        }
        tv_item_name.setText(selected_items);
      /*  df2 = new DecimalFormat("#.##");*/
        //String ft =df2.format(Total);
        //String ft = Double.toString(Total);
      /*  String ft = String.format("%.3f", Total);*/
        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), Total);
        tv_add_cartt.setText(String.valueOf(ft)+" "+AppSession.getInstance().getRestraunt_Currency());
    }
    private void setTextPreference() {
        if( !sharedpreferencesData.getString("dinein_GuestName","").equals(""))
        {
            dinein_edt_guest_name.setText( sharedpreferencesData.getString("dinein_GuestName",""));
        }
        if( !sharedpreferencesData.getString("dinein_GuestNumber","").equals(""))
        {
            deinin_edt_number.setText( sharedpreferencesData.getString("dinein_GuestNumber",""));
        }
        if( !sharedpreferencesData.getString("dinein_guest_count","").equals("")
                &&!sharedpreferencesData.getString("dinein_guest_count","").equals("0"))
        {
            dinein_guest_count.setText( sharedpreferencesData.getString("dinein_guest_count",""));
        }
        if( !sharedpreferencesData.getString("dinein_edt_senior_guest","").equals("")
                &&!sharedpreferencesData.getString("dinein_edt_senior_guest","").equals("0")
                )
        {
            dinein_edt_senior_guest.setText( sharedpreferencesData.getString("dinein_edt_senior_guest",""));
        }
        if( !sharedpreferencesData.getString("dinein_edt_solo_guest","").equals("")
                &&!sharedpreferencesData.getString("dinein_edt_solo_guest","").equals("0"))
        {
            dinein_edt_solo_guest.setText( sharedpreferencesData.getString("dinein_edt_solo_guest",""));
        }
        if( !sharedpreferencesData.getString("dinein_edt_disabled","").equals("")
                &&!sharedpreferencesData.getString("dinein_edt_disabled","").equals("0"))
        {
            dinein_edt_disabled.setText( sharedpreferencesData.getString("dinein_edt_disabled",""));
        }
        if( !sharedpreferencesData.getString("takeaway_GuestName","").equals(""))
        {
            takeaway_edt_guest_name.setText( sharedpreferencesData.getString("takeaway_GuestName",""));
        }
        if( !sharedpreferencesData.getString("takeaway_GuestNumber","").equals(""))
        {
            takeaway_edt_guest_number.setText( sharedpreferencesData.getString("takeaway_GuestNumber",""));
        }
        if( !sharedpreferencesData.getString("delivery_GuestName","").equals(""))
        {
            delivery_edt_guest_name.setText( sharedpreferencesData.getString("delivery_GuestName",""));
        }
        if( !sharedpreferencesData.getString("delivery_GuestNumber","").equals(""))
        {
            delivery_edt_number.setText( sharedpreferencesData.getString("delivery_GuestNumber",""));
        }
        if( !sharedpreferencesData.getString("DetailOne","").equals(""))
        {
            edt_one.setText( sharedpreferencesData.getString("DetailOne",""));
        }
        if( !sharedpreferencesData.getString("Detailtwo","").equals(""))
        {
            edt_two.setText( sharedpreferencesData.getString("Detailtwo",""));
        }
        if( !sharedpreferencesData.getString("DetailThree","").equals(""))
        {
            edt_three.setText( sharedpreferencesData.getString("DetailThree",""));
        }
        if( !sharedpreferencesData.getString("DetailFour","").equals(""))
        {
            edt_four.setText( sharedpreferencesData.getString("DetailFour",""));
        }
        if( !sharedpreferencesData.getString("DeatilFive","").equals(""))
        {
            edt_five.setText( sharedpreferencesData.getString("DeatilFive",""));
        }
    }
    private void setPreference() {

        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        if(dinein_edt_senior_guest.getText().length()==0)
        {
            senior="0";
        }
        else
        {
            senior=dinein_edt_senior_guest.getText().toString().trim();
        }
        if(dinein_edt_solo_guest.getText().length()==0)
        {
            solo="0";
        }
        else
        {
            solo=dinein_edt_solo_guest.getText().toString().trim();
        }
        if(dinein_edt_disabled.getText().length()==0)
        {
            disabled="0";
        }
        else
        {
            disabled=dinein_edt_disabled.getText().toString().trim();
        }
        editor1.putString("dinein_GuestName", dinein_edt_guest_name.getText().toString().trim());
        editor1.putString("dinein_GuestNumber", deinin_edt_number.getText().toString().trim());
        editor1.putString("dinein_guest_count", dinein_guest_count.getText().toString().trim());
        editor1.putString("dinein_edt_senior_guest", senior);
        editor1.putString("dinein_edt_solo_guest", solo);
        editor1.putString("dinein_edt_disabled", disabled);


        editor1.putString("takeaway_GuestName", takeaway_edt_guest_name.getText().toString().trim());
        editor1.putString("takeaway_GuestNumber", takeaway_edt_guest_number.getText().toString().trim());

        editor1.putString("delivery_GuestName", delivery_edt_guest_name.getText().toString().trim());
        editor1.putString("delivery_GuestNumber", delivery_edt_number.getText().toString().trim());
        editor1.putString("DetailOne", edt_one.getText().toString().trim());
        editor1.putString("Detailtwo", edt_two.getText().toString().trim());
        editor1.putString("DetailThree", edt_three.getText().toString().trim());
        editor1.putString("DetailFour",edt_four.getText().toString().trim());
        editor1.putString("DeatilFive", edt_five.getText().toString().trim());

        editor1.commit();

        AppSession.getInstance().setGuest_Count(dinein_guest_count.getText().toString().trim());
    }
    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ItemClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ItemClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job

        initializeTimerTask();
        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 0, 20000); //
    }

    public void stoptimertask(View v) {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp
                        callme();
                    }
                });
            }
        };
    }
    private void initCallBackLisrners() {


        mItemClickListner = new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onItemSelected(String tag, int i, Object response) {

                if (tag.equals("submenu_click")) {


                    mProductLists4=mProductLists3.get(i).getMenuOptions();
                    // String selectedItem = adapterView.getItemAtPosition(i).toString();
             /*   Toast.makeText(getActivity(), "Single Click on position        :"+selectedItem,
                        Toast.LENGTH_SHORT).show();*/

                    if(mProductLists3!=null&&mProductLists3.get(i).getState()==1)
                    {

                        mProductLists3.get(i).setState(-1);
                        DBMethodsCart.newInstance(mContext).removeFromCart( mProductLists3.get(i).getMenuId().toString());
                        DBMethodsCart.newInstance(mContext).removeFromModifier( mProductLists3.get(i).getMenuId().toString());

                    }
                    else
                    {

                        mProductLists3.get(i).setState(1);
                   /* if(AppSession.getInstance().getTableContinueOrder().equals("true"))
                    {
                        AppSession.getInstance().getTableContinueOrder().equals("false");
                        ContinueOrder();
                    }*/
                        if(!AppSession.getInstance().getTable_ID().equals("")||
                                !AppSession.getInstance().getServiceType().equals("dine_in"))
                        {

                            cartItems = DBMethodsCart.newInstance(mContext).getProductById( mProductLists3.get(i).getMenuId().toString());


                            if(cartItems.size()>0)
                            {

                            }
                            else
                            {
                                if(mProductLists3!=null&&mProductLists3.get(i).getMenuOptions().size()!=0)
                                {
                                    req="1";
                                }
                                // DecimalFormat df2 = new DecimalFormat("###.###");
                                double f = Double.valueOf( mProductLists3.get(i).getMenuPrice().toString());
                                //String ft =df2.format(f);
                                String order_m_id=  mProductLists3.get(i).getOrder_menu_id();
                                if(order_m_id==null||order_m_id.equals(""))
                                {
                                    order_m_id=  mProductLists3.get(i).getMenuId().toString();
                                }
                               // String ft =df2.format(f);
                               /* String ft = String.format("%.3f", f);*/
                                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                                DBMethodsCart.newInstance(getActivity()).insertCartItems(

                                        mProductLists3.get(i).getMenuId().toString(),
                                        order_m_id,
                                        category_name,
                                        mProductLists3.get(i).getMenuName().toString(),

                                        mProductLists3.get(i).getMenuName2().toString(),
                                        "",
                                        "",
                                        "",
                                        mProductLists3.get(i).getLocation().toString(),
                                        mProductLists3.get(i).getMenuPriority().toString(),
                                        "",
                                        ft,
                                        Integer.valueOf(mProductLists3.get(i).getMinimumQty().toString()),
                                        Integer.valueOf(mProductLists3.get(i).getMinimumQty().toString()),
                                        0,
                                        (Double.valueOf(ft)*
                                                Double.valueOf(mProductLists3.get(i).getMinimumQty().toString())),
                                        req,
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        0.0,
                                        0.0,
                                        "");



                            }


                        }
                        else
                        {

                            Toast.makeText(mContext,getResources().getString(R.string.select_table), Toast.LENGTH_SHORT).show();
                        }
                    }

                    bottomBarSettext();

                    lasthrAdapter3.notifyDataSetChanged();

                }
                else
                if (tag.equals("table_click")) {
                   /* final TableList item = mProductLists.get(i);*/
                    final TableList3 item = mProductLists.get(i);
                    cartcount = DBMethodsCart.newInstance(mContext).getCartItemsCount();
                    if(cartcount>0&&!AppSession.getInstance().getTABLE_ChangeFLAG().equals(""))

                    {
                        List<CartItem> new_orders;
                        new_orders = DBMethodsCart.newInstance(mContext).getByOrderStatus( "");
                        if(new_orders.size()>0)
                        {
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(mContext);
                            }
                            builder.setTitle("Changing Table Will Clear Your Cart Items ")
                                    .setMessage("Are you sure you want to change the selected Table?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            /*AppSession.getInstance().setTABLE_ChangeFLAG("");
                                            DBMethodsCart.newInstance(mContext).clearCart();
                                            DBMethodsCart.newInstance(mContext).clearModifier();*/
                                            tv_item_name.setText("");
                                            tv_add_cartt.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
                                            row_index=position;

                                            AppSession.getInstance().setTable_ID(item.getTable_id());
                                            AppSession.getInstance().setTable_Name(item.getTable_name());
                                            sharedpreferencesData = mContext.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                                            SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                                            editor1.clear();
                                            editor1.putString("TableName", item.getTable_name());
                                            editor1.commit();
                                            lasthrAdapter.notifyDataSetChanged();
                                            if(AppSession.getInstance().getTable_ID().equals(item.getTable_id())
                                                    &&item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
                                            {
                                                AppSession.getInstance().setTableContinueOrder("true");

                                                DBMethodsCart.newInstance(mContext).clearCart();
                                                DBMethodsCart.newInstance(mContext).clearModifier();

                                                if (! getActivity().isFinishing()) {
                                                    loadDataTableCountinue();
                                                }

                                            }
                                            else
                                            {


                                                    DBMethodsCart.newInstance(mContext).clearCart();
                                                    DBMethodsCart.newInstance(mContext).clearModifier();
                                                    AppSession.getInstance().setTABLE_ChangeFLAG("");
                                                    AppSession.getInstance().setTableContinueOrder("");
                                                    AppSession.getInstance().setOrderNumber("");
                                                    AppSession.getInstance().setOrderTotal("0");


                                                SharedPreferences.Editor editor2 = sharedpreferencesData.edit();
                                                    editor2.putString("order_comment","");
                                                    editor2.commit();

                                                    Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                                                    getActivity().startActivity(myIntent);



                                            }
                                           /* Intent i = new Intent(mContext, NavigationMainActivity.class);
                                            mContext.startActivity(i);*/

                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing

                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();


                        }
                        else
                        {
                            AppSession.getInstance().setTABLE_ChangeFLAG("");
                            DBMethodsCart.newInstance(mContext).clearCart();
                            DBMethodsCart.newInstance(mContext).clearModifier();
                            tv_item_name.setText("");
                            tv_add_cartt.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
                            row_index=position;

                            AppSession.getInstance().setTable_ID(item.getTable_id());
                            AppSession.getInstance().setTable_Name(item.getTable_name());
                            sharedpreferencesData = mContext.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                            SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                            editor1.clear();
                            editor1.putString("TableName", item.getTable_name());
                            editor1.commit();
                            lasthrAdapter.notifyDataSetChanged();

                            if(AppSession.getInstance().getTable_ID().equals(item.getTable_id())
                                    &&item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
                            {
                                AppSession.getInstance().setTableContinueOrder("true");

                                DBMethodsCart.newInstance(mContext).clearCart();
                                DBMethodsCart.newInstance(mContext).clearModifier();

                                if (! getActivity().isFinishing()) {
                                    loadDataTableCountinue();
                                }

                            }
                            else
                            {

                                if(DBMethodsCart.newInstance(mContext).getByOrderStatus( "").size()<=0)
                                {
                                    DBMethodsCart.newInstance(mContext).clearCart();
                                    DBMethodsCart.newInstance(mContext).clearModifier();
                                    AppSession.getInstance().setTABLE_ChangeFLAG("");
                                    AppSession.getInstance().setTableContinueOrder("");
                                    AppSession.getInstance().setOrderNumber("");
                                    AppSession.getInstance().setOrderTotal("0");
                                    SharedPreferences.Editor editor2 = sharedpreferencesData.edit();
                                    editor2.putString("order_comment","");
                                    editor2.commit();

                                    Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                                    getActivity().startActivity(myIntent);
                                }


                            }
                        }
                        //alert();

                    }
                    else
                    {


                        row_index=i;

                        AppSession.getInstance().setTable_ID(item.getTable_id());
                        AppSession.getInstance().setTable_Name(item.getTable_name());
                        sharedpreferencesData = mContext.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.clear();
                        editor1.putString("TableName", item.getTable_name());
                        editor1.commit();
                        lasthrAdapter.notifyDataSetChanged();

                        if(AppSession.getInstance().getTable_ID().equals(item.getTable_id())
                                &&item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
                        {
                            AppSession.getInstance().setTableContinueOrder("true");

                            DBMethodsCart.newInstance(mContext).clearCart();
                            DBMethodsCart.newInstance(mContext).clearModifier();

                            if (! getActivity().isFinishing()) {
                                loadDataTableCountinue();
                            }

                        }
                        else
                        {

                            if(DBMethodsCart.newInstance(mContext).getByOrderStatus( "").size()<=0)
                            {
                                DBMethodsCart.newInstance(mContext).clearCart();
                                DBMethodsCart.newInstance(mContext).clearModifier();
                                AppSession.getInstance().setTABLE_ChangeFLAG("");
                                AppSession.getInstance().setTableContinueOrder("");
                                AppSession.getInstance().setOrderNumber("");
                                AppSession.getInstance().setOrderTotal("0");
                                SharedPreferences.Editor editor2 = sharedpreferencesData.edit();
                                editor2.putString("order_comment","");
                                editor2.commit();

                                Intent myIntent = new Intent(getActivity(), NavigationMainActivity.class);
                                getActivity().startActivity(myIntent);
                            }


                        }

                    }
                    ll_rightside_frame.setVisibility(View.VISIBLE);


                }


            }
        };


    }



}