package android.xtapps.com.dampafeastandroid.Interfaces;

import android.view.View;

public  interface ItemClickListener{
    void onClick(View view, int position);
    void onLongClick(View view, int position);
    void onItemSelected(String item,int position, Object response);
}