package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.CartListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 7/18/2018.
 */

public class ViewCartList_Fragment extends Fragment {

    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{
            R.drawable.ic_menu_camera,R.drawable.ic_menu_gallery,R.drawable.ic_menu_send,
            R.drawable.ic_menu_share,R.drawable.ic_menu_slideshow,R.drawable.ic_menu_manage
            ,R.drawable.ic_menu_slideshow};
    private ListView listview;
    CartListAdapter  adapter;
    public static ViewCartList_Fragment newInstance() {
        ViewCartList_Fragment fragment = new ViewCartList_Fragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.cartlist_activity, container, false);
        listview = view.findViewById(R.id.listview);

        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();
        List<CartItem> cartItems = DBMethodsCart.newInstance(getActivity()).getCartItems( );

       /* adapter = new CartListAdapter(getActivity(),cartItems);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();*/
        return view;
    }

    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 7; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawablee(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }
}
