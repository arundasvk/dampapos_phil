package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.view.WindowManager;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.RestaurantLocationResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.RestaurantLocationResponse.location.Items;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 7/17/2018.
 */


public class SplashScreenActivity extends RootActivity implements onApiFinish {
    private int SPLASH_TIME_OUT = 5000;
    private String FROM_RESTAURANT_LOCATION = "restaurant_location";
    private Context mContext;
    private ArrayList<String> LocationListArray = new ArrayList<String>();
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();

    HashMap<String, String> map1;
    public static final String LOCAION_ARRAY = "LOCAION_ARRAY"; // parent node
    public static final String LOCAION_ID = "LOCAION_ID"; // parent node
    public static final String LOCAION_NAME = "LOCAION_NAME";
    public static final String LOCAION_ADD1 = "LOCAION_ADD1";
    public static final String LOCAION_ADD2 = "LOCAION_ADD2";
    public static final String LOCAION_IMAGE = "LOCAION_IMAGE";// parent node
    private String android_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen_activity);
        mContext=this;
        DBMethodsCart.newInstance(SplashScreenActivity.this).clearCart();
        DBMethodsCart.newInstance(SplashScreenActivity.this).clearModifier();
        DBMethodsCart.newInstance(SplashScreenActivity.this).clearTaxItems();
         android_id = Secure.getString(getContext().getContentResolver(),
                Secure.ANDROID_ID);
         AppSession.getInstance().setAndroid_id(android_id);
        String URL = Constants.API.BASE_RESTAURANT_LOCATION;

        AppSession.getInstance().setBitmapFlag(true);

        HashMap<String, String> values = new HashMap<>();
        //values.put("init", "basic");
        values.put("init", Constants.API.ACTION_BASE_RESTAURANT_LOCATION);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(SplashScreenActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_RESTAURANT_LOCATION, "POST");

        new Handler().postDelayed(new Runnable() {



            @Override
            public void run() {

                callHome();

            }
        }, SPLASH_TIME_OUT);

    }

    private void callHome() {

        if (!AppSession.getInstance().getisFirstTimeLoader())
        {

            Intent intent = new Intent(this, RegistrationActivity.class);
            intent.putExtra(LOCAION_ARRAY, categoryList);
            startActivity(intent);
            finish();

        }

        else if (AppSession.getInstance().getisLoginStatus()) {

            Intent intent = new Intent(this, NavigationMainActivity.class);
            startActivity(intent);
            finish();
        }
        else {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();

        }

    }





    @Override
    public void onBackPressed()
    {
        finish();
        super.onBackPressed();  // optional depending on your needs
    }

    @Override
    public void onSuccess(String tag, String response) {

        if (tag.equals(FROM_RESTAURANT_LOCATION)) {

            Gson gson = new Gson();
            RestaurantLocationResponse restaurantLocationResponse = gson.fromJson(response, RestaurantLocationResponse.class);

            if (restaurantLocationResponse.getStatus().equals("success")) {

                String message = restaurantLocationResponse.getMessage();
                List<Items> movies =  restaurantLocationResponse.getLocation().getLocations();
                if (movies.size()==0)
                {

                }
                else {
                    for (int i = 0; i< movies.size(); i++) {
                        String LocationId = movies.get(i).getLocation_id();
                        String LocationName = movies.get(i).getLocation_name();
                        String LocationADD1 = movies.get(i).getLocation_add1();
                        String LocationADD2 = movies.get(i).getLocation_add2();
                        String LocationImage = movies.get(i).getLocation_Image();
                        map1 = new HashMap<String, String>();

                        map1.put(LOCAION_ID, LocationId);
                        map1.put(LOCAION_NAME, LocationName);
                        map1.put(LOCAION_ADD1, LocationADD1);
                        map1.put(LOCAION_ADD2, LocationADD2);
                        map1.put(LOCAION_IMAGE, LocationImage);

                        categoryList.add(map1);
                        LocationListArray.add(movies.get(i).getLocation_name());


                      //  Toast.makeText(getApplicationContext(),LocationId+""+LocationName, Toast.LENGTH_LONG).show();
                    }
                }


            }

            else {
                //Toast.makeText(getApplicationContext(),restaurantLocationResponse.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onFailed(String tag, String response) {
        //Toast.makeText(getApplicationContext(),response, Toast.LENGTH_LONG).show();

    }





    @Override
    public void onJsonError(String tag, String response) {

      //  Toast.makeText(getApplicationContext(),"jsonerror", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onServerError(String tag, String response) {

        //Toast.makeText(getApplicationContext(),"server error", Toast.LENGTH_LONG).show();

    }
}
