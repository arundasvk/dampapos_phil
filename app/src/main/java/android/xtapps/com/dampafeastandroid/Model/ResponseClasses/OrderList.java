package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderList {

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("order_total")
    @Expose
    private String order_total;

    @SerializedName("order_date")
    @Expose
    private String order_date;

    @SerializedName("order_time")
    @Expose
    private String order_time;

    @SerializedName("order_type")
    @Expose
    private String order_type;

    @SerializedName("coupon_code")
    @Expose
    private String coupon_code;

    @SerializedName("payment_type")
    @Expose
    private String payment_type;

    @SerializedName("menu_status_id")
    @Expose
    private String menu_status_id;

    @SerializedName("menu_status_name")
    @Expose
    private String menu_status_name;

    @SerializedName("menu_status_comment")
    @Expose
    private String menu_status_comment;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("row_id")
    @Expose
    private String rowId;

    @SerializedName("menu_id")
    @Expose
    private String menuId;

    @SerializedName("quantity")
    @Expose
    private String comment;

    @SerializedName("comment")
    @Expose
    private String quantity;

    @SerializedName("total_discount")
    @Expose
    private String total_discount;

    @SerializedName("total_tax")
    @Expose
    private String total_tax;

    @SerializedName("items")
    @Expose
    private ArrayList<ItemList> items;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getMenu_status_id() {
        return menu_status_id;
    }

    public void setMenu_status_id(String menu_status_id) {
        this.menu_status_id = menu_status_id;
    }

    public String getMenu_status_name() {
        return menu_status_name;
    }

    public void setMenu_status_name(String menu_status_name) {
        this.menu_status_name = menu_status_name;
    }

    public String getMenu_status_comment() {
        return menu_status_comment;
    }

    public void setMenu_status_comment(String menu_status_comment) {
        this.menu_status_comment = menu_status_comment;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public ArrayList<ItemList> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemList> items) {
        this.items = items;
    }

    public String getOrder_total() {
        return order_total;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }
}