package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuItemsList;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by xt on 18/7/18.
 */

public class SubScreenGridViewAdapter extends BaseAdapter {
    ItemClickListener mItemClickListner;
    private Activity activity;
    private ArrayList<ImageModel> data;
    //  private List<AllProductsApiResponse.ProductList> mProductLists;
    private static LayoutInflater inflater=null;
    private  ViewHolder holder = null;
    HashMap<String, String> song;
    List<MenuItemsList> mProductList;

    int row_index=-1;

    public SubScreenGridViewAdapter(Activity a,  List<MenuItemsList> mStoreList,ItemClickListener itemClickListner) {
        activity = a;
        mProductList=mStoreList;
        //itemClickListner = clickListner;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return  mProductList.size();
        //return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
        TextView tv_recy_name;
        ImageView img_recy_item;
        ListView listview;
        RatingBar recy_rating_bar;
        LinearLayout ll_outer;
        TextView tv_tablename,tv_staff_name;
        LinearLayout l_layout_table_item;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final MenuItemsList object = mProductList.get(position);

        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.sub_menu_list_row, null);
            holder = new ViewHolder();

            holder.l_layout_table_item = (LinearLayout) vi.findViewById(R.id.l_layout_table_item);

            holder.tv_tablename= (TextView) vi.findViewById(R.id.tv_tablename);

            vi.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)vi.getTag();
        }
        final MenuItemsList item = mProductList.get(position);
        holder.tv_tablename.setText(item.getMenuName());

        List<CartItem> cartItems = DBMethodsCart.newInstance(activity).getProductById( item.getMenuId());


        if(cartItems!=null&&cartItems.size()>0)
        {
            object.setState(1);
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
        }
        else
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);
        }


        holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // row_index=position;
                mItemClickListner.onItemSelected("submenu_click", position, object);
                notifyDataSetChanged();
                // Toast.makeText(activity,"order", Toast.LENGTH_SHORT).show();

            }
        });


        if (object.getState() == 1) {

            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);



        }
        else if (object.getState() == -1) {

            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);



        }
       /* else
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);

        }*/



   /*     holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
            int check=1;
            @Override
            public void onClick(View view) {
                if(check==1)
                {
                  holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
                 check=0;
                }
                else
                {
                    holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);
                    check=1;
                }


            }});*/



        return vi;
    }




}