package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ItemList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderList;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;

/**
 * Created by user on 10/5/18.
 */

public class MyOrderExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<OrderList> headerList;
    ArrayList<ItemList> childList;
      String oderid;
   /* private ArrayList<ResponseOrderitems> originalList;*/

    public MyOrderExpandableListAdapter(Context context, ArrayList<OrderList> headerList) {
        this.context = context;
        this.headerList=headerList;

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
      /*  ArrayList<ItemList> */childList = headerList.get(groupPosition).getItems();
        return childList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        ItemList country = (ItemList) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.listitemchildorder, null);
        }

        TextView qantity = view.findViewById(R.id.qantity);
        TextView product = view.findViewById(R.id.product);
        TextView price = view.findViewById(R.id.price);
        TextView tv_tax_price = view.findViewById(R.id.tv_tax_price);
        TextView tv_disc_price = view.findViewById(R.id.tv_disc_price);

        LinearLayout ll_tax_disc_container = view.findViewById(R.id.ll_tax_disc_container);
        if(childPosition+1==childList.size())
        {
            ll_tax_disc_container.setVisibility(View.VISIBLE);

        }
        else
        {
            ll_tax_disc_container.setVisibility(View.GONE);
        }
        qantity.setText(country.getName());
        product.setText(country.getQty());
        tv_disc_price.setText(headerList.get(groupPosition).getTotal_discount());
        tv_tax_price.setText(headerList.get(groupPosition).getTotal_tax());
// split price
        String pri=country.getPrice();
        String arrr[] = pri.split("\\.");
        String pric=arrr[0];
        String pricc=arrr[1];
        double aDouble = Double.parseDouble(pri);

        price.setText(String.format("%.2f",aDouble));


        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<ItemList> childList = headerList.get(groupPosition).getItems();
        return childList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return headerList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return headerList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view,
                             ViewGroup parent) {

     final   OrderList continent = (OrderList) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.orderheader, null);

        }
       /* if (isExpanded) {
            RelativeLayout ree1 = (RelativeLayout)view. findViewById(R.id.re1);
            RelativeLayout ree2 = (RelativeLayout)view. findViewById(R.id.re2);
            RelativeLayout headercolor = (RelativeLayout)view. findViewById(R.id.head);
            TextView reorder= (TextView)view. findViewById(R.id.reorder);
            ree1.setVisibility(View.VISIBLE);
            ree2.setVisibility(View.GONE);
            headercolor.setBackgroundResource(R.color.fil);
            reorder.setVisibility(View.VISIBLE);

        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon
            RelativeLayout ree1 = (RelativeLayout)view. findViewById(R.id.re1);
            RelativeLayout ree2 = (RelativeLayout)view. findViewById(R.id.re2);
            RelativeLayout headercolor = (RelativeLayout)view. findViewById(R.id.head);
            TextView reorder= (TextView)view. findViewById(R.id.reorder);
            reorder.setVisibility(View.GONE);
            ree1.setVisibility(View.GONE);
            ree2.setVisibility(View.VISIBLE);
            headercolor.setBackgroundResource(R.color.color_white);

        }*/

        TextView qantity = view.findViewById(R.id.tv_ordertotal);
        TextView product = view.findViewById(R.id.oredrid);
        ImageView orderchanged = view.findViewById(R.id.orderchanged);


       // qantity.setText(continent.getOrder_id());
        product.setText(String.valueOf(continent.getOrder_id()));
        String pri=continent.getOrder_total();
        String arrr[] = pri.split("\\.");
        String pric=arrr[0];
        String pricc=arrr[1];
        double aDouble = Double.parseDouble(pri);

        qantity.setText(String.format("%.2f",aDouble)+" "+AppSession.getInstance().getRestraunt_Currency());




       /* orderchanged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,continent.getOrder_id(), Toast.LENGTH_SHORT).show();

            }
        });*/



        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

   /* public void filterData(String query) {

        query = query.toLowerCase();
        Log.v("MyListAdapter", String.valueOf(continentList.size()));
        continentList.clear();

        if (query.isEmpty()) {
            continentList.addAll(originalList);
        } else {

            for (Continent continent : originalList) {

                ArrayList<Country> countryList = continent.getCountryList();
                ArrayList<Country> newList = new ArrayList<Country>();
                for (Country country : countryList) {
                    if (country.getCode().toLowerCase().contains(query) ||
                            country.getName().toLowerCase().contains(query)) {
                        newList.add(country);
                    }
                }
                if (newList.size() > 0) {
                    Continent nContinent = new Continent(continent.getName(), newList);
                    continentList.add(nContinent);
                }
            }
        }

        Log.v("MyListAdapter", String.valueOf(continentList.size()));
        notifyDataSetChanged();

    }*/

}