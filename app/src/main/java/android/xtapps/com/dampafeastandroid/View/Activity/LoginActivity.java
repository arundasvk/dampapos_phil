package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.LoginResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;

import java.util.HashMap;

import static android.xtapps.com.dampafeastandroid.Controller.Constants.API.ACTION_LOGIN;

/**
 * Created by USER on 7/17/2018.
 */

public class LoginActivity extends RootActivity implements onApiFinish {

    private TextView tv_register,tv_username;
    private EditText edt_username,edt_password;
    private final String FROM_LOGIN = "login";
    private String userName,password;
    private Context mContext;
    ProgressBar simpleProgressBar;
    private ConnectionDetector objDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        mContext = this;
        objDetector = new ConnectionDetector(getApplicationContext());
        tv_register= findViewById(R.id.tv_register);
        tv_username= findViewById(R.id.tv_username);

        edt_username= findViewById(R.id.edt_username);
        edt_password= findViewById(R.id.edt_password);

        simpleProgressBar = findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);
        if(!AppSession.getInstance().getExpired_Flag().equals(""))
        {
            AppSession.getInstance().setExpired_Flag("");
            edt_username.setText(AppSession.getInstance().getUser_Name());

        }
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                userName = edt_username.getText().toString().trim();
                password = edt_password.getText().toString().trim();
                if (edt_username.getText().toString().trim().length() != 0 ||edt_password.getText().toString().trim().length() != 0 ){

                    if (edt_username.getText().toString().length()==0){
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.name_valid),Toast.LENGTH_LONG).show();

                    }
                    else if (edt_password.getText().toString().length() == 0) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.pswd_valid),Toast.LENGTH_LONG).show();

                    }
                    else {

                        if (!ConnectionDetector.isConnectingToInternet()) {

                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_internet),Toast.LENGTH_LONG).show();

                        }
                        else
                        {
                            simpleProgressBar.setVisibility(View.VISIBLE);
                            login();
                        }



                    }

                }

                else {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.field_valid),Toast.LENGTH_LONG).show();


                }

                /*Intent intent=new Intent(LoginActivity.this,NavigationMainActivity.class);
                startActivity(intent);
                finish();*/
            }
        });
    }

    private void login() {
        String URL = AppSession.getInstance().getRegUrl()+Constants.API.LOGIN;



        HashMap<String, String> values = new HashMap<>();
        values.put("username", userName);
        values.put("password", password);
      /*  values.put("action","StaffLogin");*/
        values.put("action",ACTION_LOGIN);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(LoginActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_LOGIN, "POST");
    }

    @Override
    public void onSuccess(String tag, String response) {

        if (tag.equals(FROM_LOGIN)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {
                simpleProgressBar.setVisibility(View.GONE);
                AppSession.getInstance().setisLoginStatus(true);
                AppSession.getInstance().setUser_Name(userName);
                AppSession.getInstance().setStaff_ID(loginResponse.getStaffInfo().getStaffId().toString());
                AppSession.getInstance().setStaff_Name(loginResponse.getStaffInfo().getName().toString());
                AppSession.getInstance().setStaff_Email(loginResponse.getStaffInfo().getEmail().toString());
                AppSession.getInstance().setUser_Type(loginResponse.getStaffInfo().getUserType().toString());
                AppSession.getInstance().setLocation_id(loginResponse.getStaffInfo().getLocation().toString());
                AppSession.getInstance().setBluetoothdevicename("");

                AppSession.getInstance().setRestraunt_Name(loginResponse.getLocationinfo().getLocationName().toString());
                AppSession.getInstance().setRestraunt_Address(loginResponse.getLocationinfo().getAddress1().toString()+", "+loginResponse.getLocationinfo().getAddress2().toString());
                AppSession.getInstance().setRestraunt_City(loginResponse.getLocationinfo().getCity().toString());
                AppSession.getInstance().setRestraunt_Country(loginResponse.getLocationinfo().getCountry().toString());
                AppSession.getInstance().setRestraunt_Currency(loginResponse.getLocationinfo().getcurrencyCode().toString());
                AppSession.getInstance().setRestraunt_CurrencySymbol(loginResponse.getLocationinfo().getCurrencySymbol().toString());
                AppSession.getInstance().setRestraunt_DecimalPosition(loginResponse.getLocationinfo().getDecimalPosition().toString());
                AppSession.getInstance().setRestraunt_Telephone(loginResponse.getLocationinfo().getLocationTelephone().toString());

                AppSession.getInstance().setRestraunt_DeliveryCharge(loginResponse.getDelivery_charge().toString());


                AppSession.getInstance().setRestraunt_tax_name(loginResponse.getLocationinfo().getTaxName());
                AppSession.getInstance().setRestraunt_tax_number(loginResponse.getLocationinfo().getTaxNumber());
                AppSession.getInstance().setRestraunt_tax_value(loginResponse.getLocationinfo().getTaxValue());
                AppSession.getInstance().setRestraunt_tax_type(loginResponse.getLocationinfo().getTaxType());


                AppSession.getInstance().setRestraunt_tax_name2(loginResponse.getLocationinfo().getTaxName2());
                AppSession.getInstance().setRestraunt_tax_number2(loginResponse.getLocationinfo().getTaxNumber2());
                AppSession.getInstance().setRestraunt_tax_value2(loginResponse.getLocationinfo().getTaxValue2());
                AppSession.getInstance().setRestraunt_tax_type2(loginResponse.getLocationinfo().getTaxType2());

                AppSession.getInstance().setRestraunt_seniorDiscount(loginResponse.getLocationinfo().getseniorDiscount());
                AppSession.getInstance().setRestraunt_disabledDiscount(loginResponse.getLocationinfo().getdisabledDiscount());
                AppSession.getInstance().setRestraunt_soloDiscount(loginResponse.getLocationinfo().getsoloDiscount());
                String decimal="%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f";
                AppSession.getInstance().setRestraunt_Decimal_Format(decimal);
                String decimal_zero="0.";
                for(int i=0;i<Integer.valueOf(AppSession.getInstance().getRestraunt_DecimalPosition());i++)
                {

                    decimal_zero=decimal_zero+"0";
                }

                AppSession.getInstance().setRestraunt_Decimal_Zero(decimal_zero);
                //  Toast.makeText(mContext,response, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(LoginActivity.this,NavigationMainActivity.class);
                /*AppSession.getInstance().setMenuprinter("123");*/
                startActivity(intent);
                finish();
            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        LoginResponse menuResponse = gson.fromJson(response, LoginResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(this);super.onBackPressed();
            return;
        }this.doubleBackToExitPressedOnce = true;
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);}


    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if(view instanceof EditText){
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}