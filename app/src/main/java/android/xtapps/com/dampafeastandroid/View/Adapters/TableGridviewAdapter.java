package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderInfo;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableContinueResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableList;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/**
 * Created by xt on 24/7/18.
 */

public class TableGridviewAdapter extends BaseAdapter {
    ItemClickListener mItemClickListner;

    private Activity activity;
    //private ArrayList<ImageModel> data;
    private List<TableList> mProductLists;
    private static LayoutInflater inflater=null;
    private  ViewHolder holder = null;
    HashMap<String, String> song;
    int row_index=-1;
    private String FROM_CONTINUE = "continue_order";
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    TableList object;
    List<OptionValuesList> mContinueLists2;
    boolean changed=false;
    onApiFinish mOnApiFinish;
    int cartcount;
    TextView tv_item_name,tv_add_cart_nav;
    LinearLayout ll_rightside_frame;
    List<OrderInfo> mContinueLists;
    private String selected_items;
    Double Total=0.00;
    DecimalFormat df2;
    public TableGridviewAdapter(Activity a, List<TableList> d,ItemClickListener itemClickListner) {
        activity = a;
        mProductLists=d;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mProductLists.size();
        //return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        TextView tv_tablename,tv_staff_name;
        LinearLayout l_layout_table_item;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        object = mProductLists.get(position);

        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.sub_screen_grid_row, null);
            holder = new ViewHolder();

            holder.tv_tablename = (TextView) vi.findViewById(R.id.tv_tablename);
            holder.tv_staff_name = (TextView) vi.findViewById(R.id.tv_staff_name);

            holder.l_layout_table_item = (LinearLayout) vi.findViewById(R.id.l_layout_table_item);

            tv_item_name =(TextView)activity.findViewById(R.id.tv_item_name);
            tv_add_cart_nav =(TextView)activity.findViewById(R.id.tv_add_cart);
            ll_rightside_frame=(LinearLayout)activity.findViewById(R.id.ll_rightside_frame);
            // holder.grid_container = (LinearLayout) vi.findViewById(R.id.grid_container);

            cartcount = DBMethodsCart.newInstance(activity).getCartItemsCount();
            df2 = new DecimalFormat(".##");


            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }




        final TableList item = mProductLists.get(position);

        holder.tv_tablename.setText(item.getTable_name());

        holder.tv_staff_name.setText(item.getStaff_name());




        initCallBackListner();






/*
        if (object.getState() == 1) {


                holder.l_layout_table_item.setBackgroundResource(R.drawable.table_yellow_bg);

            }
            else
            {
                holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);


        }*/

        holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListner.onItemSelected("table_click", position, object);
                notifyDataSetChanged();

             /*   if(cartcount>0&&!AppSession.getInstance().getTABLE_ChangeFLAG().equals(""))

                {
                    List<CartItem> new_orders;
                    new_orders = DBMethodsCart.newInstance(activity).getByOrderStatus( "");
                    if(new_orders.size()>0)
                    {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(activity);
                        }
                        builder.setTitle("Changing Table Will Clear Your Cart Items ")
                                .setMessage("Are you sure you want to change the selected Table?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        AppSession.getInstance().setTABLE_ChangeFLAG("");
                                        DBMethodsCart.newInstance(activity).clearCart();
                                        DBMethodsCart.newInstance(activity).clearModifier();
                                        tv_item_name.setText("");
                                        tv_add_cart_nav.setText("0.0 "+AppSession.getInstance().getRestraunt_Currency());
                                        row_index=position;

                                        AppSession.getInstance().setTable_ID(item.getTable_id());
                                        AppSession.getInstance().setTable_Name(item.getTable_name());
                                        sharedpreferencesData = activity.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                                        editor1.putString("TableName", item.getTable_name());
                                        editor1.commit();
                                        notifyDataSetChanged();
                                        Intent i = new Intent(activity, NavigationMainActivity.class);
                                        activity.startActivity(i);

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }
                    else
                    {
                        AppSession.getInstance().setTABLE_ChangeFLAG("");
                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();
                        tv_item_name.setText("");
                        tv_add_cart_nav.setText("0.0 "+AppSession.getInstance().getRestraunt_Currency());
                        row_index=position;

                        AppSession.getInstance().setTable_ID(item.getTable_id());
                        AppSession.getInstance().setTable_Name(item.getTable_name());
                        sharedpreferencesData = activity.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("TableName", item.getTable_name());
                        editor1.commit();
                        notifyDataSetChanged();
                    }
                    //alert();

                }
                else
                {


                    row_index=position;

                    AppSession.getInstance().setTable_ID(item.getTable_id());
                    AppSession.getInstance().setTable_Name(item.getTable_name());
                    sharedpreferencesData = activity.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("TableName", item.getTable_name());
                    editor1.commit();
                    notifyDataSetChanged();

                }
                ll_rightside_frame.setVisibility(View.VISIBLE);
                if(AppSession.getInstance().getTable_ID().equals(item.getTable_id())
                        &&item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
                {
                    AppSession.getInstance().setTableContinueOrder("true");

                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();

                    if (! activity.isFinishing()) {
                        loadData();
                    }

                }
                else
                {

                    if(DBMethodsCart.newInstance(activity).getByOrderStatus( "").size()<=0)
                    {
                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();
                    }
                   // AppSession.getInstance().setTableContinueOrder("");
                    AppSession.getInstance().setOrderNumber("");
                    Intent i = new Intent(activity, NavigationMainActivity.class);
                    activity.startActivity(i);
                }*/

                /* holder.l_layout_table_item.setBackgroundColor(Color.parseColor("#567845"));*/
            }
        });
        if(row_index==position){
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
            AppSession.getInstance().setTable_ID(item.getTable_id());
            AppSession.getInstance().setTable_Name(item.getTable_name());
        }
        else
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);

        }
        if(item.getTable_status().equals("0")&&!item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_reserved_bg);
            holder.tv_staff_name.setVisibility(View.VISIBLE);
            holder.l_layout_table_item.setEnabled(false);
            holder.l_layout_table_item.setClickable(false);
        }
        else
        if(item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
        {

            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_yellow_bg);
            holder.tv_staff_name.setVisibility(View.VISIBLE);
            holder.l_layout_table_item.setEnabled(true);
            holder.l_layout_table_item.setClickable(true);
               /* if(row_index==position){
                    holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);

                }*/
            if(AppSession.getInstance().getTable_ID().equals(item.getTable_id()))
            {
                holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
            }

        }
        else
        if(AppSession.getInstance().getTable_ID().equals(item.getTable_id()))
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
            holder.tv_staff_name.setVisibility(View.VISIBLE);
            holder.l_layout_table_item.setEnabled(true);
            holder.l_layout_table_item.setClickable(true);

           /* if(AppSession.getInstance().getTable_ID().equals(item.getTable_id())||
                    item.getTable_reserved_staff().equals(AppSession.getInstance().getStaff_ID()))
            {
                holder.l_layout_table_item.setBackgroundResource(R.drawable.table_yellow_bg);
                holder.tv_staff_name.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);
                holder.tv_staff_name.setVisibility(View.GONE);
            }*/


        }
        if(cartcount<=0)
        {
            tv_item_name.setText("");
            tv_add_cart_nav.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
        }
        else
        {
            if (! activity.isFinishing()) {
                bottomBarSettext();
            }
           /* Intent i= new Intent(activity, NavigationMainActivity.class);
            activity.startActivity(i);*/

        }

        return vi;
    }

    private void bottomBarSettext() {
        selected_items="";
        Total=Double.valueOf(AppSession.getInstance().getRestraunt_Decimal_Zero());
        List<CartItem> cartItems;
        cartItems =DBMethodsCart.newInstance(activity).getItemname_qty( );
        for(int i=0;i<cartItems.size();i++)
        {
            selected_items=selected_items+cartItems.get(i).getMenu_name()+" X "+
                    cartItems.get(i).getMenu_qty()+", ";
            Total=Total+cartItems.get(i).getMenu_qty_X_menu_price();
        }
        tv_item_name.setText(selected_items);
       // String ft =df2.format(Total);
        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), Total);
        tv_add_cart_nav.setText(String.valueOf(ft)+" "+AppSession.getInstance().getRestraunt_Currency());
    }

    private void loadData() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.CONTINUE_TABLE_ORDER;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", Constants.API.ACTION_CONTINUE_TABLE_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(activity,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_CONTINUE , "POST");
    }

    private void selecttableclick() {
    }




    public void filterlist(List<TableList> ploads) {
        mProductLists=ploads;
        notifyDataSetChanged();
    }

    private void initCallBackListner() {
        mOnApiFinish = new onApiFinish() {
            @Override
            public void onSuccess(String tag, String response) {


                if (tag.equals(FROM_CONTINUE)) {

                    Gson gson = new Gson();
                    TableContinueResponse tableResponse = gson.fromJson(response, TableContinueResponse.class);
                    if (tableResponse.getStatus().equals("success")) {

                  /*      DBMethodsCart.newInstance(activity).insertCartItems(
                                mProductLists2.get(i).getMenuId().toString(),
                                mProductLists2.get(i).getMenuName().toString(),
                                "",
                                "",
                                mProductLists2.get(i).getLocation().toString(),
                                mProductLists2.get(i).getMenuPriority().toString(),
                                "",
                                mProductLists2.get(i).getMenuPrice().toString(),
                                Integer.valueOf(mProductLists2.get(i).getMinimumQty().toString()),
                                Integer.valueOf(mProductLists2.get(i).getMinimumQty().toString()),
                                (Double.valueOf(mProductLists2.get(i).getMenuPrice().toString())*
                                        Double.valueOf(mProductLists2.get(i).getMinimumQty().toString())),
                                req,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "");*/
                        /*DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();*/
                        mContinueLists = tableResponse.getData();
                        for (int i=0;i<mContinueLists.size();i++)
                        {
                            AppSession.getInstance().setOrderNumber(mContinueLists.get(i).getOrder_id().toString());
                            AppSession.getInstance().setOrder_status(mContinueLists.get(i).getOrder_status().toString());
                            AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                            for(int j=0;j<mContinueLists.get(i).getCart().size();j++)
                            {
                                // DecimalFormat df2 = new DecimalFormat("###.###");
                                double f = Double.parseDouble( mContinueLists.get(i).getCart().get(j).getPrice().toString());
                                // String ft =df2.format(f);
                                String order_m_id=   mContinueLists.get(i).getCart().get(j).getOrder_menu_id().toString();
                                if(order_m_id==null||order_m_id.equals(""))
                                {
                                    order_m_id= mContinueLists.get(i).getCart().get(j).getMenu_id().toString();
                                }
                               // String ft = String.format("%.3f", f);
                                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);

                                DBMethodsCart.newInstance(activity).insertCartItems(
                                        mContinueLists.get(i).getCart().get(j).getMenu_id().toString(),
                                        order_m_id,
                                        "",
                                        mContinueLists.get(i).getCart().get(j).getName().toString(),
                                        mContinueLists.get(i).getCart().get(j).getName2().toString(),
                                        mContinueLists.get(i).getOrder_id().toString(),
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        ft,
                                        1,
                                        Integer.valueOf(mContinueLists.get(i).getCart().get(j).getQuantity().toString()),
                                        Integer.valueOf(mContinueLists.get(i).getCart().get(j).getQuantity().toString()),
                                        (Double.valueOf(ft)*
                                                Double.valueOf(mContinueLists.get(i).getCart().get(j).getQuantity().toString())),
                                        "2",
                                        mContinueLists.get(i).getCart().get(j).getComment().toString(),

                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        "order_sent",
                                        Double.valueOf( mContinueLists.get(i).getCart().get(j).getDiscount_value().toString()),
                                        Double.valueOf( mContinueLists.get(i).getCart().get(j).getDiscount().toString()),
                                        mContinueLists.get(i).getCart().get(j).getDiscount_comment().toString());


                                mContinueLists2=mContinueLists.get(i).getCart().get(j).getOptionValues();
                                if(mContinueLists2 !=null&& mContinueLists2.size()>0  )
                                { for(int k =0;k<mContinueLists2.size();k++)
                                { DBMethodsCart.newInstance(activity).insertModifierItems( mContinueLists.get(i).getCart().get(j).getMenu_id().toString(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists.get(i).getCart().get(j).getOrder_menu_id().toString(),
                                        mContinueLists2.get(k).getMenu_option_value_id(),
                                        mContinueLists2.get(k).getOption_value_id(),
                                        mContinueLists2.get(k).getOption_id(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists2.get(k).getDisplay_type(),
                                        mContinueLists2.get(k).getValue(),
                                        mContinueLists2.get(k).getNew_price(),
                                        mContinueLists2.get(k).getPrice()); } }

                                mContinueLists.get(i).getCustomer().getName();
                            }


                        }
                        notifyDataSetChanged();
                        //  Toast.makeText(activity,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        if (! activity.isFinishing()) {
                            bottomBarSettext();
                        }

                    }

                    else {
                        Toast.makeText(activity,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onFailed(String tag, String response) {
                Gson gson = new Gson();
                TableContinueResponse tableResponse = gson.fromJson(response, TableContinueResponse.class);
                Toast.makeText(activity,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onJsonError(String tag, String response) {
                Toast.makeText(activity,activity.getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServerError(String tag, String response) {
                // Toast.makeText(activity,"server error", Toast.LENGTH_SHORT).show();
                Toast.makeText(activity,activity.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();

            }

        };

    }

}