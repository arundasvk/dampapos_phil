package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.AddCartResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.PaymentAdapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CompletePopupFinalActivity extends AppCompatActivity implements onApiFinish {
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    LinearLayout ll_paymenttype_holder,ll_addmore;
    private final int COMPLETED_FINAL_POPUP = 5;
   /* private ListView listview;*/
    NonScrollListView listview;
    ImageView img_success,img_print;
    Spinner spinner,spinner_card;
    TextView tv_tender_change,tv_tender_change_total,tv_payable_amount;
    String paidtype,card_TType;
    int paid_position,card_position;
    private Context mContext;
    LinearLayout ll_paid_by,ll_card_type,ll_tender_cash,ll_tender_change,ll_tender_change_total,ll_id_proof,ll_coupon_disc,ll_solo_parent,
            ll_card_number,ll_card_holder,ll_amount,ll_print,ll_complete;
    EditText ed_received_cash,ed_id_proof,ed_amount,ed_card_holder,ed_card_number,edt_order_comments;
    private final int CANCEL_POPUP = 3;
    private final String FROM_PAYMENT_TYPES = "paymentTypes";
    private final String FROM_COMPLETE_ORDER = "complete_order";
    private final String FROM_ORDERBILL_AGAIN = "orderbill_again";
    /** Items entered by the user is stored in this ArrayList variable */
    ArrayList<String> list = new ArrayList<String>();

    /** Declaring an ArrayAdapter to set items to ListView */
    ArrayAdapter<String> adapter;

    ArrayList<String> PaymentTypelist = new ArrayList<String>();
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    HashMap<String, String> map5_payment;
    ArrayList<HashMap<String, String>> categoryList_payment = new ArrayList<>();
    HashMap<String, String> map1;
    public static final String PAID_BY = "PAID_BY"; // parent node
    public static final String CARD_TYPE = "CARD_TYPE"; // parent node
    public static final String RECEIVED_AMOUNT = "RECEIVED_AMOUNT";
    public static final String TENDER_CHANGE = "TENDER_CHANGE";
    public static final String AMOUNT = "AMOUNT";
    PaymentAdapter adapter2;
    ItemClickListener mItemClickListner;
    ProgressBar simpleProgressBar;
    String order_number,subtotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_complete_popup_final);
        mContext=this;
        ll_paymenttype_holder = (LinearLayout) findViewById(R.id.ll_paymenttype_holder);
        ll_addmore = (LinearLayout) findViewById(R.id.ll_addmore);
        //listview = (ListView)findViewById(R.id.listview);
       /* NonScrollListView */listview = (NonScrollListView) findViewById(R.id.lv_nonscroll_list);
        simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);

       /* img_success = (ImageView) findViewById(R.id.img_success);
        img_print= (ImageView) findViewById(R.id.img_print);*/
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner_card = (Spinner) findViewById(R.id.spinner_card);

        ll_paid_by = (LinearLayout) findViewById(R.id.ll_paid_by);
        ll_card_type = (LinearLayout) findViewById(R.id.ll_card_type);
        ll_amount = (LinearLayout) findViewById(R.id.ll_amount);
        ll_tender_cash = (LinearLayout) findViewById(R.id.ll_tender_cash);
        ll_tender_change = (LinearLayout)findViewById(R.id.ll_tender_change);
        ll_tender_change_total = (LinearLayout)findViewById(R.id.ll_tender_change_total);
        ll_id_proof = (LinearLayout) findViewById(R.id.ll_id_proof);

        ll_print = (LinearLayout)findViewById(R.id.ll_print);
        ll_complete = (LinearLayout) findViewById(R.id.ll_complete);

       /* ll_card_number = (LinearLayout) dialogView.findViewById(R.id.ll_card_number);
        ll_card_holder = (LinearLayout) dialogView.findViewById(R.id.ll_card_holder);*/

       /* ed_card_number= (EditText) dialogView.findViewById(R.id.ed_card_number);
        ed_card_holder= (EditText) dialogView.findViewById(R.id.ed_card_holder);*/
        ed_received_cash= (EditText) findViewById(R.id.ed_received_cash);
        ed_id_proof= (EditText) findViewById(R.id.ed_id_proof);
        ed_amount= (EditText) findViewById(R.id.ed_amount);
        tv_tender_change= (TextView) findViewById(R.id.tv_tender_change);
        tv_tender_change_total= (TextView) findViewById(R.id.tv_tender_change_total);
        tv_payable_amount= (TextView) findViewById(R.id.tv_payable_amount);

        sharedpreferencesData = CompletePopupFinalActivity.this.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


        order_number =getIntent().getStringExtra("order_number");
        subtotal =getIntent().getStringExtra("subtotal");
        tv_payable_amount.setText(AppSession.getInstance().getOrderTotal());

        initCallBackLisrners();
        paymentTypes();

       // adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        adapter2 = new PaymentAdapter(CompletePopupFinalActivity.this, categoryList,mItemClickListner);

        /*      adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        *//** Defining a click event listener for the button "Add" *//*
        OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit = (EditText) findViewById(R.id.txtItem);
                list.add(edit.getText().toString());
                edit.setText("");
                adapter.notifyDataSetChanged();
            }
        };

        *//** Setting the event listener for the add button *//*
        ll_addmore.setOnClickListener(listener);

        *//** Setting the adapter to the ListView *//*
        listview.setAdapter(adapter);*/
        listview.setAdapter(adapter2);
        ll_addmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addPayment();

            }

        });

        ll_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OrderBillAgain();
            }

        });

        ll_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(categoryList!=null&&categoryList.size()>0)
                {
                    completeOrder();
                }
                else
                {
                    Toast.makeText(mContext,getResources().getString(R.string.payment_valid), Toast.LENGTH_SHORT).show();
                }

            }

        });

    }



    @Override
    public void onSuccess(String tag, String response) {
        if (tag.equals(FROM_PAYMENT_TYPES)) {

            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                PaymentTypelist.clear();
                if(menuResponse.getPayment_types().length>0)
                {
                    for(int i=0;i<menuResponse.getPayment_types().length;i++)
                    {
                        PaymentTypelist.add(menuResponse.getPayment_types()[i]);
                    }
                }
               completeBill();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_COMPLETE_ORDER)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                if(!isFinishing()&&mContext!=null)
                {

                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                        editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                        editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                        editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                        editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                        editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                        editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                        editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                        editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                        editor1.putString("vat", menuResponse.getOrder_details().getVat());
                        editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                        editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());

                        editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                        editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                        editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                        editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                        editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                        editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                        editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());
                        editor1.putString("payment_type",""/*menuResponse.getOrder_details().getPayment_type()*/);
                        editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                        editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                        editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                        editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                        editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                        editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                        editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                        editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                        editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                        editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                        editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                        editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                        editor1.putString("location", menuResponse.getOrder_details().getLocation());
                        editor1.putString("area", menuResponse.getOrder_details().getArea());
                        editor1.putString("city", menuResponse.getOrder_details().getCity());

                        editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                        editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());

                        editor1.putString("card_name", menuResponse.getOrder_details().getCard_name());
                        editor1.putString("card_number", menuResponse.getOrder_details().getCard_number());
                        editor1.putString("card_type", menuResponse.getOrder_details().getCard_type());
                    if(menuResponse.getOrder_details().getPayment_type()!=null
                            &&menuResponse.getOrder_details().getPayment_type().size()>0)
                    {
                        for(int k=0;k<menuResponse.getOrder_details().getPayment_type().size();k++)
                        {
                            map5_payment = new HashMap<String, String>();
                            map5_payment.put("payment", menuResponse.getOrder_details().getPayment_type().get(k).getPayment());
                            map5_payment.put("amount", menuResponse.getOrder_details().getPayment_type().get(k).getAmount());
                            map5_payment.put("card_type", menuResponse.getOrder_details().getPayment_type().get(k).getCard_type());

                            categoryList_payment.add(map5_payment);
                        }
                    }

                    saveArrayList(categoryList_payment,"Payment_types");
                   /* categoryList_payment.clear();
                    categoryList_payment= getArrayList("Payment_types");
                    if (categoryList_payment != null && categoryList_payment.size() > 0) {
                        for (int i = 0; i < categoryList_payment.size(); i++) {
                            String paymentdetailss = "";
                            paymentdetailss="Payment Type: "+categoryList_payment.get(i).get("payment")+ "         " + "\n" +
                                    "Payment Recieved : " + categoryList_payment.get(i).get("amount") + "        " + "\n" +
                                    "Card Type : " + categoryList_payment.get(i).get("card_type") + "        " + "\n";
                            Toast.makeText(mContext,paymentdetailss, Toast.LENGTH_LONG).show();

                        }
                    }*/

                        editor1.commit();

                    Intent intent=new Intent();
                    setResult(COMPLETED_FINAL_POPUP,intent);
                    finish();

                }
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_ORDERBILL_AGAIN)) {
            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                AppSession.getInstance().setInvoice_No(menuResponse.getOrder_details().getInvoice_no());
                AppSession.getInstance().setOrder_status("22");
                AppSession.getInstance().setOrderNumber(order_number);

                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                editor1.putString("sale_no", menuResponse.getOrder_details().getSale_no());
                editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                editor1.putString("vat", menuResponse.getOrder_details().getVat());
                editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());
                editor1.putString("payment_type", /*menuResponse.getOrder_details().getPayment_type()*/"");
                editor1.putString("card_type", menuResponse.getOrder_details().getCard_type());

                editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());

                editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                editor1.putString("location", menuResponse.getOrder_details().getLocation());
                editor1.putString("area", menuResponse.getOrder_details().getArea());
                editor1.putString("city", menuResponse.getOrder_details().getCity());

                editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());

                editor1.putString("table_name", menuResponse.getOrder_details().getTable_info().getTable_name());
                editor1.putString("table_id", menuResponse.getOrder_details().getTable_info().getTable_id());

                if(menuResponse.getOrder_details().getPayment_type()!=null
                        &&menuResponse.getOrder_details().getPayment_type().size()>0)
                {
                    for(int k=0;k<menuResponse.getOrder_details().getPayment_type().size();k++)
                    {
                        map5_payment = new HashMap<String, String>();
                        map5_payment.put("payment", menuResponse.getOrder_details().getPayment_type().get(k).getPayment());
                        map5_payment.put("amount", menuResponse.getOrder_details().getPayment_type().get(k).getAmount());
                        map5_payment.put("card_type", menuResponse.getOrder_details().getPayment_type().get(k).getCard_type());

                        categoryList_payment.add(map5_payment);
                    }
                }

                saveArrayList(categoryList_payment,"Payment_types");
                /*categoryList_payment.clear();
                categoryList_payment= getArrayList("Payment_types");
                if (categoryList_payment != null && categoryList_payment.size() > 0) {
                    for (int i = 0; i < categoryList_payment.size(); i++) {
                        String paymentdetailss = "";
                        paymentdetailss="Payment Type: "+categoryList_payment.get(i).get("payment")+ "         " + "\n" +
                                "Payment Recieved : " + categoryList_payment.get(i).get("amount") + "        " + "\n" +
                                "Card Type : " + categoryList_payment.get(i).get("card_type") + "        " + "\n";
                        Toast.makeText(mContext,paymentdetailss, Toast.LENGTH_LONG).show();

                    }
                }*/

                editor1.commit();

                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);
                Intent i= new Intent(CompletePopupFinalActivity.this,Main_ActivityPrinterLongin.class);
                AppSession.getInstance().setcompletecart("123");startActivity(i);

            }
            else
            {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onFailed(String tag, String response) {
        Toast.makeText(mContext,response, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJsonError(String tag, String response) {
        Toast.makeText(mContext,response, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        Toast.makeText(mContext,"server error", Toast.LENGTH_SHORT).show();
    }

    @Override public void onBackPressed() {

        Intent intent=new Intent();
        setResult(CANCEL_POPUP,intent);
        finish();
        /* super.onBackPressed();*/

    }
    private void initCallBackLisrners() {


        mItemClickListner = new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onItemSelected(String tag, int position, Object response) {






            }
        };


    }

    private void completeOrder() {


        try {
            JSONArray jsonArray = new JSONArray();
            Double tender_cash_double=0.0,received_amount_double=0.0,tender_change_double=0.0;
            for (int i=0;i<categoryList.size();i++) {
                tender_cash_double=tender_cash_double+Double.valueOf(categoryList.get(i).get(AMOUNT));
                received_amount_double=received_amount_double+Double.valueOf(categoryList.get(i).get(RECEIVED_AMOUNT));
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("payment", categoryList.get(i).get(PAID_BY));
                jsonObject.put("amount", categoryList.get(i).get(RECEIVED_AMOUNT));
                jsonObject.put("card_type", categoryList.get(i).get(CARD_TYPE));
                jsonArray.put(jsonObject);
            }
          /*  tender_change_double=tender_cash_double-received_amount_double;*/
            tender_change_double=tender_cash_double-Double.valueOf(AppSession.getInstance().getOrderTotal());
            if(tender_change_double>0)
            {
                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), tender_change_double);
                tv_tender_change_total.setText(String.valueOf(ft));
                ll_tender_change.setVisibility(View.VISIBLE);
                //tv_tender_change_total.setText(String.valueOf(tender_change_double));
            }
            else
            {
                ll_tender_change.setVisibility(View.GONE);
            }
           /* tv_tender_change_total.setText(String.valueOf(tender_change_double));*/

            if(received_amount_double>=Double.valueOf(tv_payable_amount.getText().toString()))
            {
                // simpleProgressBar.setVisibility(View.VISIBLE);
                String URL = AppSession.getInstance().getRegUrl()+ API.COMPLETE_ORDER;



                HashMap<String, String> values = new HashMap<>();
                /* values.put("action", "completeOrder");*/
                values.put("action", API.ACTION_COMPLETE_ORDER);
                values.put("tab_token", AppSession.getInstance().getTabToken());
                values.put("location_id", AppSession.getInstance().getLocation_id());
                values.put("staff_id", AppSession.getInstance().getStaff_ID());


                values.put("order_id", order_number);
                values.put("prev_subtotal", AppSession.getInstance().gettotal());
                values.put("subtotal", subtotal);
                /*  values.put("coupon_code", ed_coupon_code.getText().toString());*/
                /*values.put("coupon_code", sharedpreferencesData.getString("Coupon_code",""));*/
                values.put("payment_mode", jsonArray.toString());
                // values.put("card_type", card_TType);
                values.put("card_number","" /*ed_card_number.getText().toString()*/);
                values.put("card_name", ""/*ed_card_holder.getText().toString()*/);
                values.put("tender_cash",String.valueOf(tender_cash_double) /*ed_tender_cash.getText().toString()*/);
                values.put("tender_change",String.valueOf(tender_change_double) /*tv_tender_change.getText().toString()*/);
                values.put("proof_ids", ed_id_proof.getText().toString());


                values.put("order_type", "1");
                CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupFinalActivity.this, values);
                objCallWebServiceTask.execute(URL, FROM_COMPLETE_ORDER, "POST");
               // Toast.makeText(mContext,"success", Toast.LENGTH_SHORT).show();

            }
            else
            {
                Toast.makeText(mContext,getResources().getString(R.string.total_recive), Toast.LENGTH_SHORT).show();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void paymentTypes() {
        String URL = AppSession.getInstance().getRegUrl()+ API.PAYMENT_TYPES;



        HashMap<String, String> values = new HashMap<>();
        values.put("action",  API.ACTION_PAYMENT_TYPES);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupFinalActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_PAYMENT_TYPES, "POST");
    }
    private void OrderBillAgain() {
        String URL = AppSession.getInstance().getRegUrl()+ API.ORDERBILL_AGAIN_API;
        HashMap values = new HashMap<>();
        values.put("action", API.ACTION_ORDER_AGAIN_BILL);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id",order_number);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupFinalActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_ORDERBILL_AGAIN, "POST");
    }

    private void completeBill() {
        final List<String> paidByType;
        if(PaymentTypelist.size()>0)
        {
            paidByType = new ArrayList<>(PaymentTypelist);
        }
        else
        {
            String[] types = getResources().getStringArray(R.array.paidBy);
            /*final List<String>*/ paidByType = new ArrayList<>(Arrays.asList(types));
        }


        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                CompletePopupFinalActivity.this, R.layout.paid_by_spinner_text, paidByType) {
            @Override
            public boolean isEnabled(int position) {
                  /*  if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {

                        return true;
                    }*/
                return true;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                  /*  if (position == 0) {
                        // Set the hint text color gray
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }*/
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.paid_by_spinner_text);
        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                // paidBy = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text


                }
                paid_position= spinner.getSelectedItemPosition();
                paidtype = spinner.getSelectedItem().toString();
                if(paidtype.equals("CARD"))
                {
                   // ll_tender_cash.setVisibility(View.GONE);
                    ll_tender_change.setVisibility(View.GONE);
                    ll_card_type.setVisibility(View.VISIBLE);
                    ll_amount.setVisibility(View.VISIBLE);
                   /* ll_card_number.setVisibility(View.VISIBLE);
                    ll_card_holder.setVisibility(View.VISIBLE);*/
                }
                else if(!paidtype.equals("CARD"))
                {
                    //ll_tender_cash.setVisibility(View.VISIBLE);
                    ll_tender_change.setVisibility(View.GONE);
                    ll_card_type.setVisibility(View.GONE);
                    ll_amount.setVisibility(View.VISIBLE);
                   /* ll_card_number.setVisibility(View.GONE);
                    ll_card_holder.setVisibility(View.GONE);*/
                }

                else
                {

                }
                // AppSession.getInstance().setGuest_Count(spinnerValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String[] cardtypes = getResources().getStringArray(R.array.cardType);
        final List<String> CardType = new ArrayList<>(Arrays.asList(cardtypes));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                CompletePopupFinalActivity.this, R.layout.paid_by_spinner_text, CardType) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {

                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        spinnerArrayAdapter2.setDropDownViewResource(R.layout.paid_by_spinner_text);
        spinner_card.setAdapter(spinnerArrayAdapter2);

        spinner_card.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                //  card_type = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text


                }
                card_position= spinner_card.getSelectedItemPosition();
                card_TType = spinner_card.getSelectedItem().toString();
                // AppSession.getInstance().setGuest_Count(spinnerValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

   /*     ed_tender_cash.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }



            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {
                if(s!=null&&s.length()!=0&&!s.equals("")) {
                    //do your work here
                    double a=  0.0;
                    double b=0.0;
                    if(AppSession.getInstance().getTax_payable_amount().equals("")
                            ||AppSession.getInstance().getTax_payable_amount()==null)
                    {
                        a=  0.0;

                    }
                    else
                    {
                        a=  Double.valueOf(AppSession.getInstance().getTax_payable_amount());

                    }
                    b= Double.valueOf(s.toString().trim());
                    tv_tender_change.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f",(b-a) ));

                }
                else
                {
                    tv_tender_change.setText("");
                }
            }
        });*/

    }
    private void addPayment() {


            if(!paidtype.equals("CARD"))
            {
               if(ed_amount.length()==0)
               {
                   Toast.makeText(mContext,getResources().getString(R.string.amount_valid), Toast.LENGTH_SHORT).show();

               }
            /*   else
               if(ed_received_cash.length()==0)
               {
                   Toast.makeText(mContext,getResources().getString(R.string.received_amount_valid), Toast.LENGTH_SHORT).show();

               }
               else if(Double.valueOf(ed_amount.getText().toString())<Double.valueOf(ed_received_cash.getText().toString()))
               {
                   Toast.makeText(mContext,getResources().getString(R.string.received_amount_amount), Toast.LENGTH_SHORT).show();

               }*/
               else
               {
                   addNext();
               }

            }
            else if(paidtype.equals("CARD"))
            {
                if(card_position==0)
                {
                    Toast.makeText(mContext,getResources().getString(R.string.select_card_type), Toast.LENGTH_SHORT).show();

                }
               else if(ed_amount.length()==0)
                {
                    Toast.makeText(mContext,getResources().getString(R.string.amount_valid), Toast.LENGTH_SHORT).show();

                }
                else
                {
                    addNext();
                }

            }
            else
            {
                Toast.makeText(mContext,getResources().getString(R.string.payment_type_not_valid), Toast.LENGTH_SHORT).show();

            }


    }

    private void addNext() {
        String received_amount="";
    /*    if(!paidtype.equals("CARD"))
        {
            received_amount=ed_received_cash.getText().toString();

        }
        else
        {
            received_amount=ed_amount.getText().toString();

        }*/
        received_amount=ed_amount.getText().toString();
        if(card_position==0)
        {
            card_TType="";
        }
        else
        {

        }
        /*  list.add(ed_tender_cash.getText().toString());*/
        map1 = new HashMap<String, String>();

        map1.put(PAID_BY, paidtype);
        map1.put(CARD_TYPE, card_TType);
        map1.put(RECEIVED_AMOUNT, received_amount);
        map1.put(TENDER_CHANGE, "");
        map1.put(AMOUNT, ed_amount.getText().toString());


        categoryList.add(map1);
        /* list.add(paidtype);*/

        //ed_received_cash.setText("");
        ed_amount.setText("");
        spinner.setSelection(0);
        spinner_card.setSelection(0);
        adapter2.notifyDataSetChanged();
        listview.setVisibility(View.VISIBLE);
        try {
            JSONArray jsonArray = new JSONArray();
            Double tender_cash_double=0.0,received_amount_double=0.0,tender_change_double=0.0;
            for (int i=0;i<categoryList.size();i++) {
                tender_cash_double=tender_cash_double+Double.valueOf(categoryList.get(i).get(AMOUNT));
                received_amount_double=received_amount_double+Double.valueOf(categoryList.get(i).get(RECEIVED_AMOUNT));
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("payment", categoryList.get(i).get(PAID_BY));
                jsonObject.put("amount", categoryList.get(i).get(RECEIVED_AMOUNT));
                jsonObject.put("card_type", categoryList.get(i).get(CARD_TYPE));
                jsonArray.put(jsonObject);
            }
            /*tender_change_double=tender_cash_double-received_amount_double;*/
            tender_change_double=tender_cash_double-Double.valueOf(AppSession.getInstance().getOrderTotal());
            if(tender_change_double>0)
            {
                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), tender_change_double);
                tv_tender_change_total.setText(String.valueOf(ft));
                ll_tender_change.setVisibility(View.VISIBLE);
            }
            else
            {
                ll_tender_change.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        }
    public void saveArrayList( ArrayList<HashMap<String, String>> list, String key){
        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor1.putString(key, json);
        editor1.apply();     // This line is IMPORTANT !!!
    }
    public  ArrayList<HashMap<String, String>> getArrayList(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesData.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }
}
