package android.xtapps.com.dampafeastandroid.View.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.broadcast_receivers.SampleAlarmReceiver;

public class MainActivity extends AppCompatActivity {
    private final SampleAlarmReceiver alarm = new SampleAlarmReceiver();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        alarm.setAlarm(this);
    }
}
