package android.xtapps.com.dampafeastandroid.Model;

/**
 * Created by xt on 18/7/18.
 */

public class ImageModel {

    private String image_drawable;

    public String getImage_drawable() {
        return image_drawable;
    }

    public void setImage_drawable(String image_drawable) {
        this.image_drawable = image_drawable;
    }

    private int image_drawablee;

    public int getImage_drawablee() {
        return image_drawablee;
    }


    public void setImage_drawablee(int image_drawablee) {
        this.image_drawablee = image_drawablee;
    }
}