package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.DB.ModifierItems;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.LogOutTimerUtil;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.AddCartResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.CheckMenuStatusResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.LoginResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuRelations;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderInfo;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.TableContinueResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.CartListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.NotificationAdapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.xtapps.com.dampafeastandroid.Controller.Constants.API.ACTION_LOGIN;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ARRAY;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_CREATED_AT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_MENU_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ORDER_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STAFF_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STATUS;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_NAME;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TEXT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TYPE;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_UPDATED_AT;

public class CartActivity extends AppCompatActivity implements onApiFinish, LogOutTimerUtil.LogOutListener {
    List<OrderInfo> mContinueLists_details;
    List<OptionValuesList> mContinueLists2;
    private BroadcastReceiver broadcastReceiver;
    Spinner spinner1;
    NotificationAdapter adapter2;
    private int SPLASH_TIME_OUT = 5000;
    HashMap<String, String> map1;
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    String delete_pending="",cancel_status="";
    private String TAG = CartActivity.class.getSimpleName();
    private SharedPreferences sharedpreferencesData;
    ItemClickListener mItemClickListner;
    List<CartItem> mCartItems;
    List<MenuRelations> mMenuRelations;
    String order_number="";
    private Context mContext;
    private ArrayList<ImageModel> imageModelArrayList;

    private int[] myImageList = new int[]{
            R.drawable.ic_menu_camera, R.drawable.ic_menu_gallery, R.drawable.ic_menu_send,
            R.drawable.ic_menu_share, R.drawable.ic_menu_slideshow, R.drawable.ic_menu_manage
            , R.drawable.ic_menu_slideshow};
    private ListView listview,listview2;
    private TextView tv_table_name,tv_dine_name,
           /* tv_dinein_guest_count,tv_dinein_edt_senior_guest,tv_dinein_edt_solo_guest,tv_dinein_edt_disabled,*/
            tv_dine_phone,tv_del_name,tv_del_phone,tv_del_one,
            tv_del_two,tv_del_three,tv_del_four,tv_del_five,tv_take_name,tv_take_phone;
    private LinearLayout linear_dine_in,linear_delivery,linear_take_away;
    private TextView tv_release_table,tv_send_order,tv_complete_order,tv_add_cartt,tv_item_discount;
    EditText tv_dinein_guest_count,tv_dinein_edt_senior_guest,tv_dinein_edt_solo_guest,tv_dinein_edt_disabled;
    CartListAdapter adapter;
    FrameLayout frag1,frag2;
    List<CartItem> data;
    ProgressBar simpleProgressBar;
    private String FROM_CART = "add_cart";
    private String FROM_STATUS = "check_menu_status";
    private String FROM_ORDER_STATUS = "check_order_status";
    private final String FROM_LOGIN = "login";
    private final String FROM_DELETE = "delete";
    private final String FROM_COUPON = "coupon";
    private final String FROM_COMPLETE_ORDER = "complete_order";
    private final String FROM_RELEASE = "release";
    private final String FROM_ORDERBILL = "orderbill";
    private final String FROM_ORDERBILL_AGAIN = "orderbill_again";
    private final String FROM_PAYMENT_TYPES = "paymentTypes";
    private final String FROM_DELETE_MENU_ORDERS = "deleteOrderMenus";
    private String FROM_CONTINUE = "continue_order";
    private String FROM_KITCHEN_LOGIN_takeaway = "takeaway_orders";
    private String FROM_KITCHEN_LOGIN_delivery = "delivery_orders";
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    LinearLayout ll_dine,ll_take_away,ll_delivery,ll_reports,ll_kitchen,ll_logout;
    LinearLayout ll_paid_by,ll_card_type,ll_tender_cash,ll_tender_change,ll_id_proof,ll_coupon_disc,ll_solo_parent,
            ll_card_number,ll_card_holder;
    TextView tv_tender_change;
    EditText ed_tender_cash,ed_id_proof,ed_card_holder,ed_card_number,edt_order_comments;
    Double price_value;
    String total;
    String release_or_remove="",discount_apply="";
    ImageView nav_back;
    private  AlertDialog.Builder dialogBuilder,dialogBuilder2,dialogBuilder3;
    private AlertDialog alertDialog,alertDialog2,alertDialog3;
    Double Total=0.0;
    DecimalFormat df2;
    String MenuId="",OrderId="",Menuquantity="",Menuname="",Menuprice="",Menuname2="",OrderMenuId="",Update_pax="",Update_pax_flag="";
    // popup
    ImageView  img_right ;
    ImageView  img_wrong;
    LinearLayout linear_two ;
    LinearLayout linear_user_pass ;
    LinearLayout linear_comment,ll_apply_coupon ;
    TextView tv_one,tv_two,tv_three,tv_subtotal ;
    TextView tv_login;
    TextView tv_submit ;
    EditText ed_coupon_code;
    EditText edt_username,edt_password,edt_comment;
    Spinner spinner,spinner_card;
    private final int COMPLETED_FINAL_POPUP = 5;
    private final int CANCEL_POPUP = 3;
    private final int COMPLETED_POPUP = 2;
    private final String FROM_SUPERVISOR_LIST = "supervisor_list";
    private ArrayList<String> SupervisorListArray = new ArrayList<String>();
    ArrayList<String> PaymentTypelist = new ArrayList<String>();
    String select,selected_id;

    ImageView img_success,img_print;
    int Service_type=0;
    int cartcount;
    int OrderIdInt;
    private ConnectionDetector objDetector;
    List<CartItem> new_orders;
    List<CartItem> old_orders;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    String release_complete="";
    String discount_comment="";
    String paidtype,card_TType;
    int paid_position,card_position;
    String senior="0",solo="0",disabled="0",pax="1";
    String pax_valid="0",paxflag="";
    HashMap<String, String> map5_payment;
    ArrayList<HashMap<String, String>> categoryList_payment = new ArrayList<>();
    TextView  tv_price_term,tv_tax_subtotal,tv_vat_amount,tv_vatable_sale,tv_nonvatable_sale,tv_service_tax,tv_discount_amount,tv_coupon_disc,tv_payable_amount,tv_order_id,tv_guestname,tv_guestcount,tv_solo_count,tv_senior_count,tv_disabled_count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cartlist_activity_new);
        mContext=this;
        objDetector = new ConnectionDetector(getApplicationContext());


        listview = (ListView)findViewById(R.id.listview);
        listview2 = (ListView)findViewById(R.id.listview2);
        listview2.setVisibility(View.GONE);
        simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);

        df2 = new DecimalFormat(".##");

        sharedpreferencesData = CartActivity.this.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

        tv_table_name = (TextView) findViewById(R.id.tv_table_name);
        tv_dine_name = (TextView) findViewById(R.id.tv_dine_name);
        tv_dine_phone = (TextView) findViewById(R.id.tv_dine_phone);
        tv_dinein_guest_count = (EditText) findViewById(R.id.tv_dinein_guest_count);
        tv_dinein_edt_senior_guest = (EditText) findViewById(R.id.tv_dinein_edt_senior_guest);
        tv_dinein_edt_solo_guest = (EditText) findViewById(R.id.tv_dinein_edt_solo_guest);
        tv_dinein_edt_disabled = (EditText) findViewById(R.id.tv_dinein_edt_disabled);
        nav_back = (ImageView) findViewById(R.id.nav_back);
        tv_del_name = (TextView) findViewById(R.id.tv_del_name);
        tv_del_phone = (TextView)findViewById(R.id.tv_del_phone);

        ll_dine = (LinearLayout) findViewById(R.id.ll_dine);
        ll_take_away = (LinearLayout) findViewById(R.id.ll_take_away);
        ll_delivery = (LinearLayout) findViewById(R.id.ll_delivery);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_kitchen = (LinearLayout) findViewById(R.id.ll_kitchen);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);

        tv_del_one = (TextView) findViewById(R.id.tv_del_one);
        tv_del_two = (TextView) findViewById(R.id.tv_del_two);
        tv_del_three = (TextView) findViewById(R.id.tv_del_three);
        tv_del_four = (TextView)findViewById(R.id.tv_del_four);
        tv_del_five = (TextView)findViewById(R.id.tv_del_five);

        tv_take_name = (TextView)findViewById(R.id.tv_take_name);
        tv_take_phone = (TextView) findViewById(R.id.tv_take_phone);

        tv_release_table = (TextView) findViewById(R.id.tv_release_table);
        tv_send_order = (TextView) findViewById(R.id.tv_send_order);
        tv_complete_order = (TextView) findViewById(R.id.tv_complete_order);
        tv_item_discount = (TextView) findViewById(R.id.tv_item_discount);
        tv_add_cartt=(TextView)findViewById(R.id.tv_add_cartt);

        linear_dine_in = (LinearLayout)findViewById(R.id.linear_dine_in);
        linear_delivery = (LinearLayout) findViewById(R.id.linear_delivery);
        linear_take_away = (LinearLayout)findViewById(R.id.linear_take_away);

        edt_order_comments= (EditText) findViewById(R.id.edt_order_comments);
       /* Intent serverIntent = new Intent(CartActivity.this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
*/
        if (!objDetector.isConnectingToInternet()) {
            setContentView(R.layout.activity_network_connection);
            simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);
            simpleProgressBar.setScaleY(3f);
            simpleProgressBar.setScaleX(3f);
            simpleProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {



                @Override
                public void run() {

                    simpleProgressBar.setVisibility(View.GONE);

                }
            }, 3000);
        }


        tv_table_name.setText(sharedpreferencesData.getString("TableName",""));

        edt_order_comments.setText(sharedpreferencesData.getString("order_comment",""));

        tv_dine_name.setText(sharedpreferencesData.getString("dinein_GuestName",""));
        tv_del_name.setText(sharedpreferencesData.getString("delivery_GuestName",""));
        tv_take_name.setText(sharedpreferencesData.getString("takeaway_GuestName",""));

        tv_dine_phone.setText(sharedpreferencesData.getString("dinein_GuestNumber",""));
        tv_del_phone.setText(sharedpreferencesData.getString("delivery_GuestNumber",""));
        tv_take_phone.setText(sharedpreferencesData.getString("takeaway_GuestNumber",""));

        tv_dinein_guest_count.setText(sharedpreferencesData.getString("dinein_guest_count",""));
        tv_dinein_edt_senior_guest.setText(sharedpreferencesData.getString("dinein_edt_senior_guest",""));
        tv_dinein_edt_solo_guest.setText(sharedpreferencesData.getString("dinein_edt_solo_guest",""));
        tv_dinein_edt_disabled.setText(sharedpreferencesData.getString("dinein_edt_disabled",""));

        tv_del_one.setText(sharedpreferencesData.getString("DetailOne",""));
        tv_del_two.setText(sharedpreferencesData.getString("Detailtwo",""));
        tv_del_three.setText(sharedpreferencesData.getString("DetailThree",""));
        tv_del_four.setText(sharedpreferencesData.getString("DetailFour",""));
        tv_del_five.setText(sharedpreferencesData.getString("DeatilFive",""));


        AppSession.getInstance().setDiscountFlag(false);

        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            tv_release_table.setEnabled(true);

            linear_dine_in.setVisibility(View.VISIBLE);
            linear_delivery.setVisibility(View.GONE);
            linear_take_away.setVisibility(View.GONE);
        }
        else if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {

            tv_release_table.setEnabled(false);
            tv_release_table.setAlpha(0.4f);
            linear_dine_in.setVisibility(View.GONE);
            linear_delivery.setVisibility(View.GONE);
            linear_take_away.setVisibility(View.VISIBLE);
        }
        else if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            tv_release_table.setEnabled(false);
            tv_release_table.setAlpha(0.4f);
            linear_dine_in.setVisibility(View.GONE);
            linear_delivery.setVisibility(View.VISIBLE);
            linear_take_away.setVisibility(View.GONE);
        }
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {

            if(AppSession.getInstance().getOrderNumber().equals(""))
            {
                order_number="";
            } else {
                order_number= AppSession.getInstance().getOrderNumber();
            }
        }
        else if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            if(AppSession.getInstance().getOrder_idTake_delivery().equals(""))
            {
                order_number="";
            } else {
                order_number= AppSession.getInstance().getOrder_idTake_delivery();
            }
        }
        else if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            if(AppSession.getInstance().getOrder_id_delivery().equals(""))
            {
                order_number="";
            } else {
                order_number= AppSession.getInstance().getOrder_id_delivery();
            }
        }


        //flContainerfull=(FrameLayout)getActivity().findViewById(R.id.flContainerfull);




        initCallBackLisrners();
        bottomBarSettext();
        if(AppSession.getInstance().getComplteBillDetails().equals("22"+order_number))
        {
            //paymentTypes();

            Intent i=new Intent(CartActivity.this,CompletePopupFinalActivity.class);
            i.putExtra("order_number",order_number);
            i.putExtra("subtotal",tv_add_cartt.getText().toString());
            startActivityForResult(i, COMPLETED_FINAL_POPUP);
        }
        else
        {

        }
        tv_item_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<CartItem> new_orders = DBMethodsCart.newInstance(getApplicationContext()).getByOrderDiscount("");
                if(new_orders!=null&&new_orders.size()>0)
                {
                    discount_apply="true";
                    supervier_list();
                    //popup();
                }
                else
                {
                    Toast.makeText(mContext,getResources().getString(R.string.no_orders_apply_disc), Toast.LENGTH_SHORT).show();
                }

            }

        });
        tv_complete_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //CompletebilldetailspopUp();
                bottomBarSettext();
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    release_complete="complete";
                    if(AppSession.getInstance().getComplteBillDetails().equals("22"+order_number))
                    {
                        // paymentTypes();
                        tv_complete_order.setEnabled(false);
                        Intent i=new Intent(CartActivity.this,CompletePopupFinalActivity.class);
                        i.putExtra("order_number",order_number);
                        i.putExtra("subtotal",tv_add_cartt.getText().toString());
                        //startActivity(i);
                        startActivityForResult(i, COMPLETED_FINAL_POPUP);
                    }
                    else
                    {
                        if(!order_number.equals(""))
                        {
                            tv_complete_order.setEnabled(false);
                            checkOrderStatus();
                        }
                        else
                        {
                            Toast.makeText(mContext,getResources().getString(R.string.no_orders_found), Toast.LENGTH_SHORT).show();
                        }
                    }
                }



            }

        });

        tv_send_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                   /* if(AppSession.getInstance().getComplteBillDetails().equals("22"+order_number))
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.already_complete), Toast.LENGTH_SHORT).show();

                    }
                    else
                    {*/
                        if(!sharedpreferencesData.getString("dinein_guest_count","").equals(tv_dinein_guest_count.getText().toString().trim())
                                ||!sharedpreferencesData.getString("dinein_edt_senior_guest","0").equals(tv_dinein_edt_senior_guest.getText().toString().trim())
                                ||!sharedpreferencesData.getString("dinein_edt_solo_guest","0").equals(tv_dinein_edt_solo_guest.getText().toString().trim())
                                ||!sharedpreferencesData.getString("dinein_edt_disabled","0").equals(tv_dinein_edt_disabled.getText().toString().trim()))
                        {
                            Update_pax_flag="true";
                            setPreference();
                        }
                        else
                        {
                            Update_pax_flag="false";
                        }

                        if(!AppSession.getInstance().getServiceType().equals("dine_in"))
                        {
                            //tv_send_order.setEnabled(false);
                            Checkpreference();
                            // addtoCart();
                        }
                        else if(AppSession.getInstance().getServiceType().equals("dine_in")&&
                                (!AppSession.getInstance().getTable_ID().equals("")&&
                                        AppSession.getInstance().getTable_ID()!=null))
                        {
                            //tv_send_order.setEnabled(false);
                            Checkpreference();
                            //addtoCart();
                        }
                        else
                        {
                            Toast.makeText(mContext,getResources().getString(R.string.select_table), Toast.LENGTH_SHORT).show();

                        }
                 //   }

                }




            }
        });
        tv_release_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    if(AppSession.getInstance().getComplteBillDetails().equals("22"+order_number))
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.already_complete), Toast.LENGTH_SHORT).show();

                    }
                    else {
                        if(AppSession.getInstance().getServiceType().equals("dine_in"))
                        {


                            new_orders = DBMethodsCart.newInstance(CartActivity.this).getByOrderStatus("order_sent");
                            if(new_orders.size()>0)
                            {

                                if(!isFinishing()&&mContext!=null)
                                {
                                    release_complete="release";
                                    if(!/*AppSession.getInstance().getOrderNumber()*/order_number.equals(""))
                                    {
                                        tv_release_table.setEnabled(false);
                                        checkOrderStatus();
                                    }
                                    else
                                    {
                                        Toast.makeText(mContext,getResources().getString(R.string.no_orders_found), Toast.LENGTH_SHORT).show();
                                    }
                                    // popup();
                                }

                            }
                            else
                            {
                                //deleteOrderMenus();
                                Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
                                startActivity(i);
                            }
                        }
                        else
                        {
                            Toast.makeText(mContext,getResources().getString(R.string.no_tables_release), Toast.LENGTH_SHORT).show();

                        }
                    }
                }


                /*release_or_remove="release";
                popup();*/
            }
        });
        nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(AppSession.getInstance().getServiceType().equals("dine_in"))
                {
                    dine_in();
                }
                else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                {
                    take_away();
                }
                else   if(AppSession.getInstance().getServiceType().equals("delivery"))
                {
                    delivery();
                }

                /*finish();*/
            }
        });




        ll_dine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusDinein();


            }
        });
        ll_take_away.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusTakeAway();

            }
        });
        ll_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusDelivery();

            }
        });
        ll_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setTick("reports");
                Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
                startActivity(i);
                finish();


            }
        });
        ll_kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setTick("kitchen");
                Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
                startActivity(i);
                finish();

            }
        });
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();

            }
        });
    }



    private void addtoCart() {
        tv_send_order.setEnabled(false);
        String order_comment="";
        if(edt_order_comments.getText().length()>0)
        {
            order_comment=edt_order_comments.getText().toString();
        }
        else
        {
            order_comment="";
        }
        List<CartItem> new_orders = DBMethodsCart.newInstance(getApplicationContext()).getByOrderStatus("");
        if(new_orders.size()>0/*||Update_pax_flag.equals("true")*/)
        {
          /*  if(new_orders.size()>0)
            {
                Update_pax="0";
            }
            else
            {
                Update_pax="1";
            }*/
            Update_pax="0";
            simpleProgressBar.setVisibility(View.VISIBLE);
            String URL = AppSession.getInstance().getRegUrl()+ API.ADD_TO_CART;
            /*current_page= page++;*/
            HashMap<String, String> values = new HashMap<>();
            /* values.put("action", "AddCartOrder");*/
            values.put("action", API.ACTION_ADD_TO_CART);
            values.put("location_id", AppSession.getInstance().getLocation_id());
            values.put("staff_id", AppSession.getInstance().getStaff_ID());
            values.put("tab_token", AppSession.getInstance().getTabToken());
            values.put("table_id", AppSession.getInstance().getTable_ID());

            values.put("course", "1");

            values.put("order_number", order_number);
            values.put("order_type", AppSession.getInstance().getServiceType());
            values.put("comment", order_comment);
            values.put("update_pax", Update_pax);

            if(AppSession.getInstance().getServiceType().equals("dine_in"))
            {

                values.put("customer_name", sharedpreferencesData.getString("dinein_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("dinein_GuestNumber",""));
                values.put("guest_count", sharedpreferencesData.getString("dinein_guest_count",""));
                values.put("no_of_pax", sharedpreferencesData.getString("dinein_guest_count",""));
            }
            else if(AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                values.put("customer_name", sharedpreferencesData.getString("takeaway_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("takeaway_GuestNumber",""));
                values.put("guest_count", "1");
                values.put("no_of_pax", "1");
            }
            else if(AppSession.getInstance().getServiceType().equals("delivery"))
            {
                values.put("customer_name", sharedpreferencesData.getString("delivery_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("delivery_GuestNumber",""));
                values.put("guest_count", "1");
                values.put("no_of_pax", "1");
            }

           /* values.put("customer_name", sharedpreferencesData.getString("GuestName",""));
            values.put("customer_mobile",sharedpreferencesData.getString("GuestNumber",""));*/
            /*  values.put("guest_count", AppSession.getInstance().getGuest_Count());*/
           /* values.put("guest_count", sharedpreferencesData.getString("dinein_guest_count",""));
            values.put("no_of_pax", sharedpreferencesData.getString("dinein_guest_count",""));*/
            values.put("senior_guest", sharedpreferencesData.getString("dinein_edt_senior_guest","0"));
            values.put("solo_guest", sharedpreferencesData.getString("dinein_edt_solo_guest","0"));
            values.put("disabled", sharedpreferencesData.getString("dinein_edt_disabled","0"));

            values.put("address1", sharedpreferencesData.getString("DetailOne",""));
            values.put("address2",sharedpreferencesData.getString("Detailtwo",""));
            values.put("location", sharedpreferencesData.getString("DetailThree",""));
            values.put("area",sharedpreferencesData.getString("DetailFour",""));
            values.put("city", sharedpreferencesData.getString("DeatilFive",""));
            try {
                JSONArray jsonArray = new JSONArray();
                for (CartItem item : new_orders) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("row_id", item.getIndex());
                    jsonObject.put("menu_id", item.getMenu_id());
                    if(item.getMenu_qty()==item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getMenu_qty());
                    }
                    else if(item.getMenu_qty()>item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getMenu_qty()-item.getPrev_quantity());
                    }
                    else if(item.getMenu_qty()<item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getPrev_quantity()-item.getMenu_qty());
                    }

                    jsonObject.put("comment", item.getComment());
                    jsonObject.put("menu_price", item.getMenu_qty_X_menu_price());
                    jsonObject.put("discount", item.getDiscount_percent());
                    jsonObject.put("discount_comment", item.getDiscount_comment());
                    JSONArray jsonArray2 = new JSONArray();
                    List<ModifierItems> modifiers = DBMethodsCart.newInstance(getApplicationContext()).getModifierByMenuId(item.getMenu_id());
                    if(modifiers.size()>0)
                    {
                        for (ModifierItems mitem : modifiers) {
                            JSONObject jsonObject2 = new JSONObject();
                            jsonObject2.put("menu_option_id", mitem.getMenu_option_id());
                            //JSONObject jsonObject3 = new JSONObject();
                            jsonObject2.put("menu_option_value_id", mitem.getMenu_option_value_id());
                            jsonObject2.put("option_value_id", mitem.getOption_value_id());
                            jsonObject2.put("menu_id", mitem.getMenu_id());
                            jsonObject2.put("option_id", mitem.getOption_id());
                            jsonObject2.put("value", mitem.getValue());
                            jsonObject2.put("new_price", mitem.getNew_price());
                            jsonObject2.put("price", mitem.getPrice());

                            // jsonObject2.put("option_value", jsonObject3);
                            jsonArray2.put(jsonObject2);
                            //    Toast.makeText(mContext,mitem.getDisplay_type(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        JSONObject jsonObject2 = new JSONObject();
                        jsonObject2.put("menu_option_id", item.getMenu_option_id());
                        //JSONObject jsonObject3 = new JSONObject();
                        jsonObject2.put("menu_option_value_id", item.getMenu_option_value_id());
                        jsonObject2.put("option_value_id", item.getOption_value_id());
                        jsonObject2.put("menu_id", item.getMenu_id());
                        jsonObject2.put("option_id", item.getOption_id());
                        jsonObject2.put("value", item.getValue());
                        jsonObject2.put("new_price", item.getNew_price());
                        jsonObject2.put("price", item.getPrice());

                        // jsonObject2.put("option_value", jsonObject3);
                        jsonArray2.put(jsonObject2);
                    }




                    jsonObject.put("menu_options", jsonArray2);
                    jsonArray.put(jsonObject);
                }


                values.put("order", jsonArray.toString());

                CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext, values);
                objCallWebServiceTask.execute(URL, FROM_CART , "POST");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(Update_pax_flag.equals("true")||AppSession.getInstance().getUpdatedFlag_sssss().equals("true"))
        {
            List<CartItem> new_orders2 = DBMethodsCart.newInstance(getApplicationContext()).getCartItems();
            if(new_orders2.size()>0/*||Update_pax_flag.equals("true")*/)
            {
                Update_pax="1";

            simpleProgressBar.setVisibility(View.VISIBLE);
            String URL = AppSession.getInstance().getRegUrl()+ API.ADD_TO_CART;
            /*current_page= page++;*/
            HashMap<String, String> values = new HashMap<>();
            /* values.put("action", "AddCartOrder");*/
            values.put("action", API.ACTION_ADD_TO_CART);
            values.put("location_id", AppSession.getInstance().getLocation_id());
            values.put("staff_id", AppSession.getInstance().getStaff_ID());
            values.put("tab_token", AppSession.getInstance().getTabToken());
            values.put("table_id", AppSession.getInstance().getTable_ID());

            values.put("course", "1");

            values.put("order_number", order_number);
            values.put("order_type", AppSession.getInstance().getServiceType());
            values.put("comment", order_comment);
            values.put("update_pax", Update_pax);

            if(AppSession.getInstance().getServiceType().equals("dine_in"))
            {

                values.put("customer_name", sharedpreferencesData.getString("dinein_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("dinein_GuestNumber",""));
                values.put("guest_count", sharedpreferencesData.getString("dinein_guest_count",""));
                values.put("no_of_pax", sharedpreferencesData.getString("dinein_guest_count",""));
            }
            else if(AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                values.put("customer_name", sharedpreferencesData.getString("takeaway_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("takeaway_GuestNumber",""));
                values.put("guest_count", "1");
                values.put("no_of_pax", "1");
            }
            else if(AppSession.getInstance().getServiceType().equals("delivery"))
            {
                values.put("customer_name", sharedpreferencesData.getString("delivery_GuestName",""));
                values.put("customer_mobile",sharedpreferencesData.getString("delivery_GuestNumber",""));
                values.put("guest_count", "1");
                values.put("no_of_pax", "1");
            }

           /* values.put("customer_name", sharedpreferencesData.getString("GuestName",""));
            values.put("customer_mobile",sharedpreferencesData.getString("GuestNumber",""));*/
            /*  values.put("guest_count", AppSession.getInstance().getGuest_Count());*/
           /* values.put("guest_count", sharedpreferencesData.getString("dinein_guest_count",""));
            values.put("no_of_pax", sharedpreferencesData.getString("dinein_guest_count",""));*/
            values.put("senior_guest", sharedpreferencesData.getString("dinein_edt_senior_guest","0"));
            values.put("solo_guest", sharedpreferencesData.getString("dinein_edt_solo_guest","0"));
            values.put("disabled", sharedpreferencesData.getString("dinein_edt_disabled","0"));

            values.put("address1", sharedpreferencesData.getString("DetailOne",""));
            values.put("address2",sharedpreferencesData.getString("Detailtwo",""));
            values.put("location", sharedpreferencesData.getString("DetailThree",""));
            values.put("area",sharedpreferencesData.getString("DetailFour",""));
            values.put("city", sharedpreferencesData.getString("DeatilFive",""));
            try {
                JSONArray jsonArray = new JSONArray();
                for (CartItem item : new_orders2) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("row_id", item.getIndex());
                    jsonObject.put("menu_id", item.getMenu_id());
                    if(item.getMenu_qty()==item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getMenu_qty());
                    }
                    else if(item.getMenu_qty()>item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getMenu_qty()-item.getPrev_quantity());
                    }
                    else if(item.getMenu_qty()<item.getPrev_quantity())
                    {
                        jsonObject.put("quantity", item.getPrev_quantity()-item.getMenu_qty());
                    }

                    jsonObject.put("comment", item.getComment());
                    jsonObject.put("menu_price", item.getMenu_qty_X_menu_price());
                    jsonObject.put("discount", item.getDiscount_percent());
                    jsonObject.put("discount_comment", item.getDiscount_comment());
                    JSONArray jsonArray2 = new JSONArray();
                    List<ModifierItems> modifiers = DBMethodsCart.newInstance(getApplicationContext()).getModifierByMenuId(item.getMenu_id());
                    if(modifiers.size()>0)
                    {
                        for (ModifierItems mitem : modifiers) {
                            JSONObject jsonObject2 = new JSONObject();
                            jsonObject2.put("menu_option_id", mitem.getMenu_option_id());
                            //JSONObject jsonObject3 = new JSONObject();
                            jsonObject2.put("menu_option_value_id", mitem.getMenu_option_value_id());
                            jsonObject2.put("option_value_id", mitem.getOption_value_id());
                            jsonObject2.put("menu_id", mitem.getMenu_id());
                            jsonObject2.put("option_id", mitem.getOption_id());
                            jsonObject2.put("value", mitem.getValue());
                            jsonObject2.put("new_price", mitem.getNew_price());
                            jsonObject2.put("price", mitem.getPrice());

                            // jsonObject2.put("option_value", jsonObject3);
                            jsonArray2.put(jsonObject2);
                            //    Toast.makeText(mContext,mitem.getDisplay_type(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        JSONObject jsonObject2 = new JSONObject();
                        jsonObject2.put("menu_option_id", item.getMenu_option_id());
                        //JSONObject jsonObject3 = new JSONObject();
                        jsonObject2.put("menu_option_value_id", item.getMenu_option_value_id());
                        jsonObject2.put("option_value_id", item.getOption_value_id());
                        jsonObject2.put("menu_id", item.getMenu_id());
                        jsonObject2.put("option_id", item.getOption_id());
                        jsonObject2.put("value", item.getValue());
                        jsonObject2.put("new_price", item.getNew_price());
                        jsonObject2.put("price", item.getPrice());

                        // jsonObject2.put("option_value", jsonObject3);
                        jsonArray2.put(jsonObject2);
                    }




                    jsonObject.put("menu_options", jsonArray2);
                    jsonArray.put(jsonObject);
                }


                values.put("order", jsonArray.toString());

                CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext, values);
                objCallWebServiceTask.execute(URL, FROM_CART , "POST");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }
            else
            {
                tv_send_order.setEnabled(true);
                Toast.makeText(mContext,getResources().getString(R.string.no_orders_tosent), Toast.LENGTH_SHORT).show();

            }
        }
        else
        {
            tv_send_order.setEnabled(true);
            Toast.makeText(mContext,getResources().getString(R.string.no_orders_tosent), Toast.LENGTH_SHORT).show();

        }
    }



    private void checkStatusDelivery() {
        if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            //finish();
            delivery();
        }
        else
        {
            Service_type=3;
            // cartcount = DBMethodsCart.newInstance(CartActivity.this).getCartItemsCount();

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("delivery"))
            {
                new_orders = DBMethodsCart.newInstance(CartActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();
                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(CartActivity.this).clearCart();
                    DBMethodsCart.newInstance(CartActivity.this).clearModifier();
                    delivery();
                }
                //alert();
            }
            else
            {
                delivery();
            }


        }
    }

    private void checkStatusTakeAway() {
        if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            // finish();
            take_away();
        }
        else
        {
            Service_type=2;

            /* cartcount = DBMethodsCart.newInstance(CartActivity.this).getCartItemsCount();*/

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                new_orders = DBMethodsCart.newInstance(CartActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(CartActivity.this).clearCart();
                    DBMethodsCart.newInstance(CartActivity.this).clearModifier();
                    take_away();
                }
                //alert();
            }
            else
            {
                take_away();
            }
        }
    }



    private void checkStatusDinein() {
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            //finish();
            dine_in();
        }
        else
        {
            Service_type=1;

            /* cartcount = DBMethodsCart.newInstance(CartActivity.this).getCartItemsCount();*/
            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("dine_in"))
            {
                new_orders = DBMethodsCart.newInstance(CartActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(CartActivity.this).clearCart();
                    DBMethodsCart.newInstance(CartActivity.this).clearModifier();

                    dine_in();
                }
                // alert();
            }
            else
            {
                dine_in();
            }
        }
    }

    private void alert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(CartActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(CartActivity.this);
        }
        builder.setTitle("You have unfinished orders")
                .setMessage("Are you sure you want to continue?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        AppSession.getInstance().setTable_ID("");
                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("order_comment","");
                        editor1.commit();
                        AppSession.getInstance().setOrderNumber("");
                        AppSession.getInstance().setOrderTotal("0");
                        AppSession.getInstance().setTable_Name("");
                        AppSession.getInstance().setOrder_idTake_delivery("");
                        AppSession.getInstance().setOrder_id_delivery("");
                        DBMethodsCart.newInstance(CartActivity.this).clearCart();
                        DBMethodsCart.newInstance(CartActivity.this).clearModifier();
                        if(Service_type==1)
                        {
                            dine_in();
                        }
                        else
                        if(Service_type==2)
                        {
                            take_away();
                        }
                        else if(Service_type==3)
                        {
                            delivery();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void alertDelete() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(CartActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(CartActivity.this);
        }
        builder.setTitle("Do you want to delete the item?")
                .setMessage("Are you sure you want to continue?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        simpleProgressBar.setVisibility(View.VISIBLE);
                        release_or_remove="remove";
                        if(!isFinishing()&&mContext!=null)
                        {
                            checkStatus();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void delivery() {
        AppSession.getInstance().setServiceType("delivery");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }

    private void dine_in() {
        AppSession.getInstance().setServiceType("dine_in");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setOrder_id_delivery("");
        Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }
    private void take_away() {
        AppSession.getInstance().setServiceType("takeaway");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        /* AppSession.getInstance().setOrder_idTake_delivery("");*/
        AppSession.getInstance().setOrder_id_delivery("");
        Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }


    private void initCallBackLisrners() {


        mItemClickListner = new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onItemSelected(String tag, int position, Object response) {
                final int pos=position;
                final CartItem item2 = mCartItems.get(position);
                CartItem item = mCartItems.get(position);
                MenuId=item.getMenu_id();
                OrderMenuId=item.getOrder_menu_id();
                Menuquantity=String.valueOf(item.getMenu_qty());
                Menuname=item.getMenu_name();
                Menuname2=item.getMenu_name2();
                Menuprice=String.valueOf(item.getMenu_qty_X_menu_price());
                AppSession.getInstance().setMenuquantity(Menuquantity);
                AppSession.getInstance().setMenuname(Menuname);

                double f = Double.parseDouble(Menuprice);
                /*  String ft = String.format("%.3f", f);*/
                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                AppSession.getInstance().setMenuPrice(ft);


                OrderId=item.getOrder_id();
                if (tag.equals("cartPlus")) {
                    simpleProgressBar.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mCartItems.get(pos).setMenu_qty(item2.getMenu_qty()+ 1);
                            DBMethodsCart.newInstance(mContext).updateCart(true, item2.getOrder_menu_id(),
                                    Double.valueOf(item2.getMenu_price())*Double.valueOf(item2.getMenu_qty()),"",
                                    String.valueOf((Double.valueOf(item2.getMenu_price())*Double.valueOf(item2.getMenu_qty()))
                                            *(item2.getDiscount_percent()/100)));
                            mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                            adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                            listview.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            bottomBarSettext();
                            simpleProgressBar.setVisibility(View.GONE);
                            AppSession.getInstance().setCartPlusFlag("");

                        }
                    }, 100);


                } else if (tag.equals("cartMinus")) {

                  /*  if (DBMethodsCart.newInstance(mContext).updateCart(false, item.getMenu_id()) == 0) {
                        DBMethodsCart.newInstance(mContext).removeFromCart(item.getMenu_id());
                        mCartItems =DBMethodsCart.newInstance(getActivity()).getCartItems( );
                        adapter = new CartListAdapter(getActivity(), mCartItems, mItemClickListner);
                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                    else {*/

                    simpleProgressBar.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {



                        @Override
                        public void run() {
                            mCartItems.get(pos).setMenu_qty(item2.getMenu_qty()- 1);
                            DBMethodsCart.newInstance(mContext).updateCart(false, item2.getOrder_menu_id(),
                                    Double.valueOf(item2.getMenu_price())*Double.valueOf(item2.getMenu_qty()),"",
                                    String.valueOf((Double.valueOf(item2.getMenu_price())*Double.valueOf(item2.getMenu_qty()))
                                            *(item2.getDiscount_percent()/100)));
                            mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                            adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                            listview.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            bottomBarSettext();
                            simpleProgressBar.setVisibility(View.GONE);
                            AppSession.getInstance().setCartPlusFlag("");
                        }
                    }, 100);



                }

                else if(tag.equals("cartDelete")){
                    /*mCartItems.remove(position);*/
                    // popup();
                    simpleProgressBar.setVisibility(View.VISIBLE);

                    if(item.getStatus().equals("order_sent"))
                    {
                        if (!objDetector.isConnectingToInternet()) {

                        }
                        else
                        {

                            new Handler().postDelayed(new Runnable() {



                                @Override
                                public void run() {
                                    alertDelete();
                                    simpleProgressBar.setVisibility(View.GONE);
                                    AppSession.getInstance().setCartPlusFlag("");
                                }
                            }, 100);
                        }

                    }
                    else
                    {

                        new Handler().postDelayed(new Runnable() {



                            @Override
                            public void run() {
                                DBMethodsCart.newInstance(mContext).removeFromCart(item2.getMenu_id());
                                DBMethodsCart.newInstance(mContext).removeFromModifier( item2.getMenu_id());

                                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                                if(mCartItems.size()>0)
                                {
                                    adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                                    listview.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    bottomBarSettext();
                                }
                                else
                                {
                                    AppSession.getInstance().setCartPlusFlag("");
                                    if(AppSession.getInstance().getServiceType().equals("dine_in"))
                                    {
                                        dine_in();
                                    }
                                    else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                                    {
                                        take_away();
                                    }
                                    else   if(AppSession.getInstance().getServiceType().equals("delivery"))
                                    {
                                        delivery();
                                    }
                                }
                                simpleProgressBar.setVisibility(View.GONE);
                                AppSession.getInstance().setCartPlusFlag("");
                            }
                        }, 100);

                    }


                   /* Toast.makeText(getApplicationContext(),item.getMenu_id(), Toast.LENGTH_SHORT).show();
                    DBMethodsCart.newInstance(mContext).removeFromCart(item.getMenu_id());
                    mCartItems =DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                    adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                    listview.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    bottomBarSettext();*/
                }

                else if(tag.equals("cartClick")){
                    mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                    price_value=Double.parseDouble(mCartItems.get(position).getMenu_price().toString());
                    total= String.valueOf((price_value)*Integer.parseInt(String.valueOf(mCartItems.get(position).getMenu_qty())));
                    if((mCartItems.get(position).getRequired().toString().equals("1"))
                            ||mCartItems.get(position).getRequired().toString().equals("2"))
                    {
                        /*ItemDetailFragment fragment2 = new ItemDetailFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();*/


                       /* Bundle bundle=new Bundle();
                        bundle.putString("menu_id", mCartItems.get(position).getMenu_id().toString());
                        bundle.putString("name", mCartItems.get(position).getMenu_name().toString());
                        bundle.putString("price", mCartItems.get(position).getMenu_price().toString());
                        bundle.putString("total", total);
                        bundle.putString("quantity", String.valueOf(mCartItems.get(position).getMenu_qty()));
                        fragment2.setArguments(bundle);

                        fragmentTransaction.replace(R.id.flContainer, fragment2);
                        fragmentTransaction.commit();*/
                        AppSession.getInstance().setModifierFlag("1");
                        Intent i = new Intent(CartActivity.this,ItemDetailsActivity.class);
                        i.putExtra("order_id", order_number);
                        i.putExtra("position", position);
                        i.putExtra("menu_id", mCartItems.get(position).getMenu_id().toString());
                        i.putExtra("order_menu_id", mCartItems.get(position).getOrder_menu_id().toString());
                        i.putExtra("category_name", mCartItems.get(position).getCategory_name().toString());
                        i.putExtra("name", mCartItems.get(position).getMenu_name().toString());
                        i.putExtra("price", mCartItems.get(position).getMenu_price().toString());
                        i.putExtra("total", total);
                        i.putExtra("quantity", String.valueOf(mCartItems.get(position).getMenu_qty()));
                        startActivity(i);
                    }
                  /*  else if(mCartItems.get(position).getRequired().toString().equals("2")) {
                        Toast.makeText(CartActivity.this,"cannot add modifiers order already send", Toast.LENGTH_SHORT).show();
                    }*/
                    else
                    {
                        Toast.makeText(CartActivity.this,getResources().getString(R.string.no_modifiers), Toast.LENGTH_SHORT).show();
                        AppSession.getInstance().setModifierFlag("");
                        Intent i = new Intent(CartActivity.this,ItemDetailsActivity.class);
                        i.putExtra("order_id", order_number);
                        i.putExtra("position", position);
                        i.putExtra("menu_id", mCartItems.get(position).getMenu_id().toString());
                        i.putExtra("order_menu_id", mCartItems.get(position).getOrder_menu_id().toString());
                        i.putExtra("category_name", mCartItems.get(position).getCategory_name().toString());
                        i.putExtra("name", mCartItems.get(position).getMenu_name().toString());
                        i.putExtra("price", mCartItems.get(position).getMenu_price().toString());
                        i.putExtra("total", total);
                        i.putExtra("quantity", String.valueOf(mCartItems.get(position).getMenu_qty()));

                        startActivity(i);

                    }


                }




            }
        };


    }



    private void popup() {
        dialogBuilder = new AlertDialog.Builder(CartActivity.this, R.style.NewDialog);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.prepration_popup, null);

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();

        /* ImageView*/  img_right = (ImageView) dialogView.findViewById(R.id.img_right);
        /* final ImageView */ img_wrong = (ImageView) dialogView.findViewById(R.id.img_wrong);

        /*final LinearLayout*/ linear_two = (LinearLayout) dialogView.findViewById(R.id.linear_two);
        /* final LinearLayout*/ linear_user_pass = (LinearLayout) dialogView.findViewById(R.id.linear_user_pass);
        /* final LinearLayout*/ linear_comment = (LinearLayout) dialogView.findViewById(R.id.linear_comment);

        tv_one = (TextView)dialogView.findViewById(R.id.tv_one);
        tv_two = (TextView)dialogView.findViewById(R.id.tv_two);
        /* final TextView*/ tv_three = (TextView)dialogView.findViewById(R.id.tv_three);
        /* final TextView*/ tv_login = (TextView)dialogView.findViewById(R.id.tv_login);
        /* final TextView*/ tv_submit = (TextView)dialogView.findViewById(R.id.tv_submit);

        edt_username = (EditText)dialogView.findViewById(R.id.edt_username);
        edt_password = (EditText)dialogView.findViewById(R.id.edt_password);
        edt_comment= (EditText)dialogView.findViewById(R.id.edt_comment);

        //final SearchableSpinner spinner1 = dialogView.findViewById(R.id.spinner);

        spinner1 = dialogView.findViewById(R.id.spinner);

        // spinner1.setTitle(getResources().getString(R.string.spinner_title));
        String[] types1 = getResources().getStringArray(R.array.select_service);
        final List<String> genderType1 = new ArrayList<>(Arrays.asList(types1));

        /* Set your ArrayAdapter with the StringWithTag, and when each entry is shown in the Spinner, .toString() is called. */
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SupervisorListArray);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position >= 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);




                    /*  Toast.makeText(getApplicationContext(),select, Toast.LENGTH_LONG).show();*/

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        if(mContext!=null)
        {
            alertDialog.show();
        }


        if(discount_apply.equals("true"))
        {
            tv_one.setText("Enter Your Discount Comment");
            tv_two.setText("");
            linear_two.setVisibility(View.VISIBLE);
            img_wrong.setVisibility(View.GONE);
        }

        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linear_two.setVisibility(View.VISIBLE);
                img_wrong.setVisibility(View.GONE);
            }
        });

        img_wrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFinishing()&&mContext!=null)
                {
                    superviser_login();
                }

               /* linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.comment));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);*/
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                if (release_or_remove.equals("remove")) {
                    if (!isFinishing() && mContext != null) {
                        simpleProgressBar.setVisibility(View.VISIBLE);
                        deleteItem();
                    }

                } else if (release_or_remove.equals("release")) {
                    if (!isFinishing() && mContext != null) {
                        simpleProgressBar.setVisibility(View.VISIBLE);
                        cancel_status="1";
                        deleteOrderMenus();
                    }


                } else if(discount_apply.equals("true"))
                {
                    // AppSession.getInstance().setDiscountFlag(true);

                    discount_comment=edt_comment.getText().toString();

                    if(discount_comment.equals("")||discount_comment==null)
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.discount_comment_valid), Toast.LENGTH_LONG).show();

                    }
                    else
                    {
                        discount_apply="";
                        //   DBMethodsCart.newInstance(mContext).updateCartDiscount(discount_comment,order_number);
                        DBMethodsCart.newInstance(mContext).updateCartDiscount(discount_comment);
                        mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                        adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                        alertDialog.hide();
                    }


                }else {
                    alertDialog.hide();
                }

                /*DBMethodsCart.newInstance(mContext).removeFromCart(MenuId);
                mCartItems =DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                bottomBarSettext();
                alertDialog.hide();*/
            }


        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    // To dismiss the fragment when the back-button is pressed.
                    alertDialog.hide();
                    return true;
                }


                // Otherwise, do nothing else
                else return false;
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){


                /****cleanup code****/
            }});
    }
    private void paymentTypes() {
        String URL = AppSession.getInstance().getRegUrl()+ API.PAYMENT_TYPES;



        HashMap<String, String> values = new HashMap<>();
        values.put("action",  API.ACTION_PAYMENT_TYPES);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_PAYMENT_TYPES, "POST");
    }
    private void releaseTable() {
        String URL = AppSession.getInstance().getRegUrl()+ API.RELEASE_TABLE;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /*  values.put("action","deleteOrderMenus");*/
        values.put("action", API.ACTION_RELEASE_TABLE);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("table_id", AppSession.getInstance().getTable_ID());

/*        if(OrderId.equals(""))
        {
            OrderIdInt=1;
        }
        else
        {
            OrderIdInt=Integer.parseInt(OrderId);
        }
        values.put("order_id", String.valueOf(OrderIdInt));*/
       /* if(AppSession.getInstance().getOrderNumber().equals(""))
        {
            order_number="";
        } else {
            order_number=AppSession.getInstance().getOrderNumber();
        }*/
        values.put("order_id", order_number);

        //values.put("order_id",String.valueOf(order_number+1));

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_RELEASE, "POST");
    }
    private void deleteOrderMenus() {
        // String URL = AppSession.getInstance().getRegUrl()+ API.RELEASE_TABLE_2;
        String URL = AppSession.getInstance().getRegUrl()+ API.DELETE_MENU_ORDERS;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /*values.put("action","deleteOrderMenus");*/
        /* values.put("action", API.ACTION_RELEASE_TABLE_2);*/
        values.put("action", API.ACTION_DELETE_MENU_ORDERS);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());

      /*  if(AppSession.getInstance().getOrderNumber().equals(""))
        {
            order_number="";
        } else {
            order_number=AppSession.getInstance().getOrderNumber();
        }*/
        if(release_or_remove.equals("release_waiter")||release_or_remove.equals("no_item"))
        {
            values.put("comment","");
        }
        else
        {
            values.put("comment",edt_comment.getText().toString().trim());
        }
        values.put("order_id", order_number);
        values.put("cancel_status", cancel_status);

     /*   if(OrderId.equals(""))
        {
            OrderIdInt=1;
        }
        else
        {
            OrderIdInt=Integer.parseInt(OrderId);
        }
        values.put("order_id", String.valueOf(OrderIdInt));*/
        /*values.put("comment",edt_comment.getText().toString().trim());

        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("table_id", AppSession.getInstance().getTable_ID());*/


        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_DELETE_MENU_ORDERS, "POST");
    }
    private void completeOrder() {
        if(card_position==0)
        {
            card_TType="";
        }
        else
        {

        }
        simpleProgressBar.setVisibility(View.VISIBLE);
        String URL = AppSession.getInstance().getRegUrl()+ API.COMPLETE_ORDER;



        HashMap<String, String> values = new HashMap<>();
        /* values.put("action", "completeOrder");*/
        values.put("action", API.ACTION_COMPLETE_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());

    /*    if(OrderId.equals(""))
        {
            OrderIdInt=1;
        }
        else
        {
            OrderIdInt=Integer.parseInt(OrderId);
        }
        values.put("order_id", String.valueOf(OrderIdInt));*/
       /* if(AppSession.getInstance().getOrderNumber().equals(""))
        {
            order_number="";
        } else {
            order_number=AppSession.getInstance().getOrderNumber();
        }*/
        values.put("order_id", order_number);
        values.put("prev_subtotal", AppSession.getInstance().gettotal());
        values.put("subtotal", tv_add_cartt.getText().toString());
        /*  values.put("coupon_code", ed_coupon_code.getText().toString());*/
        /*values.put("coupon_code", sharedpreferencesData.getString("Coupon_code",""));*/
        values.put("payment_mode", paidtype);
        values.put("card_type", card_TType);
        values.put("card_number", ed_card_number.getText().toString());
        values.put("card_name", ed_card_holder.getText().toString());
        values.put("tender_cash", ed_tender_cash.getText().toString());
        values.put("tender_change", tv_tender_change.getText().toString());
        values.put("proof_ids", ed_id_proof.getText().toString());
        values.put("order_type", "1");
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_COMPLETE_ORDER, "POST");
    }
    private void checkStatus() {
        String URL = AppSession.getInstance().getRegUrl()+ API.CHECK_MENU_STATUS;



        HashMap<String, String> values = new HashMap<>();
        /*values.put("action", "checkOrderMenuStatus");*/
        values.put("action",  API.ACTION_CHECK_MENU_STATUS);
        values.put("menu_id", MenuId);
        values.put("order_menu_id", OrderMenuId);
        //values.put("order_id",String.valueOf(order_number+1));
        values.put("order_id",String.valueOf(OrderId));
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_STATUS, "POST");
    }
    private void checkOrderStatus() {
        String URL = AppSession.getInstance().getRegUrl()+ API.CHECK_ORDER_STATUS;



        HashMap<String, String> values = new HashMap<>();
        /*values.put("action", "checkOrderMenuStatus");*/
        values.put("action",  API.ACTION_CHECK_ORDER_STATUS);
      /*  if(AppSession.getInstance().getOrderNumber().equals(""))
        {
            order_number="";
        } else {
            order_number=AppSession.getInstance().getOrderNumber();
        }*/
        values.put("order_id",String.valueOf(order_number));
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_ORDER_STATUS, "POST");
    }

    private void deleteItem() {
        String URL = AppSession.getInstance().getRegUrl()+ API.DELETE_MENU_ORDER;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","deleteMenuOrder");*/
        values.put("action", API.ACTION_DELETE_MENU_ORDER);
        values.put("menu_id", MenuId);
        values.put("order_menu_id", OrderMenuId);
        values.put("order_id",OrderId);
        if(!delete_pending.equals("true"))
        {
            values.put("comment",edt_comment.getText().toString().trim());
        }
        else
        {
            values.put("comment","");
        }

        //values.put("order_id",String.valueOf(order_number+1));
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_type", AppSession.getInstance().getServiceType());

        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            values.put("no_of_pax", sharedpreferencesData.getString("dinein_guest_count",""));
        }
        else if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            values.put("no_of_pax", "1");
        }
        else if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            values.put("no_of_pax", "1");
        }
        values.put("senior_citizen", sharedpreferencesData.getString("dinein_edt_senior_guest",""));
        values.put("disabled", sharedpreferencesData.getString("dinein_edt_disabled",""));
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_DELETE, "POST");
    }

    private void superviser_login() {
        String URL = AppSession.getInstance().getRegUrl()+ API.LOGIN;
        HashMap<String, String> values = new HashMap<>();
        // values.put("username", edt_username.getText().toString());
        values.put("username", select);
        values.put("password", edt_password.getText().toString());
        /* values.put("action","StaffLogin");*/
        values.put("action",ACTION_LOGIN);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_LOGIN, "POST");
    }

    private void applyCoupon() {

        String URL = AppSession.getInstance().getRegUrl()+ API.APPLY_COUPON;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","verifyCoupon");*/
        values.put("action", API.ACTION_APPLY_COUPON);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("coupon", ed_coupon_code.getText().toString());
        values.put("subtotal",AppSession.getInstance().getSubTotal());
        values.put("payable_amount",AppSession.getInstance().getTax_payable_amount());
        values.put("order_id",String.valueOf(order_number));

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_COUPON, "POST");
    }
    private void OrderBill() {
        String URL = AppSession.getInstance().getRegUrl()+ API.ORDERBILLAPI;
        HashMap values = new HashMap<>();
        values.put("action", API.ACTION_ORDERBILL);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id",order_number);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_ORDERBILL, "POST");}

    private void OrderBillAgain() {
        String URL = AppSession.getInstance().getRegUrl()+ API.ORDERBILL_AGAIN_API;
        HashMap values = new HashMap<>();
        values.put("action", API.ACTION_ORDER_AGAIN_BILL);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id",order_number);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_ORDERBILL_AGAIN, "POST");
    }

    private void supervier_list() {
        String URL = AppSession.getInstance().getRegUrl()+ API.SUPERVISOR_LIST;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","verifyCoupon");*/
        values.put("action", API.ACTION_SUPERVISOR_LIST);
        values.put("location_id", AppSession.getInstance().getLocation_id());

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_SUPERVISOR_LIST, "POST");
    }
    private void loadDataTakeaway() {
        Log.d(TAG, "####################################################loadDataDelivery###########################");


        String URL = AppSession.getInstance().getRegUrl()+ API.KITCHEN_LOGIN_takeaway;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", API.ACTION_KITCHEN_LOGIN_takeaway);
        // values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        //values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_type", AppSession.getInstance().getServiceType());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_KITCHEN_LOGIN_takeaway , "POST");

    }
    private void loadDataDelivery() {
        Log.d(TAG, "####################################################loadDataDelivery###########################");

        String URL = AppSession.getInstance().getRegUrl()+ API.KITCHEN_LOGIN_takeaway;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", API.ACTION_KITCHEN_LOGIN_takeaway);
        // values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        //values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_type", AppSession.getInstance().getServiceType());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_KITCHEN_LOGIN_delivery , "POST");
    }
    private void loadDataTableCountinue() {
        String URL = AppSession.getInstance().getRegUrl()+ API.CONTINUE_TABLE_ORDER;
        /*current_page= page++;*/HashMap values = new HashMap<>();
        values.put("action", API.ACTION_CONTINUE_TABLE_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("table_id", AppSession.getInstance().getTable_ID());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CartActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_CONTINUE , "POST");
    }

    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 7; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawablee(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }
    @Override
    public void onResume() {
        super.onResume();
        simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);
        simpleProgressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {



            @Override
            public void run() {
                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                Log.d("cartItem", "onCreateView: " + mCartItems.size());

                //mItemClickListnerFromActivity.onItemSelected("cartUpdateItems", 0, mCartItems);
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                simpleProgressBar.setVisibility(View.GONE);
                tv_complete_order.setEnabled(true);
                AppSession.getInstance().setCartPlusFlag("");
            }
        }, 500);


    }

    private void bottomBarSettext() {
        mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
        Total=0.0;
        for(int i=0;i<mCartItems.size();i++)
        {
            Total=Total+mCartItems.get(i).getMenu_qty_X_menu_price();
        }

        String ft =df2.format(Total);
        tv_add_cartt.setText(ft);
    }
    @Override
    public void onSuccess(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Log.e("mytag",tag);
        if (tag.equals(FROM_CART)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                AppSession.getInstance().setUpdatedFlag_sssss("");
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                tv_send_order.setEnabled(true);
                if(menuResponse.getMenu_relations().size()>0)
                {
                    for(int i=0;i<menuResponse.getMenu_relations().size();i++)
                    {
                        DBMethodsCart.newInstance(mContext).updateCart(menuResponse.getMenu_relations().get(i).getMenu_id(),menuResponse.getMenu_relations().get(i).getOrder_menu_id(),"order_sent",menuResponse.getMenu_relations().get(i).getQuantity(),menuResponse.getOrder_id());
                    }
                }
                order_number=menuResponse.getOrder_id();
                if(AppSession.getInstance().getServiceType().equals("dine_in"))
                {
                    AppSession.getInstance().setOrderNumber(menuResponse.getOrder_id());
                }
                else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                {
                    AppSession.getInstance().setOrder_idTake_delivery(menuResponse.getOrder_id());
                }
                else if(AppSession.getInstance().getServiceType().equals("delivery"))
                {
                    AppSession.getInstance().setOrder_id_delivery(menuResponse.getOrder_id());
                }

               /* DBMethodsCart.newInstance(mContext).insertTaxItems( menuResponse.getOrder_id(),
                        "",
                        "",
                       "",
                        "",
                       ""
                       );*/
                /* AppSession.getInstance().setOrderNumber(menuResponse.getOrder_id());*/
                //Toast.makeText(mContext,menuResponse.getOrder_id()+" "+menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
               /* AppSession.getInstance().setTable_ID("");
                AppSession.getInstance().setTable_Name("");*/
                /*DBMethodsCart.newInstance(CartActivity.this).clearCart();
                DBMethodsCart.newInstance(CartActivity.this).clearModifier();*/
                if(AppSession.getInstance().getServiceType().equals("dine_in"))
                {
                    //loadData();

                    loadDataTableCountinue();
                }
                else
                if(AppSession.getInstance().getServiceType().equals("takeaway"))
                {

                    loadDataTakeaway();
                }
                else
                if(AppSession.getInstance().getServiceType().equals("delivery"))
                {

                    loadDataDelivery();
                }
            }

            else {
                tv_send_order.setEnabled(true);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_STATUS)) {


            Gson gson = new Gson();
            CheckMenuStatusResponse menuResponse = gson.fromJson(response, CheckMenuStatusResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                if(!isFinishing()&&mContext!=null)
                {
                    if(menuResponse.getOrder().getMenu_status_id().equals("13")
                            ||menuResponse.getOrder().getMenu_status_id().equals("14"))
                    {
                        supervier_list();
                        //  popup();
                    }
                    else
                    {
                       /* DBMethodsCart.newInstance(mContext).removeFromCart(menuResponse.getOrder().getMenu_id());
                        DBMethodsCart.newInstance(mContext).removeFromModifier( menuResponse.getOrder().getMenu_id());

                        mCartItems =DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                        adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        bottomBarSettext();*/
                        delete_pending="true";
                        deleteItem();
                    }

                }

                //Toast.makeText(mContext,menuResponse.getOrder().getMenu_status_id(), Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_ORDER_STATUS)) {


            Gson gson = new Gson();
            CheckMenuStatusResponse menuResponse = gson.fromJson(response, CheckMenuStatusResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                if(!isFinishing()&&mContext!=null)
                {
                    if(release_complete.equals("release")) {
                        if(menuResponse.getOrder_status().equals("13")
                                ||menuResponse.getOrder_status().equals("14"))
                        {
                            release_or_remove="release";
                            supervier_list();
                            //  popup();

                        }
                        else
                        {
                            if (!isFinishing() && mContext != null) {
                                release_or_remove="release_waiter";
                                cancel_status="0";
                                deleteOrderMenus();
                            }
                        }
                    }
                    else if(release_complete.equals("complete"))
                    {
                        release_complete="";
                        if(menuResponse.getOrder_status().equals("14")
                                ||menuResponse.getOrder_status().equals("22")
                                ||menuResponse.getOrder_status().equals("15")
                                ||!AppSession.getInstance().getServiceType().equals("dine_in"))
                        {
                            String coupon_applied =menuResponse.getCoupon_applied();
                            AppSession.getInstance().setSubTotal(menuResponse.getTax_details().getSubtotal());
                            AppSession.getInstance().setTax_discount_amount(menuResponse.getTax_details().getDiscount_amount());
                            AppSession.getInstance().setTax_payable_amount(menuResponse.getTax_details().getPayable_amount());
                            AppSession.getInstance().setTax_service_charge(menuResponse.getTax_details().getService_charge());
                            AppSession.getInstance().setTax_tax_exemption(menuResponse.getTax_details().getTax_exemption());
                            AppSession.getInstance().setTax_vat_amount(menuResponse.getTax_details().getVat_amount());
                            AppSession.getInstance().setTax_coupon_discount(menuResponse.getTax_details().getCoupon_discount());
                            AppSession.getInstance().setTax_vatable_sales(menuResponse.getTax_details().getVatable_sales());
                            AppSession.getInstance().setTax_non_vatable_sales(menuResponse.getTax_details().getNon_vatable_sales());


                            SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                            editor1.putString("GuestName", menuResponse.getTax_details().getName());
                            editor1.putString("dinein_guest_count", menuResponse.getTax_details().getNo_of_pax());
                            editor1.putString("dinein_edt_senior_guest", menuResponse.getTax_details().getSenior_citizen());
                            editor1.putString("dinein_edt_solo_guest", menuResponse.getTax_details().getSolo_parent());
                            editor1.putString("dinein_edt_disabled",  menuResponse.getTax_details().getDisabled());
                            editor1.commit();
                            // completebill();
                            // CompletebilldetailspopUp();
                            if (!/*AppSession.getInstance().getOrderNumber()*/order_number.equals("")) {
                                Intent i=new Intent(CartActivity.this,CompletePopupActivity.class);
                                i.putExtra("order_number",order_number);
                                i.putExtra("coupon_applied",coupon_applied);
                                i.putExtra("settotal",tv_add_cartt.getText().toString());
                                //startActivity(i);
                                startActivityForResult(i, COMPLETED_POPUP);
                            }
                            else
                            {
                                tv_release_table.setEnabled(true);
                                Toast.makeText(mContext,getResources().getString(R.string.sent_order_first), Toast.LENGTH_SHORT).show();

                            }

                        }
                        else
                        {
                            if (!isFinishing() && mContext != null) {
                                tv_release_table.setEnabled(true);
                                Toast.makeText(getApplicationContext(),menuResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }


                }

                //Toast.makeText(mContext,menuResponse.getOrder().getMenu_status_id(), Toast.LENGTH_SHORT).show();
            }

            else {
                tv_release_table.setEnabled(true);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_LOGIN)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {

               /* AppSession.getInstance().setisLoginStatus(true);
                AppSession.getInstance().setStaff_ID(loginResponse.getStaffInfo().getStaffId().toString());
                AppSession.getInstance().setStaff_Name(loginResponse.getStaffInfo().getName().toString());
                AppSession.getInstance().setStaff_Email(loginResponse.getStaffInfo().getEmail().toString());
                AppSession.getInstance().setUser_Type(loginResponse.getStaffInfo().getUserType().toString());
                AppSession.getInstance().setLocation_id(loginResponse.getStaffInfo().getLocation().toString());*/

                // Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.comment));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);
            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_DELETE)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                release_or_remove="";
                simpleProgressBar.setVisibility(View.GONE);
                AppSession.getInstance().setMenuid(MenuId);
                AppSession.getInstance().setMenuid(OrderMenuId);
                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems();
                if(delete_pending.equals("true"))
                {
                    DBMethodsCart.newInstance(mContext).removeFromCartByOrderMenuId(OrderMenuId);
                    DBMethodsCart.newInstance(mContext).removeFromModifierByOrderMenuId(MenuId);

                    mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                    if(mCartItems!=null&&mCartItems.size()>0)
                    {
                        adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                    else
                    {
                        if(AppSession.getInstance().getServiceType().equals("dine_in"))
                        {
                            release_or_remove="no_item";
                            cancel_status="1";
                            deleteOrderMenus();
                            //dine_in();
                        }
                        else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                        {
                            take_away();
                        }
                        else   if(AppSession.getInstance().getServiceType().equals("delivery"))
                        {
                            delivery();
                        }
                    }


                }
                else
                {
                    DBMethodsCart.newInstance(mContext).removeFromCartByOrderMenuId(OrderMenuId);
                    DBMethodsCart.newInstance(mContext).removeFromModifierByOrderMenuId(MenuId);
                    mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                    if(mCartItems!=null&&mCartItems.size()>0)
                    {
                        alertDialog.hide();
                        Intent i=new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                        AppSession.getInstance().setCancelcart("123");
                        startActivity(i);
                    }
                    else
                    {

                        if(AppSession.getInstance().getServiceType().equals("dine_in")){
                            release_or_remove="no_item";
                            cancel_status="1";
                            deleteOrderMenus();
                        }
                        else{
                            alertDialog.hide();
                        }
                    }



                }

                /*DBMethodsCart.newInstance(mContext).removeFromCart(MenuId);
                DBMethodsCart.newInstance(mContext).removeFromModifier(MenuId);*/

               /* adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();*/
                bottomBarSettext();
                /*finish();
                startActivity(getIntent());*/


            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_COUPON)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                Toast.makeText(mContext,menuResponse.getMessage()+menuResponse.getPrev_subtotal(), Toast.LENGTH_SHORT).show();
                // tv_subtotal.setText(menuResponse.getPrev_subtotal());
                ll_coupon_disc.setVisibility(View.VISIBLE);
                tv_coupon_disc.setText(menuResponse.getDis_amount());
                tv_payable_amount.setText(menuResponse.getPayable_amount());
                AppSession.getInstance().setSubTotal(menuResponse.getSubtotal());
                AppSession.getInstance().setTax_payable_amount(menuResponse.getPayable_amount());
               /* DecimalFormat df2 = new DecimalFormat("###.###");
                double f = Double.parseDouble(menuResponse.getSubtotal().toString());
                String ft =df2.format(f);
                Toast.makeText(mContext,ft, Toast.LENGTH_SHORT).show();*/
                AppSession.getInstance().setApplyCoupon("true");
                AppSession.getInstance().setOrderTotal(menuResponse.getPayable_amount());
                AppSession.getInstance().settotal(menuResponse.getPrev_subtotal());
                AppSession.getInstance().setCouponTotal(menuResponse.getDis_amount());
                AppSession.getInstance().setCouponper(menuResponse.getDis_percent());
                //  Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_RELEASE)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                release_or_remove="";
                // alertDialog.hide();
                tv_complete_order.setEnabled(true);

            }

            else {
                tv_complete_order.setEnabled(true);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_COMPLETE_ORDER)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                simpleProgressBar.setVisibility(View.GONE);
                if(!isFinishing()&&mContext!=null)
                {
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                    editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                    editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                    editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                    editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                    editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                    editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                    editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                    editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                    editor1.putString("vat", menuResponse.getOrder_details().getVat());
                    editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                    editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());

                    editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                    editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                    editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                    editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                    editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                    editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                    editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());
                    editor1.putString("payment_type",""/*menuResponse.getOrder_details().getPayment_type()*/);
                    editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                    editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                    editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                    editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                    editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                    editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                    editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                    editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                    editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                    editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                    editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                    editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                    editor1.putString("location", menuResponse.getOrder_details().getLocation());
                    editor1.putString("area", menuResponse.getOrder_details().getArea());
                    editor1.putString("city", menuResponse.getOrder_details().getCity());

                    editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                    editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());

                    editor1.putString("card_name", menuResponse.getOrder_details().getCard_name());
                    editor1.putString("card_number", menuResponse.getOrder_details().getCard_number());
                    editor1.putString("card_type", menuResponse.getOrder_details().getCard_type());

                    editor1.commit();


                    if(AppSession.getInstance().getServiceType().equals("dine_in")){
                        /* releaseTable2();*/
                        releaseTable();}else{
                        alertDialog.hide();
                    }


                    Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                    AppSession.getInstance().setcompletecart("125");
                    startActivity(i);



                }
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_DELETE_MENU_ORDERS)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                if(release_or_remove.equals("release"))
                {
                    alertDialog.hide();
                }
                if(release_or_remove.equals("no_item")&&!delete_pending.equals("true"))
                {
                    alertDialog.hide();
                }
                else
                {
                    simpleProgressBar.setVisibility(View.GONE);
                }
                release_or_remove="";
                if(delete_pending.equals("true"))
                {
                    dine_in();
                }
                else
                {
                    Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                    AppSession.getInstance().setCancelcart("124");
                    startActivity(i);
                }
               /* AppSession.getInstance().setTable_ID("");
                AppSession.getInstance().setTable_Name("");
                DBMethodsCart.newInstance(CartActivity.this).clearCart();
                DBMethodsCart.newInstance(CartActivity.this).clearModifier();*/
                /*AppSession.getInstance().settotal(tv_add_cartt.getText().toString());*/

                //  Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_ORDERBILL)) {
            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
//                AppSession.getInstance().setInvoice_No(menuResponse.getInv_number());
//                AppSession.getInstance().setOrder_status("22");
//                AppSession.getInstance().setOrderNumber(order_number);
//                Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
//                AppSession.getInstance().setcompletecart("123");startActivity(i);
//                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);
//                checkcompletebipass();
                tv_release_table.setEnabled(true);
                AppSession.getInstance().setInvoice_No(menuResponse.getOrder_details().getInvoice_no());
                AppSession.getInstance().setOrder_status("22");
                AppSession.getInstance().setOrderNumber(order_number);

                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                editor1.putString("sale_no", menuResponse.getOrder_details().getSale_no());

                editor1.putString("extra_item", menuResponse.getOrder_details().getExtra_item());
                editor1.putString("other_extra", menuResponse.getOrder_details().getOther_extra());


                editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                AppSession.getInstance().setOrderTotal(menuResponse.getOrder_details().getFinal_total());

                editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                editor1.putString("vat", menuResponse.getOrder_details().getVat());
                editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());

                editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());
                editor1.putString("payment_type",""/*menuResponse.getOrder_details().getPayment_type()*/);
                editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                editor1.putString("location", menuResponse.getOrder_details().getLocation());
                editor1.putString("area", menuResponse.getOrder_details().getArea());
                editor1.putString("city", menuResponse.getOrder_details().getCity());

                editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());


                editor1.commit();

                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);

                Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                AppSession.getInstance().setcompletecart("123");startActivity(i);
                /* checkcompletebipass();*/

            }
            else
            {
                tv_release_table.setEnabled(true);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
               // AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);

                /*  checkcompletebipass();*/
            }
        }
        if (tag.equals(FROM_ORDERBILL_AGAIN)) {
            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                AppSession.getInstance().setInvoice_No(menuResponse.getOrder_details().getInvoice_no());
                AppSession.getInstance().setOrder_status("22");
                AppSession.getInstance().setOrderNumber(order_number);

                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                editor1.putString("sale_no", menuResponse.getOrder_details().getSale_no());
                editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                editor1.putString("vat", menuResponse.getOrder_details().getVat());
                editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());
                editor1.putString("payment_type", ""/*menuResponse.getOrder_details().getPayment_type()*/);
                editor1.putString("card_type", menuResponse.getOrder_details().getCard_type());

                editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());
                editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                editor1.putString("location", menuResponse.getOrder_details().getLocation());
                editor1.putString("area", menuResponse.getOrder_details().getArea());
                editor1.putString("city", menuResponse.getOrder_details().getCity());

                editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());

                editor1.putString("table_name", menuResponse.getOrder_details().getTable_info().getTable_name());
                editor1.putString("table_id", menuResponse.getOrder_details().getTable_info().getTable_id());

                if(menuResponse.getOrder_details().getPayment_type()!=null
                        &&menuResponse.getOrder_details().getPayment_type().size()>0)
                {
                    for(int k=0;k<menuResponse.getOrder_details().getPayment_type().size();k++)
                    {
                        map5_payment = new HashMap<String, String>();
                        map5_payment.put("payment", menuResponse.getOrder_details().getPayment_type().get(k).getPayment());
                        map5_payment.put("amount", menuResponse.getOrder_details().getPayment_type().get(k).getAmount());
                        map5_payment.put("card_type", menuResponse.getOrder_details().getPayment_type().get(k).getCard_type());

                        categoryList_payment.add(map5_payment);
                    }
                }

                saveArrayList(categoryList_payment,"Payment_types");
                editor1.commit();
                categoryList_payment.clear();
                categoryList_payment= getArrayList("Payment_types");
                if (categoryList_payment != null && categoryList_payment.size() > 0) {
                    for (int i = 0; i < categoryList_payment.size(); i++) {
                        String paymentdetailss = "";
                        paymentdetailss="Payment Type: "+categoryList_payment.get(i).get("payment")+ "         " + "\n" +
                                "Payment Recieved : " + categoryList_payment.get(i).get("amount") + "        " + "\n" +
                                "Card Type : " + categoryList_payment.get(i).get("card_type") + "        " + "\n";
                        Toast.makeText(mContext,paymentdetailss, Toast.LENGTH_SHORT).show();

                    }
                }
                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);

                Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                AppSession.getInstance().setcompletecart("123");startActivity(i);
                /* checkcompletebipass();*/
            }
            else
            {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_SUPERVISOR_LIST)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {
                SupervisorListArray.clear();
                for(int i=0;i<loginResponse.getData().size();i++)
                {
                    SupervisorListArray.add(loginResponse.getData().get(i).getUsername());

                }
                if(!isFinishing()&&mContext!=null)
                {
                    popup();
                }

            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_KITCHEN_LOGIN_takeaway)) {

            Gson gson = new Gson();
            TableContinueResponse tableResponse2 = gson.fromJson(response, TableContinueResponse.class);
            if (tableResponse2.getStatus().equals("success")) {
                simpleProgressBar.setVisibility(View.GONE);
                mContinueLists_details = tableResponse2.getData();
                if(mContinueLists_details.size()==0)
                {


                }
                else
                {
                    DBMethodsCart.newInstance(mContext).clearCart();
                    DBMethodsCart.newInstance(mContext).clearModifier();
                    int i=0;
                    if(AppSession.getInstance().getContinue_position()==5000)
                    {
                        i=mContinueLists_details.size()-1;

                    }
                    else
                    {
                        i=AppSession.getInstance().getContinue_position();

                    }

                    for (int j = 0; j < mContinueLists_details.get(i).getItems().size(); j++) {
                        // DecimalFormat df2 = new DecimalFormat("###.###");
                        AppSession.getInstance().setOrderTotal(mContinueLists_details.get(i).getOrder_total().toString());

                        double f = Double.parseDouble(mContinueLists_details.get(i).getItems().get(j).getPrice().toString());
                        // String ft =df2.format(f);
                        /*  String ft = String.format("%.3f", f);*/
                        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                        String order_m_id = mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id();
                        if (order_m_id == null || order_m_id.equals("")) {
                            order_m_id = mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString();
                        }
                        DBMethodsCart.newInstance(mContext).insertCartItems(
                                mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString(),
                                mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id().toString(),
                                "",
                                mContinueLists_details.get(i).getItems().get(j).getName().toString(),
                                mContinueLists_details.get(i).getItems().get(j).getName2().toString(),
                                mContinueLists_details.get(i).getOrder_id().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                ft,
                                1,
                                Integer.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString()),
                                Integer.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString()),
                                (Double.valueOf(ft) *
                                        Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString())),
                                "2",
                                mContinueLists_details.get(i).getItems().get(j).getComment().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "order_sent",
                                Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getDiscount_value().toString()),
                                Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getDiscount().toString()),
                                mContinueLists_details.get(i).getItems().get(j).getDiscount_comment().toString());


                        mContinueLists2 = mContinueLists_details.get(i).getItems().get(j).getOptionValues();
                        if (mContinueLists2.size() > 0 && mContinueLists2 != null) {
                            for (int k = 0; k < mContinueLists2.size(); k++) {
                                DBMethodsCart.newInstance(mContext).insertModifierItems(mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id().toString(),
                                        mContinueLists2.get(k).getMenu_option_value_id(),
                                        mContinueLists2.get(k).getOption_value_id(),
                                        mContinueLists2.get(k).getOption_id(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists2.get(k).getDisplay_type(),
                                        mContinueLists2.get(k).getValue(),
                                        mContinueLists2.get(k).getNew_price(),
                                        mContinueLists2.get(k).getPrice());
                            }
                        }

                    }

                }
                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                bottomBarSettext();

            }

            else {
                Toast.makeText(mContext,tableResponse2.getMessage(), Toast.LENGTH_SHORT).show();

            }

        }
        if (tag.equals(FROM_KITCHEN_LOGIN_delivery)) {

            Gson gson = new Gson();
            TableContinueResponse tableResponse2 = gson.fromJson(response, TableContinueResponse.class);
            if (tableResponse2.getStatus().equals("success")) {
                simpleProgressBar.setVisibility(View.GONE);

                mContinueLists_details = tableResponse2.getData();
                if(mContinueLists_details.size()==0)
                {

                }
                else {
                    DBMethodsCart.newInstance(mContext).clearCart();
                    DBMethodsCart.newInstance(mContext).clearModifier();
                    int i=0;
                    if(AppSession.getInstance().getContinue_position()==5000)
                    {
                        i=mContinueLists_details.size()-1;

                    }
                    else
                    {
                        i=AppSession.getInstance().getContinue_position();

                    }


                    for (int j = 0; j < mContinueLists_details.get(i).getItems().size(); j++) {
                        AppSession.getInstance().setOrderTotal(mContinueLists_details.get(i).getOrder_total().toString());

                        // DecimalFormat df2 = new DecimalFormat("###.###");
                        double f = Double.parseDouble(mContinueLists_details.get(i).getItems().get(j).getPrice().toString());
                        // String ft =df2.format(f);
                        /*String ft = String.format("%.3f", f);*/
                        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                        String order_m_id = mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id();
                        if (order_m_id == null || order_m_id.equals("")) {
                            order_m_id = mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString();
                        }
                        DBMethodsCart.newInstance(mContext).insertCartItems(
                                mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString(),
                                mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id().toString(),
                                "",
                                mContinueLists_details.get(i).getItems().get(j).getName().toString(),
                                mContinueLists_details.get(i).getItems().get(j).getName2().toString(),
                                mContinueLists_details.get(i).getOrder_id().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                ft,
                                1,
                                Integer.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString()),
                                Integer.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString()),
                                (Double.valueOf(ft) *
                                        Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getQuantity().toString())),
                                "2",
                                mContinueLists_details.get(i).getItems().get(j).getComment().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "order_sent",
                                Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getDiscount_value().toString()),
                                Double.valueOf(mContinueLists_details.get(i).getItems().get(j).getDiscount().toString()),
                                mContinueLists_details.get(i).getItems().get(j).getDiscount_comment().toString());


                        mContinueLists2 = mContinueLists_details.get(i).getItems().get(j).getOptionValues();
                        if (mContinueLists2.size() > 0 && mContinueLists2 != null) {
                            for (int k = 0; k < mContinueLists2.size(); k++) {
                                DBMethodsCart.newInstance(mContext).insertModifierItems(mContinueLists_details.get(i).getItems().get(j).getMenu_id().toString(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists_details.get(i).getItems().get(j).getOrder_menu_id().toString(),
                                        mContinueLists2.get(k).getMenu_option_value_id(),
                                        mContinueLists2.get(k).getOption_value_id(),
                                        mContinueLists2.get(k).getOption_id(),
                                        mContinueLists2.get(k).getOption_name(),
                                        mContinueLists2.get(k).getDisplay_type(),
                                        mContinueLists2.get(k).getValue(),
                                        mContinueLists2.get(k).getNew_price(),
                                        mContinueLists2.get(k).getPrice());
                            }
                        }

                    }

                }

                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                bottomBarSettext();


            }

            else {
                Toast.makeText(mContext,tableResponse2.getMessage(), Toast.LENGTH_SHORT).show();

            }

        }
        if (tag.equals(FROM_CONTINUE)) {

            Gson gson = new Gson();
            TableContinueResponse tableResponse = gson.fromJson(response, TableContinueResponse.class);
            if (tableResponse.getStatus().equals("success")) {
                DBMethodsCart.newInstance(mContext).clearCart();
                DBMethodsCart.newInstance(mContext).clearModifier();
                mContinueLists_details = tableResponse.getData();
                for (int i=0;i<mContinueLists_details.size();i++)
                {
                    AppSession.getInstance().setOrderTotal(mContinueLists_details.get(i).getOrder_total().toString());
                    AppSession.getInstance().setOrderNumber(mContinueLists_details.get(i).getOrder_id().toString());
                    for(int j=0;j<mContinueLists_details.get(i).getCart().size();j++)
                    {
                        // DecimalFormat df2 = new DecimalFormat("###.###");
                        double f = Double.parseDouble( mContinueLists_details.get(i).getCart().get(j).getPrice().toString());
                        String order_m_id= mContinueLists_details.get(i).getCart().get(j).getOrder_menu_id().toString();
                        if(order_m_id==null||order_m_id.equals(""))
                        {
                            order_m_id= mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString();
                        }
                        // String ft =df2.format(f);
                        /*  String ft = String.format("%.3f", f);*/
                        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                        DBMethodsCart.newInstance(mContext).insertCartItems(
                                mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString(),
                                order_m_id,
                                "",
                                mContinueLists_details.get(i).getCart().get(j).getName().toString(),
                                mContinueLists_details.get(i).getCart().get(j).getName2().toString(),
                                mContinueLists_details.get(i).getOrder_id().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                ft,
                                1,
                                Integer.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString()),
                                Integer.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString()),
                                (Double.valueOf(ft)*
                                        Double.valueOf(mContinueLists_details.get(i).getCart().get(j).getQuantity().toString())),
                                "2",
                                mContinueLists_details.get(i).getCart().get(j).getComment().toString(),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "order_sent",
                                Double.valueOf( mContinueLists_details.get(i).getCart().get(j).getDiscount_value().toString()),
                                Double.valueOf( mContinueLists_details.get(i).getCart().get(j).getDiscount().toString()),
                                mContinueLists_details.get(i).getCart().get(j).getDiscount_comment().toString());


                        mContinueLists2=mContinueLists_details.get(i).getCart().get(j).getOptionValues();
                        if(mContinueLists2.size()>0 && mContinueLists2 !=null)
                        { for(int k =0;k<mContinueLists2.size();k++)
                        {
                            DBMethodsCart.newInstance(mContext).insertModifierItems( mContinueLists_details.get(i).getCart().get(j).getMenu_id().toString(),
                                    mContinueLists2.get(k).getOption_name(),
                                    mContinueLists_details.get(i).getCart().get(j).getOrder_menu_id().toString(),
                                    mContinueLists2.get(k).getMenu_option_value_id(),
                                    mContinueLists2.get(k).getOption_value_id(),
                                    mContinueLists2.get(k).getOption_id(),
                                    mContinueLists2.get(k).getOption_name(),
                                    mContinueLists2.get(k).getDisplay_type(),
                                    mContinueLists2.get(k).getValue(),
                                    mContinueLists2.get(k).getNew_price(),
                                    mContinueLists2.get(k).getPrice()); } }
                    }


                }

                mCartItems = DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                bottomBarSettext();

            }

            else {
                Toast.makeText(mContext,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }

        }
        if (tag.equals(FROM_PAYMENT_TYPES)) {

            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {

                PaymentTypelist.clear();
                if(menuResponse.getPayment_types().length>0)
                {
                    for(int i=0;i<menuResponse.getPayment_types().length;i++)
                    {
                        PaymentTypelist.add(menuResponse.getPayment_types()[i]);
                    }
                }
                completebill();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (tag.equals(FROM_CART)) {
            tv_send_order.setEnabled(true);
        }
        tv_release_table.setEnabled(true);
        tv_complete_order.setEnabled(true);

    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
        if (tag.equals(FROM_CART)) {
            tv_send_order.setEnabled(true);
        }
        tv_release_table.setEnabled(true);
        tv_complete_order.setEnabled(true);
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();

        if (tag.equals(FROM_CART)) {
            tv_send_order.setEnabled(true);
        }
        tv_release_table.setEnabled(true);
        tv_complete_order.setEnabled(true);
    }
    /* @Override public void onStart() {
         super.onStart();
         registerReceiver();
     }*/
    @Override
    protected void onStop() {
        super.onStop();
        /*
         * Step 4: Ensure to unregister the receiver when the activity is destroyed so that
         * you don't face any memory leak issues in the app
         */
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
    private void registerReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(AppSession.getInstance().getUser_Type().equals("WAITER")){
                    String otpCode = intent.getStringExtra(NOTI_ARRAY);
              /*  final ArrayList<HashMap<String, String>> map1 =(ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
                for (int i=0;i<map1.size();i++) {
                  String  id = map1.get(i).get(SplashScreenActivity.LOCAION_ID);
                    String location_name = map1.get(i).get(SplashScreenActivity.LOCAION_NAME);
                    *//* Build the StringWithTag List using these keys and values. *//*
                    Toast.makeText(NavigationMainActivity.this, id+" "+location_name, Toast.LENGTH_SHORT).show();
                }*/


                    vibrate();
                    JSONArray jArray = null;
                    try {
                        jArray = new JSONArray(otpCode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (jArray.length()==0)
                    {

                    }
                    else
                    {
                        categoryList.clear();
                        for(int i=0;i<jArray.length();i++){

                            JSONObject json_data = null;
                            try {
                                json_data = jArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String text1 = null;
                            String table_id = null;
                            String staff_id = null;
                            String menu_id = null;
                            String order_id = null;
                            String status = null;
                            String created_at = null;
                            String updated_at = null;
                            String table_name = null;
                            String order_type = null;
                            try {
                                text1 = json_data.getString("text");
                                table_id = json_data.getString("id");
                                order_type = json_data.getString("order_type");
                                table_name = json_data.getString("table_name");
                                staff_id = json_data.getString("staff_id");
                                menu_id = json_data.getString("menu_id");
                                order_id = json_data.getString("order_id");
                                status = json_data.getString("status");
                                created_at = json_data.getString("created_at");
                                updated_at = json_data.getString("updated_at");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            map1 = new HashMap<String, String>();

                            map1.put(NOTI_TABLE_ID, table_id);
                            map1.put(NOTI_TABLE_NAME, table_name);
                            map1.put(NOTI_TYPE, order_type);
                            map1.put(NOTI_TEXT, text1);
                            map1.put(NOTI_STAFF_ID, staff_id);
                            map1.put(NOTI_MENU_ID, menu_id);
                            map1.put(NOTI_ORDER_ID, order_id);
                            map1.put(NOTI_STATUS, status);
                            map1.put(NOTI_CREATED_AT, created_at);
                            map1.put(NOTI_UPDATED_AT, updated_at);
                            categoryList.add(map1);


                        }

                    }
                    /*
                     * Step 3: We can update the UI of the activity here
                     * */


                    listview2.setVisibility(View.VISIBLE);
                    adapter2 = new NotificationAdapter(CartActivity.this,categoryList);
                    listview2.setAdapter(adapter2);
                    adapter2.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {



                        @Override
                        public void run() {

                            listview2.setVisibility(View.GONE);

                        }
                    }, SPLASH_TIME_OUT);

                }else{

                }

            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("notification_update_UI"));

    }

    private void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void CompletebilldetailspopUp() {

        if (!/*AppSession.getInstance().getOrderNumber()*/order_number.equals("")) {

            /*dialogBuilder = new AlertDialog.Builder(CartActivity.this, R.style.NewDialog);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View dialogView = inflater.inflate(R.layout.order_complete_popup, null);
            tv_subtotal = (TextView) dialogView.findViewById(R.id.tv_subtotal);
            ed_coupon_code = (EditText) dialogView.findViewById(R.id.ed_coupon_code);
            spinner = (Spinner) dialogView.findViewById(R.id.spinner);
            img_success = (ImageView) dialogView.findViewById(R.id.img_success);
            ll_apply_coupon = (LinearLayout) dialogView.findViewById(R.id.ll_apply_coupon);
            tv_subtotal.setText(tv_add_cartt.getText().toString());
            AppSession.getInstance().settotal(tv_add_cartt.getText().toString());
            img_success.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isFinishing()&&mContext!=null)
                    {
                        *//*completeOrder();*//*
                        Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                        AppSession.getInstance().setcompletecart("123");
                        startActivity(i);
                    }

                        *//*Intent i=new Intent(CartActivity.this,Main_Activity.class);
                        startActivity(i);*//*
                    alertDialog.hide();
                }
            });

            ll_apply_coupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isFinishing()&&mContext!=null)
                    {
                        applyCoupon();
                    }

    }


            });}*/
            dialogBuilder2 = new AlertDialog.Builder(CartActivity.this, R.style.NewDialog);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View dialogView = inflater.inflate(R.layout.order_complete_popup, null);
            tv_vat_amount = (TextView) dialogView.findViewById(R.id.tv_vat_amount);
            tv_price_term = (TextView) dialogView.findViewById(R.id.tv_price_term);
            tv_vatable_sale = (TextView) dialogView.findViewById(R.id.tv_vatable_amount);
            tv_nonvatable_sale = (TextView) dialogView.findViewById(R.id.tv_nonvatable_amount);
            tv_service_tax= (TextView) dialogView.findViewById(R.id.tv_service_tax);
            tv_discount_amount = (TextView) dialogView.findViewById(R.id.tv_discount_amount);
            tv_payable_amount = (TextView) dialogView.findViewById(R.id.tv_payable_amount);
            tv_order_id = (TextView) dialogView.findViewById(R.id.tv_order_id);
            tv_guestname = (TextView) dialogView.findViewById(R.id.tv_guestname);
            tv_guestcount = (TextView) dialogView.findViewById(R.id.tv_guestcount);
            tv_senior_count = (TextView) dialogView.findViewById(R.id.tv_senior_count);
            tv_solo_count = (TextView) dialogView.findViewById(R.id.tv_solo_count);
            tv_disabled_count = (TextView) dialogView.findViewById(R.id.tv_disabled_count);
            tv_coupon_disc= (TextView) dialogView.findViewById(R.id.tv_coupon_disc);
            tv_tax_subtotal = (TextView) dialogView.findViewById(R.id.tv_tax__subtotal);
            ed_coupon_code = (EditText) dialogView.findViewById(R.id.ed_coupon_code);

            img_success = (ImageView) dialogView.findViewById(R.id.img_success);
            ll_apply_coupon = (LinearLayout) dialogView.findViewById(R.id.ll_apply_coupon);
            ll_coupon_disc= (LinearLayout) dialogView.findViewById(R.id.ll_coupon_disc);
            ll_solo_parent= (LinearLayout) dialogView.findViewById(R.id.ll_solo_parent);
            Float taxforSubtotal=Float.parseFloat(AppSession.getInstance().getSubTotal())*Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())/100;
            Float taxedSubtotal=taxforSubtotal+Float.parseFloat(AppSession.getInstance().getSubTotal());
            tv_tax_subtotal.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", taxedSubtotal));
            Float serviceTax=Float.parseFloat(AppSession.getInstance().getSubTotal())*Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2())/100;
            tv_service_tax.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", serviceTax));
            tv_discount_amount.setText(AppSession.getInstance().getTax_discount_amount());
            tv_order_id.setText(order_number);

            SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
            editor1.putString("Coupon_code", ed_coupon_code.getText().toString().trim());
            editor1.commit();

            String GuestName=  sharedpreferencesData.getString("GuestName","");
            String no_of_pax=  sharedpreferencesData.getString("dinein_guest_count","");
            String senior= sharedpreferencesData.getString("dinein_edt_senior_guest","");
            String solo=  sharedpreferencesData.getString("dinein_edt_solo_guest","");
            String disabled= sharedpreferencesData.getString("dinein_edt_disabled","");
            Float vatableSales=Float.parseFloat(AppSession.getInstance().getSubTotal())*(Float.parseFloat(no_of_pax)-(Float.parseFloat(senior)+Float.parseFloat(disabled)))/Float.parseFloat(no_of_pax);
            Float nonvatableSales= Float.parseFloat(AppSession.getInstance().getSubTotal())-vatableSales;

            tv_vatable_sale.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f",vatableSales));
            tv_nonvatable_sale.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", nonvatableSales));
            Float discount_amount=Float.parseFloat(AppSession.getInstance().getRestraunt_seniorDiscount())*nonvatableSales/100;
            tv_discount_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", discount_amount));
            Float vatAmount=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())*vatableSales/100;
            tv_vat_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", vatAmount));
            Float payableTotal=Float.parseFloat(AppSession.getInstance().getSubTotal())-discount_amount+vatAmount+serviceTax;
            tv_payable_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", payableTotal));
            tv_guestname.setText(GuestName);
            tv_guestcount.setText(no_of_pax);
            tv_senior_count.setText(senior);
            tv_disabled_count.setText(disabled);
            tv_solo_count.setText(solo);
            tv_price_term.setText(AppSession.getInstance().getRestraunt_CurrencySymbol());
         /*   if(AppSession.getInstance().getServiceType().equals("dine_in"))
            {
                tv_guestname.setText(tv_dine_name.getText().toString());
                tv_guestcount.setText(tv_dinein_guest_count.getText().toString());
                tv_senior_count.setText(tv_dinein_edt_senior_guest.getText().toString());
                tv_disabled_count.setText(tv_dinein_edt_disabled.getText().toString());
                tv_solo_count.setText(tv_dinein_edt_solo_guest.getText().toString());

                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

                editor1.putString("GuestName", tv_dine_name.getText().toString().trim());
                editor1.putString("dinein_guest_count", tv_dinein_guest_count.getText().toString().trim());
                editor1.putString("dinein_edt_senior_guest", tv_dinein_edt_senior_guest.getText().toString());
                editor1.putString("dinein_edt_solo_guest", tv_dinein_edt_solo_guest.getText().toString());
                editor1.putString("dinein_edt_disabled", tv_dinein_edt_disabled.getText().toString());
                editor1.commit();

            }
            else if(AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                tv_guestname.setText(tv_take_name.getText().toString());

            }
            else if(AppSession.getInstance().getServiceType().equals("delivery"))
            {
                tv_guestname.setText(tv_del_name.getText().toString());

            }*/

            AppSession.getInstance().settotal(tv_add_cartt.getText().toString());
            img_success.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isFinishing()&&mContext!=null)
                    {
                       /* Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
                        AppSession.getInstance().setcompletecart("123");
                        startActivity(i);*/
                        OrderBill();

                    }

                        /*Intent i=new Intent(CartActivity.this,Main_Activity.class);
                        startActivity(i);*/
                    alertDialog.hide();
                }
            });

            ll_apply_coupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isFinishing()&&mContext!=null)
                    {
                        applyCoupon();
                    }

                }
            });
















            dialogBuilder2.setView(dialogView);
            alertDialog = dialogBuilder2.create();



            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
            if(mContext!=null)
            {
                alertDialog.show();
            }




            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog,
                                     int keyCode, android.view.KeyEvent event) {
                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        // To dismiss the fragment when the back-button is pressed.
                        alertDialog.hide();
                        return true;
                    }


                    // Otherwise, do nothing else
                    else return false;
                }
            });

            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
                @Override
                public void onCancel(DialogInterface dialog){


                    //***cleanup code***
                }});


        }
        else
        {
            Toast.makeText(mContext,getResources().getString(R.string.sent_order_first), Toast.LENGTH_SHORT).show();

        }
    }





    private void completebill(){

        if (!/*AppSession.getInstance().getOrderNumber()*/order_number.equals("")) {

            dialogBuilder3 = new AlertDialog.Builder(CartActivity.this, R.style.NewDialog);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View dialogView = inflater.inflate(R.layout.printbilldetailspopup, null);
            // tv_subtotal = (TextView) dialogView.findViewById(R.id.tv_subtotal);
            // ed_coupon_code = (EditText) dialogView.findViewById(R.id.ed_coupon_code);
            // spinner = (Spinner) dialogView.findViewById(R.id.spinner);
            img_success = (ImageView) dialogView.findViewById(R.id.img_success);
            img_print= (ImageView) dialogView.findViewById(R.id.img_print);
            spinner = (Spinner) dialogView.findViewById(R.id.spinner);
            spinner_card = (Spinner) dialogView.findViewById(R.id.spinner_card);

            ll_paid_by = (LinearLayout) dialogView.findViewById(R.id.ll_paid_by);
            ll_card_type = (LinearLayout) dialogView.findViewById(R.id.ll_card_type);
            ll_tender_cash = (LinearLayout) dialogView.findViewById(R.id.ll_tender_cash);
            ll_tender_change = (LinearLayout) dialogView.findViewById(R.id.ll_tender_change);
            ll_id_proof = (LinearLayout) dialogView.findViewById(R.id.ll_id_proof);

            ll_card_number = (LinearLayout) dialogView.findViewById(R.id.ll_card_number);
            ll_card_holder = (LinearLayout) dialogView.findViewById(R.id.ll_card_holder);

            ed_card_number= (EditText) dialogView.findViewById(R.id.ed_card_number);
            ed_card_holder= (EditText) dialogView.findViewById(R.id.ed_card_holder);
            ed_tender_cash= (EditText) dialogView.findViewById(R.id.ed_tender_cash);
            ed_id_proof= (EditText) dialogView.findViewById(R.id.ed_id_proof);
            tv_tender_change= (TextView) dialogView.findViewById(R.id.tv_tender_change);
            //ll_apply_coupon = (LinearLayout) dialogView.findViewById(R.id.ll_apply_coupon);
            // tv_subtotal.setText(tv_add_cartt.getText().toString());
            //AppSession.getInstance().settotal(tv_add_cartt.getText().toString());

            dialogBuilder3.setView(dialogView);
            alertDialog = dialogBuilder3.create();
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(false);
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
            if(mContext!=null)
            {
                alertDialog.show();
            }



            ed_tender_cash.addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {

                }



                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                public void afterTextChanged(Editable s) {
                    if(s!=null&&s.length()!=0&&!s.equals("")) {
                        //do your work here
                        double a=  0.0;
                        double b=0.0;
                        if(AppSession.getInstance().getTax_payable_amount().equals("")
                                ||AppSession.getInstance().getTax_payable_amount()==null)
                        {
                            a=  0.0;

                        }
                        else
                        {
                            a=  Double.valueOf(AppSession.getInstance().getTax_payable_amount());

                        }
                        b= Double.valueOf(s.toString().trim());
                        tv_tender_change.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f",(b-a) ));

                    }
                    else
                    {
                        tv_tender_change.setText("");
                    }
                }
            });
            img_success.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  /*  if(paid_position==0)
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.select_payment_mode), Toast.LENGTH_SHORT).show();


                    }
                    else*/
                    /*  if(!paidtype.equals("CASH")&&card_position==0)*/
                    if((paidtype.equals("CARD")||paidtype.equals("BOTH"))&&card_position==0)
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.select_card_type), Toast.LENGTH_SHORT).show();

                    }
                    else
                    if((paidtype.equals("CASH")||paidtype.equals("BOTH"))&&
                            ed_tender_cash.getText().length()==0)
                    {
                        Toast.makeText(mContext,getResources().getString(R.string.valid_tender_cash), Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        if(!isFinishing()&&mContext!=null)
                        {
                            completeOrder();

                        }
                        alertDialog.hide();

                    }




                }
            });
            img_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    OrderBillAgain();
                }
            });





            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog,
                                     int keyCode, android.view.KeyEvent event) {
                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        // To dismiss the fragment when the back-button is pressed.
                        alertDialog.hide();
                        return true;
                    }


                    // Otherwise, do nothing else
                    else return false;
                }
            });

            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
                @Override
                public void onCancel(DialogInterface dialog){


                    //***cleanup code***
                }});
            final List<String> paidByType;
            if(PaymentTypelist.size()>0)
            {
                paidByType = new ArrayList<>(PaymentTypelist);
            }
            else
            {
                String[] types = getResources().getStringArray(R.array.paidBy);
                /*final List<String>*/ paidByType = new ArrayList<>(Arrays.asList(types));
            }

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    CartActivity.this, R.layout.paid_by_spinner_text, paidByType) {
                @Override
                public boolean isEnabled(int position) {
                  /*  if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {

                        return true;
                    }*/
                    return true;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                  /*  if (position == 0) {
                        // Set the hint text color gray
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }*/
                    tv.setTextColor(Color.BLACK);
                    return view;
                }
            };
            spinnerArrayAdapter.setDropDownViewResource(R.layout.paid_by_spinner_text);
            spinner.setAdapter(spinnerArrayAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                    // paidBy = (String) parent.getItemAtPosition(position);
                    // If user change the default selection
                    // First item is disable and it is used for hint
                    if (position > 0) {
                        // Notify the selected item text


                    }
                    paid_position= spinner.getSelectedItemPosition();
                    paidtype = spinner.getSelectedItem().toString();
                    if(paidtype.equals("CARD"))
                    {
                        ll_tender_cash.setVisibility(View.GONE);
                        ll_tender_change.setVisibility(View.GONE);
                        ll_card_type.setVisibility(View.VISIBLE);
                        ll_card_number.setVisibility(View.VISIBLE);
                        ll_card_holder.setVisibility(View.VISIBLE);
                    }
                    else if(paidtype.equals("CASH"))
                    {
                        ll_tender_cash.setVisibility(View.VISIBLE);
                        ll_tender_change.setVisibility(View.VISIBLE);
                        ll_card_type.setVisibility(View.GONE);
                        ll_card_number.setVisibility(View.GONE);
                        ll_card_holder.setVisibility(View.GONE);
                    }
                    else if(paidtype.equals("BOTH"))
                    {
                        ll_tender_cash.setVisibility(View.VISIBLE);
                        ll_tender_change.setVisibility(View.VISIBLE);
                        ll_card_type.setVisibility(View.VISIBLE);
                        ll_card_number.setVisibility(View.VISIBLE);
                        ll_card_holder.setVisibility(View.VISIBLE);
                    }
                    // AppSession.getInstance().setGuest_Count(spinnerValue);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            String[] cardtypes = getResources().getStringArray(R.array.cardType);
            final List<String> CardType = new ArrayList<>(Arrays.asList(cardtypes));

            // Initializing an ArrayAdapter
            final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                    CartActivity.this, R.layout.paid_by_spinner_text, CardType) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {

                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };

            spinnerArrayAdapter2.setDropDownViewResource(R.layout.paid_by_spinner_text);
            spinner_card.setAdapter(spinnerArrayAdapter2);

            spinner_card.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                    //  card_type = (String) parent.getItemAtPosition(position);
                    // If user change the default selection
                    // First item is disable and it is used for hint
                    if (position > 0) {
                        // Notify the selected item text


                    }
                    card_position= spinner_card.getSelectedItemPosition();
                    card_TType = spinner_card.getSelectedItem().toString();
                    // AppSession.getInstance().setGuest_Count(spinnerValue);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
        else
        {
            Toast.makeText(mContext,getResources().getString(R.string.sent_order_first), Toast.LENGTH_SHORT).show();

        }
    }



/*    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if(view instanceof EditText){
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }*/


    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "OnStart () &&& Starting timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "User interacting with screen");
    }


    @Override
    protected void onPause() {

        super.onPause();
        Log.e(TAG, "onPause()");
    }



    /**
     * Performing idle time logout
     */
    @Override
    public void doLogout() {
        if(AppSession.getInstance().getUser_Type().equals("WAITER")
                ||AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            logout();
        }
        else
        {

        }
        // write your stuff here
    }

    private void logout() {
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setOrder_id_delivery("");
        AppSession.getInstance().setTick("logout");
        Intent i=new Intent(CartActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }
    private void checkcompletebipass() {
        if(AppSession.getInstance().getComplteBillDetails().equals("22"+order_number))
        {

        }
        else
        {

        }
    }

    // Call Back method  to get the Message form other Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(resultCode==COMPLETED_POPUP)
        {
           /* String message=data.getStringExtra("MESSAGE");
            Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();*/
           /* Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
            AppSession.getInstance().setcompletecart("123");
            startActivity(i);*/
            OrderBill();
        }
        else if(resultCode==COMPLETED_FINAL_POPUP)
        {
            if(AppSession.getInstance().getServiceType().equals("dine_in")){
                /* releaseTable2();*/
                releaseTable();}else{

                /*  alertDialog.hide();*/
            }

            tv_complete_order.setEnabled(true);
            Intent i= new Intent(CartActivity.this,Main_ActivityPrinterLongin.class);
            AppSession.getInstance().setcompletecart("125");
            startActivity(i);
            //Toast.makeText(mContext,"completed", Toast.LENGTH_SHORT).show();
        }
        else  if(resultCode==CANCEL_POPUP)
        {
            //Toast.makeText(mContext,"back", Toast.LENGTH_SHORT).show();

        }
    }
    private void setPreference() {

        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        if(tv_dinein_edt_senior_guest.getText().length()==0)
        {
            senior="0";
        }
        else
        {
            senior=tv_dinein_edt_senior_guest.getText().toString().trim();
        }
        if(tv_dinein_edt_solo_guest.getText().length()==0)
        {
            solo="0";
        }
        else
        {
            solo=tv_dinein_edt_solo_guest.getText().toString().trim();
        }
        if(tv_dinein_edt_disabled.getText().length()==0)
        {
            disabled="0";
        }
        else
        {
            disabled=tv_dinein_edt_disabled.getText().toString().trim();
        }
        if(tv_dinein_guest_count.getText().length()==0)
        {
            pax="1";
        }
        else
        {
            pax=tv_dinein_guest_count.getText().toString().trim();
        }

        editor1.putString("dinein_guest_count", pax);
        editor1.putString("dinein_edt_senior_guest", senior);
        editor1.putString("dinein_edt_solo_guest", solo);
        editor1.putString("dinein_edt_disabled", disabled);





        editor1.commit();

        AppSession.getInstance().setGuest_Count(tv_dinein_guest_count.getText().toString().trim());
    }
    private void Checkpreference() {
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            if(tv_dinein_guest_count.getText().length()==0)
            {
                pax_valid="0";
                paxflag="false";
            }
            else
            {
                pax_valid=tv_dinein_guest_count.getText().toString();
                paxflag="true";
            }
            if(paxflag.equals("false"))
            {
                Toast.makeText(mContext,getResources().getString(R.string.valid_no_pax), Toast.LENGTH_SHORT).show();

            }
            else if(Integer.valueOf(senior)>Integer.valueOf(pax_valid))
            {
                Toast.makeText(mContext,getResources().getString(R.string.s_citizen_no_pax), Toast.LENGTH_SHORT).show();
            }
            else if(Integer.valueOf(solo)>Integer.valueOf(pax_valid))
            {
                Toast.makeText(mContext,getResources().getString(R.string.s_parent_no_pax), Toast.LENGTH_SHORT).show();
            }
            else if(Integer.valueOf(disabled)>Integer.valueOf(pax_valid))
            {
                Toast.makeText(mContext,getResources().getString(R.string.disabled_no_pax), Toast.LENGTH_SHORT).show();
            }
            else if(Integer.valueOf(disabled)+Integer.valueOf(solo)
                    +Integer.valueOf(senior)>Integer.valueOf(pax_valid))
            {
                Toast.makeText(mContext,getResources().getString(R.string.total_no_pax), Toast.LENGTH_SHORT).show();
            }
            else
            {
                addtoCart();
            }
        }


        else
        {
            addtoCart();
        }
    }
    public void saveArrayList( ArrayList<HashMap<String, String>> list, String key){
        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();

        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor1.putString(key, json);
        editor1.apply();     // This line is IMPORTANT !!!
    }
    public  ArrayList<HashMap<String, String>> getArrayList(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesData.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }
}
