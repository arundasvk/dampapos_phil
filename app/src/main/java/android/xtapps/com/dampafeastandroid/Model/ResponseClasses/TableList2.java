package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TableList2 {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("position_name")
    @Expose
    private String position_name;

    @SerializedName("info")
    @Expose
    private ArrayList<TableList3> info;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public ArrayList<TableList3> getInfo() {
        return info;
    }

    public void setInfo(ArrayList<TableList3> info) {
        this.info = info;
    }
}
