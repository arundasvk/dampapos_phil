package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xt on 23/7/18.
 */

public class TableList {

    @SerializedName("table_id")
    @Expose
    private String table_id;

    @SerializedName("table_name")
    @Expose
    private String table_name;

    @SerializedName("min_capacity")
    @Expose
    private String min_capacity;

    @SerializedName("max_capacity")
    @Expose
    private String max_capacity;

    @SerializedName("table_status")
    @Expose
    private String table_status;

    @SerializedName("table_reserved_staff")
    @Expose
    private String table_reserved_staff;

    @SerializedName("staff_name")
    @Expose
    private String staff_name;

    @SerializedName("sync_id")
    @Expose
    private String sync_id;

    private int state;

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getMin_capacity() {
        return min_capacity;
    }

    public void setMin_capacity(String min_capacity) {
        this.min_capacity = min_capacity;
    }

    public String getMax_capacity() {
        return max_capacity;
    }

    public void setMax_capacity(String max_capacity) {
        this.max_capacity = max_capacity;
    }

    public String getTable_status() {
        return table_status;
    }

    public void setTable_status(String table_status) {
        this.table_status = table_status;
    }

    public String getTable_reserved_staff() {
        return table_reserved_staff;
    }

    public void setTable_reserved_staff(String table_reserved_staff) {
        this.table_reserved_staff = table_reserved_staff;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getSync_id() {
        return sync_id;
    }

    public void setSync_id(String sync_id) {
        this.sync_id = sync_id;
    }
}
