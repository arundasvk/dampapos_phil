package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.DB.ModifierItems;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.R;

import java.text.DecimalFormat;
import java.util.List;

public class CartListAdapter extends BaseAdapter {

    private Activity activity;
    List<CartItem> data;
    private static LayoutInflater inflater=null;
    private CartListAdapter.ViewHolder holder = null;
    public int count = 1;
    Double price_value;
    String total,Modifiers,item_discount_percent="";
    Double item_total=0.00,item_discount=0.00,item_vat=1.00,item_subtotal=0.00;
    int rowid;

    ItemClickListener mItemClickListner;
    /*ArrayList<FavotateProductlistitemResponce> FavorateProductList;*/
    DecimalFormat df2;

    public CartListAdapter(Activity a,  List<CartItem>  d,ItemClickListener itemClickListner) {
        activity = a;
        data=d;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
        TextView name,product_description,tv_price,tv_qty_value,tv_price_value,tv_minus,tv_plus,order_send;
        ImageView iv_catimg,delete;
        LinearLayout ll_outer,ll_image,ll_item_name,ll_discount;
        TextView modifiers,comments,tv_per_price_value,tv_price_term,tv_vat_amount,tv_item_total_value
                ,tv_vat_value,tv_discount_value,ed_discount_percent;
       /* EditText ed_discount_percent;*/


    }

    @Override
    public View getView(final int  position, View convertView, ViewGroup parent) {



        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.activity_cart_row, null);
            holder = new CartListAdapter.ViewHolder();


            holder.name = vi.findViewById(R.id.product_name);
            holder.order_send = vi.findViewById(R.id.order_send);
            holder.product_description = vi.findViewById(R.id.product_description);
            holder.tv_price = vi.findViewById(R.id.tv_price);
            holder.tv_price_value = vi.findViewById(R.id.tv_price_value);
            holder.tv_qty_value = vi.findViewById(R.id.tv_qty_value);
            holder.tv_minus = vi.findViewById(R.id.tv_minus);
            holder.tv_plus = vi.findViewById(R.id.tv_plus);
            holder.iv_catimg = vi.findViewById(R.id.productimage);
            holder.delete = vi.findViewById(R.id.delete);
            holder.ll_item_name = vi.findViewById(R.id.ll_item_name);
            holder.ll_discount = vi.findViewById(R.id.ll_discount);

            holder.modifiers = vi.findViewById(R.id.modifiers);
            holder.comments = vi.findViewById(R.id.comments);
            holder.tv_per_price_value = vi.findViewById(R.id.tv_per_price_value);
            holder.tv_price_term = vi.findViewById(R.id.tv_price_term);
            holder.tv_vat_amount = vi.findViewById(R.id.tv_vat_amount);
            holder.tv_item_total_value = vi.findViewById(R.id.tv_item_total_value);
            holder.tv_vat_value = vi.findViewById(R.id.tv_vat_value);
            holder.tv_discount_value = vi.findViewById(R.id.tv_discount_value);

            holder.ed_discount_percent = vi.findViewById(R.id.ed_discount_percent);
            //holder.ll_image = (LinearLayout) vi.findViewById(R.id.ll_image);


          /*  holder.ed_discount_percent.addTextChangedListener(new TextWatcher() {


                @Override
                public void afterTextChanged(Editable s) {


                    // total= String.valueOf((price_value)*Integer.parseInt(String.valueOf(data.get(position).getMenu_qty())));
                    if(s.length() == 0)
                    {
                        item_discount_percent="0";
                    }
                    else
                    {
                        item_discount_percent= s.toString();
                    }



                    item_discount=(Double.valueOf(item_discount_percent)*(Double.parseDouble(data.get(position).getMenu_price())*
                            Integer.parseInt(String.valueOf(data.get(position).getMenu_qty()))))/100;
                    DBMethodsCart.newInstance(activity).updateCartDiscount(data.get(position).getOrder_menu_id(),item_discount,Double.valueOf(item_discount_percent),data.get(position).getOrder_id());
                  //  Toast.makeText(activity,data.get(position).getMenu_id()+":"+item_discount, Toast.LENGTH_SHORT).show();
                    data.get(position).setDiscount(item_discount);
                    data.get(position).setDiscount_percent(Double.valueOf(item_discount_percent));
                    notifyDataSetChanged();


                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                }

            });*/


            vi.setTag(holder);
        }
        else {
            holder = (CartListAdapter.ViewHolder) vi.getTag();
        }

        final CartItem item = data.get(position);

        Modifiers="";

        String previous="";
        String[] selectedOptions = new String[]{};
        final List<ModifierItems> modifiers = DBMethodsCart.newInstance(activity).getModifierByMenuId(item.getMenu_id());
        if(modifiers.size()>0) {
            for (ModifierItems mitem : modifiers) {



                if(previous.equals("")){
                    Modifiers=mitem.getOption_name()+": "+mitem.getValue()+", ";

                }else if(previous.equals(mitem.getOption_name())){
                    Modifiers=Modifiers+mitem.getValue()+", ";
                }else{
                    Modifiers=Modifiers+"\n"+mitem.getOption_name()+": "+mitem.getValue()+" ,";
                }

                previous=mitem.getOption_name();

                // modifierss = modifierss + mitem.getValue();


            }
        }

        price_value=Double.parseDouble(data.get(position).getMenu_price().toString());
        item_subtotal=price_value*Integer.parseInt(String.valueOf(data.get(position).getMenu_qty()));
        total= String.valueOf((price_value)*Integer.parseInt(String.valueOf(data.get(position).getMenu_qty())));


        holder.name.setText(data.get(position).getMenu_name().toString());
        holder.ed_discount_percent.setHint(data.get(position).getDiscount_percent().toString());
        holder.modifiers.setText(Modifiers);


        holder.comments.setText(item.getComment());

        holder.tv_price_term.setText(AppSession.getInstance().getRestraunt_Currency());
        holder.tv_vat_amount.setText(AppSession.getInstance().getRestraunt_tax_value());



        // holder.product_description.setText(data.get(position).getMenu_description().toString());
        // holder.tv_price_value.setText(data.get(position).getMenu_price().toString());
        /* DecimalFormat*/ df2 = new DecimalFormat("#0.00");
        item_discount=Double.valueOf(data.get(position).getDiscount());
        double f = Double.parseDouble( total)-item_discount;
       /* String ft =df2.format(f);*/
        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);


        item_vat=f*(Double.valueOf(AppSession.getInstance().getRestraunt_tax_value())/100);

        /* item_total=item_subtotal+item_vat-item_discount;*/
        item_total=item_subtotal+item_vat-item_discount;
        String ft1 = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), item_total);
        String ft2 = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), item_vat);
        String ft3 = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), item_discount);
       /* String ft1 =df2.format(item_total);
        String ft2 =df2.format(item_vat);
        String ft3 =df2.format(item_discount);*/

        holder.tv_price.setText(AppSession.getInstance().getRestraunt_Currency());
        holder.tv_price_value.setText(ft);
        holder.tv_per_price_value.setText(item.getMenu_price());
        holder.tv_qty_value.setText(String.valueOf(data.get(position).getMenu_qty()));

        holder.tv_item_total_value.setText(String.valueOf(ft1));
        holder.tv_vat_value.setText(ft2);
        holder.tv_discount_value.setText(ft3);
        if(!item.getDiscount_comment().equals(""))
        {
            holder.ll_discount.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.ll_discount.setVisibility(View.GONE);
        }
        if(item.getStatus().equals(""))
        {
            holder.order_send.setText("Newly Added");
            holder.order_send.setTextColor(Color.parseColor("#6ab409"));
            item.setSetCannot_decrease(0);
        }
        else {
            holder.order_send.setText("Order In Kitchen/ Bar");
            holder.order_send.setTextColor(Color.parseColor("#fa5850"));
            item.setSetCannot_decrease(1);
        }
        if(AppSession.getInstance().getCartPlusFlag().equals(""))
        {
            holder.tv_plus.setEnabled(true);
            holder.tv_minus.setEnabled(true);
            holder.delete.setEnabled(true);
            holder.ll_item_name.setEnabled(true);
        }
        else
        {

        }
        if(item.getPrev_quantity()==0)
        {
            holder.tv_minus.setEnabled(true);
        }

        else
        {
            holder.tv_minus.setEnabled(false);
        }


        holder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    AppSession.getInstance().setCartPlusFlag("+");
                    holder.tv_plus.setEnabled(false);
                    mItemClickListner.onItemSelected("cartPlus", position, item);
                }

               /* count++;
                // data.get(position).setMinimum_qty(Integer.parseInt(String.valueOf(count)));
                DBMethodsCart.newInstance(activity).updateCart(true, item.getMenu_id());
               */

            }
        });

        holder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    AppSession.getInstance().setCartPlusFlag("-");
                    holder.tv_minus.setEnabled(false);
                    mItemClickListner.onItemSelected("cartMinus", position, item);
                }
               /* count--;
                // data.get(position).setMinimum_qty(Integer.parseInt(String.valueOf(count)));
                DBMethodsCart.newInstance(activity).updateCart(false, item.getMenu_id());
                if (count < 1) {
                    count = 1;
                }
                holder.tv_qty_value.setText(String.valueOf(count));*/
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    AppSession.getInstance().setCartPlusFlag("del");
                    holder.delete.setEnabled(false);
                    mItemClickListner.onItemSelected("cartDelete", position, item);
                }

              /*  data.remove(position);
                DBMethodsCart.newInstance(activity).removeFromCart(item.getMenu_id());*/
            }
        });





       /* HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);*/



        /* final FavotateProductlistitemResponce item = FavorateProductList.get(position);*/


        /*   holder.name.setText(""+data.get(position));*/

//        Glide.with(activity).load(data.get(position).toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter()
//                //.placeholder(R.drawable.home_page_banner_two)
//                .into( holder.iv_catimg);

//        holder.ll_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mItemClickListner.onItemSelected("cartClick", position, item);
//               /* count++;
//                // data.get(position).setMinimum_qty(Integer.parseInt(String.valueOf(count)));
//                DBMethodsCart.newInstance(activity).updateCart(true, item.getMenu_id());
//               */
//
//            }
//        });
        holder.ll_item_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppSession.getInstance().getCartPlusFlag().equals(""))
                {
                    mItemClickListner.onItemSelected("cartClick", position, item);
                }



            }
        });

        return vi;
    }


}
