package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuItemsList;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;

public class KitchenOrderListDetailsadapter extends BaseAdapter {

    ItemClickListener mItemClickListner;
    customButtonListener customListner;
    public interface customButtonListener {
        public void onButtonClickListner(int position);
    }

    public void setCustomButtonListner(customButtonListener listener) {
        this.customListner = listener;
    }





    int row_index=-1;
    int row_indexx=-1;
    String Clicked="";
    private Activity activity;
    ArrayList<MenuItemsList> data;
    private static LayoutInflater inflater=null;
    private ViewHolder holder = null;
    String Modifiers="";
    String With="",Without="",other="",othername="";

    /*ArrayList<FavotateProductlistitemResponce> FavorateProductList;*/


    public KitchenOrderListDetailsadapter(Activity a, ArrayList<MenuItemsList> d, ItemClickListener itemClickListner) {
        activity = a;
        data=d;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    class ViewHolder {
        TextView tv_table_id,tv_table_idd,small_tv_status,name,description,description2,description3,description4,oredrid,quantity,tv_status,small_name,small_description,small_description2,small_description4,small_description3,small_oredrid;
        ImageView iv_catimg,orderchange,orderchanged;
        RelativeLayout itemm,rl_small_row;
        LinearLayout ll_big_row;
        Switch simpleSwitchh;
        ImageView productimage,productimagee;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final MenuItemsList item = data.get(position);


        View vi=convertView;
        if(convertView==null) {

            vi = inflater.inflate(R.layout.activity_order_row_new, null);
            holder = new ViewHolder();


            holder.name = (TextView) vi.findViewById(R.id.product_name);
            holder.small_name = (TextView) vi.findViewById(R.id.product_namee);
            holder.description = (TextView) vi.findViewById(R.id.product_description);
            holder.description2 = (TextView) vi.findViewById(R.id.product_description2);
            holder.description3 = (TextView) vi.findViewById(R.id.product_description3);
            holder.description4 = (TextView) vi.findViewById(R.id.product_description4);
            holder.small_description = (TextView) vi.findViewById(R.id.product_descriptionn);
            holder.small_description2 = (TextView) vi.findViewById(R.id.product_descriptionn2);
            holder.small_description3 = (TextView) vi.findViewById(R.id.product_descriptionn3);
            holder.small_description4 = (TextView) vi.findViewById(R.id.product_descriptionn4);
            holder.oredrid = (TextView) vi.findViewById(R.id.oredrid);
            //holder.small_oredrid = (TextView) vi.findViewById(R.id.oredridd);
            holder.tv_status = (TextView) vi.findViewById(R.id.tv_status);
            holder.small_tv_status = (TextView) vi.findViewById(R.id.product_status);

            holder.orderchange = (ImageView) vi.findViewById(R.id.orderchange);
            holder.orderchanged = (ImageView) vi.findViewById(R.id.orderchanged);
            holder.ll_big_row = (LinearLayout) vi.findViewById(R.id.ll_big_row);
            holder.rl_small_row = (RelativeLayout) vi.findViewById(R.id.rl_small_row);
            holder.simpleSwitchh = (Switch) vi.findViewById(R.id.simpleSwitchh);
            holder.productimage = (ImageView) vi.findViewById(R.id.productimage);
            holder.productimagee = (ImageView) vi.findViewById(R.id.productimagee);

            //holder.tv_table_id = (TextView) vi.findViewById(R.id.tv_table_id);

            //holder.tv_table_idd = (TextView) vi.findViewById(R.id.tv_table_idd);

            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        if(item.getOptions().size()>0)
        {
            Modifiers="";
            With="";
            Without="";
            other="";
            String previous="";
            String[] selectedOptions = new String[]{};
            for(int i=0;i<item.getOptions().size();i++)
            {
                if(previous.equals("")){
                    Modifiers=item.getOptions().get(i).getOption_name()+": "+item.getOptions().get(i).getValue()+", ";

                }else if(previous.equals(item.getOptions().get(i).getOption_name())){
                    Modifiers=Modifiers+item.getOptions().get(i).getValue()+", ";
                }else{
                    Modifiers=Modifiers+"\n"+item.getOptions().get(i).getOption_name()+": "+item.getOptions().get(i).getValue()+" ,";
                }

                previous=item.getOptions().get(i).getOption_name();
            }

            if(!Modifiers.equals(""))
            {
                holder.description2.setVisibility(View.VISIBLE);
                holder.description2.setText(Modifiers);
            }
            else
            {
                holder.description2.setVisibility(View.GONE);
                holder.description3.setVisibility(View.GONE);
                holder.description4.setVisibility(View.GONE);
                holder.small_description2.setVisibility(View.GONE);
                holder.small_description3.setVisibility(View.GONE);
                holder.small_description4.setVisibility(View.GONE);
            }

            // holder.small_description.setText(item.getComment()+" ,"+Modifiers);
        }
        else
        {
            holder.description2.setVisibility(View.GONE);
            holder.description3.setVisibility(View.GONE);
            holder.description4.setVisibility(View.GONE);
            holder.small_description2.setVisibility(View.GONE);
            holder.small_description3.setVisibility(View.GONE);
            holder.small_description4.setVisibility(View.GONE);
        }

        if(!item.getComment().equals(""))
        {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(activity.getResources().getString(R.string.comments)+item.getComment());
            holder.small_description.setVisibility(View.VISIBLE);
            holder.small_description.setText(activity.getResources().getString(R.string.comments)+item.getComment());
        }
        else
        {
            holder.description.setVisibility(View.GONE);
            holder.small_description.setVisibility(View.GONE);
        }
        //String full_name=item.getName()+" X "+item.getQuantity();
        String full_name=item.getName()+" X "+item.getQuantity();
        holder.name.setText(full_name);
        holder.small_name.setText(full_name);
        // holder.description.setText(item.getComment());
        //holder.small_description.setText(item.getComment());
        //holder.small_oredrid.setText(item.getTable_name());
        //holder.oredrid.setText(item.getTable_name());

        // holder.tv_status.setText(item.getMenu_status_name());
        //holder.small_tv_status.setText(item.getMenu_status_name());
        if(AppSession.getInstance().getOrderType().equals("dine_in"))
        {
            //holder.tv_table_id.setText("Table");
            //holder.tv_table_idd.setText("Table");
//            holder.productimage.setBackgroundResource(R.drawable.dine_in);
//            holder.productimagee.setBackgroundResource(R.drawable.dine_in);
        }
        else if (AppSession.getInstance().getOrderType().equals("takeaway"))
        {
            //holder.tv_table_id.setText("");
            //holder.tv_table_idd.setText("");
//            holder.productimage.setBackgroundResource(R.drawable.take_away);
//            holder.productimagee.setBackgroundResource(R.drawable.take_away);
        }
        else if(AppSession.getInstance().getOrderType().equals("delivery"))
        {
            //holder.tv_table_id.setText("");
            //holder.tv_table_idd.setText("");
//            holder.productimage.setBackgroundResource(R.drawable.delivery);
//            holder.productimagee.setBackgroundResource(R.drawable.delivery);
        }


        if(item.getMenu_status_id().equals("13"))
        {
            item.setState(1);
            holder.orderchange.setVisibility(View.VISIBLE);
            //holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            // holder.tv_status.setText(item.getMenu_status_name());
            holder.tv_status.setText("Preparation");
        }

        else if(item.getMenu_status_id().equals("14"))
        {
            item.setState(2);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            // holder.tv_status.setText(item.getMenu_status_name());

        }
        else if(item.getMenu_status_id().equals("19")||item.getMenu_status_id().equals("20"))
        {
            item.setState(3);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setVisibility(View.INVISIBLE);
           /* holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);*/
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            // holder.tv_status.setText(item.getMenu_status_name());
            holder.tv_status.setText("Cancelled");
        }else if(item.getMenu_status_id().equals("12"))
        {

            item.setState(0);
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(true);
            holder.simpleSwitchh.setChecked(false);
            // Toast.makeText(activity,item.getMenu_status_id(), Toast.LENGTH_SHORT).show();
            holder.tv_status.setText("Pending");
        }

        if (item.getState() == 1) {
           /* holder.orderchange.setVisibility(View.VISIBLE);
            holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            holder.orderchange.setVisibility(View.VISIBLE);
            //holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            holder.simpleSwitchh.setVisibility(View.VISIBLE);
            //  holder.tv_status.setText(item.getMenu_status_name());

        }
        else if(item.getState() == 2)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            //  holder.tv_status.setText(item.getMenu_status_name());
        }
        else if(item.getState() == 3)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            holder.simpleSwitchh.setVisibility(View.INVISIBLE);
           /* holder.orderchange.setVisibility(View.GONE);
            holder.orderchanged.setVisibility(View.VISIBLE);
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);*/
            // holder.tv_status.setText(item.getMenu_status_name());
            holder.small_description.setVisibility(View.VISIBLE);
            holder.small_description.setText("Cancelled");
        }

        else if(item.getState() == 0)
        {
            holder.orderchange.setVisibility(View.VISIBLE);
            // holder.orderchanged.setVisibility(View.GONE);
            holder.ll_big_row.setVisibility(View.VISIBLE);
            holder.rl_small_row.setVisibility(View.GONE);
            holder.simpleSwitchh.setClickable(true);
            holder.simpleSwitchh.setChecked(false);
            holder.simpleSwitchh.setVisibility(View.VISIBLE);
        }


        holder.orderchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;
                if( item.getState()!=3)
                {

                    mItemClickListner.onItemSelected("tick_button2", position, item);
                    notifyDataSetChanged();
                }
                else
                {
                    Toast.makeText(activity,activity.getResources().getString(R.string.cancelled_cant_complete), Toast.LENGTH_SHORT).show();
                }
              /*  row_index=position;
                mItemClickListner.onItemSelected("tick_button2", position, item);
                notifyDataSetChanged();*/
                // Toast.makeText(activity,"order", Toast.LENGTH_SHORT).show();

            }
        });

        holder.simpleSwitchh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                row_indexx=position;
                if( item.getState()!=3)
                {


                    mItemClickListner.onItemSelected("slide_button2", position, item);
                    notifyDataSetChanged();

                }
                else
                {
                    Toast.makeText(activity,activity.getResources().getString(R.string.cancelled_cant_changed), Toast.LENGTH_SHORT).show();
                }
              /*  row_indexx=position;
                mItemClickListner.onItemSelected("slide_button2", position, item);
                notifyDataSetChanged();
*/

            }
        });

        if(row_index==position){
            item.setMenu_status_id("14");
            //  item.setMenu_status_name("Delivery");
            holder.ll_big_row.setVisibility(View.GONE);
            holder.rl_small_row.setVisibility(View.VISIBLE);
            notifyDataSetChanged();

        }

        if(row_indexx==position){
            item.setMenu_status_id("13");
            holder.simpleSwitchh.setClickable(false);
            holder.simpleSwitchh.setChecked(true);
            holder.tv_status.setText("Preparation");
            //item.setMenu_status_name("Preparation");
            notifyDataSetChanged();

        }
        if(!AppSession.getInstance().getUser_Type().equals("KITCHEN")
                &&!AppSession.getInstance().getUser_Type().equals("BAR"))
        {
            holder.simpleSwitchh.setVisibility(View.INVISIBLE);
            holder.orderchange.setEnabled(false);
        }


        return vi;
    }



}