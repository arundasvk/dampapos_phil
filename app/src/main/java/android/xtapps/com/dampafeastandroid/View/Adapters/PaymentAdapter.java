package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
import java.util.HashMap;

public class PaymentAdapter extends BaseAdapter {
    public static final String PAID_BY = "PAID_BY"; // parent node
    public static final String CARD_TYPE = "CARD_TYPE"; // parent node
    public static final String RECEIVED_AMOUNT = "RECEIVED_AMOUNT";
    public static final String TENDER_CHANGE = "TENDER_CHANGE";
    public static final String AMOUNT = "AMOUNT";

    private Activity activity;
    ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    private PaymentAdapter.ViewHolder holder = null;
    public int count = 1;
    Double price_value;
    String total,Modifiers,item_discount_percent="";
    Double item_total=0.00,item_discount=0.00,item_vat=1.00,item_subtotal=0.00;
    int rowid;

    ItemClickListener mItemClickListner;



    public PaymentAdapter(Activity a,  ArrayList<HashMap<String, String>>  d,ItemClickListener itemClickListner) {
        activity = a;
        data=d;
        mItemClickListner = itemClickListner;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
       TextView tv_paidby,tv_cardtype,tv_tender_cash,tv_tender_change,tv_amount;
       LinearLayout ll_amount,ll_card_type,ll_tender_cash,ll_tender_change;
    }

    @Override
    public View getView(final int  position, View convertView, ViewGroup parent) {



        View vi=convertView;
        if(convertView==null) {

            vi = inflater.inflate(R.layout.payment_type_list_item, null);
            holder = new PaymentAdapter.ViewHolder();

            holder.ll_amount = vi.findViewById(R.id.ll_amount);
            holder.ll_card_type = vi.findViewById(R.id.ll_card_type);
            holder.ll_tender_cash = vi.findViewById(R.id.ll_tender_cash);
            holder.ll_tender_change = vi.findViewById(R.id.ll_tender_change);

            holder.tv_paidby = vi.findViewById(R.id.tv_paidby);
            holder.tv_cardtype = vi.findViewById(R.id.tv_cardtype);
            holder.tv_amount = vi.findViewById(R.id.tv_amount);
            holder.tv_tender_cash = vi.findViewById(R.id.tv_tender_cash);
            holder.tv_tender_change = vi.findViewById(R.id.tv_tender_change);

            vi.setTag(holder);
        }
        else {
            holder = (PaymentAdapter.ViewHolder) vi.getTag();
        }




        if(data.get(position).get(PAID_BY).equals("CARD"))
        {
          //  holder.ll_tender_cash.setVisibility(View.GONE);
           // holder.ll_tender_change.setVisibility(View.GONE);
            holder.ll_card_type.setVisibility(View.VISIBLE);
            holder.ll_amount.setVisibility(View.VISIBLE);
                   /* ll_card_number.setVisibility(View.VISIBLE);
                    ll_card_holder.setVisibility(View.VISIBLE);*/
        }
        else if(!data.get(position).get(PAID_BY).equals("CARD"))
        {
           // holder.ll_tender_cash.setVisibility(View.VISIBLE);
           // holder.ll_tender_change.setVisibility(View.GONE);
            holder.ll_card_type.setVisibility(View.GONE);
            holder.ll_amount.setVisibility(View.VISIBLE);
                   /* ll_card_number.setVisibility(View.GONE);
                    ll_card_holder.setVisibility(View.GONE);*/
        }

        else
        {

        }

        holder.tv_paidby.setText(data.get(position).get(PAID_BY));
        holder.tv_cardtype.setText(data.get(position).get(CARD_TYPE));
        holder.tv_amount.setText(data.get(position).get(AMOUNT));
        holder.tv_tender_cash.setText(data.get(position).get(RECEIVED_AMOUNT));
        holder.tv_tender_change.setText(data.get(position).get(TENDER_CHANGE));




        return vi;
    }


}
