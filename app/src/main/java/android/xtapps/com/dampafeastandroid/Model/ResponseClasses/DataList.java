package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataList {
    @SerializedName("category_id")
    @Expose
    private String categoryId;

    @SerializedName("category_name")
    @Expose
    private String categoryName;

    @SerializedName("menu_items")
    @Expose
    private ArrayList<MenuItemsList> menuItemsLists;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<MenuItemsList> getMenuItemsLists() {
        return menuItemsLists;
    }

    public void setMenuItemsLists(ArrayList<MenuItemsList> menuItemsLists) {
        this.menuItemsLists = menuItemsLists;
    }




}