package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Model.ImageModel;
import android.xtapps.com.dampafeastandroid.R;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class OrderlistAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<ImageModel> data;
    private static LayoutInflater inflater=null;
    private OrderlistAdapter.ViewHolder holder = null;
    /*ArrayList<FavotateProductlistitemResponce> FavorateProductList;*/


    public OrderlistAdapter(Activity a, ArrayList<ImageModel> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {
        TextView name,distance;
        ImageView iv_catimg;
        RelativeLayout itemm;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {



        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.activity_order_row, null);
            holder = new OrderlistAdapter.ViewHolder();


            holder.name = vi.findViewById(R.id.product_name);
            holder.distance = vi.findViewById(R.id.product_description);
            holder.iv_catimg = vi.findViewById(R.id.productimage);
          //  holder.itemm = (RelativeLayout) vi.findViewById(R.id.ll_outer);


            vi.setTag(holder);
        }
        else {
            holder = (OrderlistAdapter.ViewHolder) vi.getTag();
        }

       /* HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);*/



        /* final FavotateProductlistitemResponce item = FavorateProductList.get(position);*/


        /*   holder.name.setText(""+data.get(position));*/
        holder.name.setText("Product Details");
        holder.distance.setText("Produts Name");
        Glide.with(activity).load(data.get(position).toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter()
                //.placeholder(R.drawable.home_page_banner_two)
                .into( holder.iv_catimg);




        return vi;
    }


}
