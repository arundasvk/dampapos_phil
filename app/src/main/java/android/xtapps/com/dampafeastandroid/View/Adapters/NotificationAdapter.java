package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationAdapter extends BaseAdapter {

    private Activity activity;
    String type="";
   // ArrayList<Kitchenorder> mCartItems = new ArrayList<Kitchenorder>();
    ArrayList<HashMap<String, String>> mCartItems = new ArrayList<>();
    private static LayoutInflater inflater = null;
    private NotificationAdapter.ViewHolder holder = null;

    public NotificationAdapter(Activity a, ArrayList<HashMap<String, String>> itemsList) {
        activity = a;
        mCartItems = itemsList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {

        return mCartItems.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {

        ImageView img_item;
        TextView tv_item_name,tv_item_quantity,tv_item_quantity_value,tv_rate,tv_rate_value;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (view == null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.notificationlistitem, null);
            holder = new NotificationAdapter.ViewHolder();

            holder.tv_item_name = vi.findViewById(R.id.tv_item_name);
             if(mCartItems.get(i).get(NavigationMainActivity.NOTI_TYPE).equals("dine_in"))
             {
                 type=activity.getResources().getString(R.string.table_smll);
             }
             else
             if(mCartItems.get(i).get(NavigationMainActivity.NOTI_TYPE).equals("takeaway"))
             {
                 type=activity.getResources().getString(R.string.take_away_smll);
             }
             else
             if(mCartItems.get(i).get(NavigationMainActivity.NOTI_TYPE).equals("delivery"))
             {
                 type=activity.getResources().getString(R.string.delivery_smll);
             }

            vi.setTag(holder);
        }
        else {
            holder = (NotificationAdapter.ViewHolder) vi.getTag();

        }
        holder.tv_item_name .setText(mCartItems.get(i).get(NavigationMainActivity.NOTI_TEXT)+" "+type
        +" "+mCartItems.get(i).get(NavigationMainActivity.NOTI_TABLE_NAME));





        return  vi;
    }
}
