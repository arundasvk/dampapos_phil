package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by USER on 7/20/2018.
 */

public class RestaurantLocationResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("tab_token")
    @Expose
    private String token;

    @SerializedName("location")
    @Expose
    public location location;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }


    public String getTabToken() {
        return token;
    }

    public void setTabtoken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public RestaurantLocationResponse.location getLocation() {
        return location;
    }

    public void setLocation(RestaurantLocationResponse.location location) {
        this.location = location;
    }

    public   class location
    {
        @SerializedName("locations")
        @Expose
        private ArrayList<Items> locations;

        public class Items {
            @SerializedName("location_id")
            @Expose
            private String location_id;

            @SerializedName("location_name")
            @Expose
            private String location_name;

            @SerializedName("location_address1")
            @Expose
            private String location_add1;
            @SerializedName("location_address2")
            @Expose
            private String location_add2;

            @SerializedName("location_image")
            @Expose
            private String location_image;

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getLocation_name() {
                return location_name;
            }

            public void setLocation_name(String location_name) {
                this.location_name = location_name;
            }



            public String getLocation_add1() {
                return location_add1;
            }

            public void setLocation_add1(String location_add1) {
                this.location_add1 = location_add1;
            }


            public String getLocation_add2() {
                return location_add2;
            }

            public void setLocation_add2(String location_add2) {
                this.location_add2 = location_add2;
            }

            public String getLocation_Image() {
                return location_image;
            }

            public void setLocation_Image(String location_image) {
                this.location_image = location_image;
            }
        }

        public ArrayList<Items> getLocations() {
            return locations;
        }

        public void setLocations(ArrayList<Items> locations) {
            this.locations = locations;
        }
    }



}
