package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WaitersOrdersList {
    @SerializedName("staff_name")
    @Expose
    private String staff_name;

    @SerializedName("staff_id")
    @Expose
    private String staff_id;

    @SerializedName("waiter_orders_count")
    @Expose
    private String waiter_orders_count;

    @SerializedName("cash_collected")
    @Expose
    private String cash_collected;

    @SerializedName("gross_amount")
    @Expose
    private String gross_amount;

    @SerializedName("net_discount")
    @Expose
    private String net_discount;

    @SerializedName("CASH")
    @Expose
    private String CASH;


    @SerializedName("MAKAN")
    @Expose
    private String MAKAN;


    @SerializedName("CARD")
    @Expose
    private String CARD;


    @SerializedName("BOTH")
    @Expose
    private String BOTH;

    @SerializedName("TALABAT")
    @Expose
    private String TALABAT;

    @SerializedName("GIFTCOUPON")
    @Expose
    private String GIFTCOUPON;

    @SerializedName("order_list")
    @Expose
    private ArrayList<Orderdetails> order_list;

  /*  @SerializedName("cancelled_orders")
    @Expose
    private ArrayList<OrderMenus> cancelled_orders;*/


    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getWaiter_orders_count() {
        return waiter_orders_count;
    }

    public void setWaiter_orders_count(String waiter_orders_count) {
        this.waiter_orders_count = waiter_orders_count;
    }

    public String getCash_collected() {
        return cash_collected;
    }

    public void setCash_collected(String cash_collected) {
        this.cash_collected = cash_collected;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getCASH() {
        return CASH;
    }

    public void setCASH(String CASH) {
        this.CASH = CASH;
    }

    public String getMAKAN() {
        return MAKAN;
    }

    public void setMAKAN(String MAKAN) {
        this.MAKAN = MAKAN;
    }

    public String getCARD() {
        return CARD;
    }

    public void setCARD(String CARD) {
        this.CARD = CARD;
    }

    public String getBOTH() {
        return BOTH;
    }

    public void setBOTH(String BOTH) {
        this.BOTH = BOTH;
    }

    public String getTALABAT() {
        return TALABAT;
    }

    public void setTALABAT(String TALABAT) {
        this.TALABAT = TALABAT;
    }

    public String getGIFTCOUPON() {
        return GIFTCOUPON;
    }

    public void setGIFTCOUPON(String GIFTCOUPON) {
        this.GIFTCOUPON = GIFTCOUPON;
    }

    public ArrayList<Orderdetails> getOrder_list() {
        return order_list;
    }

    public void setOrder_list(ArrayList<Orderdetails> order_list) {
        this.order_list = order_list;
    }

   /* public ArrayList<OrderMenus> getCancelled_orders() {
        return cancelled_orders;
    }

    public void setCancelled_orders(ArrayList<OrderMenus> cancelled_orders) {
        this.cancelled_orders = cancelled_orders;
    }*/

    public String getGross_amount() {
        return gross_amount;
    }

    public void setGross_amount(String gross_amount) {
        this.gross_amount = gross_amount;
    }

    public String getNet_discount() {
        return net_discount;
    }

    public void setNet_discount(String net_discount) {
        this.net_discount = net_discount;
    }
}
