package android.xtapps.com.dampafeastandroid.DB;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxItems {

        @SerializedName("vat_amount")
        @Expose
        private String vat_amount;

        @SerializedName("tax_exemption")
        @Expose
        private String tax_exemption;

        @SerializedName("discount_amount")
        @Expose
        private String discount_amount;

        @SerializedName("service_charge")
        @Expose
        private String service_charge;

        @SerializedName("payable_amount")
        @Expose
        private String payable_amount;

        private String index;

        private String order_id;

        public String getVat_amount() {
            return vat_amount;
        }

        public void setVat_amount(String vat_amount) {
            this.vat_amount = vat_amount;
        }

        public String getTax_exemption() {
            return tax_exemption;
        }

        public void setTax_exemption(String tax_exemption) {
            this.tax_exemption = tax_exemption;
        }

        public String getDiscount_amount() {
            return discount_amount;
        }

        public void setDiscount_amount(String discount_amount) {
            this.discount_amount = discount_amount;
        }

        public String getService_charge() {
            return service_charge;
        }

        public void setService_charge(String service_charge) {
            this.service_charge = service_charge;
        }

        public String getPayable_amount() {
            return payable_amount;
        }

        public void setPayable_amount(String payable_amount) {
            this.payable_amount = payable_amount;
        }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public TaxItems(String  index, String order_id, String vat_amount, String tax_exemption, String discount_amount,
                    String service_charge, String payable_amount) {

        this.index=index;
        this.order_id=order_id;
        this.vat_amount=vat_amount;
        this.tax_exemption=tax_exemption;
        this.discount_amount= discount_amount;
        this.service_charge=service_charge;
        this.payable_amount= payable_amount;


    }
    }