package android.xtapps.com.dampafeastandroid.View.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.util.LruCache;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderList;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.Command;
import android.xtapps.com.dampafeastandroid.View.Adapters.PrintPicture;
import android.xtapps.com.dampafeastandroid.View.Adapters.PrinterCommand;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Set;

import zj.com.customize.sdk.Other;

import static java.lang.Integer.parseInt;

public class Main_ActivityPrinterLongin extends Activity implements OnClickListener {
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    public static final String MyPREFERENCES_CLOSE = "MyPrefsCloseRestraunt";
    private SharedPreferences sharedpreferencesCloseRestraunt;
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList2 = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList3 = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList4_cancelled = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList_payment = new ArrayList<>();
    Bitmap mBitmap1;
    /******************************************************************************************************/
    // Debugging
    private static final String TAG = "Main_Activity";
    private static final boolean DEBUG = true;
    /******************************************************************************************************/
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    /*******************************************************************************************************/
    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    String flag_closeorderbill="";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CHOSE_BMP = 3;
    private static final int REQUEST_CAMER = 4;

    //QRcode
    private static final int QR_WIDTH = 350;
    private static final int QR_HEIGHT = 350;
    /*******************************************************************************************************/
    private static final String CHINESE = "GBK";
    private static final String THAI = "CP874";
    private static final String KOREAN = "EUC-KR";
    private static final String BIG5 = "BIG5";

    /*********************************************************************************/
    private TextView mTitle;
    EditText editText;
    ImageView imageViewPicture,connectDevice;
    private static boolean is58mm = true;
    private RadioButton width_58mm, width_80;
    private RadioButton thai, big5, Simplified, Korean;
    private CheckBox hexBox;
    private Button sendButton = null;
    private Button testButton = null;
    private Button printbmpButton = null;
    private Button btnScanButton = null;
    private Button btnScandeny= null;
    private Button btnClose = null;
    private Button btn_BMP = null;
    private Button btn_ChoseCommand = null;
    private Button btn_prtsma = null;
    private Button btn_prttableButton = null;
    private Button btn_prtcodeButton = null;
    private Button btn_scqrcode = null;
    private Button btn_camer = null;
    private LinearLayout ppprint;
    private LinearLayout selectionprint ;

    TextView Texttablename;

    private Button btnscann = null;
    private Button btnbillprint = null;
    private Button btnCloseprint = null;
    private Button btn_close_restraunt = null;
    private Button btn_close_order = null;
    private Button btnmyorder= null;
    private Button btn_myorder_expnd = null;
    private Button btn_report_expnd= null;
    String a=" ";
    /******************************************************************************************************/
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    private BluetoothService mService = null;

    /***************************   Ã¦Å’â€¡                 Ã¤Â»Â¤****************************************************************/
    final String[] items = { "Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº", "Ã¦â€°â€œÃ¥ÂÂ°Ã¥Â¹Â¶Ã¨ÂµÂ°Ã§ÂºÂ¸", "Ã¦ â€¡Ã¥â€¡â€ ASCIIÃ¥Â­â€”Ã¤Â½â€œ", "Ã¥Å½â€¹Ã§Â¼Â©ASCIIÃ¥Â­â€”Ã¤Â½â€œ", "Ã¦Â­Â£Ã¥Â¸Â¸Ã¥Â¤Â§Ã¥Â°Â",
            "Ã¤ÂºÅ’Ã¥â‚¬ÂÃ©Â«ËœÃ¥â‚¬ÂÃ¥Â®Â½", "Ã¤Â¸â€°Ã¥â‚¬ÂÃ©Â«ËœÃ¥â‚¬ÂÃ¥Â®Â½", "Ã¥â€ºâ€ºÃ¥â‚¬ÂÃ©Â«ËœÃ¥â‚¬ÂÃ¥Â®Â½", "Ã¥Ââ€“Ã¦Â¶Ë†Ã¥Å  Ã§Â²â€”Ã¦Â¨Â¡Ã¥Â¼Â", "Ã©â‚¬â€°Ã¦â€¹Â©Ã¥Å  Ã§Â²â€”Ã¦Â¨Â¡Ã¥Â¼Â", "Ã¥Ââ€“Ã¦Â¶Ë†Ã¥â‚¬â€™Ã§Â½Â®Ã¦â€°â€œÃ¥ÂÂ°", "Ã©â‚¬â€°Ã¦â€¹Â©Ã¥â‚¬â€™Ã§Â½Â®Ã¦â€°â€œÃ¥ÂÂ°", "Ã¥Ââ€“Ã¦Â¶Ë†Ã©Â»â€˜Ã§â„¢Â½Ã¥ÂÂÃ¦ËœÂ¾", "Ã©â‚¬â€°Ã¦â€¹Â©Ã©Â»â€˜Ã§â„¢Â½Ã¥ÂÂÃ¦ËœÂ¾",
            "Ã¥Ââ€“Ã¦Â¶Ë†Ã©Â¡ÂºÃ¦â€”Â¶Ã©â€™Ë†Ã¦â€”â€¹Ã¨Â½Â¬90Ã‚Â°", "Ã©â‚¬â€°Ã¦â€¹Â©Ã©Â¡ÂºÃ¦â€”Â¶Ã©â€™Ë†Ã¦â€”â€¹Ã¨Â½Â¬90Ã‚Â°", "Ã¨ÂµÂ°Ã§ÂºÂ¸Ã¥Ë†Â°Ã¥Ë†â€¡Ã¥Ë†â‚¬Ã¤Â½ÂÃ§Â½Â®Ã¥Â¹Â¶Ã¥Ë†â€¡Ã§ÂºÂ¸", "Ã¨Å“â€šÃ©Â¸Â£Ã¦Å’â€¡Ã¤Â»Â¤", "Ã¦ â€¡Ã¥â€¡â€ Ã©â€™Â±Ã§Â®Â±Ã¦Å’â€¡Ã¤Â»Â¤",
            "Ã¥Â®Å¾Ã¦â€”Â¶Ã¥Â¼Â¹Ã©â€™Â±Ã§Â®Â±Ã¦Å’â€¡Ã¤Â»Â¤", "Ã¨Â¿â€ºÃ¥â€¦Â¥Ã¥Â­â€”Ã§Â¬Â¦Ã¦Â¨Â¡Ã¥Â¼Â", "Ã¨Â¿â€ºÃ¥â€¦Â¥Ã¤Â¸Â­Ã¦â€“â€¡Ã¦Â¨Â¡Ã¥Â¼Â", "Ã¦â€°â€œÃ¥ÂÂ°Ã¨â€¡ÂªÃ¦Â£â‚¬Ã©Â¡Âµ", "Ã§Â¦ÂÃ¦Â­Â¢Ã¦Å’â€°Ã©â€Â®", "Ã¥Ââ€“Ã¦Â¶Ë†Ã§Â¦ÂÃ¦Â­Â¢Ã¦Å’â€°Ã©â€Â®" ,
            "Ã¨Â®Â¾Ã§Â½Â®Ã¦Â±â€°Ã¥Â­â€”Ã¥Â­â€”Ã§Â¬Â¦Ã¤Â¸â€¹Ã¥Ë†â€™Ã§ÂºÂ¿", "Ã¥Ââ€“Ã¦Â¶Ë†Ã¦Â±â€°Ã¥Â­â€”Ã¥Â­â€”Ã§Â¬Â¦Ã¤Â¸â€¹Ã¥Ë†â€™Ã§ÂºÂ¿", "Ã¨Â¿â€ºÃ¥â€¦Â¥Ã¥ÂÂÃ¥â€¦Â­Ã¨Â¿â€ºÃ¥Ë†Â¶Ã¦Â¨Â¡Ã¥Â¼Â" };
    final String[] itemsen = { "Print Init", "Print and Paper", "Standard ASCII font", "Compressed ASCII font", "Normal size",
            "Double high power wide", "Twice as high power wide", "Three times the high-powered wide", "Off emphasized mode", "Choose bold mode", "Cancel inverted Print", "Invert selection Print", "Cancel black and white reverse display", "Choose black and white reverse display",
            "Cancel rotated clockwise 90 Ã‚Â°", "Select the clockwise rotation of 90 Ã‚Â°", "Feed paper Cut", "Beep", "Standard CashBox",
            "Open CashBox", "Char Mode", "Chinese Mode", "Print SelfTest", "DisEnable Button", "Enable Button" ,
            "Set Underline", "Cancel Underline", "Hex Mode" };
    final byte[][] byteCommands = {
            { 0x1b, 0x40, 0x0a },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x0a }, //Ã¦â€°â€œÃ¥ÂÂ°Ã¥Â¹Â¶Ã¨ÂµÂ°Ã§ÂºÂ¸
            { 0x1b, 0x4d, 0x00 },// Ã¦ â€¡Ã¥â€¡â€ ASCIIÃ¥Â­â€”Ã¤Â½â€œ
            { 0x1b, 0x4d, 0x01 },// Ã¥Å½â€¹Ã§Â¼Â©ASCIIÃ¥Â­â€”Ã¤Â½â€œ
            { 0x1d, 0x21, 0x00 },// Ã¥Â­â€”Ã¤Â½â€œÃ¤Â¸ÂÃ¦â€Â¾Ã¥Â¤Â§
            { 0x1d, 0x21, 0x11 },// Ã¥Â®Â½Ã©Â«ËœÃ¥Å  Ã¥â‚¬Â
            { 0x1d, 0x21, 0x22 },// Ã¥Â®Â½Ã©Â«ËœÃ¥Å  Ã¥â‚¬Â
            { 0x1d, 0x21, 0x33 },// Ã¥Â®Â½Ã©Â«ËœÃ¥Å  Ã¥â‚¬Â
            { 0x1b, 0x45, 0x00 },// Ã¥Ââ€“Ã¦Â¶Ë†Ã¥Å  Ã§Â²â€”Ã¦Â¨Â¡Ã¥Â¼Â
            { 0x1b, 0x45, 0x01 },// Ã©â‚¬â€°Ã¦â€¹Â©Ã¥Å  Ã§Â²â€”Ã¦Â¨Â¡Ã¥Â¼Â
            { 0x1b, 0x7b, 0x00 },// Ã¥Ââ€“Ã¦Â¶Ë†Ã¥â‚¬â€™Ã§Â½Â®Ã¦â€°â€œÃ¥ÂÂ°
            { 0x1b, 0x7b, 0x01 },// Ã©â‚¬â€°Ã¦â€¹Â©Ã¥â‚¬â€™Ã§Â½Â®Ã¦â€°â€œÃ¥ÂÂ°
            { 0x1d, 0x42, 0x00 },// Ã¥Ââ€“Ã¦Â¶Ë†Ã©Â»â€˜Ã§â„¢Â½Ã¥ÂÂÃ¦ËœÂ¾
            { 0x1d, 0x42, 0x01 },// Ã©â‚¬â€°Ã¦â€¹Â©Ã©Â»â€˜Ã§â„¢Â½Ã¥ÂÂÃ¦ËœÂ¾
            { 0x1b, 0x56, 0x00 },// Ã¥Ââ€“Ã¦Â¶Ë†Ã©Â¡ÂºÃ¦â€”Â¶Ã©â€™Ë†Ã¦â€”â€¹Ã¨Â½Â¬90Ã‚Â°
            { 0x1b, 0x56, 0x01 },// Ã©â‚¬â€°Ã¦â€¹Â©Ã©Â¡ÂºÃ¦â€”Â¶Ã©â€™Ë†Ã¦â€”â€¹Ã¨Â½Â¬90Ã‚Â°
            { 0x0a, 0x1d, 0x56, 0x42, 0x01, 0x0a },//Ã¥Ë†â€¡Ã¥Ë†â‚¬Ã¦Å’â€¡Ã¤Â»Â¤
            { 0x1b, 0x42, 0x03, 0x03 },//Ã¨Å“â€šÃ©Â¸Â£Ã¦Å’â€¡Ã¤Â»Â¤
            { 0x1b, 0x70, 0x00, 0x50, 0x50 },//Ã©â€™Â±Ã§Â®Â±Ã¦Å’â€¡Ã¤Â»Â¤
            { 0x10, 0x14, 0x00, 0x05, 0x05 },//Ã¥Â®Å¾Ã¦â€”Â¶Ã¥Â¼Â¹Ã©â€™Â±Ã§Â®Â±Ã¦Å’â€¡Ã¤Â»Â¤
            { 0x1c, 0x2e },// Ã¨Â¿â€ºÃ¥â€¦Â¥Ã¥Â­â€”Ã§Â¬Â¦Ã¦Â¨Â¡Ã¥Â¼Â
            { 0x1c, 0x26 }, //Ã¨Â¿â€ºÃ¥â€¦Â¥Ã¤Â¸Â­Ã¦â€“â€¡Ã¦Â¨Â¡Ã¥Â¼Â
            { 0x1f, 0x11, 0x04 }, //Ã¦â€°â€œÃ¥ÂÂ°Ã¨â€¡ÂªÃ¦Â£â‚¬Ã©Â¡Âµ
            { 0x1b, 0x63, 0x35, 0x01 }, //Ã§Â¦ÂÃ¦Â­Â¢Ã¦Å’â€°Ã©â€Â®
            { 0x1b, 0x63, 0x35, 0x00 }, //Ã¥Ââ€“Ã¦Â¶Ë†Ã§Â¦ÂÃ¦Â­Â¢Ã¦Å’â€°Ã©â€Â®
            { 0x1b, 0x2d, 0x02, 0x1c, 0x2d, 0x02 }, //Ã¨Â®Â¾Ã§Â½Â®Ã¤Â¸â€¹Ã¥Ë†â€™Ã§ÂºÂ¿
            { 0x1b, 0x2d, 0x00, 0x1c, 0x2d, 0x00 }, //Ã¥Ââ€“Ã¦Â¶Ë†Ã¤Â¸â€¹Ã¥Ë†â€™Ã§ÂºÂ¿
            { 0x1f, 0x11, 0x03 }, //Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºÃ¨Â¿â€ºÃ¥â€¦Â¥16Ã¨Â¿â€ºÃ¥Ë†Â¶Ã¦Â¨Â¡Ã¥Â¼Â
    };
    /***************************Ã¦ÂÂ¡                          Ã§ Â***************************************************************/
    final String[] codebar = { "UPC_A", "UPC_E", "JAN13(EAN13)", "JAN8(EAN8)",
            "CODE39", "ITF", "CODABAR", "CODE93", "CODE128", "QR Code" };
    final byte[][] byteCodebar = {
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
            { 0x1b, 0x40 },// Ã¥Â¤ÂÃ¤Â½ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº
    };
    /******************************************************************************************************/
    public static final int STATE_CONNECTED = 3;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DEBUG)
            Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout

        setContentView(R.layout.mainprinter);

        // Set up the custom title
        mTitle = (TextView) findViewById(R.id.title_left_text);

        mTitle = (TextView) findViewById(R.id.title_right_text);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            finish();
        }


    }

    @Override
    public void onStart() {
        super.onStart();

        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (mService == null)
                KeyListenerInit();//Ã§â€ºâ€˜Ã¥ÂÂ¬
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        if (mService != null) {

            if (mService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService.start();
            }
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (DEBUG)
            Log.e(TAG, "- ON PAUSE -");
    }

   /* @Override
    public void onStop() {
        super.onStop();
        if (DEBUG)
            Log.e(TAG, "-- ON STOP --");
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (mService != null)
            mService.stop();
        if (DEBUG)
            Log.e(TAG, "--- ON DESTROY ---");
    }

    /*****************************************************************************************************/
    private void KeyListenerInit() {

        editText = (EditText) findViewById(R.id.edit_text_out);
        Texttablename = (TextView) findViewById(R.id.printtablename);
        Texttablename.setText("Bill for Table "+AppSession.getInstance().getTable_Name());

        ppprint = (LinearLayout) findViewById(R.id.printprinter);
        selectionprint = (LinearLayout) findViewById(R.id.selectionprinter);

        connectDevice=(ImageView) findViewById(R.id.connect_Device);
        connectDevice.setOnClickListener(this);

        sendButton = (Button) findViewById(R.id.Send_Button);
        sendButton.setOnClickListener(this);

        testButton = (Button) findViewById(R.id.btn_test);
        testButton.setOnClickListener(this);

        btnscann = (Button) findViewById(R.id.button_scann);
        btnscann.setEnabled(false);
        btnscann.setOnClickListener(this);
        btnCloseprint = (Button) findViewById(R.id.btn_testt);
        btnCloseprint.setEnabled(false);
        btnCloseprint.setOnClickListener(this);
        btnbillprint = (Button) findViewById(R.id.btn_printpictureee);
        btnbillprint.setEnabled(false);
        btnbillprint.setOnClickListener(this);

        btn_close_restraunt = (Button) findViewById(R.id.btn_close_restraunt);
        btn_close_restraunt.setEnabled(false);
        btn_close_restraunt.setOnClickListener(this);

        btnmyorder = (Button) findViewById(R.id.btn_myorder);
        btnmyorder.setEnabled(false);
        btnmyorder.setOnClickListener(this);

        btn_myorder_expnd = (Button) findViewById(R.id.btn_myorder_expnd);
        btn_myorder_expnd.setEnabled(false);
        btn_myorder_expnd.setOnClickListener(this);

        btn_report_expnd = (Button) findViewById(R.id.btn_report_expnd);
        btn_report_expnd.setEnabled(false);
        btn_report_expnd.setOnClickListener(this);

        printbmpButton = (Button) findViewById(R.id.btn_printpicture);
        printbmpButton.setOnClickListener(this);

        btn_close_order = (Button) findViewById(R.id.btn_completeorderprint);
        btn_close_order.setOnClickListener(this);

        btnScanButton = (Button)findViewById(R.id.button_scan);
        btnScanButton.setOnClickListener(this);

        btnScandeny = (Button)findViewById(R.id.button_delay);
        btnScandeny.setOnClickListener(this);

        hexBox = (CheckBox)findViewById(R.id.checkBoxHEX);
        hexBox.setOnClickListener(this);

        width_58mm = (RadioButton)findViewById(R.id.width_58mm);
        width_58mm.setOnClickListener(this);

        width_80 = (RadioButton)findViewById(R.id.width_80mm);
        width_80.setOnClickListener(this);

        imageViewPicture = (ImageView) findViewById(R.id.imageViewPictureUSB);
        imageViewPicture.setOnClickListener(this);

        btnClose = (Button)findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

        btn_BMP = (Button)findViewById(R.id.btn_prtbmp);
        btn_BMP.setOnClickListener(this);

        btn_ChoseCommand = (Button)findViewById(R.id.btn_prtcommand);
        btn_ChoseCommand.setOnClickListener(this);

        btn_prtsma = (Button)findViewById(R.id.btn_prtsma);
        btn_prtsma.setOnClickListener(this);

        btn_prttableButton = (Button)findViewById(R.id.btn_prttable);
        btn_prttableButton.setOnClickListener(this);

        btn_prtcodeButton = (Button)findViewById(R.id.btn_prtbarcode);
        btn_prtcodeButton.setOnClickListener(this);

        btn_camer = (Button)findViewById(R.id.btn_dyca);
        btn_camer.setOnClickListener(this);

        btn_scqrcode = (Button)findViewById(R.id.btn_scqr);
        btn_scqrcode.setOnClickListener(this);

        Simplified = (RadioButton)findViewById(R.id.gbk12);
        Simplified.setOnClickListener(this);
        big5 = (RadioButton)findViewById(R.id.big5);
        big5.setOnClickListener(this);
        thai = (RadioButton)findViewById(R.id.thai);
        thai.setOnClickListener(this);
        Korean = (RadioButton)findViewById(R.id.kor);
        Korean.setOnClickListener(this);



        Bitmap bm = getImageFromAssetsFile("demo.jpg");
        if (null != bm) {
            imageViewPicture.setImageBitmap(bm);
        }
     /*if(AppSession.getInstance().getBluetoothdevicename()!=null){
            Toast.makeText(Main_ActivityPrinterLongin.this, "hgkjdf", Toast.LENGTH_SHORT).show();
           mService.setState(STATE_CONNECTED);
        }*/
         mBitmap1=StringToBitMap(AppSession.getInstance().getPrintericon());

        if(AppSession.getInstance().getCancelcart().equals("123")){

            /* mService.setState(3);*/
           /* printbmpButton.setVisibility(View.GONE);
            testButton.setVisibility(View.VISIBLE);
            CancelledBill();
            ppprint.setVisibility(View.VISIBLE);
            selectionprint.setVisibility(View.GONE);*/
            AppSession.getInstance().setCancelcart("");
            btnCloseprint.setVisibility(View.VISIBLE);
            btn_close_restraunt.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.GONE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);



        }else  if(AppSession.getInstance().getcompletecart().equals("123")){
            /* mService.setState(3);*/
           /* AppSession.getInstance().setcompletecart(" ");
            printbmpButton.setVisibility(View.VISIBLE);
            testButton.setVisibility(View.GONE);
           *//* CompletedBill();*//*
            ppprint.setVisibility(View.VISIBLE);
            selectionprint.setVisibility(View.GONE);*/
            btnCloseprint.setVisibility(View.GONE);
            btn_close_restraunt.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.VISIBLE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.GONE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            AppSession.getInstance().setcompletecart("");





        }
        else if(AppSession.getInstance().getcompletecart().equals("124")){
            /* mService.setState(3);*/
           /* AppSession.getInstance().setcompletecart(" ");
            printbmpButton.setVisibility(View.VISIBLE);
            testButton.setVisibility(View.GONE);
           *//* CompletedBill();*//*
            ppprint.setVisibility(View.VISIBLE);
            selectionprint.setVisibility(View.GONE);*/
            btn_close_restraunt.setVisibility(View.VISIBLE);
            btnCloseprint.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.GONE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            AppSession.getInstance().setcompletecart("");
        }
        else if(AppSession.getInstance().getcompletecart().equals("125")){
            btn_close_restraunt.setVisibility(View.GONE);
            btnCloseprint.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.GONE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.VISIBLE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            AppSession.getInstance().setcompletecart("");





        }
        else if(AppSession.getInstance().getcompletecart().equals("126")){
            flag_closeorderbill="126";
            btn_close_restraunt.setVisibility(View.GONE);
            btnCloseprint.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.VISIBLE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            AppSession.getInstance().setcompletecart("");





        }
        else if(AppSession.getInstance().getcompletecart().equals("127")){
            flag_closeorderbill="127";
            btn_close_restraunt.setVisibility(View.GONE);
            btnCloseprint.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            btnmyorder.setVisibility(View.GONE);
            btn_myorder_expnd.setVisibility(View.VISIBLE);
            btn_report_expnd.setVisibility(View.GONE);
            selectionprint.setVisibility(View.GONE);
            btn_close_order.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            AppSession.getInstance().setcompletecart("");





        }
        else if (AppSession.getInstance().getCancelcart().equals("124")){

            AppSession.getInstance().setCancelcart("");
            btnmyorder.setVisibility(View.VISIBLE);
            btn_myorder_expnd.setVisibility(View.GONE);
            btn_report_expnd.setVisibility(View.GONE);
            btn_close_restraunt.setVisibility(View.GONE);
            btnCloseprint.setVisibility(View.GONE);
            btnbillprint.setVisibility(View.GONE);
            ppprint.setVisibility(View.VISIBLE);
            selectionprint.setVisibility(View.GONE);
            Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
        }else{

        }

        editText.setEnabled(false);
        imageViewPicture.setEnabled(false);
        width_58mm.setEnabled(false);
        width_80.setEnabled(false);
        hexBox.setEnabled(false);
        sendButton.setEnabled(false);
        testButton.setEnabled(false);
        printbmpButton.setEnabled(false);
        btnClose.setEnabled(false);
        btn_BMP.setEnabled(false);
        btn_ChoseCommand.setEnabled(false);
        btn_prtcodeButton.setEnabled(false);
        btn_prtsma.setEnabled(false);
        btn_prttableButton.setEnabled(false);
        btn_camer.setEnabled(false);
        btn_scqrcode.setEnabled(false);
        Simplified.setEnabled(false);
        Korean.setEnabled(false);
        big5.setEnabled(false);
        thai.setEnabled(false);

        mService = new BluetoothService(this, mHandler);


    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.connect_Device:{
                mService.stop();
                Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                break;
            }
            case R.id.button_delay:{
                finish();
                break;
            }
            case R.id.button_scann:{
                Intent serverIntent = new Intent(Main_ActivityPrinterLongin.this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                break;
            }

            case R.id.btn_close:{
                mService.stop();
                editText.setEnabled(false);
                imageViewPicture.setEnabled(false);
                width_58mm.setEnabled(false);
                width_80.setEnabled(false);
                hexBox.setEnabled(false);
                sendButton.setEnabled(false);
                testButton.setEnabled(false);
                printbmpButton.setEnabled(false);
                btnClose.setEnabled(false);
                btn_BMP.setEnabled(false);
                btn_ChoseCommand.setEnabled(false);
                btn_prtcodeButton.setEnabled(false);
                btn_prtsma.setEnabled(false);
                btn_prttableButton.setEnabled(false);
                btn_camer.setEnabled(false);
                btn_scqrcode.setEnabled(false);
                btnScanButton.setEnabled(true);
                Simplified.setEnabled(false);
                Korean.setEnabled(false);
                big5.setEnabled(false);
                thai.setEnabled(false);
                btnScanButton.setText(getText(R.string.connect));
                break;
            }
            case R.id.btn_test:{
                CancelledBill();
                Intent i= new Intent(Main_ActivityPrinterLongin.this,CartActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case R.id.btn_testt:{
                CancelledBill();
                Intent i= new Intent(Main_ActivityPrinterLongin.this,CartActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case R.id.Send_Button:{
                if (hexBox.isChecked()) {
                    String str = editText.getText().toString().trim();//Ã¥Å½Â»Ã¦Å½â€°Ã¥Â¤Â´Ã¥Â°Â¾Ã§Â©ÂºÃ§â„¢Â½
                    if(str.length() > 0){
                        str = Other.RemoveChar(str, ' ').toString();
                        if (str.length() <= 0)
                            return;
                        if ((str.length() % 2) != 0) {
                            Toast.makeText(getApplicationContext(), getString(R.string.msg_state),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        byte[] buf = Other.HexStringToBytes(str);
                        SendDataByte(buf);
                    }else{
                        Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String msg = editText.getText().toString();
                    if(msg.length()>0){
                        if(thai.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, THAI, 255, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(big5.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, BIG5, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(Korean.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, KOREAN, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(Simplified.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }
                    }else{
                        Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
            case R.id.width_58mm:
            case R.id.width_80mm:{
                is58mm = v == width_58mm;
                width_58mm.setChecked(is58mm);
                width_80.setChecked(!is58mm);
                break;
            }
            case R.id.btn_completeorderprint:{
                CompletedOrderBill();
                // AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                Intent i= new Intent(Main_ActivityPrinterLongin.this,NavigationMainActivity.class);
                startActivity(i);
                finish();
                break;
            }case R.id.btn_printpicture:{
                CompletedBill();
                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                Intent i= new Intent(Main_ActivityPrinterLongin.this,CartActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case R.id.btn_printpictureee:{
                CompletedBill();
                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                Intent i= new Intent(Main_ActivityPrinterLongin.this,CartActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case R.id.btn_close_restraunt:{
                CloseRestrauntBill();
               /* AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                Intent i= new Intent(Main_ActivityPrinterLongin.this,CartActivity.class);
                startActivity(i);*/
                finish();
                break;
            }
            case R.id.imageViewPictureUSB:{
                Intent loadpicture = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(loadpicture, REQUEST_CHOSE_BMP);
                break;
            }
            case R.id.btn_myorder:{

                myorderprint();
                Intent i= new Intent(Main_ActivityPrinterLongin.this,MyOrderActivity.class);
                startActivity(i);
                finish();
                break;


            }
            case R.id.btn_myorder_expnd:{

                CompletedOrderBill();
                // CompletedBill();
                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                Intent i= new Intent(Main_ActivityPrinterLongin.this,MyOrderActivity.class);
                startActivity(i);
                finish();
                break;


            }
            case R.id.btn_report_expnd:{

                CompletedOrderBill();
                //  CompletedBill();
                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                finish();
                break;


            }
            case R.id.btn_prtcommand:{
                CommandTest();
                break;
            }
            case R.id.btn_prtsma:{
                SendDataByte(Command.ESC_Init);
                SendDataByte(Command.LF);
                Print_Ex();
                break;
            }
            case R.id.btn_prttable:{
                SendDataByte(Command.ESC_Init);
                SendDataByte(Command.LF);
                PrintTable();
                break;
            }
            case R.id.btn_prtbarcode:{
                printBarCode();
                break;
            }
            case R.id.btn_scqr:{
                createImage();
                break;
            }
            case R.id.btn_dyca:{
                dispatchTakePictureIntent(REQUEST_CAMER);
                break;
            }
            default:
                break;
        }
    }

    /*****************************************************************************************************/
    /*
     * SendDataString
     */
    private void SendDataString(String data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
           /* Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();*/
            return;
        }
        if (data.length() > 0) {
            try {
                mService.write(data.getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     *SendDataByte
     */
    private void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
           /* Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();*/
            return;
        }
        mService.write(data);
    }

    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (DEBUG)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            /*   mTitle.setText(R.string.title_connected_to);*/
                            /* mTitle.append(mConnectedDeviceName);*/
                            btnScanButton.setText(getText(R.string.Connecting));
                            btnScanButton.setEnabled(false);
                            AppSession.getInstance().setMenuprinter(" ");
                            editText.setEnabled(true);
                            imageViewPicture.setEnabled(true);
                            width_58mm.setEnabled(true);
                            width_80.setEnabled(true);
                            hexBox.setEnabled(true);
                            sendButton.setEnabled(true);
                            testButton.setEnabled(true);
                            printbmpButton.setEnabled(true);
                            btnClose.setEnabled(true);
                            btn_BMP.setEnabled(true);
                            btn_ChoseCommand.setEnabled(true);
                            btn_prtcodeButton.setEnabled(true);
                            btn_prtsma.setEnabled(true);
                            btn_prttableButton.setEnabled(true);
                            btn_camer.setEnabled(true);
                            btn_scqrcode.setEnabled(true);
                            Simplified.setEnabled(true);
                            Korean.setEnabled(true);
                            big5.setEnabled(true);
                            thai.setEnabled(true);

                            /*if(a.equals("123"))
                            {
                                Intent i = new Intent(Main_ActivityPrinterLongin.this, NavigationMainActivity.class);
                                startActivity(i);
                            }else{

                            }*/
                           /*Intent i = new Intent(Main_ActivityPrinterLongin.this, NavigationMainActivity.class);
                            startActivity(i);*/
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            /*mTitle.setText(R.string.title_connecting);*/
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:

                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();

                    btnCloseprint.setEnabled(true);
                    btnbillprint.setEnabled(true);
                    btnmyorder.setEnabled(true);
                    btn_myorder_expnd.setEnabled(true);
                    btn_close_order.setEnabled(true);
                    btn_close_restraunt.setEnabled(true);
                    ppprint.setEnabled(true);
                    btn_report_expnd.setEnabled(true);
                    selectionprint.setEnabled(true);

                    break;
                case MESSAGE_TOAST:
                   /* Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();*/
                    break;
                case MESSAGE_CONNECTION_LOST:    //Ã¨â€œÂÃ§â€°â„¢Ã¥Â·Â²Ã¦â€“Â­Ã¥Â¼â‚¬Ã¨Â¿Å¾Ã¦Å½Â¥
                    /*Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();*/
                    editText.setEnabled(false);
                    imageViewPicture.setEnabled(false);
                    width_58mm.setEnabled(false);
                    width_80.setEnabled(false);
                    hexBox.setEnabled(false);
                    sendButton.setEnabled(false);
                    testButton.setEnabled(false);
                    printbmpButton.setEnabled(false);
                    btnClose.setEnabled(false);
                    btn_BMP.setEnabled(false);
                    btn_ChoseCommand.setEnabled(false);
                    btn_prtcodeButton.setEnabled(false);
                    btn_prtsma.setEnabled(false);
                    btn_prttableButton.setEnabled(false);
                    btn_camer.setEnabled(false);
                    btn_scqrcode.setEnabled(false);
                    Simplified.setEnabled(false);
                    Korean.setEnabled(false);
                    big5.setEnabled(false);
                    thai.setEnabled(false);
                    break;
                case MESSAGE_UNABLE_CONNECT:     //Ã¦â€” Ã¦Â³â€¢Ã¨Â¿Å¾Ã¦Å½Â¥Ã¨Â®Â¾Ã¥Â¤â€¡
                    /*Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();*/
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (DEBUG)
            Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:{
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    if(AppSession.getInstance().getBluetoothdevicename()!=null){
                        String address = AppSession.getInstance().getBluetoothdevicename();
                        mService.setState(STATE_CONNECTED);
                        // Get the BLuetoothDevice object
                        if (BluetoothAdapter.checkBluetoothAddress(address)) {
                            BluetoothDevice device = mBluetoothAdapter
                                    .getRemoteDevice(address);
                            // Attempt to connect to the device
                            mService.connect(device);
                            /*if(AppSession.getInstance().getCancelcart().equals("123")){
                                AppSession.getInstance().setCancelcart(" ");
                                CancelledBill();

                            }else  if(AppSession.getInstance().getcompletecart().equals("123")){
                                AppSession.getInstance().setcompletecart(" ");
                                CompletedBill();
                            }*/
                        }

                    }else {

                        String address = data.getExtras().getString(
                                DeviceListActivity.EXTRA_DEVICE_ADDRESS);

                        // Get the BLuetoothDevice object
                        if (BluetoothAdapter.checkBluetoothAddress(address)) {
                            BluetoothDevice device = mBluetoothAdapter
                                    .getRemoteDevice(address);
                            // Attempt to connect to the device
                            mService.connect(device);
                            if(AppSession.getInstance().getCancelcart().equals("123")){
                                CancelledBill();

                            }else  if(AppSession.getInstance().getcompletecart().equals("123")){
                                CompletedBill();
                                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                            }
                            else  if(AppSession.getInstance().getcompletecart().equals("124")){
                                CloseRestrauntBill();
                            }
                            else  if(AppSession.getInstance().getcompletecart().equals("125")){
                                CompletedOrderBill();
                            }
                            else  if(AppSession.getInstance().getcompletecart().equals("126")){
                                flag_closeorderbill="126";
                                CompletedOrderBill();
                                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                            }
                            else  if(AppSession.getInstance().getcompletecart().equals("127")){
                                flag_closeorderbill="127";
                                CompletedOrderBill();
                                AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());
                            }
                            else  if(AppSession.getInstance().getCancelcart().equals("124")){

                                CancelledBill();
                            }
                            AppSession.getInstance().setCancelcart("");
                            AppSession.getInstance().setcompletecart("");
                        }



                    }


                }
                break;
            }
            case REQUEST_ENABLE_BT:{
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    KeyListenerInit();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled");
                   /* Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();*/
                    finish();
                }
                break;
            }
            case REQUEST_CHOSE_BMP:{
                if (resultCode == Activity.RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaColumns.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, opts);
                    opts.inJustDecodeBounds = false;
                    if (opts.outWidth > 1200) {
                        opts.inSampleSize = opts.outWidth / 1200;
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, opts);
                    if (null != bitmap) {
                        imageViewPicture.setImageBitmap(bitmap);
                    }
                }else{
                    Toast.makeText(this, getString(R.string.msg_statev1), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case REQUEST_CAMER:{
                if (resultCode == Activity.RESULT_OK){
                    handleSmallCameraPhoto(data);
                }else{
                    Toast.makeText(this, getText(R.string.camer), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }


    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¦Âµâ€¹Ã¨Â¯â€¢Ã©Â¡Âµ
     * @param mPrinter
     */
    private void CancelledBill() {
        String msg = "";
        String msgg = "";
        String lang = getString(R.string.strLang);
        String abc;
        if(AppSession.getInstance().getGuest_Count().equals("No of Guests")||AppSession.getInstance().getGuest_Count().equals("")){
            abc= "0";
        }else{
            abc= AppSession.getInstance().getGuest_Count();
        }
        String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
        String time = new SimpleDateFormat("HH:mm aa", Locale.getDefault()).format(new Date());
        if((lang.compareTo("en")) == 0 ){
            msgg = "CANCELLED BILL\n\n";
            msg =  AppSession.getInstance().getLocationbill_Name()+"\n"+
                    "Sales Date:-"+date+"\n" +
                    "Sales Time:-"+time+"\n" +
                    "Order Taken By:"+ AppSession.getInstance().getStaff_Name()/*+(AppSession.getInstance().getStaff_ID())*/+"\n" +
                    "------------------------------------------\n"+
                    "Table No:-"+ AppSession.getInstance().getTable_Name()+"\n"+
                    /* "Guest:- "+abc+"\n"+*/
                    "------------------------------------------\n"+
                    (AppSession.getInstance().getMenuquantity() +" X "+ AppSession.getInstance().getMenuname() + "  " + AppSession.getInstance().getMenuPrice() + "  " + AppSession.getInstance().getMenuPrice())+"\n"+
                    ""+AppSession.getInstance().getMenuname2()+"\n"+
                    "------------------------------------------\n";

            Command.ESC_Align[2] = 0x00;
            SendDataByte(Command.ESC_Align);
            SendDataString(msgg);
            SendDataString(msg);

            AppSession.getInstance().setMenuquantity("");
            AppSession.getInstance().setMenuPrice("");
            AppSession.getInstance().setMenuname("");
            //AppSession.getInstance().setTable_ID("");
            //AppSession.getInstance().setTable_Name("");
            DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).removeFromCart(AppSession.getInstance().getMenuid());
            DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).removeFromModifier(AppSession.getInstance().getMenuid());
         /*   DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearCart();
            DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearModifier();*/
           /* Intent i= new Intent(Main_ActivityPrinterLongin.this,NavigationMainActivity.class);
            startActivity(i);
            finish();*/

        }else if((lang.compareTo("cn")) == 0){
            msg = "Ã¦Ë†â€˜Ã¥ÂÂ¸Ã¦ËœÂ¯Ã¤Â¸â‚¬Ã¥Â®Â¶Ã©â€ºâ€ Ã§Â§â€˜Ã§ â€Ã¥Â¼â‚¬Ã¥Ââ€˜Ã£â‚¬ÂÃ§â€Å¸Ã¤ÂºÂ§Ã§Â»ÂÃ¨ÂÂ¥Ã¥â€™Å’Ã¦Å“ÂÃ¥Å Â¡Ã¤ÂºÅ½Ã¤Â¸â‚¬Ã¤Â½â€œÃ§Å¡â€žÃ©Â«ËœÃ¦Å â‚¬Ã¦Å“Â¯Ã§ â€Ã¥Ââ€˜Ã£â‚¬ÂÃ§â€Å¸Ã¤ÂºÂ§Ã¥Å¾â€¹Ã¤Â¼ÂÃ¤Â¸Å¡Ã¯Â¼Å’Ã¤Â¸â€œÃ¤Â¸Å¡Ã¤Â»Å½Ã¤Âºâ€¹Ã©â€¡â€˜Ã¨Å¾ÂÃ£â‚¬ÂÃ¥â€¢â€ Ã¤Â¸Å¡Ã©â€ºÂ¶Ã¥â€Â®Ã£â‚¬ÂÃ©Â¤ÂÃ©Â¥Â®Ã£â‚¬ÂÃ©â€¦â€™Ã¥ÂÂ§Ã£â‚¬ÂÃ¦Â­Å’Ã¥ÂÂ§Ã§Â­â€°Ã©Â¢â€ Ã¥Å¸Å¸Ã§Å¡â€žPOSÃ§Â»Ë†Ã§Â«Â¯Ã£â‚¬ÂÃ¨Â®Â¡Ã§Â®â€”Ã¦Å“ÂºÃ§Â»Ë†Ã§Â«Â¯Ã£â‚¬ÂÃ¨â€¡ÂªÃ¥Å Â©Ã§Â»Ë†Ã§Â«Â¯Ã¥â€˜Â¨Ã¨Â¾Â¹Ã©â€¦ÂÃ¥Â¥â€”Ã¨Â®Â¾Ã¥Â¤â€¡Ã§Å¡â€žÃ§ â€Ã¥Ââ€˜Ã£â‚¬ÂÃ¥Ë†Â¶Ã©â‚¬ Ã¥ÂÅ Ã©â€â‚¬Ã¥â€Â®Ã¯Â¼Â\nÃ¥â€¦Â¬Ã¥ÂÂ¸Ã§Å¡â€žÃ§Â»â€žÃ§Â»â€¡Ã¦Å“ÂºÃ¦Å¾â€žÃ§Â®â‚¬Ã§Â»Æ’Ã¥Â®Å¾Ã§â€Â¨Ã¯Â¼Å’Ã¤Â½Å“Ã©Â£Å½Ã¥Å Â¡Ã¥Â®Å¾Ã¤Â¸Â¥Ã¨Â°Â¨Ã¯Â¼Å’Ã¨Â¿ÂÃ¨Â¡Å’Ã©Â«ËœÃ¦â€¢Ë†Ã£â‚¬â€šÃ¨Â¯Å¡Ã¤Â¿Â¡Ã£â‚¬ÂÃ¦â€¢Â¬Ã¤Â¸Å¡Ã£â‚¬ÂÃ¥â€ºÂ¢Ã§Â»â€œÃ£â‚¬ÂÃ©Â«ËœÃ¦â€¢Ë†Ã¦ËœÂ¯Ã¥â€¦Â¬Ã¥ÂÂ¸Ã§Å¡â€žÃ¤Â¼ÂÃ¤Â¸Å¡Ã§Ââ€ Ã¥Â¿ÂµÃ¥â€™Å’Ã¤Â¸ÂÃ¦â€“Â­Ã¨Â¿Â½Ã¦Â±â€šÃ¤Â»Å Ã¥Â¤Â©Ã¯Â¼Å’Ã¦Å“ÂÃ¦Â°â€Ã¨â€œÂ¬Ã¥â€¹Æ’Ã¯Â¼Å’Ã¥â€¦Â¬Ã¥ÂÂ¸Ã¥Â°â€ Ã¤Â»Â¥Ã©â€ºâ€žÃ¥Å½Å¡Ã§Å¡â€žÃ§Â§â€˜Ã¦Å â‚¬Ã¥Å â€ºÃ©â€¡ÂÃ¯Â¼Å’Ã¦Â°Â¸Ã¦Ââ€™Ã§Å¡â€žÃ¥Ë†â€ºÃ¤Â¸Å¡Ã§Â²Â¾Ã§Â¥Å¾Ã¯Â¼Å’Ã¤Â¸ÂÃ¦â€“Â­Ã¥Â¼â‚¬Ã¦â€¹â€œÃ¥Ë†â€ºÃ¦â€“Â°Ã§Å¡â€žÃ¥Â§Â¿Ã¦â‚¬ÂÃ¯Â¼Å’Ã¥â€¦â€¦Ã¦Â»Â¡Ã¤Â¿Â¡Ã¥Â¿Æ’Ã§Å¡â€žÃ¦Å“ÂÃ§Ââ‚¬Ã¥â€ºÂ½Ã©â„¢â€¦Ã¥Å’â€“Ã¤Â¿Â¡Ã¦ÂÂ¯Ã¤ÂºÂ§Ã¤Â¸Å¡Ã©Â¢â€ Ã¥Å¸Å¸Ã¯Â¼Å’Ã¤Â¸Å½Ã¦Å“â€¹Ã¥Ââ€¹Ã¤Â»Â¬Ã¦ÂÂºÃ¦â€°â€¹Ã¥â€¦Â±Ã¥Ë†â€ºÃ¤Â¿Â¡Ã¦ÂÂ¯Ã¤ÂºÂ§Ã¤Â¸Å¡Ã§Å¡â€žÃ¨Â¾â€°Ã§â€¦Å’!!!\n\n\n";
            SendDataString(msg);
        }else if((lang.compareTo("hk")) == 0){
            msg = "Ã¦Ë†â€˜Ã¥ÂÂ¸Ã¦ËœÂ¯Ã¤Â¸â‚¬Ã¥Â®Â¶Ã©â€ºâ€ Ã§Â§â€˜Ã§ â€Ã©â€“â€¹Ã§â„¢Â¼Ã£â‚¬ÂÃ§â€Å¸Ã§â€Â¢Ã§Â¶â€œÃ§â€¡Å¸Ã¥â€™Å’Ã¦Å“ÂÃ¥â€¹â„¢Ã¦â€“Â¼Ã¤Â¸â‚¬Ã©Â«â€Ã§Å¡â€žÃ©Â«ËœÃ¦Å â‚¬Ã¨Â¡â€œÃ§ â€Ã§â„¢Â¼Ã£â‚¬ÂÃ§â€Å¸Ã§â€Â¢Ã¥Å¾â€¹Ã¤Â¼ÂÃ¦Â¥Â­Ã¯Â¼Å’Ã¥Â°Ë†Ã¦Â¥Â­Ã¥Â¾Å¾Ã¤Âºâ€¹Ã©â€¡â€˜Ã¨Å¾ÂÃ£â‚¬ÂÃ¥â€¢â€ Ã¦Â¥Â­Ã©â€ºÂ¶Ã¥â€Â®Ã£â‚¬ÂÃ©Â¤ÂÃ©Â£Â²Ã£â‚¬ÂÃ©â€¦â€™Ã¥ÂÂ§Ã£â‚¬ÂÃ¦Â­Å’Ã¥ÂÂ§Ã§Â­â€°Ã© ËœÃ¥Å¸Å¸Ã§Å¡â€žPOSÃ§Âµâ€šÃ§Â«Â¯Ã£â‚¬ÂÃ¨Â¨Ë†Ã§Â®â€”Ã¦Â©Å¸Ã§Âµâ€šÃ§Â«Â¯Ã£â‚¬ÂÃ¨â€¡ÂªÃ¥Å Â©Ã§Âµâ€šÃ§Â«Â¯Ã¥â€˜Â¨Ã©â€šÅ Ã©â€¦ÂÃ¥Â¥â€”Ã¨Â¨Â­Ã¥â€šâ„¢Ã§Å¡â€žÃ§ â€Ã§â„¢Â¼Ã£â‚¬ÂÃ¨Â£Â½Ã©â‚¬ Ã¥ÂÅ Ã©Å Â·Ã¥â€Â®Ã¯Â¼Â \nÃ¥â€¦Â¬Ã¥ÂÂ¸Ã§Å¡â€žÃ§Âµâ€žÃ§Â¹â€Ã¦Â©Å¸Ã¦Â§â€¹Ã§Â°Â¡Ã§Â·Â´Ã¥Â¯Â¦Ã§â€Â¨Ã¯Â¼Å’Ã¤Â½Å“Ã©Â¢Â¨Ã¥â€¹â„¢Ã¥Â¯Â¦Ã¥Å¡Â´Ã¨Â¬Â¹Ã¯Â¼Å’Ã©Ââ€¹Ã¨Â¡Å’Ã©Â«ËœÃ¦â€¢Ë†Ã£â‚¬â€šÃ¨Âª Ã¤Â¿Â¡Ã£â‚¬ÂÃ¦â€¢Â¬Ã¦Â¥Â­Ã£â‚¬ÂÃ¥Å“ËœÃ§ÂµÂÃ£â‚¬ÂÃ©Â«ËœÃ¦â€¢Ë†Ã¦ËœÂ¯Ã¥â€¦Â¬Ã¥ÂÂ¸Ã§Å¡â€žÃ¤Â¼ÂÃ¦Â¥Â­Ã§Ââ€ Ã¥Â¿ÂµÃ¥â€™Å’Ã¤Â¸ÂÃ¦â€“Â·Ã¨Â¿Â½Ã¦Â±â€šÃ¤Â»Å Ã¥Â¤Â©Ã¯Â¼Å’Ã¦Å“ÂÃ¦Â°Â£Ã¨â€œÂ¬Ã¥â€¹Æ’Ã¯Â¼Å’Ã¥â€¦Â¬Ã¥ÂÂ¸Ã¥Â°â€¡Ã¤Â»Â¥Ã©â€ºâ€žÃ¥Å½Å¡Ã§Å¡â€žÃ§Â§â€˜Ã¦Å â‚¬Ã¥Å â€ºÃ©â€¡ÂÃ¯Â¼Å’Ã¦Â°Â¸Ã¦Ââ€ Ã§Å¡â€žÃ¥â€°ÂµÃ¦Â¥Â­Ã§Â²Â¾Ã§Â¥Å¾Ã¯Â¼Å’Ã¤Â¸ÂÃ¦â€“Â·Ã©â€“â€¹Ã¦â€¹â€œÃ¥â€°ÂµÃ¦â€“Â°Ã§Å¡â€žÃ¥Â§Â¿Ã¦â€¦â€¹Ã¯Â¼Å’Ã¥â€¦â€¦Ã¦Â»Â¿Ã¤Â¿Â¡Ã¥Â¿Æ’Ã§Å¡â€žÃ¦Å“ÂÃ¨â€˜â€”Ã¥Å“â€¹Ã©Å¡â€ºÃ¥Å’â€“Ã¤Â¿Â¡Ã¦ÂÂ¯Ã§â€Â¢Ã¦Â¥Â­Ã© ËœÃ¥Å¸Å¸Ã¯Â¼Å’Ã¨Ë†â€¡Ã¦Å“â€¹Ã¥Ââ€¹Ã¥â‚¬â€˜Ã¦â€Å“Ã¦â€°â€¹Ã¥â€¦Â±Ã¥â€°ÂµÃ¤Â¿Â¡Ã¦ÂÂ¯Ã§â€Â¢Ã¦Â¥Â­Ã§Å¡â€žÃ¨Â¼ÂÃ§â€¦Å’!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, BIG5, 0, 0, 0, 0));
        }else if((lang.compareTo("kor")) == 0){
            msg = "Ã«Â¶â‚¬Ã«Â¬Â¸ IÃ«Å â€ ÃªÂ¸Ë†Ã¬Å“Âµ, Ã¬â€ Å’Ã«Â§Â¤, Ã« Ë†Ã¬Å Â¤Ã­â€  Ã«Å¾â€˜, Ã«Â°â€, Ã«â€¦Â¸Ã«Å¾Ëœ Ã«Â°Â ÃªÂ¸Â°Ã­Æ’â‚¬ Ã«Â¶â€žÃ¬â€¢Â¼, Ã¬Â»Â´Ã­â€œÂ¨Ã­â€žÂ° Ã«â€¹Â¨Ã«Â§ÂÃªÂ¸Â°, Ã¬â€¦â‚¬Ã­â€â€ž Ã¬â€žÅ“Ã«Â¹â€žÃ¬Å Â¤ Ã­â€žÂ°Ã«Â¯Â¸Ã«â€žÂ Ã¬Â£Â¼Ã«Â³â‚¬ Ã¬Å¾Â¥Ã¬Â¹Ëœ POS Ã­â€žÂ°Ã«Â¯Â¸Ã«â€žÂÃ¬Ââ€ž Ã¬ â€žÃ«Â¬Â¸Ã¬Å“Â¼Ã«Â¡Å“ Ã­â€¢Å“ Ã¬Â²Â¨Ã«â€¹Â¨ ÃªÂ¸Â°Ã¬Ë†  Ã¬â€”Â°ÃªÂµÂ¬ Ã«Â°Â ÃªÂ°Å“Ã«Â°Å“, Ã¬Æ’ÂÃ¬â€šÂ° Ã¬Â§â‚¬Ã­â€“Â¥Ã¬ Â Ã¬ÂÂ¸ ÃªÂ¸Â°Ã¬â€”â€¦Ã¬ÂËœ Ã¬â€”Â°ÃªÂµÂ¬ Ã«Â°Â ÃªÂ°Å“Ã«Â°Å“, Ã¬Æ’ÂÃ¬â€šÂ° Ã«Â°Â Ã¬â€žÅ“Ã«Â¹â€žÃ¬Å Â¤Ã¬Å¾â€¦Ã«â€¹Ë†Ã«â€¹Â¤ R & D, Ã¬ Å“Ã¬Â¡Â° Ã«Â°Â Ã­Å’ÂÃ«Â§Â¤! \n Ã­Å¡Å’Ã¬â€šÂ¬Ã¬ÂËœ Ã¬Â¡Â°Ã¬Â§Â ÃªÂµÂ¬Ã¬Â¡Â°Ã¬ÂËœ ÃªÂ°â€žÃªÂ²Â°Ã­â€¢ËœÃªÂ³  Ã¬â€”â€žÃªÂ²Â©Ã­â€¢Å“, Ã­Å¡Â¨Ã¬Å“Â¨Ã¬ ÂÃ¬ÂÂ¸ Ã¬Å¡Â´Ã¬ËœÂÃ¬ÂËœ Ã¬â€¹Â¤Ã¬ Å“, Ã¬â€¹Â¤Ã¬Å¡Â©Ã¬ ÂÃ¬ÂÂ¸ Ã¬Å Â¤Ã­Æ’â‚¬Ã¬ÂÂ¼. Ã«Â¬Â´ÃªÂ²Â°Ã¬â€žÂ±, Ã­â€”Å’Ã¬â€¹ , Ã«â€¹Â¨ÃªÂ²Â°, Ã­Å¡Â¨Ã¬Å“Â¨Ã¬ ÂÃ¬ÂÂ¸ Ã­Å¡Å’Ã¬â€šÂ¬Ã¬ÂËœ ÃªÂ¸Â°Ã¬â€”â€¦ Ã¬Â² Ã­â€¢â„¢Ã¬ÂÂ´Ã«Â©Â°, Ã¬Â§â‚¬Ã¬â€ ÂÃ¬ ÂÃ¬Å“Â¼Ã«Â¡Å“, Ã­â„¢Å“ÃªÂ¸Â°Ã¬Â°Â¬,Ã¬ÂÂ´ Ã­Å¡Å’Ã¬â€šÂ¬Ã«Å â€ ÃªÂ°â€¢Ã« Â¥Ã­â€¢Å“ ÃªÂ³Â¼Ã­â€¢â„¢ ÃªÂ¸Â°Ã¬Ë†  ÃªÂ°â€¢Ã«Ââ€ž, ÃªÂ¸Â°Ã¬â€”â€¦ÃªÂ°â‚¬ Ã¬ â€¢Ã¬â€¹ Ã¬ÂËœ Ã¬ËœÂÃ¬â€ºÂÃ­â€¢Å“ Ã¬ â€¢Ã¬â€¹ Ã¬ÂÂ´ Ã«Â  ÃªÂ²Æ’Ã¬Å¾â€¦Ã«â€¹Ë†Ã«â€¹Â¤ Ã¬ËœÂ¤Ã«Å ËœÃ¬Ââ€ž Ã¬Å“â€žÃ­â€¢Â´ Ã«â€¦Â¸Ã« Â¥, ÃªÂ°Å“Ã¬Â²â„¢ÃªÂ³Â¼ Ã­ËœÂÃ¬â€¹ Ã¬ ÂÃ¬ÂÂ¸ Ã­Æ’Å“Ã«Ââ€ž, ÃªÂµÂ­Ã¬ Å“ Ã¬ â€¢Ã«Â³Â´ Ã¬â€šÂ°Ã¬â€”â€¦Ã¬Ââ€ž Ã­â€“Â¥Ã­â€¢Â´ Ã¬Å¾ÂÃ¬â€¹ ÃªÂ°Â, Ã¬Â¹Å“ÃªÂµÂ¬Ã¬â„¢â‚¬ Ã­â€¢Â¨ÃªÂ»Ëœ Ã­â„¢â€Ã« Â¤Ã­â€¢Å“ Ã¬ â€¢Ã«Â³Â´ Ã¬â€šÂ°Ã¬â€”â€¦Ã¬Ââ€ž Ã«Â§Å’Ã«â€œÂ¤ Ã¬Ë†Ëœ Ã¬Å¾Ë†Ã¬Å ÂµÃ«â€¹Ë†Ã«â€¹Â¤!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, KOREAN, 0, 0, 0, 0));
        }else if((lang.compareTo("thai")) == 0){
            msg = "Ã Â¸ÂªÃ Â¹Ë†Ã Â¸Â§Ã Â¸â„¢Ã Â¸â€°Ã Â¸Â±Ã Â¸â„¢Ã Â¸â€žÃ Â¸Â·Ã Â¸Â­Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Â§Ã Â¸Â´Ã Â¸Ë†Ã Â¸Â±Ã Â¸Â¢Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å¾Ã Â¸Â±Ã Â¸â€™Ã Â¸â„¢Ã Â¸Â²Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å“Ã Â¸Â¥Ã Â¸Â´Ã Â¸â€¢Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¹Æ’Ã Â¸â„¢Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Â§Ã Â¸Â´Ã Â¸Ë†Ã Â¸Â±Ã Â¸Â¢Ã Â¸Â«Ã Â¸â„¢Ã Â¸Â¶Ã Â¹Ë†Ã Â¸â€¡Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¡Ã Â¸ÂµÃ Â¹â‚¬Ã Â¸â€”Ã Â¸â€žÃ Â¹â€šÃ Â¸â„¢Ã Â¹â€šÃ Â¸Â¥Ã Â¸Â¢Ã Â¸ÂµÃ Â¸ÂªÃ Â¸Â¹Ã Â¸â€¡Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å¾Ã Â¸Â±Ã Â¸â€™Ã Â¸â„¢Ã Â¸Â²Ã Â¸ÂªÃ Â¸â€“Ã Â¸Â²Ã Â¸â„¢Ã Â¸â€ºÃ Â¸Â£Ã Â¸Â°Ã Â¸ÂÃ Â¸Â­Ã Â¸Å¡Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å“Ã Â¸Â¥Ã Â¸Â´Ã Â¸â€¢Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¡Ã Â¸Â¸Ã Â¹Ë†Ã Â¸â€¡Ã Â¹â‚¬Ã Â¸â„¢Ã Â¹â€°Ã Â¸â„¢Ã Â¸â€žÃ Â¸Â§Ã Â¸Â²Ã Â¸Â¡Ã Â¹â‚¬Ã Â¸Å Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¢Ã Â¸Â§Ã Â¸Å Ã Â¸Â²Ã Â¸ÂÃ Â¹Æ’Ã Â¸â„¢Ã Â¸â€šÃ Â¸Â±Ã Â¹â€°Ã Â¸Â§ POS Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¹â‚¬Ã Â¸â€¡Ã Â¸Â´Ã Â¸â„¢, Ã Â¸â€žÃ Â¹â€°Ã Â¸Â²Ã Â¸â€ºÃ Â¸Â¥Ã Â¸ÂµÃ Â¸Â, Ã Â¸Â£Ã Â¹â€°Ã Â¸Â²Ã Â¸â„¢Ã Â¸Â­Ã Â¸Â²Ã Â¸Â«Ã Â¸Â²Ã Â¸Â£, Ã Â¸Å¡Ã Â¸Â²Ã Â¸Â£Ã Â¹Å’, Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â¥Ã Â¸â€¡Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸Å¾Ã Â¸Â·Ã Â¹â€°Ã Â¸â„¢Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â­Ã Â¸Â·Ã Â¹Ë†Ã Â¸â„¢ Ã Â¹â€  , Ã Â¹â‚¬Ã Â¸â€žÃ Â¸Â£Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸â€¡Ã Â¸â€žÃ Â¸Â­Ã Â¸Â¡Ã Â¸Å¾Ã Â¸Â´Ã Â¸Â§Ã Â¹â‚¬Ã Â¸â€¢Ã Â¸Â­Ã Â¸Â£Ã Â¹Å’, Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸â€¢Ã Â¸â„¢Ã Â¹â‚¬Ã Â¸Â­Ã Â¸â€¡Ã Â¸â€šÃ Â¸Â±Ã Â¹â€°Ã Â¸Â§Ã Â¸Â­Ã Â¸Â¸Ã Â¸â€ºÃ Â¸ÂÃ Â¸Â£Ã Â¸â€œÃ Â¹Å’Ã Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¸Å¾Ã Â¹Ë†Ã Â¸Â§Ã Â¸â€¡ R & D, Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸Å“Ã Â¸Â¥Ã Â¸Â´Ã Â¸â€¢Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸Â¢Ã Â¸Â­Ã Â¸â€Ã Â¸â€šÃ Â¸Â²Ã Â¸Â¢! \n Ã Â¸ÂÃ Â¸Â£Ã Â¸Â°Ã Â¸Å Ã Â¸Â±Ã Â¸Å¡Ã Â¹â€šÃ Â¸â€žÃ Â¸Â£Ã Â¸â€¡Ã Â¸ÂªÃ Â¸Â£Ã Â¹â€°Ã Â¸Â²Ã Â¸â€¡ Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”  Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸Â­Ã Â¸â€¡Ã Â¸â€žÃ Â¹Å’Ã Â¸ÂÃ Â¸Â£Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸â€ºÃ Â¸ÂÃ Â¸Â´Ã Â¸Å¡Ã Â¸Â±Ã Â¸â€¢Ã Â¸Â´Ã Â¹Æ’Ã Â¸â„¢Ã Â¸â€”Ã Â¸Â²Ã Â¸â€¡Ã Â¸â€ºÃ Â¸ÂÃ Â¸Â´Ã Â¸Å¡Ã Â¸Â±Ã Â¸â€¢Ã Â¸Â´Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸ÂªÃ Â¹â€žÃ Â¸â€¢Ã Â¸Â¥Ã Â¹Å’Ã Â¸Â­Ã Â¸Â¢Ã Â¹Ë†Ã Â¸Â²Ã Â¸â€¡Ã Â¹â‚¬Ã Â¸â€šÃ Â¹â€°Ã Â¸Â¡Ã Â¸â€¡Ã Â¸Â§Ã Â¸â€Ã Â¸â€Ã Â¸Â³Ã Â¹â‚¬Ã Â¸â„¢Ã Â¸Â´Ã Â¸â„¢Ã Â¸â€¡Ã Â¸Â²Ã Â¸â„¢Ã Â¸Â¡Ã Â¸ÂµÃ Â¸â€ºÃ Â¸Â£Ã Â¸Â°Ã Â¸ÂªÃ Â¸Â´Ã Â¸â€”Ã Â¸ËœÃ Â¸Â´Ã Â¸ Ã Â¸Â²Ã Â¸Å¾ Ã Â¸â€žÃ Â¸Â§Ã Â¸Â²Ã Â¸Â¡Ã Â¸â€¹Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸ÂªÃ Â¸Â±Ã Â¸â€¢Ã Â¸Â¢Ã Â¹Å’Ã Â¸â€”Ã Â¸Â¸Ã Â¹Ë†Ã Â¸Â¡Ã Â¹â‚¬Ã Â¸â€”Ã Â¸â€žÃ Â¸Â§Ã Â¸Â²Ã Â¸Â¡Ã Â¸ÂªÃ Â¸Â²Ã Â¸Â¡Ã Â¸Â±Ã Â¸â€žÃ Â¸â€žÃ Â¸ÂµÃ Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸Â¡Ã Â¸ÂµÃ Â¸â€ºÃ Â¸Â£Ã Â¸Â°Ã Â¸ÂªÃ Â¸Â´Ã Â¸â€”Ã Â¸ËœÃ Â¸Â´Ã Â¸ Ã Â¸Â²Ã Â¸Å¾Ã Â¸â€žÃ Â¸Â·Ã Â¸Â­Ã Â¸â€ºÃ Â¸Â£Ã Â¸Â±Ã Â¸Å Ã Â¸ÂÃ Â¸Â²Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸Â­Ã Â¸â€¡Ã Â¸â€žÃ Â¹Å’Ã Â¸ÂÃ Â¸Â£Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡ Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€” Ã Â¸Â­Ã Â¸Â¢Ã Â¹Ë†Ã Â¸Â²Ã Â¸â€¡Ã Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¹â‚¬Ã Â¸â„¢Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸â€¡Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸Â¡Ã Â¸Â¸Ã Â¹Ë†Ã Â¸â€¡Ã Â¸Â¡Ã Â¸Â±Ã Â¹Ë†Ã Â¸â„¢Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸Â§Ã Â¸Â±Ã Â¸â„¢Ã Â¸â„¢Ã Â¸ÂµÃ Â¹â€°Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸ÂªÃ Â¸â€Ã Â¹Æ’Ã Â¸ÂªÃ Â¸â€šÃ Â¸Â­Ã Â¸â€¡ Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€” Ã Â¸Ë†Ã Â¸Â°Ã Â¸Â¡Ã Â¸ÂµÃ Â¸ÂÃ Â¸Â³Ã Â¸Â¥Ã Â¸Â±Ã Â¸â€¡Ã Â¹ÂÃ Â¸Â£Ã Â¸â€¡Ã Â¸â€šÃ Â¸Â¶Ã Â¹â€°Ã Â¸â„¢Ã Â¸â€”Ã Â¸Â²Ã Â¸â€¡Ã Â¸Â§Ã Â¸Â´Ã Â¸â€”Ã Â¸Â¢Ã Â¸Â²Ã Â¸Â¨Ã Â¸Â²Ã Â¸ÂªÃ Â¸â€¢Ã Â¸Â£Ã Â¹Å’Ã Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¹â‚¬Ã Â¸â€”Ã Â¸â€žÃ Â¹â€šÃ Â¸â„¢Ã Â¹â€šÃ Â¸Â¥Ã Â¸Â¢Ã Â¸ÂµÃ Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¹ÂÃ Â¸â€šÃ Â¹â€¡Ã Â¸â€¡Ã Â¹ÂÃ Â¸ÂÃ Â¸Â£Ã Â¹Ë†Ã Â¸â€¡Ã Â¸Ë†Ã Â¸Â´Ã Â¸â€¢Ã Â¸Â§Ã Â¸Â´Ã Â¸ÂÃ Â¸ÂÃ Â¸Â²Ã Â¸â€œÃ Â¸â„¢Ã Â¸Â´Ã Â¸Â£Ã Â¸Â±Ã Â¸â„¢Ã Â¸â€Ã Â¸Â£Ã Â¹Å’Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸Å“Ã Â¸Â¹Ã Â¹â€°Ã Â¸â€ºÃ Â¸Â£Ã Â¸Â°Ã Â¸ÂÃ Â¸Â­Ã Â¸Å¡Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¡Ã Â¸ÂµÃ Â¸â€”Ã Â¸Â±Ã Â¸Â¨Ã Â¸â„¢Ã Â¸â€žÃ Â¸â€¢Ã Â¸Â´Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¹â‚¬Ã Â¸â€ºÃ Â¹â€¡Ã Â¸â„¢Ã Â¸Å“Ã Â¸Â¹Ã Â¹â€°Ã Â¸Å¡Ã Â¸Â¸Ã Â¸ÂÃ Â¹â‚¬Ã Â¸Å¡Ã Â¸Â´Ã Â¸ÂÃ Â¹ÂÃ Â¸Â¥Ã Â¸Â°Ã Â¸â„¢Ã Â¸Â§Ã Â¸Â±Ã Â¸â€¢Ã Â¸ÂÃ Â¸Â£Ã Â¸Â£Ã Â¸Â¡Ã Â¸â€žÃ Â¸Â§Ã Â¸Â²Ã Â¸Â¡Ã Â¹â‚¬Ã Â¸Å Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸Â¡Ã Â¸Â±Ã Â¹Ë†Ã Â¸â„¢Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¡Ã Â¸ÂµÃ Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¸Â­Ã Â¸Â¸Ã Â¸â€¢Ã Â¸ÂªÃ Â¸Â²Ã Â¸Â«Ã Â¸ÂÃ Â¸Â£Ã Â¸Â£Ã Â¸Â¡Ã Â¸â€šÃ Â¹â€°Ã Â¸Â­Ã Â¸Â¡Ã Â¸Â¹Ã Â¸Â¥Ã Â¸Â£Ã Â¸Â°Ã Â¸Â«Ã Â¸Â§Ã Â¹Ë†Ã Â¸Â²Ã Â¸â€¡Ã Â¸â€ºÃ Â¸Â£Ã Â¸Â°Ã Â¹â‚¬Ã Â¸â€”Ã Â¸Â¨ Ã Â¸ÂÃ Â¸Â±Ã Â¸Å¡Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸â„¢ Ã Â¹â€  Ã Â¹Æ’Ã Â¸â„¢Ã Â¸ÂÃ Â¸Â²Ã Â¸Â£Ã Â¸ÂªÃ Â¸Â£Ã Â¹â€°Ã Â¸Â²Ã Â¸â€¡Ã Â¸Â­Ã Â¸Â¸Ã Â¸â€¢Ã Â¸ÂªÃ Â¸Â²Ã Â¸Â«Ã Â¸ÂÃ Â¸Â£Ã Â¸Â£Ã Â¸Â¡Ã Â¸â€šÃ Â¹â€°Ã Â¸Â­Ã Â¸Â¡Ã Â¸Â¹Ã Â¸Â¥Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¢Ã Â¸Â­Ã Â¸â€Ã Â¹â‚¬Ã Â¸Â¢Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Â¢Ã Â¸Â¡!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, THAI, 255, 0, 0, 0));
        }
    }





    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¦Âµâ€¹Ã¨Â¯â€¢Ã©Â¡Âµ
     * @param mPrinter
     */
    private void myorderprint() {
        String msg = "";
        String lang = getString(R.string.strLang);
        String abcd="";
        ArrayList<OrderList> headerList = new ArrayList<OrderList>();
        String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
        if((lang.compareTo("en")) == 0 ){
            for(int i = 0; i< headerList.size(); i++) {
                abcd="Order Id#"+ headerList.get(i).getOrder_id();
                abcd=abcd+"\n";
            }
            msg =  "Dampa feast sea food rest.Food court.\n"
                    +"TEL:-"+ AppSession.getInstance().getRestraunt_Telephone()+"\n" +
                    "Date:-"+date+"\n" +
                    "Order Taken By:-Waiter:"+ AppSession.getInstance().getStaff_Name()/*+(AppSession.getInstance().getStaff_ID())*/+"\n" +
                    "-------------------------------\n"
                    +"Selected orders Id:- "+abcd+"\n" +
                    "Check Print Dine In\n"+
                    "--------------------------------\n"+
                    "--------------------------------\n";


            SendDataString(msg);
        }
    }

    /*
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¥â€ºÂ¾Ã§â€°â€¡
     */
    private void Print_BMP(){

        // byte[] buffer = PrinterCommand.POS_Set_PrtInit();
        Bitmap mBitmap = ((BitmapDrawable) imageViewPicture.getDrawable())
                .getBitmap();
        int nMode = 0;
        int nPaperWidth = 384;
        if(width_58mm.isChecked())
            nPaperWidth = 384;
        else if (width_80.isChecked())
            nPaperWidth = 576;
        if(mBitmap != null)
        {
            /**
             * Parameters:
             * mBitmap  Ã¨Â¦ÂÃ¦â€°â€œÃ¥ÂÂ°Ã§Å¡â€žÃ¥â€ºÂ¾Ã§â€°â€¡
             * nWidth   Ã¦â€°â€œÃ¥ÂÂ°Ã¥Â®Â½Ã¥ÂºÂ¦Ã¯Â¼Ë†58Ã¥â€™Å’80Ã¯Â¼â€°
             * nMode    Ã¦â€°â€œÃ¥ÂÂ°Ã¦Â¨Â¡Ã¥Â¼Â
             * Returns: byte[]
             */
            byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
            // SendDataByte(buffer);
            SendDataByte(Command.ESC_Init);
            SendDataByte(Command.LF);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }
    }

    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¨â€¡ÂªÃ¥Â®Å¡Ã¤Â¹â€°Ã¨Â¡Â¨Ã¦ Â¼
     */
    @SuppressLint("SimpleDateFormat")
    private void PrintTable(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyyÃ¥Â¹Â´MMÃ¦Å“Ë†ddÃ¦â€”Â¥ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//Ã¨Å½Â·Ã¥Ââ€“Ã¥Â½â€œÃ¥â€°ÂÃ¦â€”Â¶Ã©â€”Â´
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if(is58mm){

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€œ\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¥Ââ€˜Ã§Â«â„¢Ã¢â€Æ’%-4sÃ¢â€Æ’Ã¥Ë†Â°Ã§Â«â„¢Ã¢â€Æ’%-6sÃ¢â€Æ’\n","Ã¦Â·Â±Ã¥Å“Â³","Ã¦Ë†ÂÃ©Æ’Â½").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¤Â»Â¶Ã¦â€¢Â°Ã¢â€Æ’%2d/%-3dÃ¢â€Æ’Ã¥Ââ€¢Ã¥ÂÂ·Ã¢â€Æ’%-8dÃ¢â€Æ’\n",1,222,555).getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¦â€Â¶Ã¤Â»Â¶Ã¤ÂºÂºÃ¢â€Æ’%-12sÃ¢â€Æ’\n","Ã£â‚¬ÂÃ©â‚¬ÂÃ£â‚¬â€˜Ã¦Âµâ€¹Ã¨Â¯â€¢/Ã¦Âµâ€¹Ã¨Â¯â€¢Ã¤ÂºÂº").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¤Â¸Å¡Ã¥Å Â¡Ã¥â€˜ËœÃ¢â€Æ’%-2sÃ¢â€Æ’Ã¥ÂÂÃ§Â§Â°Ã¢â€Æ’%-6sÃ¢â€Æ’\n","Ã¦Âµâ€¹Ã¨Â¯â€¢","Ã¦Â·Â±Ã¥Å“Â³").getBytes("GBK"),
                            String.format("Ã¢â€â€”Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€º\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Ã¨â€¡ÂªÃ¥Å Â¨Ã§â€Å¸Ã¦Ë†ÂÃ§Å¡â€ž catch Ã¥Ââ€”
                    e.printStackTrace();
                }
            }else {

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€œ\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¥Ââ€˜Ã§Â«â„¢Ã¢â€Æ’%-12sÃ¢â€Æ’Ã¥Ë†Â°Ã§Â«â„¢Ã¢â€Æ’%-14sÃ¢â€Æ’\n", "Ã¦Â·Â±Ã¥Å“Â³", "Ã¦Ë†ÂÃ©Æ’Â½").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¤Â»Â¶Ã¦â€¢Â°Ã¢â€Æ’%6d/%-7dÃ¢â€Æ’Ã¥Ââ€¢Ã¥ÂÂ·Ã¢â€Æ’%-16dÃ¢â€Æ’\n", 1, 222, 55555555).getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¦â€Â¶Ã¤Â»Â¶Ã¤ÂºÂºÃ¢â€Æ’%-28sÃ¢â€Æ’\n", "Ã£â‚¬ÂÃ©â‚¬ÂÃ£â‚¬â€˜Ã¦Âµâ€¹Ã¨Â¯â€¢/Ã¦Âµâ€¹Ã¨Â¯â€¢Ã¤ÂºÂº").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’Ã¤Â¸Å¡Ã¥Å Â¡Ã¥â€˜ËœÃ¢â€Æ’%-10sÃ¢â€Æ’Ã¥ÂÂÃ§Â§Â°Ã¢â€Æ’%-14sÃ¢â€Æ’\n", "Ã¦Âµâ€¹Ã¨Â¯â€¢", "Ã¦Â·Â±Ã¥Å“Â³").getBytes("GBK"),
                            String.format("Ã¢â€â€”Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€º\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Ã¨â€¡ÂªÃ¥Å Â¨Ã§â€Å¸Ã¦Ë†ÂÃ§Å¡â€ž catch Ã¥Ââ€”
                    e.printStackTrace();
                }
            }
        }else if((lang.compareTo("en")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd/ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//Ã¨Å½Â·Ã¥Ââ€“Ã¥Â½â€œÃ¥â€°ÂÃ¦â€”Â¶Ã©â€”Â´
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if(is58mm){

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€œ\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXÃ¢â€Æ’%-6sÃ¢â€Æ’XXXXÃ¢â€Æ’%-8sÃ¢â€Æ’\n","XXXX","XXXX").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXÃ¢â€Æ’%2d/%-3dÃ¢â€Æ’XXXXÃ¢â€Æ’%-8dÃ¢â€Æ’\n",1,222,555).getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXXXÃ¢â€Æ’%-18sÃ¢â€Æ’\n","Ã£â‚¬ÂXXÃ£â‚¬â€˜XXXX/XXXXXX").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXXXÃ¢â€Æ’%-2sÃ¢â€Æ’XXXXÃ¢â€Æ’%-8sÃ¢â€Æ’\n","XXXX","XXXX").getBytes("GBK"),
                            String.format("Ã¢â€â€”Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€º\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Ã¨â€¡ÂªÃ¥Å Â¨Ã§â€Å¸Ã¦Ë†ÂÃ§Å¡â€ž catch Ã¥Ââ€”
                    e.printStackTrace();
                }
            }else {

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€œ\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXÃ¢â€Æ’%-14sÃ¢â€Æ’XXXXÃ¢â€Æ’%-16sÃ¢â€Æ’\n", "XXXX", "XXXX").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXÃ¢â€Æ’%6d/%-7dÃ¢â€Æ’XXXXÃ¢â€Æ’%-16dÃ¢â€Æ’\n", 1, 222, 55555555).getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXXXÃ¢â€Æ’%-34sÃ¢â€Æ’\n", "Ã£â‚¬ÂXXÃ£â‚¬â€˜XXXX/XXXXXX").getBytes("GBK"),
                            String.format("Ã¢â€Â£Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€¢â€¹Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€Â³Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â«\n").getBytes("GBK"),
                            String.format("Ã¢â€Æ’XXXXXXÃ¢â€Æ’%-12sÃ¢â€Æ’XXXXÃ¢â€Æ’%-16sÃ¢â€Æ’\n", "XXXX", "XXXX").getBytes("GBK"),
                            String.format("Ã¢â€â€”Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€Â»Ã¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€ÂÃ¢â€â€º\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Ã¨â€¡ÂªÃ¥Å Â¨Ã§â€Å¸Ã¦Ë†ÂÃ§Å¡â€ž catch Ã¥Ââ€”
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¨â€¡ÂªÃ¥Â®Å¡Ã¤Â¹â€°Ã¥Â°ÂÃ§Â¥Â¨
     */
    @SuppressLint("SimpleDateFormat")
    private void Print_Ex(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyyÃ¥Â¹Â´MMÃ¦Å“Ë†ddÃ¦â€”Â¥ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//Ã¨Å½Â·Ã¥Ââ€“Ã¥Â½â€œÃ¥â€°ÂÃ¦â€”Â¶Ã©â€”Â´
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if (is58mm) {

                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Ã§Æ’Â­Ã¦â€¢ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº!", 0, 3, 6);//
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKEÃ¤Â¸â€œÃ¥Ââ€“Ã¥Âºâ€”\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Ã©â€”Â¨Ã¥Âºâ€”Ã¥ÂÂ·: 888888\nÃ¥Ââ€¢Ã¦ÂÂ®  S00003333\nÃ¦â€Â¶Ã©â€œÂ¶Ã¥â€˜ËœÃ¯Â¼Å¡1001\nÃ¥Ââ€¢Ã¦ÂÂ®Ã¦â€”Â¥Ã¦Å“Å¸Ã¯Â¼Å¡xxxx-xx-xx\nÃ¦â€°â€œÃ¥ÂÂ°Ã¦â€”Â¶Ã©â€”Â´Ã¯Â¼Å¡xxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Ã¥â€œÂÃ¥ÂÂ       Ã¦â€¢Â°Ã©â€¡Â    Ã¥Ââ€¢Ã¤Â»Â·    Ã©â€¡â€˜Ã©Â¢Â\nNIKEÃ¨Â·â€˜Ã©Å¾â€¹   10.00   899     8990\nNIKEÃ§Â¯Â®Ã§ÂÆ’Ã©Å¾â€¹ 10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("Ã¦â€¢Â°Ã©â€¡ÂÃ¯Â¼Å¡                20.00\nÃ¦â‚¬Â»Ã¨Â®Â¡Ã¯Â¼Å¡                16889.00\nÃ¤Â»ËœÃ¦Â¬Â¾Ã¯Â¼Å¡                17000.00\nÃ¦â€°Â¾Ã©â€ºÂ¶Ã¯Â¼Å¡                111.00\n".getBytes("GBK"));
                    SendDataByte("Ã¥â€¦Â¬Ã¥ÂÂ¸Ã¥ÂÂÃ§Â§Â°Ã¯Â¼Å¡NIKE\nÃ¥â€¦Â¬Ã¥ÂÂ¸Ã§Â½â€˜Ã¥Ââ‚¬Ã¯Â¼Å¡www.xxx.xxx\nÃ¥Å“Â°Ã¥Ââ‚¬Ã¯Â¼Å¡Ã¦Â·Â±Ã¥Å“Â³Ã¥Â¸â€šxxÃ¥Å’ÂºxxÃ¥ÂÂ·\nÃ§â€ÂµÃ¨Â¯ÂÃ¯Â¼Å¡0755-11111111\nÃ¦Å“ÂÃ¥Å Â¡Ã¤Â¸â€œÃ§ÂºÂ¿Ã¯Â¼Å¡400-xxx-xxxx\n================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Ã¨Â°Â¢Ã¨Â°Â¢Ã¦Æ’ Ã©Â¡Â¾,Ã¦Â¬Â¢Ã¨Â¿Å½Ã¥â€ ÂÃ¦Â¬Â¡Ã¥â€¦â€°Ã¤Â¸Â´!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);

                    SendDataByte("(Ã¤Â»Â¥Ã¤Â¸Å Ã¤Â¿Â¡Ã¦ÂÂ¯Ã¤Â¸ÂºÃ¦Âµâ€¹Ã¨Â¯â€¢Ã¦Â¨Â¡Ã¦ÂÂ¿,Ã¥Â¦â€šÃ¦Å“â€°Ã¨â€¹Å¸Ã¥ÂÅ’Ã¯Â¼Å’Ã§ÂºÂ¯Ã¥Â±Å¾Ã¥Â·Â§Ã¥ÂË†!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Ã§Æ’Â­Ã¦â€¢ÂÃ¦â€°â€œÃ¥ÂÂ°Ã¦Å“Âº!", 0, 3, 6);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKEÃ¤Â¸â€œÃ¥Ââ€“Ã¥Âºâ€”\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Ã©â€”Â¨Ã¥Âºâ€”Ã¥ÂÂ·: 888888\nÃ¥Ââ€¢Ã¦ÂÂ®  S00003333\nÃ¦â€Â¶Ã©â€œÂ¶Ã¥â€˜ËœÃ¯Â¼Å¡1001\nÃ¥Ââ€¢Ã¦ÂÂ®Ã¦â€”Â¥Ã¦Å“Å¸Ã¯Â¼Å¡xxxx-xx-xx\nÃ¦â€°â€œÃ¥ÂÂ°Ã¦â€”Â¶Ã©â€”Â´Ã¯Â¼Å¡xxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Ã¥â€œÂÃ¥ÂÂ            Ã¦â€¢Â°Ã©â€¡Â    Ã¥Ââ€¢Ã¤Â»Â·    Ã©â€¡â€˜Ã©Â¢Â\nNIKEÃ¨Â·â€˜Ã©Å¾â€¹        10.00   899     8990\nNIKEÃ§Â¯Â®Ã§ÂÆ’Ã©Å¾â€¹      10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("Ã¦â€¢Â°Ã©â€¡ÂÃ¯Â¼Å¡                20.00\nÃ¦â‚¬Â»Ã¨Â®Â¡Ã¯Â¼Å¡                16889.00\nÃ¤Â»ËœÃ¦Â¬Â¾Ã¯Â¼Å¡                17000.00\nÃ¦â€°Â¾Ã©â€ºÂ¶Ã¯Â¼Å¡                111.00\n".getBytes("GBK"));
                    SendDataByte("Ã¥â€¦Â¬Ã¥ÂÂ¸Ã¥ÂÂÃ§Â§Â°Ã¯Â¼Å¡NIKE\nÃ¥â€¦Â¬Ã¥ÂÂ¸Ã§Â½â€˜Ã¥Ââ‚¬Ã¯Â¼Å¡www.xxx.xxx\nÃ¥Å“Â°Ã¥Ââ‚¬Ã¯Â¼Å¡Ã¦Â·Â±Ã¥Å“Â³Ã¥Â¸â€šxxÃ¥Å’ÂºxxÃ¥ÂÂ·\nÃ§â€ÂµÃ¨Â¯ÂÃ¯Â¼Å¡0755-11111111\nÃ¦Å“ÂÃ¥Å Â¡Ã¤Â¸â€œÃ§ÂºÂ¿Ã¯Â¼Å¡400-xxx-xxxx\n===========================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Ã¨Â°Â¢Ã¨Â°Â¢Ã¦Æ’ Ã©Â¡Â¾,Ã¦Â¬Â¢Ã¨Â¿Å½Ã¥â€ ÂÃ¦Â¬Â¡Ã¥â€¦â€°Ã¤Â¸Â´!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("(Ã¤Â»Â¥Ã¤Â¸Å Ã¤Â¿Â¡Ã¦ÂÂ¯Ã¤Â¸ÂºÃ¦Âµâ€¹Ã¨Â¯â€¢Ã¦Â¨Â¡Ã¦ÂÂ¿,Ã¥Â¦â€šÃ¦Å“â€°Ã¨â€¹Å¸Ã¥ÂÅ’Ã¯Â¼Å’Ã§ÂºÂ¯Ã¥Â±Å¾Ã¥Â·Â§Ã¥ÂË†!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }else if((lang.compareTo("en")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd/ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//Ã¨Å½Â·Ã¥Ââ€“Ã¥Â½â€œÃ¥â€°ÂÃ¦â€”Â¶Ã©â€”Â´
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if (is58mm) {

                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 6);//
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKE Shop\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Number:  888888\nReceipt  S00003333\nCashierÃ¯Â¼Å¡1001\nDateÃ¯Â¼Å¡xxxx-xx-xx\nPrint TimeÃ¯Â¼Å¡xxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Name    Quantity    price  Money\nShoes   10.00       899     8990\nBall    10.00       1599    15990\n".getBytes("GBK"));
                    SendDataByte("QuantityÃ¯Â¼Å¡             20.00\ntotalÃ¯Â¼Å¡                16889.00\npaymentÃ¯Â¼Å¡              17000.00\nKeep the changeÃ¯Â¼Å¡      111.00\n".getBytes("GBK"));
                    SendDataByte("company nameÃ¯Â¼Å¡NIKE\nSiteÃ¯Â¼Å¡www.xxx.xxx\naddressÃ¯Â¼Å¡ShenzhenxxAreaxxnumber\nphone numberÃ¯Â¼Å¡0755-11111111\nHelplineÃ¯Â¼Å¡400-xxx-xxxx\n================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Welcome again!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);

                    SendDataByte("(The above information is for testing template, if agree, is purely coincidental!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 8);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKE Shop\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Number: 888888\nReceipt  S00003333\nCashierÃ¯Â¼Å¡1001\nDateÃ¯Â¼Å¡xxxx-xx-xx\nPrint TimeÃ¯Â¼Å¡xxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Name                    Quantity price  Money\nNIKErunning shoes        10.00   899     8990\nNIKEBasketball Shoes     10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("QuantityÃ¯Â¼Å¡               20.00\ntotalÃ¯Â¼Å¡                  16889.00\npaymentÃ¯Â¼Å¡                17000.00\nKeep the changeÃ¯Â¼Å¡                111.00\n".getBytes("GBK"));
                    SendDataByte("company nameÃ¯Â¼Å¡NIKE\nSiteÃ¯Â¼Å¡www.xxx.xxx\naddressÃ¯Â¼Å¡shenzhenxxAreaxxnumber\nphone numberÃ¯Â¼Å¡0755-11111111\nHelplineÃ¯Â¼Å¡400-xxx-xxxx\n================================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Welcome again!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("(The above information is for testing template, if agree, is purely coincidental!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¦ÂÂ¡Ã§ ÂÃ£â‚¬ÂÃ¤ÂºÅ’Ã§Â»Â´Ã§ Â
     */
    private void printBarCode() {

        new AlertDialog.Builder(Main_ActivityPrinterLongin.this).setTitle(getText(R.string.btn_prtcode))
                .setItems(codebar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SendDataByte(byteCodebar[which]);
                        String str = editText.getText().toString();
                        if(which == 0)
                        {
                            if(str.length() == 11 || str.length() == 12)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 65, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("UPC_A\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 1)
                        {
                            if(str.length() == 6 || str.length() == 7)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 66, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("UPC_E\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 2)
                        {
                            if(str.length() == 12 || str.length() == 13)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 67, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("JAN13(EAN13)\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 3)
                        {
                            if(str.length() >0 )
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 68, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("JAN8(EAN8)\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 4)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 69, 3, 168, 1, 2);
                                SendDataString("CODE39\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 5)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 70, 3, 168, 1, 2);
                                SendDataString("ITF\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 6)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 71, 3, 168, 1, 2);
                                SendDataString("CODABAR\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 7)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 72, 3, 168, 1, 2);
                                SendDataString("CODE93\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 8)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 73, 3, 168, 1, 2);
                                SendDataString("CODE128\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 9)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.empty1), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getBarCommand(str, 1, 3, 8);
                                SendDataString("QR Code\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                    }
                }).create().show();
    }


    /**
     * public static Bitmap createAppIconText(Bitmap icon, String txt, boolean is58mm, int hight)
     * Bitmap  icon     æºå›¾
     * String txt       è¦è½¬æ¢çš„å­—ç¬¦ä¸²
     * boolean is58mm   æ‰“å°å®½åº¦(58å’Œ80)
     * int hight        è½¬æ¢åŽçš„å›¾ç‰‡é«˜åº¦
     */

    private void CloseRestrauntBill()
    {
        sharedpreferencesCloseRestraunt = getApplicationContext().getSharedPreferences(MyPREFERENCES_CLOSE, Context.MODE_PRIVATE);
        String collection= sharedpreferencesCloseRestraunt.getString("collection","");
        String data_collection= sharedpreferencesCloseRestraunt.getString("data_collection","");
        String data_gross_amount= sharedpreferencesCloseRestraunt.getString("data_gross_amount","");
        String data_net_discount= sharedpreferencesCloseRestraunt.getString("data_net_discount","");

        String data_cash_short= sharedpreferencesCloseRestraunt.getString("data_cash_short","");
        String data_total_sales= sharedpreferencesCloseRestraunt.getString("data_total_sales","");
        String data_net_amount= sharedpreferencesCloseRestraunt.getString("data_net_amount","");
        String data_orders_count= sharedpreferencesCloseRestraunt.getString("data_orders_count","");

        Double data_cash_short_d=0.00,data_total_sales_d=0.00,data_net_amount_d=0.00,data_net_discount_d=0.00,
                data_gross_amount_d=0.00,data_orders_count_d=0.00,data_tender_change_d=0.00,data_payment_received_d=0.00;
        if(data_cash_short!=null&&!data_cash_short.equals(""))
        {
            data_cash_short_d= Double.valueOf(data_cash_short);
        }
        if(data_total_sales!=null&&!data_total_sales.equals(""))
        {
            data_total_sales_d= Double.valueOf(data_total_sales);
        }
        if(data_net_amount!=null&&!data_net_amount.equals(""))
        {
            data_net_amount_d= Double.valueOf(data_net_amount);
        }
        if(data_net_discount!=null&&!data_net_discount.equals(""))
        {
            data_net_discount_d= Double.valueOf(data_net_discount);
        }
        if(data_gross_amount!=null&&!data_gross_amount.equals(""))
        {
            data_gross_amount_d= Double.valueOf(data_gross_amount);
        }
        if(data_orders_count!=null&&!data_orders_count.equals(""))
        {
            data_orders_count_d= Double.valueOf(data_orders_count);
        }

      //  Double data_total_sales_d=Double.valueOf(data_total_sales);
       // Double data_net_amount_d=Double.valueOf(data_net_amount);
        //Double data_net_discount_d=Double.valueOf(data_net_discount);
        //Double data_gross_amount_d=Double.valueOf(data_gross_amount);
       // Double data_orders_count_d=Double.valueOf(data_orders_count);


        String data_closed_by= sharedpreferencesCloseRestraunt.getString("data_closed_by","");
        String data_tender_cash= sharedpreferencesCloseRestraunt.getString("data_tender_cash","");
        String data_tender_change= sharedpreferencesCloseRestraunt.getString("data_tender_change","");
        String data_payment_received= sharedpreferencesCloseRestraunt.getString("data_payment_received","");

        if(data_tender_change!=null&&!data_tender_change.equals(""))
        {
            data_tender_change_d= Double.valueOf(data_tender_change);
        }

        if(data_payment_received!=null&&!data_payment_received.equals(""))
        {
            data_payment_received_d= Double.valueOf(data_payment_received);
        }


        String data_CASH= sharedpreferencesCloseRestraunt.getString("data_CASH","");
        String data_CARD= sharedpreferencesCloseRestraunt.getString("data_CARD","");
        String data_BOTH= sharedpreferencesCloseRestraunt.getString("data_BOTH","");
        String data_TALABAT= sharedpreferencesCloseRestraunt.getString("data_TALABAT","");
        String data_GIFTCOUPON= sharedpreferencesCloseRestraunt.getString("data_GIFTCOUPON","");
        String data_MAKAN= sharedpreferencesCloseRestraunt.getString("data_MAKAN","");
        Double data_CASH_d=0.00,data_CARD_d=0.00,data_TALABAT_d=0.00,data_GIFTCOUPON_d=0.00,data_MAKAN_d=0.00;
        if(data_CASH!=null&&!data_CASH.equals(""))
        {
             data_CASH_d=Double.valueOf(data_CASH);
        }
        if(data_CARD!=null&&!data_CARD.equals(""))
        {
            data_CARD_d=Double.valueOf(data_CARD);
        }
        if(data_TALABAT!=null&&!data_TALABAT.equals(""))
        {
            data_TALABAT_d=Double.valueOf(data_TALABAT);
        }
        if(data_GIFTCOUPON!=null&&!data_GIFTCOUPON.equals(""))
        {
            data_GIFTCOUPON_d=Double.valueOf(data_GIFTCOUPON);
        }
        if(data_MAKAN!=null&&!data_MAKAN.equals(""))
        {
             data_MAKAN_d=Double.valueOf(data_MAKAN);
        }


        String report_date= sharedpreferencesCloseRestraunt.getString("report_date","");
        String closing_time= sharedpreferencesCloseRestraunt.getString("closing_time","");

        String json = sharedpreferencesCloseRestraunt.getString("WaiterOrders", null);
        categoryList= getArrayList("WaiterOrders2");
        categoryList2= getArrayList("WaiterOrders_orderlist");
        categoryList_payment= getArrayList("Payment_types");

        categoryList4_cancelled= getArrayList("Cancelled_orders");

        if(categoryList!=null&&categoryList.size()>0)
        {
            for(int i=0;i<categoryList.size();i++)
            {
                categoryList.get(i).get("staff_name");
                categoryList.get(i).get("waiter_orders_count");
                categoryList.get(i).get("cash_collected");
                categoryList.get(i).get("gross_amount");
                categoryList.get(i).get("net_discount");

                categoryList.get(i).get("CASH");
                categoryList.get(i).get("CARD");
                categoryList.get(i).get("BOTH");

                categoryList.get(i).get("TALABAT");
                categoryList.get(i).get("GIFTCOUPON");
                categoryList.get(i).get("MAKAN");
            }
        }




        String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
        String time = new SimpleDateFormat("HH:mm aa", Locale.getDefault()).format(new Date());
        String  msgg="";
        if(AppSession.getInstance().getUser_Type().equals("WAITER"))
        {
            msgg = "Waiter Sale Report: "+AppSession.getInstance().getUser_Name();
        }
        else
        {
            msgg = "SALE REPORT";
        }
        //String  msgg = "SALE REPORT";
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(msgg);
        SendDataString("\n"+"------------------------------------------\n");

        String msg = AppSession.getInstance().getLocationbill_Name()+"\n"+"Sales Date: "+report_date+"\n" +"Closed Time: "+closing_time +"\n" +"Closed By: "+data_closed_by+"\n" +"Printed at: "+date+"\n" +"------------------------------------------\n";
        SendDataString(msg);
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);

        String  total_sales = "TOTAL SALES : "+" ( "+AppSession.getInstance().getRestraunt_CurrencySymbol()+" ) "+ data_cash_short;
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(total_sales);
        SendDataString("\n"+"------------------------------------------\n");

        SendDataByte(PrinterCommand.POS_Set_Bold(0));

        if(data_cash_short_d<=0)
        {
            data_cash_short="";
        }
        else
        {
            data_cash_short="Cash Short: "+data_cash_short+"\n";
        }
        if(data_orders_count_d<=0)
        {
            data_orders_count="";
        }
        else
        {
            data_orders_count="No Of Bills: "+data_orders_count+"\n";
        }
        if(data_gross_amount_d<=0)
        {
            data_gross_amount="";
        }
        else
        {
            data_gross_amount="Gross Amount: "+data_gross_amount+"\n";
        }
        if(data_net_discount_d<=0)
        {
            data_net_discount="";
        }
        else
        {
            data_net_discount="Discount: "+data_net_discount+"\n";
        }
        if(data_payment_received_d<=0)
        {
            data_payment_received="";
        }
        else
        {
            data_payment_received="Payment Received: "+data_payment_received+"\n";
        }


        if(data_tender_change_d<=0)
        {
            data_tender_change="";
        }
        else
        {
            data_tender_change="Tender Change: "+data_tender_change+"\n";
        }

        if(data_net_amount_d<=0)
        {
            data_net_amount="";
        }
        else
        {
            data_net_amount="Net Amount: "+data_net_amount+"\n";
        }

        String  total_saless = data_cash_short+ data_orders_count+data_gross_amount + data_net_discount +"------------------------------------------\n";

        SendDataString(total_saless);

        String  net_amount = data_payment_received+data_tender_change+data_net_amount;
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(net_amount);
        SendDataString("\n"+"------------------------------------------\n");

        SendDataByte(PrinterCommand.POS_Set_Bold(0));

        String  payment_type = "PAYMENT TYPE";
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(payment_type);
        SendDataString("\n"+"------------------------------------------\n");

        SendDataByte(PrinterCommand.POS_Set_Bold(0));
        if(data_CASH_d<=0)
        {
            data_CASH="";
        }
        else
        {
            data_CASH="Cash Sale: "+data_CASH+"\n";
        }
        if(data_CARD_d<=0)
        {
            data_CARD="";
        }
        else
        {
            data_CARD="Card Sale: "+data_CARD+"\n";
        }
        if(data_TALABAT_d<=0)
        {
            data_TALABAT="";
        }
        else
        {
            data_TALABAT="Talabat Sale: "+data_TALABAT+"\n";
        }
        if(data_GIFTCOUPON_d<=0)
        {
            data_GIFTCOUPON="";
        }
        else
        {
            data_GIFTCOUPON="GIFTCOUPON: "+data_GIFTCOUPON+"\n";
        }
        if(data_MAKAN_d<=0)
        {
            data_MAKAN="";
        }
        else
        {
            data_MAKAN="MAKAN Sale: "+data_MAKAN+"\n";
        }
        String  payment_types =/*"Cash Sale: "+data_CASH+"\n"*/ data_CASH+/*"Card Sale: "+data_CARD+"\n"*/ data_CARD+/*"Card/Card Sale: "+data_BOTH+"\n" +*//*"Talabat Sale: "+data_TALABAT+"\n"*/data_TALABAT +/*"MAKAN Sale: "+data_MAKAN+"\n"*/ data_MAKAN+data_GIFTCOUPON +"------------------------------------------\n";

        SendDataString(payment_types);
        if(!AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            String  order_summary = "ORDER SUMMARY";
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            Command.ESC_Align[2] = 0x00;
            SendDataByte(Command.ESC_Align);
            SendDataString(order_summary);
            SendDataString("\n"+"------------------------------------------\n");

            SendDataByte(PrinterCommand.POS_Set_Bold(0));

            Command.ESC_Align[2] = 0x02;
            SendDataByte(Command.ESC_Align);
            String  order_detail_heading ="OR TYPE        D.AMT           NET\n";
            SendDataString(order_detail_heading);
            SendDataString("------------------------------------------\n");
            if(categoryList2!=null&&categoryList2.size()>0)
            {   String  order_details="";
                for(int i=0;i<categoryList2.size();i++)
                {
                    categoryList2.get(i).get("invoice_no");
                    categoryList2.get(i).get("invoice_date");
                    categoryList2.get(i).get("sale_no");
                    categoryList2.get(i).get("order_type");
                    categoryList2.get(i).get("payment_type");
                    String order_type="";
                    if( categoryList2.get(i).get("order_type").equals("dine_in"))
                    {
                        order_type="DINE IN";
                    }
                    else
                    if( categoryList2.get(i).get("order_type").equals("delivery"))
                    {
                        order_type="DELIVERY";
                    }
                    else
                    if( categoryList2.get(i).get("order_type").equals("takeaway"))
                    {
                        order_type="TAKE AWAY";
                    }
                    Float discount=Float.parseFloat(categoryList2.get(i).get("coupon_discount"))+Float.parseFloat(categoryList2.get(i).get("other_discount"));

                    order_details =order_type+"    "+discount+"     "+categoryList2.get(i).get("final_total")+"\n";
                    SendDataString(order_details);
                }


            }
            SendDataString("\n"+"------------------------------------------\n");
        }

        String  cancelled_orders = "CANCELLED ITEMS";
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(cancelled_orders);
        SendDataString("\n"+"------------------------------------------\n");
        SendDataByte(PrinterCommand.POS_Set_Bold(0));

        Command.ESC_Align[2] = 0x02;
        SendDataByte(Command.ESC_Align);
        String  cancelled_item_header ="NO.  ITEM   QTY   AMT   NET\n";
        SendDataString(cancelled_item_header);
        SendDataString("------------------------------------------\n");
        if(categoryList4_cancelled!=null&&categoryList4_cancelled.size()>0)
        {   String  order_details="";
            for(int i=0;i<categoryList4_cancelled.size();i++)
            {
                categoryList4_cancelled.get(i).get("order_id");
                categoryList4_cancelled.get(i).get("menu_id");
                categoryList4_cancelled.get(i).get("name");
                categoryList4_cancelled.get(i).get("price");
                categoryList4_cancelled.get(i).get("quantity");
                categoryList4_cancelled.get(i).get("subtotal");
                //Float discount=Float.parseFloat(categoryList4_cancelled.get(i).get("coupon_discount"))+Float.parseFloat(categoryList4_cancelled.get(i).get("other_discount"));

                order_details =(i+1)+"  "+
                        categoryList4_cancelled.get(i).get("name")+" "+categoryList4_cancelled.get(i).get("quantity")+" X "+categoryList4_cancelled.get(i).get("price")+"  "+categoryList4_cancelled.get(i).get("subtotal")+"\n";
                SendDataString(order_details);
            }


        }

        SendDataString("\n"+"------------------------------------------\n");

        String  waiter_summary = "WAITER SUMMARY";
        SendDataByte(PrinterCommand.POS_Set_Bold(1));
        Command.ESC_Align[2] = 0x00;
        SendDataByte(Command.ESC_Align);
        SendDataString(waiter_summary);
        SendDataString("\n"+"------------------------------------------\n");
        if(categoryList.size()>0)
        {
            for(int i=0;i<categoryList.size();i++)
            {
                SendDataByte(PrinterCommand.POS_Set_Bold(1));
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(categoryList.get(i).get("staff_name"));
                SendDataString("\n"+"--------------\n");
                SendDataByte(PrinterCommand.POS_Set_Bold(0));

                SendDataString("Total Orders: "+categoryList.get(i).get("waiter_orders_count")+"\n");
                if((categoryList.get(i).get("CASH")!=null&&!categoryList.get(i).get("CASH").equals("")))
                {
                    if(Double.valueOf(categoryList.get(i).get("CASH"))>0)
                    SendDataString("CASH : "+categoryList.get(i).get("CASH")+"\n");
                }
                else
                {
                   /* SendDataString("CASH : "+categoryList.get(i).get("CASH")+"\n");*/
                }
                if((categoryList.get(i).get("CARD")!=null&&!categoryList.get(i).get("CARD").equals("")))

                {
                    if(Double.valueOf(categoryList.get(i).get("CARD"))>0)
                    SendDataString("CARD : "+categoryList.get(i).get("CARD")+"\n");
                }
                else
                {
                   /* SendDataString("CARD : "+categoryList.get(i).get("CARD")+"\n");*/
                }
                if((categoryList.get(i).get("TALABAT")!=null&&!categoryList.get(i).get("TALABAT").equals("")))
                {
                    if(Double.valueOf(categoryList.get(i).get("TALABAT"))>0)
                    SendDataString("TALABAT : "+categoryList.get(i).get("TALABAT")+"\n");
                }
                else
                {
                   /* SendDataString("TALABAT : "+categoryList.get(i).get("TALABAT")+"\n");*/
                }
                if((categoryList.get(i).get("GIFTCOUPON")!=null&&!categoryList.get(i).get("GIFTCOUPON").equals("")))
                {
                    if(Double.valueOf(categoryList.get(i).get("GIFTCOUPON"))>0)
                    SendDataString("GIFTCOUPON : "+categoryList.get(i).get("GIFTCOUPON")+"\n");
                }
                else
                {
                   /* SendDataString("GIFTCOUPON : "+categoryList.get(i).get("GIFTCOUPON")+"\n");*/
                }
                if((categoryList.get(i).get("MAKAN")!=null&&!categoryList.get(i).get("MAKAN").equals("")))
                {
                    if(Double.valueOf(categoryList.get(i).get("MAKAN"))>0)
                    SendDataString("MAKAN : "+categoryList.get(i).get("MAKAN")+"\n");
                }
                else
                {
                   /* SendDataString("MAKAN : "+categoryList.get(i).get("MAKAN")+"\n");*/
                }

                // SendDataString("BOTH : "+categoryList.get(i).get("BOTH")+"\n");




            }
        }
        SendDataString("------------------------------------------\n");

        SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
        SendDataByte(PrinterCommand.POS_Set_Cut(1));
        SendDataByte(PrinterCommand.POS_Set_PrtInit());
        SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
    }
    public  ArrayList<HashMap<String, String>> getArrayList(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesCloseRestraunt.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }
    public  ArrayList<HashMap<String, String>> getArrayList2(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesData.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }
    private void CompletedBill(){
        sharedpreferencesData = getApplicationContext().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


        String GuestName=  sharedpreferencesData.getString("GuestName","");
        String no_of_pax=  sharedpreferencesData.getString("dinein_guest_count","0");
        String senior= sharedpreferencesData.getString("dinein_edt_senior_guest","0");
        String solo=  sharedpreferencesData.getString("dinein_edt_solo_guest","0");
        String disabled= sharedpreferencesData.getString("dinein_edt_disabled","0");
        String sale_no=  sharedpreferencesData.getString("sale_no","");

        String extra_item=  sharedpreferencesData.getString("extra_item","");
        String other_extra=  sharedpreferencesData.getString("other_extra","");

        String order_id=  sharedpreferencesData.getString("order_id","");
        String invoice_no=  sharedpreferencesData.getString("invoice_no","");
        String invoice_date= sharedpreferencesData.getString("invoice_date","");
        String coupon_discount=  sharedpreferencesData.getString("coupon_discount","");
        String other_discount=  sharedpreferencesData.getString("other_discount","");
        String subtotal= sharedpreferencesData.getString("subtotal","");
        String tender_cash=  sharedpreferencesData.getString("tender_cash","");
        String tender_change=  sharedpreferencesData.getString("tender_change","");
        String vat= sharedpreferencesData.getString("vat","");
        String vat_percentage=  sharedpreferencesData.getString("vat_percentage","");
        String service_charge_percentage= sharedpreferencesData.getString("service_charge_percentage","");

        String service_charge=  sharedpreferencesData.getString("service_charge","");
        String tax1_applicable_sale=  sharedpreferencesData.getString("tax1_applicable_sale","");
        String tax2_applicable_sale=  sharedpreferencesData.getString("tax2_applicable_sale","");
        String tax1_non_applicable_sale=  sharedpreferencesData.getString("tax1_non_applicable_sale","");
        String tax2_non_applicable_sale=  sharedpreferencesData.getString("tax2_non_applicable_sale","");



        String vat_exempted=  sharedpreferencesData.getString("vat_exempted","");
        String service_charge_exempted= sharedpreferencesData.getString("service_charge_exempted","");
        String payment_type=  sharedpreferencesData.getString("payment_type","");
        String card_type=  sharedpreferencesData.getString("card_type","");
        String total= sharedpreferencesData.getString("final_total","");

        String qk_name=  sharedpreferencesData.getString("qk_name","");
        String mobile_number=  sharedpreferencesData.getString("mobile_number","");


        String order_firstname= sharedpreferencesData.getString("order_firstname","");
        String order_telephone=  sharedpreferencesData.getString("order_telephone","");
        String order_type=  sharedpreferencesData.getString("order_type","");
        String address1= sharedpreferencesData.getString("address1","");
        String address2=  sharedpreferencesData.getString("address2","");
        String location= sharedpreferencesData.getString("location","");

        String area=  sharedpreferencesData.getString("area","");
        String city= sharedpreferencesData.getString("city","");

        String coupon_type=  sharedpreferencesData.getString("coupon_type","");
        String coupon_code= sharedpreferencesData.getString("coupon_code","");



        /* Bitmap mBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.damo);*/
//        Bitmap mBitmap=null;
//        mBitmap.recycle();
//
//        mBitmap= StringToBitMap(AppSession.getInstance().getPrintericon());
//        float aspectRatio = mBitmap.getWidth() /
//                (float) mBitmap.getHeight();
//        int width = 180;
//        int height = Math.round(width / aspectRatio);
//
//        Bitmap resizedBitmap = Bitmap.createScaledBitmap(
//                mBitmap, width, height, false);
        /*try {
            URL url = new URL("http://18.220.183.40/dampafeast_cloud/assets/images/data/Dampa_Logo_line_work.jpg");
            Bitmap mBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());*/
        String txt_msg = "Dempafeast";
        if(txt_msg.length() == 0){
            Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.empty1), Toast.LENGTH_SHORT).show();
            return;
        }else{
           /* String abc;
            if(AppSession.getInstance().getGuest_Count().equals("No of Guests")){
                abc= "0";
            }else{
                abc= AppSession.getInstance().getGuest_Count();
            }*/
            AppSession.getInstance().setSubTotal(AppSession.getInstance().gettotal());

            if(AppSession.getInstance().getCouponTotal().equals("")){
                AppSession.getInstance().setCouponTotal("0.00");
                AppSession.getInstance().setCouponper("0");
                AppSession.getInstance().setSubTotal(AppSession.getInstance().gettotal());
            }
            String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
            String time = new SimpleDateFormat("HH:mm aa", Locale.getDefault()).format(new Date());
           /* Bitmap mBitmap = ((BitmapDrawable) imageViewPicture.getDrawable())
                    .getBitmap();*/


            /* imageView.setImageBitmap(bmp);*/
            int nMode = 0;
            int nPaperWidth = 270;
            if(width_58mm.isChecked())
                nPaperWidth = 270;
            else if (width_80.isChecked())
                nPaperWidth = 576;
            if(mBitmap1 != null)
            {
            String lang = getString(R.string.strLang);
            if((lang.compareTo("en")) == 0) {
                    mBitmap1=addPaddingLeftForBitmap(mBitmap1,120);
                    byte[] data = PrintPicture.POS_PrintBMP(mBitmap1, nPaperWidth, nMode);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(data);
//                    mBitmap=addPaddingLeftForBitmap(mBitmap,120);
//                    byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
//                    //SendDataByte(Command.ESC_Init);
//                    Command.ESC_Align[2] = 0x01;
//                    SendDataByte(data);

                String msggg = "";
                String abcd = "";
                String servitype = "";
                if (AppSession.getInstance().getServiceType().equals("delivery")) {
                    servitype = "DELIVERY";
                } else if (AppSession.getInstance().getServiceType().equals("dine_in")) {
                    servitype = "DINE IN";

                } else if (AppSession.getInstance().getServiceType().equals("takeaway")) {
                    servitype = "TAKE AWAY";
                }


                String restaurant_name = AppSession.getInstance().getLocationbill_Name() + "\n";
                String restaurant_address = AppSession.getInstance().getRestraunt_Address() + "\n";
                String restaurant_address2 = AppSession.getInstance().getRestraunt_City() + " " + AppSession.getInstance().getRestraunt_Country() + "\n";
                String tinString = "TIN NO:010-016-233-000";
                String billNo = "Bill No:#" + invoice_no;

                String saletype = "Sale Type: " + servitype;
                String billString = "Sale No: " + sale_no;
               // String billString = "Sale No: " + order_id;

                SendDataByte(PrinterCommand.POS_Set_Bold(1));
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_name);
                SendDataByte(PrinterCommand.POS_Set_Bold(0));


                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_address);

                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_address2);

                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(tinString + " ");
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(billNo + "\n");

                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(saletype + "     ");
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(billString + "\n" + "------------------------------------------\n");

                String msg2 =
                        "Date: " + date + "         " + "Time: " + time + "\n" +
                                "Table : " + AppSession.getInstance().getTable_Name() + "               " +
                                "Waiter: " + AppSession.getInstance().getStaff_Name() + "\n" +
                                "------------------------------------------\n";

                SendDataByte(PrinterCommand.POS_Set_Bold(0));
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(msg2);

                String customer_msg =
                        "Customer: " + GuestName + "        " + "Guest Count: " + no_of_pax + "\n" +
                                "Senior/Disabled : " + (parseInt(senior) + parseInt(disabled)) + "               " +
                                "Solo: " + solo + "\n" +
                                "------------------------------------------\n";

                SendDataByte(PrinterCommand.POS_Set_Bold(0));
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(customer_msg);

                /*Toast.makeText(Main_ActivityPrinterLongin.this, tt, Toast.LENGTH_SHORT).show();*/
                //getConverTextUTFToNormal(str);
                for (int i = 0; i < DBMethodsCart.newInstance(getApplicationContext()).getCartItems().size(); i++) {

                    abcd = (DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_qty() + " X " + DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name());
                    /*+ DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name2()*/
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(abcd + "\n");
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    Float menuTax = Float.parseFloat(DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_price()) * (Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())) / 100;
                    Float menuWithTax = menuTax + Float.parseFloat(DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_price());
                    Float withTaxQuantity = DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_qty() * menuWithTax;
                    abcd = String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", menuWithTax) + "   " + (String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", withTaxQuantity) + "\n");
                    SendDataString(abcd);
                    //abcd=DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name2();
                }
                String vatSales = "";
                //String nonvatSales=;
                vatSales += AppSession.getInstance().getRestraunt_tax_name() + " Sales" + " " + " :              " + tax1_applicable_sale + "\n";
                vatSales += "Non " + AppSession.getInstance().getRestraunt_tax_name() + " Sales" + "" + " :              " + tax1_non_applicable_sale + "\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(vatSales);


                //Toast.makeText(Main_ActivityPrinterLongin.this,  msggg+AppSession.getInstance().getOrderNumber(), Toast.LENGTH_LONG).show();
                String discount = "";

                discount += "DISCOUNT :  " + "           -" + other_discount + "\n";


                if (AppSession.getInstance().getApplyCoupon().equals("true")) {
                    if (coupon_type.equals("P")) {
                        discount += "COUPON DISCOUNT (" + AppSession.getInstance().getRestraunt_CurrencySymbol() + ") " + AppSession.getInstance().getCouponper() + "% :  " + "     -" + AppSession.getInstance().getCouponTotal() + "\n";

                    } else {
                        discount += "COUPON DISCOUNT (" + AppSession.getInstance().getRestraunt_CurrencySymbol() + ") " + AppSession.getInstance().getCouponper() + " :  " + "     -" + AppSession.getInstance().getCouponTotal() + "\n";

                    }

                } else {
                    discount += "";
                }

                String msgg = "------------------------------------------\n" + "  SUB TOTAL (" + AppSession.getInstance().getRestraunt_Currency() + ") :              " + subtotal + "\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(msgg);



                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(discount);



                String taxstring = "", taxstring2 = "";
                float taxamount = 0, taxamount2 = 0;
                if (!AppSession.getInstance().getRestraunt_tax_value().equals("0")) {
                    if (AppSession.getInstance().getRestraunt_tax_type().equals("%")) {
                        //float taxvalue=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value());
                        //float subtotal=Float.parseFloat(ft);
                        //taxamount=taxPayable+taxExemption;

                        //VAT Excemption code here
                    } else {
                        //taxamount=Float.parseFloat();
                    }

                    taxstring = AppSession.getInstance().getRestraunt_tax_name() + "(+) " + AppSession.getInstance().getRestraunt_tax_value() + AppSession.getInstance().getRestraunt_tax_type() + " :              " + vat + "\n";
                    //Condition for count of senior citizen for 20% discount
                    //Condition for count of Disabled for 20%
                    //Condition for solo parent for now keep 0%
                    // if tax excemption > 0


                }
                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(taxstring);
                if (!AppSession.getInstance().getRestraunt_tax_value2().equals("0")) {
                    if (AppSession.getInstance().getRestraunt_tax_type2().equals("%")) {
                        float taxvalue2 = Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2());
//                            float subtotal=Float.parseFloat(ft);
//                            taxamount2=taxvalue2*subtotal/100;
                        //Service charge excemption code here


                    } else {
                        // taxamount2=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2());
                    }
                    taxstring2 = AppSession.getInstance().getRestraunt_tax_name2() + "(+) " + AppSession.getInstance().getRestraunt_tax_value2() + AppSession.getInstance().getRestraunt_tax_type2() + " :              " + service_charge + "\n";
                }
                if (other_extra!=null&&!other_extra.equals(""))
                {
                    if(Double.valueOf(other_extra)>0)
                    {
                        String  del_charge = extra_item+" :  " + "           -" +other_extra + "\n";
                        Command.ESC_Align[2] = 0x02;
                        SendDataByte(Command.ESC_Align);
                        SendDataString(del_charge);
                    }


                }

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(taxstring2);
                SendDataByte(PrinterCommand.POS_Set_Bold(1));
                msgg = "------------------------------------------\n" +
                        "  TOTAL (" + AppSession.getInstance().getRestraunt_Currency() + ") : " + "            " + total.toString() + "\n" +
                        "------------------------------------------\n";

                SendDataString(msgg);
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataByte(PrinterCommand.POS_Set_Bold(0));

                msgg = "TOTAL ITEMS: " + DBMethodsCart.newInstance(getApplicationContext()).getCartItems().size() + "\n" +
                        /* "Printed Date:- "+getDateTime()+"\n" +*/
                        "\n";
                SendDataString(msgg);

//
                String Customermsg = "Thank You Visit Us Again !!! \n\n"
                        +"Customer Sign ____________________ \n\n"+"Customer Info"+"\n";
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(Customermsg);
                // Customer name should come here .. if already given
                String Customertext="Name           : ____________________ \n"+"Address        : ____________________ \n"+"TIN            : ____________________ \n"+"Business Style : ____________________ \n"+"------------------------------------------\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(Customertext);


                String specialText="Senior Citizen/PWD TIN : _______________ \n"+"OSCA ID No./ PWD ID No. : _____________ \n"+"TIN            : _________________ \n"+"Signature : _________________ \n"+"------------------------------------------\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(specialText);

                String POStext="POS Provider:\nDAMPA FEAST PH INC\nTIN: 010-016-233-000\n\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(POStext);

//                    // Customer name should come here .. if already given
//                    String Customertext="Name           : ____________________ \n"+"Address        : ____________________ \n"+"TIN            : ____________________ \n"+"Business Style : ____________________ \n"+"------------------------------------------\n";
//                    Command.ESC_Align[2] = 0x00;
//                    SendDataByte(Command.ESC_Align);
//                    SendDataString(Customertext);
//
//                    String footerText="This serves as your OFFICIAL RECIEPT \n\n"+"" +
//                            "This invoice reciept shall be valid for   five(5) years from the date of the permit to use \n\n\n";
//                    Command.ESC_Align[2] = 0x01;
//                    SendDataByte(Command.ESC_Align);
//                    SendDataString(footerText);
                    /*AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setCouponper("");
                    AppSession.getInstance().setCouponTotal("");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrderNumber("");
                    DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearCart();
                    DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearModifier();
*/
                 /*   AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.clear();
                    editor1.commit();
                    AppSession.getInstance().setGuest_Count("");
                    AppSession.getInstance().setComplteBillDetails("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setCouponper("");
                    AppSession.getInstance().setCouponTotal("");
                    AppSession.getInstance().setTable_Name("");
                    DBMethodsCart.newInstance(this).clearCart();
                    DBMethodsCart.newInstance(this).clearModifier();
*/

                //AppSession.getInstance().setTable_ID("");
                       /* AppSession.getInstance().settotal("");
                        AppSession.getInstance().setSubTotal("");*/
                SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
                SendDataByte(PrinterCommand.POS_Set_Cut(1));
                SendDataByte(PrinterCommand.POS_Set_PrtInit());
                SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
                //Command.ESC_Align[2] = 0x02;
                //SendDataByte(Command.ESC_Align);
                //SendDataString(date);



                    /*Intent i= new Intent(Main_ActivityPrinterLongin.this,NavigationMainActivity.class);
                    startActivity(i);
                    finish();*/

                  }

            }

        }


    }
    private void CompletedOrderBill(){

        sharedpreferencesData = getApplicationContext().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

        String GuestName=  sharedpreferencesData.getString("GuestName","");
        String no_of_pax=  sharedpreferencesData.getString("dinein_guest_count","0");
        String senior= sharedpreferencesData.getString("dinein_edt_senior_guest","0");
        String solo=  sharedpreferencesData.getString("dinein_edt_solo_guest","0");
        String disabled= sharedpreferencesData.getString("dinein_edt_disabled","0");

        String order_id=  sharedpreferencesData.getString("order_id","");
        String sale_no=  sharedpreferencesData.getString("sale_no","");
        String invoice_no=  sharedpreferencesData.getString("invoice_no","");
        String invoice_date= sharedpreferencesData.getString("invoice_date","");
        String coupon_discount=  sharedpreferencesData.getString("coupon_discount","");
        String other_discount=  sharedpreferencesData.getString("other_discount","");
        String subtotal= sharedpreferencesData.getString("subtotal","");
        String tender_cash=  sharedpreferencesData.getString("tender_cash","");
        String tender_change=  sharedpreferencesData.getString("tender_change","");
        String vat= sharedpreferencesData.getString("vat","");
        String vat_percentage=  sharedpreferencesData.getString("vat_percentage","");
        String service_charge_percentage= sharedpreferencesData.getString("service_charge_percentage","");

        String service_charge=  sharedpreferencesData.getString("service_charge","");
        String tax1_applicable_sale=  sharedpreferencesData.getString("tax1_applicable_sale","");
        String tax2_applicable_sale=  sharedpreferencesData.getString("tax2_applicable_sale","");
        String tax1_non_applicable_sale=  sharedpreferencesData.getString("tax1_non_applicable_sale","");
        String tax2_non_applicable_sale=  sharedpreferencesData.getString("tax2_non_applicable_sale","");



        String vat_exempted=  sharedpreferencesData.getString("vat_exempted","");
        String service_charge_exempted= sharedpreferencesData.getString("service_charge_exempted","");
        String payment_type=  sharedpreferencesData.getString("payment_type","");
        String card_type=  sharedpreferencesData.getString("card_type","");
        String total= sharedpreferencesData.getString("final_total","");

        String qk_name=  sharedpreferencesData.getString("qk_name","");
        String mobile_number=  sharedpreferencesData.getString("mobile_number","");


        String order_firstname= sharedpreferencesData.getString("order_firstname","");
        String order_telephone=  sharedpreferencesData.getString("order_telephone","");
        String order_type=  sharedpreferencesData.getString("order_type","");
        String address1= sharedpreferencesData.getString("address1","");
        String address2=  sharedpreferencesData.getString("address2","");
        String location= sharedpreferencesData.getString("location","");

        String area=  sharedpreferencesData.getString("area","");
        String city= sharedpreferencesData.getString("city","");

        String coupon_type=  sharedpreferencesData.getString("coupon_type","");
        String coupon_code= sharedpreferencesData.getString("coupon_code","");

        String card_name=  sharedpreferencesData.getString("card_name","");
        String card_number= sharedpreferencesData.getString("coupon_code","");
        String table_name= sharedpreferencesData.getString("table_name","");
        String table_id= sharedpreferencesData.getString("table_id","");

        categoryList_payment= getArrayList2("Payment_types");

        categoryList3= getArrayList2("ItemOrders");

      /*  if(categoryList3!=null&&categoryList3.size()>0)
        {
            for(int i=0;i<categoryList3.size();i++)
            {
                categoryList3.get(i).get("name");
                categoryList3.get(i).get("quantity");
                categoryList3.get(i).get("price");

                categoryList3.get(i).get("subtotal");
                categoryList3.get(i).get("discount");
                categoryList3.get(i).get("discount_value");

            }
        }*/

        /* Bitmap mBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.damo);*/
//        Bitmap mBitmap=null;
//        mBitmap.recycle();
//
//        mBitmap= StringToBitMap(AppSession.getInstance().getPrintericon());
//       // Bitmap mBitmap=StringToBitMap(AppSession.getInstance().getPrintericon());
//        float aspectRatio = mBitmap.getWidth() /
//                (float) mBitmap.getHeight();
//        int width = 180;
//        int height = Math.round(width / aspectRatio);
//
//        Bitmap resizedBitmap = Bitmap.createScaledBitmap(
//                mBitmap, width, height, false);
        /*try {
            URL url = new URL("http://18.220.183.40/dampafeast_cloud/assets/images/data/Dampa_Logo_line_work.jpg");
            Bitmap mBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());*/
        String txt_msg = "Dempafeast";
        if(txt_msg.length() == 0){
            Toast.makeText(Main_ActivityPrinterLongin.this, getText(R.string.empty1), Toast.LENGTH_SHORT).show();
            return;
        }else{
           /* String abc;
            if(AppSession.getInstance().getGuest_Count().equals("No of Guests")){
                abc= "0";
            }else{
                abc= AppSession.getInstance().getGuest_Count();
            }*/
            AppSession.getInstance().setSubTotal(AppSession.getInstance().gettotal());

            if(AppSession.getInstance().getCouponTotal().equals("")){
                AppSession.getInstance().setCouponTotal("0.00");
                AppSession.getInstance().setCouponper("0");
                AppSession.getInstance().setSubTotal(AppSession.getInstance().gettotal());
            }
            String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
            String time = new SimpleDateFormat("HH:mm aa", Locale.getDefault()).format(new Date());
           /* Bitmap mBitmap = ((BitmapDrawable) imageViewPicture.getDrawable())
                    .getBitmap();*/


            /* imageView.setImageBitmap(bmp);*/
            int nMode = 0;
            int nPaperWidth = 270;
            if(width_58mm.isChecked())
                nPaperWidth = 270;
            else if (width_80.isChecked())
                nPaperWidth = 576;

            if(mBitmap1 != null)
            {
            String lang = getString(R.string.strLang);
            if((lang.compareTo("en")) == 0) {
                //mBitmap=addPaddingLeftForBitmap(mBitmap,120);
                //byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
                //SendDataByte(Command.ESC_Init);
                //Command.ESC_Align[2] = 0x01;
                //SendDataByte(data);
                mBitmap1=addPaddingLeftForBitmap(mBitmap1,120);
                byte[] data = PrintPicture.POS_PrintBMP(mBitmap1, nPaperWidth, nMode);
                Command.ESC_Align[2] = 0x01;
                SendDataByte(data);
                String msggg = "";
                String abcd = "";
                String servitype = "";
                String TableName = "";
                if (flag_closeorderbill.equals("126") || flag_closeorderbill.equals("127")) {

                    if (order_type.equals("delivery")) {
                        servitype = "DELIVERY";
                    } else if (order_type.equals("dine_in")) {
                        servitype = "DINE IN";

                    } else if (order_type.equals("takeaway")) {
                        servitype = "TAKE AWAY";
                    }
                    TableName = table_name;
                } else {
                    if (AppSession.getInstance().getServiceType().equals("delivery")) {
                        servitype = "DELIVERY";
                    } else if (AppSession.getInstance().getServiceType().equals("dine_in")) {
                        servitype = "DINE IN";

                    } else if (AppSession.getInstance().getServiceType().equals("takeaway")) {
                        servitype = "TAKE AWAY";
                    }
                    TableName = AppSession.getInstance().getTable_Name();
                }


                String restaurant_name = AppSession.getInstance().getLocationbill_Name() + "\n";
                String restaurant_address = AppSession.getInstance().getRestraunt_Address() + "\n";
                String restaurant_address2 = AppSession.getInstance().getRestraunt_City() + " " + AppSession.getInstance().getRestraunt_Country() + "\n";
                String tinString = "TIN NO:010-016-233-000";
                String billNo = "Bill No:#" + invoice_no;

                String saletype = "Sale Type: " + servitype;
                /*String billString="Sale No: "+ order_id;*/
                String billString = "Sale No: " + sale_no;

                SendDataByte(PrinterCommand.POS_Set_Bold(1));
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_name);
                SendDataByte(PrinterCommand.POS_Set_Bold(0));


                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_address);

                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(restaurant_address2);

                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(tinString + " ");
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(billNo + "\n");

                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(saletype + "     ");
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(billString + "\n" + "------------------------------------------\n");

                String msg2 =
                        "Date: " + date + "         " + "Time: " + time + "\n" +
                                "Table : " + TableName/*AppSession.getInstance().getTable_Name()*/ + "               " +
                                "Waiter: " + AppSession.getInstance().getStaff_Name() + "\n" +
                                "------------------------------------------\n";

                SendDataByte(PrinterCommand.POS_Set_Bold(0));
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(msg2);

                String customer_msg =
                        "Customer: " + GuestName + "        " + "Guest Count: " + no_of_pax + "\n" +
                                "Senior/Disabled : " + (parseInt(senior) + parseInt(disabled)) + "               " +
                                "Solo: " + solo + "\n" +
                                "------------------------------------------\n";

                SendDataByte(PrinterCommand.POS_Set_Bold(0));
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(customer_msg);

                String TotalItems="";
                /*Toast.makeText(Main_ActivityPrinterLongin.this, tt, Toast.LENGTH_SHORT).show();*/
                //getConverTextUTFToNormal(str);
                if (flag_closeorderbill.equals("126") || flag_closeorderbill.equals("127")) {
                    flag_closeorderbill = "";
                    if (categoryList3 != null && categoryList3.size() > 0) {
                        TotalItems=String.valueOf(categoryList3.size());
                        for (int i = 0; i < categoryList3.size(); i++) {
                                /*categoryList3.get(i).get("name");
                                categoryList3.get(i).get("quantity");
                                categoryList3.get(i).get("price");

                                categoryList3.get(i).get("subtotal");
                                categoryList3.get(i).get("discount");
                                categoryList3.get(i).get("discount_value");*/

                            abcd = (categoryList3.get(i).get("quantity") + " X " + categoryList3.get(i).get("name"));

                            Command.ESC_Align[2] = 0x00;
                            SendDataByte(Command.ESC_Align);
                            SendDataString(abcd + "\n");
                            Command.ESC_Align[2] = 0x02;
                            SendDataByte(Command.ESC_Align);
                            Float menuTax = Float.parseFloat(categoryList3.get(i).get("price")) * (Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())) / 100;
                            Float menuWithTax = menuTax + Float.parseFloat(categoryList3.get(i).get("price"));
                            Float withTaxQuantity = Float.parseFloat(categoryList3.get(i).get("quantity")) * menuWithTax;
                            abcd = String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", menuWithTax) + "   " + (String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", withTaxQuantity) + "\n");
                            SendDataString(abcd);
                        }
                    }

                } else {
                    TotalItems=String.valueOf(DBMethodsCart.newInstance(getApplicationContext()).getCartItems().size());
                    flag_closeorderbill = "";
                    for (int i = 0; i < DBMethodsCart.newInstance(getApplicationContext()).getCartItems().size(); i++) {

                        abcd = (DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_qty() + " X " + DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name());
                        /*+ DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name2()*/
                        Command.ESC_Align[2] = 0x00;
                        SendDataByte(Command.ESC_Align);
                        SendDataString(abcd + "\n");
                        Command.ESC_Align[2] = 0x02;
                        SendDataByte(Command.ESC_Align);
                        Float menuTax = Float.parseFloat(DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_price()) * (Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())) / 100;
                        Float menuWithTax = menuTax + Float.parseFloat(DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_price());
                        Float withTaxQuantity = DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_qty() * menuWithTax;
                        abcd = String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", menuWithTax) + "   " + (String.format("%." + AppSession.getInstance().getRestraunt_DecimalPosition() + "f", withTaxQuantity) + "\n");
                        SendDataString(abcd);
                        //abcd=DBMethodsCart.newInstance(getApplicationContext()).getCartItems().get(i).getMenu_name2();
                    }
                }

                String vatSales = "";
                //String nonvatSales=;
                vatSales += AppSession.getInstance().getRestraunt_tax_name() + " Sales" + " " + " :              " + tax1_applicable_sale + "\n";
                vatSales += "Non " + AppSession.getInstance().getRestraunt_tax_name() + " Sales" + "" + " :              " + tax1_non_applicable_sale + "\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(vatSales);


                //Toast.makeText(Main_ActivityPrinterLongin.this,  msggg+AppSession.getInstance().getOrderNumber(), Toast.LENGTH_LONG).show();
                String discount = "";

                discount += "DISCOUNT :  " + "           -" + other_discount + "\n";


                if (AppSession.getInstance().getApplyCoupon().equals("true")) {
                    if (coupon_type.equals("P")) {
                        discount += "COUPON DISCOUNT (" + AppSession.getInstance().getRestraunt_CurrencySymbol() + ") " + AppSession.getInstance().getCouponper() + "% :  " + "     -" + AppSession.getInstance().getCouponTotal() + "\n";

                    } else {
                        discount += "COUPON DISCOUNT (" + AppSession.getInstance().getRestraunt_CurrencySymbol() + ") " + AppSession.getInstance().getCouponper() + " :  " + "     -" + AppSession.getInstance().getCouponTotal() + "\n";

                    }

                } else {
                    discount += "";
                }

                String msgg = "------------------------------------------\n" + "  SUB TOTAL (" + AppSession.getInstance().getRestraunt_Currency() + ") :              " + subtotal + "\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(msgg);

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(discount);

                String taxstring = "", taxstring2 = "";
                float taxamount = 0, taxamount2 = 0;
                if (!AppSession.getInstance().getRestraunt_tax_value().equals("0")) {
                    if (AppSession.getInstance().getRestraunt_tax_type().equals("%")) {
                        //float taxvalue=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value());
                        //float subtotal=Float.parseFloat(ft);
                        //taxamount=taxPayable+taxExemption;

                        //VAT Excemption code here
                    } else {
                        //taxamount=Float.parseFloat();
                    }

                    taxstring = AppSession.getInstance().getRestraunt_tax_name() + "(+) " + AppSession.getInstance().getRestraunt_tax_value() + AppSession.getInstance().getRestraunt_tax_type() + " :              " + vat + "\n";
                    //Condition for count of senior citizen for 20% discount
                    //Condition for count of Disabled for 20%
                    //Condition for solo parent for now keep 0%
                    // if tax excemption > 0


                }
                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(taxstring);
                if (!AppSession.getInstance().getRestraunt_tax_value2().equals("0")) {
                    if (AppSession.getInstance().getRestraunt_tax_type2().equals("%")) {
                        float taxvalue2 = Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2());
//                            float subtotal=Float.parseFloat(ft);
//                            taxamount2=taxvalue2*subtotal/100;
                        //Service charge excemption code here


                    } else {
                        // taxamount2=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2());
                    }
                    taxstring2 = AppSession.getInstance().getRestraunt_tax_name2() + "(+) " + AppSession.getInstance().getRestraunt_tax_value2() + AppSession.getInstance().getRestraunt_tax_type2() + " :              " + service_charge + "\n";
                }

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(taxstring2);
                SendDataByte(PrinterCommand.POS_Set_Bold(1));
                msgg = "------------------------------------------\n" +
                        "  TOTAL (" + AppSession.getInstance().getRestraunt_Currency() + ") : " + "            " + total.toString() + "\n" +
                        "------------------------------------------\n";

                SendDataString(msgg);
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataByte(PrinterCommand.POS_Set_Bold(0));

                        msgg = "TOTAL ITEMS: " + TotalItems + "\n" +

                      //  msgg = "TOTAL ITEMS: " + DBMethodsCart.newInstance(getApplicationContext()).getCartItems().size() + "\n" +
                        /* "Printed Date:- "+getDateTime()+"\n" +*/
                        "\n";
                SendDataString(msgg);
          /*      String paymentdetails = "";
                if (!payment_type.equals("") && payment_type.equals("CASH")) {
                    paymentdetails = "Payment Type: " + payment_type + "         " + "\n" +
                           *//* "Tender Cash : " + tender_cash + "        " +*//*
                            "Payment Recieved : " + tender_cash + "        " +
                            "Tender Change: " + tender_change + "\n" +
                            "------------------------------------------\n";
                } else if (!payment_type.equals("") && payment_type.equals("CARD")) {
                    paymentdetails =
                            "Payment Type: " + payment_type + "    " + "Card Type: " + card_type + "\n" + "------------------------------------------\n";
                } else if (!payment_type.equals("") && payment_type.equals("BOTH")) {
                    paymentdetails =
                            "Payment Type: " + payment_type + "    " + "Card Type: " + card_type + "\n" + "------------------------------------------\n";
                } else {
                    paymentdetails =
                            "Payment Type: " + payment_type + "         " + "\n" +
                                   *//* "Tender Cash : " + tender_cash + "        " +*//*
                                    "Payment Recieved : " + tender_cash + "        " +
                                    "Tender Change: " + tender_change + "\n" +
                                    "------------------------------------------\n";
                }
                 SendDataString(paymentdetails);
                */
                if (categoryList_payment != null && categoryList_payment.size() > 0) {
                    for (int i = 0; i < categoryList_payment.size(); i++) {
                        String paymentdetailss = "";
                        String Card_Type = "";
                        paymentdetailss="Payment Type: "+categoryList_payment.get(i).get("payment")+ "         " + "\n" +
                                "Payment Recieved : " + categoryList_payment.get(i).get("amount") + "        " + "\n" ;
                                if(Card_Type!=null&&!Card_Type.equals(""))
                                {
                                    Card_Type=       "Card Type : " + categoryList_payment.get(i).get("card_type") + "        " + "\n";

                                }
                                else
                                {
                                    Card_Type="";
                                }
                        SendDataString(paymentdetailss+Card_Type);
                    }
                }
                String Total_Paid = "";
                //String nonvatSales=;
                Total_Paid = "Total Paid "+tender_cash+"\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(Total_Paid);

                String Total_change = "";
                //String nonvatSales=;
                Total_change = "Tender Change"+tender_change+"\n";

                Command.ESC_Align[2] = 0x02;
                SendDataByte(Command.ESC_Align);
                SendDataString(Total_change);


//
                String Customermsg = "REFERENCE BILL \n\n" +
                        "Customer Sign ____________________ \n\n" + "Customer Info" + "\n";
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(Customermsg);
                // Customer name should come here .. if already given
                String Customertext = "Name           : ____________________ \n" + "Address        : ____________________ \n" + "TIN            : ____________________ \n" + "Business Style : ____________________ \n" + "------------------------------------------\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(Customertext);


                String specialText = "Senior Citizen/PWD TIN : _______________ \n" + "OSCA ID No./ PWD ID No. : _____________ \n" + "TIN            : _________________ \n" + "Signature : _________________ \n" + "------------------------------------------\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(specialText);

                String POStext = "POS Provider:\nDAMPA FEAST PH INC\nTIN: 010-016-233-000\n\n";
                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                SendDataString(POStext);


                String footerText = "This serves as your OFFICIAL RECIEPT \n\n" + "" +
                        "This invoice reciept shall be valid for   five(5) years from the date of the permit to use \n\n\n";
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataString(footerText);
                    /*AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setCouponper("");
                    AppSession.getInstance().setCouponTotal("");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrderNumber("");
                    DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearCart();
                    DBMethodsCart.newInstance(Main_ActivityPrinterLongin.this).clearModifier();
*/
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.clear();
                editor1.commit();
                AppSession.getInstance().setGuest_Count("");
                AppSession.getInstance().setComplteBillDetails("");
                AppSession.getInstance().setOrderNumber("");
                AppSession.getInstance().setOrder_id_delivery("");
                AppSession.getInstance().setOrder_idTake_delivery("");
                AppSession.getInstance().setCouponper("");
                AppSession.getInstance().setCouponTotal("");
                AppSession.getInstance().setTable_Name("");
                DBMethodsCart.newInstance(this).clearCart();
                DBMethodsCart.newInstance(this).clearModifier();


                //AppSession.getInstance().setTable_ID("");
                       /* AppSession.getInstance().settotal("");
                        AppSession.getInstance().setSubTotal("");*/
                SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
                SendDataByte(PrinterCommand.POS_Set_Cut(1));
                SendDataByte(PrinterCommand.POS_Set_PrtInit());
                SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(10));
                //Command.ESC_Align[2] = 0x02;
                //SendDataByte(Command.ESC_Align);
                //SendDataString(date);



                    /*Intent i= new Intent(Main_ActivityPrinterLongin.this,NavigationMainActivity.class);
                    startActivity(i);
                    finish();*/


            }
            }

        }


    }

    /**
     * Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å’â€¡Ã¤Â»Â¤Ã¦Âµâ€¹Ã¨Â¯â€¢
     */
    private void CommandTest(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            new AlertDialog.Builder(Main_ActivityPrinterLongin.this).setTitle(getText(R.string.chosecommand))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SendDataByte(byteCommands[which]);
                            try {
                                if(which == 16 || which == 17 || which == 18 || which == 19 || which == 22
                                        || which == 23 || which == 24|| which == 0 || which == 1 || which == 27){
                                    return ;
                                }else {
                                    SendDataByte("Ã§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\nÃ§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\nÃ§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\nÃ§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\nÃ§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\nÃ§Æ’Â­Ã¦â€¢ÂÃ§Â¥Â¨Ã¦ÂÂ®Ã¦â€°â€œÃ¥ÂÂ°Ã¦Å“ÂºABCDEFGabcdefg123456,.;'/[{}]!\n".getBytes("GBK"));
                                }

                            } catch (UnsupportedEncodingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }).create().show();
        }else if((lang.compareTo("en")) == 0){
            new AlertDialog.Builder(Main_ActivityPrinterLongin.this).setTitle(getText(R.string.chosecommand))
                    .setItems(itemsen, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SendDataByte(byteCommands[which]);
                            try {
                                if(which == 16 || which == 17 || which == 18 || which == 19 || which == 22
                                        || which == 23 || which == 24|| which == 0 || which == 1 || which == 27){
                                    return ;
                                }else {
                                    SendDataByte("Thermal Receipt Printer ABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\n".getBytes("GBK"));
                                }

                            } catch (UnsupportedEncodingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }).create().show();
        }
    }
    /************************************************************************************************/
    /*
     * Ã§â€Å¸Ã¦Ë†ÂQRÃ¥â€ºÂ¾
     */
    private void createImage() {
        try {
            // Ã©Å“â‚¬Ã¨Â¦ÂÃ¥Â¼â€¢Ã¥â€¦Â¥zxingÃ¥Å’â€¦
            QRCodeWriter writer = new QRCodeWriter();

            String text = editText.getText().toString();

            Log.i(TAG, "Ã§â€Å¸Ã¦Ë†ÂÃ§Å¡â€žÃ¦â€“â€¡Ã¦Å“Â¬Ã¯Â¼Å¡" + text);
            if (text == null || "".equals(text) || text.length() < 1) {
                Toast.makeText(this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                return;
            }

            // Ã¦Å Å Ã¨Â¾â€œÃ¥â€¦Â¥Ã§Å¡â€žÃ¦â€“â€¡Ã¦Å“Â¬Ã¨Â½Â¬Ã¤Â¸ÂºÃ¤ÂºÅ’Ã§Â»Â´Ã§ Â
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE,
                    QR_WIDTH, QR_HEIGHT);

            System.out.println("w:" + martix.getWidth() + "h:"
                    + martix.getHeight());

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }

                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.ARGB_8888);

            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);

            byte[] data = PrintPicture.POS_PrintBMP(bitmap, 384, 0);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
    //************************************************************************************************//
    /*
     * Ã¨Â°Æ’Ã§â€Â¨Ã§Â³Â»Ã§Â»Å¸Ã§â€ºÂ¸Ã¦Å“Âº
     */
    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, actionCode);
    }

    private void handleSmallCameraPhoto(Intent intent) {
        Bundle extras = intent.getExtras();
        Bitmap mImageBitmap = (Bitmap) extras.get("data");
        imageViewPicture.setImageBitmap(mImageBitmap);
    }
/****************************************************************************************************/
    /**
     * Ã¥Å  Ã¨Â½Â½assetsÃ¦â€“â€¡Ã¤Â»Â¶Ã¨Âµâ€žÃ¦ÂºÂ
     */
    private Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;

    }
    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }


    protected String getConverTextUTFToNormal(String arabicValue) {
        String getConvertValue = "";
        try {
            getConvertValue = new String(arabicValue.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            getConvertValue = arabicValue;
            e.printStackTrace();
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(getConvertValue, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(getConvertValue).toString();
        }
    }
    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static String fixedLengthString(String string, int length) {
        return String.format(string,"%1$"+length+ "s");
    }
    public Bitmap addPaddingLeftForBitmap(Bitmap bitmap, int paddingLeft) {
        Bitmap outputBitmap = Bitmap.createBitmap(bitmap.getWidth() + paddingLeft, bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, paddingLeft, 0, null);
        return outputBitmap;
    }

    @Override

    protected void onStop(){





        mBitmap1.recycle();





        System.gc();

        super.onStop();

    }


}

