package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by USER on 7/24/2018.
 */

public class UpdateCartResponse {

    @SerializedName("action")
    @Expose
    private String action;

    @SerializedName("location_id")
    @Expose
    private String locationId;

    @SerializedName("staff_id")
    @Expose
    private String staffId;

    @SerializedName("tab_token")
    @Expose
    private String tabToken;

    @SerializedName("order")
    @Expose
    private ArrayList<OrderList> order;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getTabToken() {
        return tabToken;
    }

    public void setTabToken(String tabToken) {
        this.tabToken = tabToken;
    }

    public ArrayList<OrderList> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<OrderList> order) {
        this.order = order;
    }


    public class OrderList {

        @SerializedName("row_id")
        @Expose
        private String rowId;

        @SerializedName("menu_id")
        @Expose
        private String menuId;

        @SerializedName("quantity")
        @Expose
        private String comment;

        @SerializedName("comment")
        @Expose
        private String quantity;

        public String getRowId() {
            return rowId;
        }

        public void setRowId(String rowId) {
            this.rowId = rowId;
        }

        public String getMenuId() {
            return menuId;
        }

        public void setMenuId(String menuId) {
            this.menuId = menuId;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

}
