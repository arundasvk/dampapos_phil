package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.KitchenOrderListResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.Kitchenorder;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.LoginResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.MenuItemsList;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.KitchenLoginOrderListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.KitchenOrderListDetailsadapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.KitchenOrderListadapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class KitchenFragment extends Fragment   {
    public static final String TAG = "KitchenFragment";
    boolean is_refresh_table=false,is_got_response=false;
    private String OrderId="",OrderMenuId="",Status="";
    private ListView listview,listview2,listview_popup;
    LinearLayout rl_container;
    KitchenOrderListadapter adapter;
    KitchenOrderListDetailsadapter adapter3;
    KitchenLoginOrderListAdapter adapter2;
    onApiFinish mOnApiFinish;
    private Context mContext;
    private String FROM_ORDERLIST = "kitchenorderlist";
    private String  FROM_ORDERLIST_satusChange="orderliststatuschange";
    private String  FROM_KITCHEN_LOGIN_ORDER="kitchen_login_orderlist";
    private String  FROM_ALL_KITCHEN_ORDER_COMPLETE="all_kitchenorder_complete";
    private GridView gridview;
    ItemClickListener mItemClickListner;
    ArrayList<Kitchenorder> Orderlist = new ArrayList<Kitchenorder>();
    ArrayList<Kitchenorder> K_login_Orderlist = new ArrayList<Kitchenorder>();
    ArrayList<MenuItemsList> K_login_Orderlist_details = new ArrayList<MenuItemsList>();
    TextView tv_toolbar_title;
    LinearLayout sidemenu;
    View view;
    ListView listView;
    ImageView img_close;
    TextView txt_complete_all;
    ProgressBar simpleProgressBar;
    private ConnectionDetector objDetector;
    private  AlertDialog.Builder dialogBuilder;
    private AlertDialog alertDialog;
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    public static KitchenFragment newInstance() {
        KitchenFragment fragment = new KitchenFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.kitchen_order_list, container, false);
        objDetector = new ConnectionDetector(getContext());
        mContext=getActivity();
        tv_toolbar_title = getActivity().findViewById(R.id.tv_toolbar_title);
        gridview = view.findViewById(R.id.gridview);
        sidemenu = getActivity().findViewById(R.id.sidemenu);
        rl_container = view.findViewById(R.id.rl_container);
        img_close = view.findViewById(R.id.img_close);
        tv_toolbar_title.setText(getResources().getString(R.string.order_head));
        listview = view.findViewById(R.id.listview);
        listview2 = view.findViewById(R.id.listview2);
        txt_complete_all = view.findViewById(R.id.txt_complete_all);
        AppSession.getInstance().setCompleteOrderList(0);
        rl_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rl_container.setVisibility(View.GONE);


            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rl_container.setVisibility(View.GONE);


            }
        });

        txt_complete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity()!=null)
                {
                    completeAll();
                }



            }
        });
        initi();

        initCallBackLisrners();
        simpleProgressBar = view.findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);
      //  simpleProgressBar.setVisibility(View.VISIBLE);
      /*  if(getActivity()!=null)
        {
            startTimer();

        }
*/

        if(AppSession.getInstance().getUser_Type().equals("KITCHEN")||
                AppSession.getInstance().getUser_Type().equals("BAR"))
        {
            tv_toolbar_title.setText(AppSession.getInstance().getUser_Type());
            sidemenu.setVisibility(View.GONE);
        }
       /* adapter = new KitchenOrderListadapter(getActivity(),Orderlist);
        adapter.setCustomButtonListner(this);*/

       /* listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                OrderId=Orderlist.get(i).getOrder_id();
                OrderMenuId=Orderlist.get(i).getOrder_menu_id();
                loadChangeOderStatusData();




            }
        });
*/
        if (!ConnectionDetector.isConnectingToInternet()) {
            View view = inflater.inflate(R.layout.activity_network_connection, null, false);
            simpleProgressBar = view.findViewById(R.id.progressSpinner);
            simpleProgressBar.setScaleY(3f);
            simpleProgressBar.setScaleX(3f);
            simpleProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {



                @Override
                public void run() {

                    simpleProgressBar.setVisibility(View.GONE);

                }
            }, 3000);
            return view;
        }
        return view;
    }



    private void initCallBackLisrners() {


        mItemClickListner = new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onItemSelected(String tag, int position, Object response) {

                if (tag.equals("tick_button")) {
                   /* Toast.makeText(getActivity(), "tick click.... " ,
                            Toast.LENGTH_SHORT).show();*/
                    Status="14";
                    OrderId=Orderlist.get(position).getOrder_id();
                    OrderMenuId=Orderlist.get(position).getOrder_menu_id();
                    if(getActivity()!=null)
                    {
                        loadChangeOderStatusData();
                    }

                    adapter.notifyDataSetChanged();
                }
                else  if (tag.equals("tick_button2")) {
                   /* Toast.makeText(getActivity(), "tick click.... " ,
                            Toast.LENGTH_SHORT).show();*/
                    Status="14";
                    OrderId=K_login_Orderlist_details.get(position).getOrder_id();
                    OrderMenuId=K_login_Orderlist_details.get(position).getOrder_menu_id();
                    if(getActivity()!=null)
                    {
                        loadChangeOderStatusData();
                    }

                    adapter3.notifyDataSetChanged();
                }
                else  if (tag.equals("slide_button")) {
                   /* Toast.makeText(getActivity(), "slide.... " ,
                            Toast.LENGTH_SHORT).show();*/
                    Status="13";
                    OrderId=Orderlist.get(position).getOrder_id();
                    OrderMenuId=Orderlist.get(position).getOrder_menu_id();
                    if(getActivity()!=null)
                    {
                        loadChangeOderStatusData();
                    }

                    adapter.notifyDataSetChanged();
                }
                else  if (tag.equals("slide_button2")) {
                   /* Toast.makeText(getActivity(), "slide.... " ,
                            Toast.LENGTH_SHORT).show();*/
                    Status="13";
                    OrderId=K_login_Orderlist_details.get(position).getOrder_id();
                    OrderMenuId=K_login_Orderlist_details.get(position).getOrder_menu_id();
                    if(getActivity()!=null)
                    {
                        loadChangeOderStatusData();
                    }

                    adapter3.notifyDataSetChanged();
                }

                else  if (tag.equals("select_order")) {
                    K_login_Orderlist_details = K_login_Orderlist.get(position).getMenuItemsLists();
                    AppSession.getInstance().setOrderType(K_login_Orderlist.get(position).getOrder_type());
                    OrderId=K_login_Orderlist.get(position).getOrder_id();
                    if(K_login_Orderlist_details.size()>0)
                    {
                        //list_popup();
                        rl_container.setVisibility(View.VISIBLE);
                        for(int i=0;i<K_login_Orderlist_details.size();i++)
                        {
                            K_login_Orderlist_details.get(i).setTable_name(K_login_Orderlist.get(position).getTable_info().getTable_name());

                            adapter3 = new KitchenOrderListDetailsadapter(getActivity(),K_login_Orderlist_details,mItemClickListner);
                            listview2.setAdapter(adapter3);
                        }

                    }

         /*    for(int i=0;i<K_login_Orderlist_details.size();i++)
             {

                 adapter3 = new KitchenOrderListDetailsadapter(getActivity(),K_login_Orderlist_details,mItemClickListner);
                 listview.setAdapter(adapter3);

                 gridview.setVisibility(View.GONE);
                 listview.setVisibility(View.VISIBLE);
                 adapter3.notifyDataSetChanged();
                 Toast.makeText(getActivity(), K_login_Orderlist_details.get(i).getMenuId().toString() ,
                         Toast.LENGTH_SHORT).show();
             }*/

                }

            }
        };


    }


    @Override
    public void onStart() {
        super.onStart();
       /* is_got_response=true;
        is_refresh_table=false;
        Log.d(TAG, "####################################################on start###########################");
        stopTimer();
        startTimer();*/
        // settime();
    }
    @Override
    public void onStop() {
        super.onStop();
     /*   Log.d(TAG, "####################################################on stop###########################");

        stopTimer();*/
    }

    @Override
    public void onResume() {
        super.onResume();
        is_got_response=true;
        is_refresh_table=false;
        Log.d(TAG, "####################################################onResume###########################");
        stopTimer();
        startTimer();
        /*adapter = new KitchenOrderListadapter(getActivity(),Orderlist,mItemClickListner);
        adapter.setCustomButtonListner(this);*/
        //callme();

    }
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "####################################################onPause###########################");

        stopTimer();
    }


    private void loadOrderData() {
        Log.d(TAG, "####################################################loadOrderData###########################");

        String URL = AppSession.getInstance().getRegUrl()+Constants.API.KITCHEN_ORDERS;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /*values.put("action", "kitchenOrders");*/
        values.put("action", Constants.API.ACTION_KITCHEN_ORDERS);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id",AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("user_type", AppSession.getInstance().getUser_Type());

        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_ORDERLIST, "POST");

    }
    private void loadOrderDataList() {
        Log.d(TAG, "####################################################loadOrderDataList###########################");

        String URL = AppSession.getInstance().getRegUrl()+ API.KITCHEN_LOGIN_ORDER;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /* values.put("action", "kitchenOrderList");*/
        values.put("action",  API.ACTION_KITCHEN_LOGIN_ORDER);
        //values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id",AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("user_type", AppSession.getInstance().getUser_Type());

        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_KITCHEN_LOGIN_ORDER, "POST");

    }
    private void loadChangeOderStatusData() {

        String URL = AppSession.getInstance().getRegUrl()+Constants.API.KITCHEN_ORDERS_STATUS_CHANGE;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /*values.put("action", "kitchenOrderstatus");*/
        values.put("action", Constants.API.ACTION_KITCHEN_ORDERS_STATUS_CHANGE);
        values.put("tab_token",AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id", OrderId);
        values.put("order_menu_id", OrderMenuId);
        values.put("status", Status);
        values.put("user_type", AppSession.getInstance().getUser_Type());
        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_ORDERLIST_satusChange, "POST");

    }
    private void completeAll() {
        String URL = AppSession.getInstance().getRegUrl()+ API.ALL_KITCHEN_ORDER_COMPLETE;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /*values.put("action", "completeKitchenOrderItems");*/
        values.put("action",  API.ACTION_ALL_KITCHEN_ORDER_COMPLETE);
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id", OrderId);
        values.put("status", "14");
        values.put("user_type", AppSession.getInstance().getUser_Type());
        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_ALL_KITCHEN_ORDER_COMPLETE, "POST");
    }
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job

        initializeTimerTask();
        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 0, 5000); //
    }
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp
                        callme();
                    }
                });
            }
        };
    }
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void callme() {
        simpleProgressBar.setVisibility(View.VISIBLE);
        if(is_got_response)
        {
            is_got_response=false;
            if(AppSession.getInstance().getUser_Type().equals("KITCHEN")||
                    AppSession.getInstance().getUser_Type().equals("BAR"))
            {
                gridview.setVisibility(View.VISIBLE);
                listview.setVisibility(View.GONE);
                /*  rl_container.setVisibility(View.GONE);*/
                if(getActivity()!=null) {
                    loadOrderDataList();
                }

               /* simpleProgressBar.setVisibility(View.GONE);*/
            }
            else
            {
                listview.setVisibility(View.VISIBLE);
                gridview.setVisibility(View.GONE);
                rl_container.setVisibility(View.GONE);
                if(getActivity()!=null) {
                    loadOrderData();
                }

            }
        }



    }
    public void initi(){
        mOnApiFinish = new onApiFinish() {
            @Override
            public void onSuccess(String tag, String response) {
                //simpleProgressBar.setVisibility(View.GONE);

                if (tag.equals(FROM_ORDERLIST)) {

                    is_got_response=true;
                    Gson gson = new Gson();
                    KitchenOrderListResponse menuResponse = gson.fromJson(response, KitchenOrderListResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        if (getActivity()!=null) {
                            Orderlist.clear();
                            Orderlist=menuResponse.getOrderItemsLists();
                            adapter = new KitchenOrderListadapter(getActivity(),Orderlist,mItemClickListner);
                            listview.setAdapter(adapter);



                        }


                        //Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }else if(tag.equals(FROM_ORDERLIST_satusChange)){
                    Gson gson = new Gson();
                    LoginResponse menuResponse = gson.fromJson(response, LoginResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        if (getActivity()!=null) {


                        }


                        // Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
                else if(tag.equals(FROM_ALL_KITCHEN_ORDER_COMPLETE)){
                    Gson gson = new Gson();
                    LoginResponse menuResponse = gson.fromJson(response, LoginResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                       /* simpleProgressBar.setVisibility(View.GONE);*/
                        rl_container.setVisibility(View.GONE);
                        if (getActivity()!=null) {
                            simpleProgressBar.setVisibility(View.VISIBLE);
                            loadOrderDataList();
                        }


                        // Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
                else if(tag.equals(FROM_KITCHEN_LOGIN_ORDER)){
                    is_got_response=true;
                    Gson gson = new Gson();
                    KitchenOrderListResponse menuResponse = gson.fromJson(response, KitchenOrderListResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);
                        if (getActivity()!=null) {

                            K_login_Orderlist.clear();
                            K_login_Orderlist=menuResponse.getOrderItemsLists();
                            if(AppSession.getInstance().getPrev_size()<(K_login_Orderlist.size()))
                            {
                                AppSession.getInstance().setKitchen_newflag(1);
                                AppSession.getInstance().setPrev_size(K_login_Orderlist.size());
                            }
                            else
                            {
                                AppSession.getInstance().setKitchen_newflag(0);
                                AppSession.getInstance().setPrev_size(K_login_Orderlist.size());
                            }
                            adapter2 = new KitchenLoginOrderListAdapter(getActivity(),K_login_Orderlist,mItemClickListner);
                            gridview.setAdapter(adapter2);
                        }


                        // Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(getActivity(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }else{

                }


            }

            @Override
            public void onFailed(String tag, String response) {
                is_got_response=true;
                Gson gson = new Gson();
                LoginResponse menuResponse = gson.fromJson(response, LoginResponse.class);
                if (getActivity()!=null) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onJsonError(String tag, String response) {
                is_got_response=true;
                if (getActivity()!=null) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onServerError(String tag, String response) {
                is_got_response=true;
                if (getActivity()!=null) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }


                //  Toast.makeText(mContext,"server error", Toast.LENGTH_SHORT).show();
            }

        };}
}

