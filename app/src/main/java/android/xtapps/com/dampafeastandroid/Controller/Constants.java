package android.xtapps.com.dampafeastandroid.Controller;

public class Constants {
    public interface API {






        String BASE_URL="http://18.220.183.40/dampafeast_cloud";
        String BASE_URL_LOGIN = "http://18.220.183.40/dampafeast_local/applicationapi/";


        String BASE_RESTAURANT_LOCATION = BASE_URL+"/admin/index.php/apicontrol/initapi";
        String BASE_URL_REGISTER = BASE_URL+"/admin/apicontrol/";
        String LOGO_IN_PRINTER = BASE_URL+"/assets/images/data/Dampa_Logo_line_work.jpg";

        String UPDATE_APK_DOWNLOAD = BASE_URL+"/admin/downloads/pampanga/dampafeast.apk";
        String UPDATE_APK_VERSIONCODE = BASE_URL+"/admin/downloads/pampanga/version.php";

        String PAGINATION ="&limit=500";
        String REGISTERATION = BASE_URL_REGISTER + "tabregister";
        String LOGIN = "applicationapi/stafflogin";
        String TABLE_LIST ="applicationapi/allTableinfo";
        String TABLE_LIST2 ="applicationapi/allTableInfo2";
        String MENU_LIST ="applicationapi/menuInfo";
        String ADD_TO_CART="applicationapi/addtoCart";
        String KITCHEN_ORDERS="applicationapi/kitchenOrders";
        String KITCHEN_ORDERS_STATUS_CHANGE="applicationapi/kOrderStatusChange";
        String CONTINUE_TABLE_ORDER = "applicationapi/continueTableOrder";
        String CHECK_MENU_STATUS = "applicationapi/checkOrderMenuStatus";
        String CHECK_ORDER_STATUS = "applicationapi/getOrderStatus";
        String DELETE_MENU_ORDER = "applicationapi/deleteMenuOrder";
        String REPORT_MENU_ORDER = "applicationapi/orderReports";
        String APPLY_COUPON = "applicationapi/validateCoupon";
        String RELEASE_TABLE = "applicationapi/releaseTable";
      /*  String RELEASE_TABLE_2 = "applicationapi/deleteOrderMenus";*/
      String DELETE_MENU_ORDERS = "applicationapi/deleteOrderMenus";

        String MODIFIERSAPI = "applicationapi/menuOptionsInfo";
        String NOTIFICATIONAPI = "applicationapi/tableNotification";
        String COMPLETE_ORDER = "applicationapi/completeOrder";
        String KITCHEN_LOGIN_ORDER = "applicationapi/kitchenOrderList";
        String ALL_KITCHEN_ORDER_COMPLETE = "applicationapi/completeKitchenOrderItems";
        String KITCHEN_LOGIN_takeaway = "applicationapi/kitchenOrderItems";
       /* String ORDERBILLAPI = "applicationapi/ordertoBill";*/
        String ORDERBILLAPI = "applicationapi/getBill";
        String ORDERBILL_AGAIN_API = "applicationapi/getBillAgain";
        String CLOSE_RESTAURANT = "applicationapi/closeRestaurant";
        String SUPERVISOR_LIST = "applicationapi/supervisorsList";
        String PAYMENT_TYPES = "applicationapi/paymentTypes";
        String PRINT_IF_CLOSED = "applicationapi/checkIfClosed";

        // Actions

        String ACTION_BASE_RESTAURANT_LOCATION = "basic";
        String ACTION_BASE_URL_REGISTER = "http://18.220.183.40/dampafeast_cloud/admin/apicontrol/";
        String ACTION_BASE_URL_LOGIN = "http://18.220.183.40/dampafeast_local/applicationapi/";



        String ACTION_PAGINATION ="&limit=500";
        String ACTION_REGISTERATION = "development";
        String ACTION_LOGIN = "StaffLogin";
        String ACTION_TABLE_LIST ="TableInfo";
        String ACTION_MENU_LIST ="MenuInfo";
        String ACTION_ADD_TO_CART="AddCartOrder";
        String ACTION_KITCHEN_ORDERS="kitchenOrders";
        String ACTION_KITCHEN_ORDERS_STATUS_CHANGE="kitchenOrderstatus";
        String ACTION_CONTINUE_TABLE_ORDER = "continueTableOrder";
        String ACTION_CHECK_MENU_STATUS = "checkOrderMenuStatus";
        String ACTION_CHECK_ORDER_STATUS = "getOrderStatus";
        String ACTION_DELETE_MENU_ORDER = "deleteMenuOrder";
        String ACTION_REPORT_MENU_ORDER = "orderReports";
        String ACTION_APPLY_COUPON = "verifyCoupon";
        String ACTION_RELEASE_TABLE = "releaseTable";
       /* String ACTION_RELEASE_TABLE_2 = "deleteOrderMenus";*/
       String ACTION_DELETE_MENU_ORDERS = "deleteOrderMenus";

        String ACTION_MODIFIERSAPI = "menuOptionsInfo";
        String ACTION_NOTIFICATIONAPI = "TableNotification";
        String ACTION_COMPLETE_ORDER = "completeOrder";
        String ACTION_KITCHEN_LOGIN_ORDER = "kitchenOrderList";
        String ACTION_ALL_KITCHEN_ORDER_COMPLETE = "completeKitchenOrderItems";
        String ACTION_KITCHEN_LOGIN_takeaway = "kitchenOrderItems";
       /* String ACTION_ORDERBILL = "ordertoBill";*/
        String ACTION_ORDERBILL = "getBill";
        String ACTION_ORDER_AGAIN_BILL = "getBillAgain";
        String ACTION_CLOSE_RESTAURANT = "closeRestaurant";
        String ACTION_SUPERVISOR_LIST = "supervisorsList";
        String ACTION_PAYMENT_TYPES = "paymentTypes";
        String ACTION_PRINT_IF_CLOSED = "checkIfClosed";
    }

    public interface FragmentLabels {
        String HOME = "home";
        String FAVOURITE = "favourite";
        String CATEGORY_LIST = "category_main";
        String CATEGORY_ITEM_LIST = "category_items";
        String FEATURED_LIST = "featured";
        String ALL_LIST = "all_list";
        String TRACK_NOW = "track_now";
    }


    public interface IntentKeys {
        String TARGET_FRAGMENT = "target_fragment";

        public interface ProductDetails {
            String ID = "_id";
            String NAME = "_name";
            String DESCRIPTION = "_desc";
            String IMAGE_URL = "_image";
            String CURRENCY = "_currecy";
            String PRICE = "_price";
            String RATING = "_rating";
            String OPTION_VALUE_ID = "_option_value_id";
            String PRODUCT_OPTION_VALUE_ID = "_product_option_value_id";
            String PRICE_PREFIX = "_price_prefix";

        }

        public interface StoreDetails{
            String STORE_ID = "_store";
        }

        public interface PromotionDetails{
            String CODE = "_code";
            String NAME = "_name";
            String DESCRIPTION = "_description";
        }
    }
}
