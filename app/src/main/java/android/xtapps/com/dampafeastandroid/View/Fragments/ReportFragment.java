package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.AddCartResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.CloseRestrauntResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ItemList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ReportResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Activity.Main_ActivityPrinterLongin;
import android.xtapps.com.dampafeastandroid.View.Adapters.MyOrderExpandableListAdapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;
import android.xtapps.com.dampafeastandroid.util.ConnectionDetector;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ReportFragment extends Fragment  {
    public static final String MyPREFERENCES_CLOSE = "MyPrefsCloseRestraunt";
    private SharedPreferences sharedpreferencesCloseRestraunt;
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList2 = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList3 = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList4_cancelled = new ArrayList<>();
    ArrayList<HashMap<String, String>> categoryList_payment = new ArrayList<>();
    HashMap<String, String> map1;
    HashMap<String, String> map2;
    HashMap<String, String> map3;
    HashMap<String, String> map4_cancelled;
    HashMap<String, String> map5_payment;
    private String TAG = ReportFragment.class.getSimpleName();
     private  LinearLayout ll_print;
    private  AlertDialog.Builder dialogBuilder;
    private AlertDialog alertDialog;
    private Context mContext;
    private ImageView img_calender;
    private TextView tv_date,tv_daily,tv_weekly,tv_monthly,tv_close_sale;
    private TextView tv_canceled_orders,tv_takeaway_orders,tv_delivery_orders,tv_dine_order,tv_total_orders,
            tv_bycash_value,tv_bycard_value,tv_cash_short,tv_no_of_bills,tv_gross_amount,tv_payment_received,
            tv_tender_change,tv_net_amount,tv_by_gift_value,tv_by_makan_value,tv_by_talabat_value,tv_net_discount;
    private int birthYear;
    private ArrayList<OrderList> headerList = new ArrayList<OrderList>();
    private ArrayList<ItemList> childList = new ArrayList<ItemList>();
    private final String FROM_ORDERBILL_AGAIN = "orderbill_again";
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    String order_number="";
    private int day1;
    private int month1;
    private MyOrderExpandableListAdapter listAdapter;
    private ExpandableListView expandableListView;
    private int year1;
    private Calendar cal1;
    String day1string, month1string, year1string;
    onApiFinish mOnApiFinish;
    private  String DateValue="",DateValue2="";
    TextView tv_toolbar_title;
    TextView tv_from_to;
    private String  FROM_REPORT="report";
    private String  FROM_CLOSE="close_restraunt";
    private String  FROM_PRINT_IF_CLOSED="print_if_closed";
    int someTimeDuringYear1,someTimeDuringMonth1,someTimeDuringDay1;
    ProgressBar simpleProgressBar;
    private ConnectionDetector objDetector;
    ListView list;
    ImageView print;
    LinearLayout sidemenu,ll_date;
    String sort="";
    ImageView img_success,img_calender_close;
    TextView tv_date_close ;
    EditText ed_total;
    public static KitchenFragment newInstance() {
        KitchenFragment fragment = new KitchenFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.report_fragment, container, false);
        objDetector = new ConnectionDetector(getContext());
        mContext = getActivity();

        tv_toolbar_title =(TextView) getActivity().findViewById(R.id.tv_toolbar_title);
        simpleProgressBar = (ProgressBar)view.findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);

        sidemenu =(LinearLayout) getActivity().findViewById(R.id.sidemenu);
        ll_date =(LinearLayout) view.findViewById(R.id.ll_date);

        img_calender =(ImageView) view.findViewById(R.id.img_calender);
        tv_date =(TextView) view.findViewById(R.id.tv_date);
       /* list=getActivity().findViewById(R.id.listview);
        list.setVisibility(View.GONE);*/
        tv_cash_short=(TextView) view.findViewById(R.id.tv_cash_short);
        tv_no_of_bills=(TextView) view.findViewById(R.id.tv_no_of_bills);
        tv_gross_amount=(TextView) view.findViewById(R.id.tv_gross_amount);
        tv_payment_received=(TextView) view.findViewById(R.id.tv_payment_received);
        tv_tender_change=(TextView) view.findViewById(R.id.tv_tender_change);
        tv_net_amount=(TextView) view.findViewById(R.id.tv_net_amount);
        tv_canceled_orders =(TextView) view.findViewById(R.id.tv_canceled_orders);
        tv_takeaway_orders =(TextView) view.findViewById(R.id.tv_takeaway_orders);
        tv_delivery_orders =(TextView) view.findViewById(R.id.tv_delivery_orders);
        tv_dine_order =(TextView) view.findViewById(R.id.tv_dine_order);
        tv_total_orders =(TextView) view.findViewById(R.id.tv_total_orders);
        tv_bycash_value =(TextView) view.findViewById(R.id.tv_bycash_value);
        tv_bycard_value =(TextView) view.findViewById(R.id.tv_bycard_value);
        tv_by_gift_value=(TextView) view.findViewById(R.id.tv_by_gift_value);
        tv_by_makan_value=(TextView) view.findViewById(R.id.tv_by_makan_value);
        tv_by_talabat_value=(TextView) view.findViewById(R.id.tv_by_talabat_value);
        tv_net_discount=(TextView) view.findViewById(R.id.tv_net_discount);

        tv_from_to =(TextView) view.findViewById(R.id.tv_from_to);
        tv_daily =(TextView) view.findViewById(R.id.tv_daily);
        tv_weekly =(TextView) view.findViewById(R.id.tv_weekly);
        tv_monthly =(TextView) view.findViewById(R.id.tv_monthly);
        tv_close_sale =(TextView) view.findViewById(R.id.tv_close_sale);

        ll_print=(LinearLayout) view.findViewById(R.id.ll_print);

        sharedpreferencesCloseRestraunt = getActivity().getSharedPreferences(MyPREFERENCES_CLOSE, Context.MODE_PRIVATE);

        sharedpreferencesData = getActivity().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


        sort="DAILY";

        tv_daily.setBackground(getResources().getDrawable(R.drawable.report_selected_rectangle));
        tv_weekly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
        tv_monthly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
      //  tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
        tv_toolbar_title.setText(getResources().getString(R.string.report_heading));

        if(AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            tv_toolbar_title.setText(AppSession.getInstance().getUser_Type());
            sidemenu.setVisibility(View.GONE);
            tv_daily.setVisibility(View.GONE);
            tv_weekly.setVisibility(View.GONE);
            tv_monthly.setVisibility(View.GONE);
          /*  ll_date.setVisibility(View.GONE);*/
            tv_close_sale.setVisibility(View.VISIBLE);
            ll_print.setVisibility(View.VISIBLE);

        }

        initApiresults();

        if(tv_date.getText().equals("Date"))
        {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateValue = df.format(c);
            tv_date.setText(DateValue);
            tv_from_to.setText(" On "+tv_date.getText().toString());
        }



        if (getActivity() != null) {
            simpleProgressBar.setVisibility(View.VISIBLE);
            loadReportData();
        }

        img_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                    DateDialog();
                }

            }
        });
        tv_close_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                   // tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_selected_rectangle));
                    close_popup();
                }

            }
        });

        ll_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity() != null) {
                    print_if_closed();
                }

            }
        });
        cal1 = Calendar.getInstance();
        day1 = cal1.get(Calendar.DAY_OF_MONTH);
        month1 = cal1.get(Calendar.MONTH);
        year1 = cal1.get(Calendar.YEAR);
        cal1.set(Calendar.YEAR, year1);
        cal1.set(Calendar.DAY_OF_MONTH,day1);
        cal1.set(Calendar.MONTH,year1);
        /*cal1.add( Calendar.YEAR, -15 );*/
        /* cal1.set(year1 - 15, month1, day1);*/


        day1string = String.valueOf(day1);
        month1string = String.valueOf(month1);
        year1string = String.valueOf(year1);





        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableList);

        //create the adapter by passing your ArrayList data
        listAdapter = new MyOrderExpandableListAdapter(getActivity(), headerList);
        //attach the adapter to the list

        expandableListView.setAdapter(listAdapter);




        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {




                //get the group header
                OrderList headerInfo = headerList .get(groupPosition);
                //get the child info
                ItemList detailInfo =  headerInfo.getItems().get(childPosition);
                //display it or do something with it
               /* Toast.makeText(getActivity(), " Clicked on :: " + headerInfo.getMenu_status_name()
                        + "/" + detailInfo.getName() + "/" +detailInfo.getPrice(), Toast.LENGTH_LONG).show();*/
                return false;
            }
        });
        // setOnGroupClickListener listener for group heading click
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                print = (ImageView) v.findViewById(R.id.orderchanged);

                //get the group header
                OrderList headerInfo = headerList.get(groupPosition);
                order_number=  headerInfo.getOrder_id();
                print .setOnClickListener(new  TextView.OnClickListener() {
                    public void onClick(View v) {
                        OrderBillAgain();
                    }
                });

                /*  v.setBackgroundResource(R.color.fil);*/
                //display it or do something with it
             /* Toast.makeText(getActivity(), " Header is :: " + headerInfo.getOrder_id()+ "/"+ headerInfo.getMenu_status_name(),
                        Toast.LENGTH_LONG).show();*/



                return false;
            }
        });


        tv_daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv_daily.setBackground(getResources().getDrawable(R.drawable.report_selected_rectangle));
                tv_weekly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                tv_monthly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                tv_from_to.setText(" On "+DateValue);
                sort="DAILY";
                simpleProgressBar.setVisibility(View.VISIBLE);
                if (getActivity()!=null) {
                    loadReportData();
                }


            }
        });
        tv_weekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_daily.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                tv_weekly.setBackground(getResources().getDrawable(R.drawable.report_selected_rectangle));
                tv_monthly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                sort="WEEKLY";
                Calendar c = Calendar.getInstance();   // this takes current date
                c.set(Calendar.DAY_OF_WEEK, 1);
                Date c1 = c.getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                tv_from_to.setText(" From "+df.format(c1).toString()+" To "+DateValue);
                simpleProgressBar.setVisibility(View.VISIBLE);

                if (getActivity()!=null) {
                    loadReportData();
                }
            }
        });

        tv_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_daily.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                tv_weekly.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                tv_monthly.setBackground(getResources().getDrawable(R.drawable.report_selected_rectangle));

                sort="MONTHLY";
                Calendar c = Calendar.getInstance();   // this takes current date
                c.set(Calendar.DAY_OF_MONTH, 1);
                Date c1 = c.getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                tv_from_to.setText(" From "+df.format(c1).toString()+" To "+DateValue);
                simpleProgressBar.setVisibility(View.VISIBLE);
                if (getActivity()!=null) {
                    loadReportData();
                }
                //Toast.makeText(mContext, df.format(c1).toString(), Toast.LENGTH_SHORT).show();
            }
        });

        if (!objDetector.isConnectingToInternet()) {
            View view1 = inflater.inflate(R.layout.activity_network_connection, null, false);
            simpleProgressBar = (ProgressBar)view1.findViewById(R.id.progressSpinner);
            simpleProgressBar.setScaleY(3f);
            simpleProgressBar.setScaleX(3f);
            simpleProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {



                @Override
                public void run() {

                    simpleProgressBar.setVisibility(View.GONE);

                }
            }, 3000);
            return view1;
        }

        return view;
    }

    private void close_popup() {
        dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.NewDialog);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View dialogView = inflater.inflate(R.layout.close_sales_popup, null);
        tv_date_close = (TextView) dialogView.findViewById(R.id.tv_date_close);
        ed_total = (EditText) dialogView.findViewById(R.id.ed_total);
        img_success = (ImageView) dialogView.findViewById(R.id.img_success);
        img_calender_close= (ImageView) dialogView.findViewById(R.id.img_calender_close);

        if(tv_date_close.getText().equals("Date"))
        {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateValue2 = df.format(c);
            tv_date_close.setText(DateValue2);

        }
        img_success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!getActivity().isFinishing()&&getActivity()!=null)
                {
                   /* Intent i= new Intent(mContext,Main_ActivityPrinterLongin.class);
                    AppSession.getInstance().setcompletecart("123");
                    startActivity(i);*/
                   if(ed_total.getText().length()==0)
                   {
                       Toast.makeText(mContext,getResources().getString(R.string.valid_amount_drawer), Toast.LENGTH_SHORT).show();
                   }
                   else
                   {
                       if (getActivity()!=null) {
                           close_Restraunt();
                       }

                   }


                }

                        /*Intent i=new Intent(CartActivity.this,Main_Activity.class);
                        startActivity(i);*/
                /*tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                alertDialog.hide();*/
            }
        });
        img_calender_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!getActivity().isFinishing()&&getActivity()!=null)
                {
                    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onDateSet(DatePicker view, int year1string, int month1string, int day1string) {

                            int day = day1string;
                            int month = month1string + 1;
                            /* int year = year1string ;*/
                            int year = year1string;



                            /*ageCalculation.setDateOfBirth(day, month, year);*/
                            tv_date_close.setText(year + "-" + month + "-" + day);
                            birthYear= Integer.parseInt(String.valueOf(year));

                            /*calculateAge();*/
                            DateValue2=String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(day);
                            //simpleProgressBar.setVisibility(View.VISIBLE);
                           // tv_from_to.setText(" On "+tv_date_close.getText().toString());
                           // loadReportData();
                        }
                    };
                    Date today = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(today);
                    c.add( Calendar.DATE, -1 ) ;
                    long minDate = c.getTime().getTime() ;
                    long now = System.currentTimeMillis() - 1000;
                    DatePickerDialog dpDialog = new DatePickerDialog(mContext, listener, year1, month1, day1);
                   /* dpDialog.getDatePicker().setMaxDate(now);
                    dpDialog.getDatePicker().setMinDate(minDate);*/
                    dpDialog.show();
                }

            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();



        alertDialog.setCancelable(true);
        alertDialog.show();



        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    // To dismiss the fragment when the back-button is pressed.
                 //   tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                    alertDialog.hide();
                    return true;
                }


                // Otherwise, do nothing else
                else return false;
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){


                //***cleanup code***
            }});
    }




    private void initApiresults() {
        mOnApiFinish = new onApiFinish() {
            @Override
            public void onSuccess(String tag, String response) {


                if (tag.equals(FROM_REPORT)) {
                    simpleProgressBar.setVisibility(View.GONE);
                    headerList.clear();
                    Gson gson = new Gson();
                    ReportResponse menuResponse = gson.fromJson(response, ReportResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        if (getActivity() != null) {


                        //simpleProgressBar.setVisibility(View.GONE);
                            tv_cash_short.setText(menuResponse.getCash_short());
                            tv_no_of_bills.setText(menuResponse.getOrders_count());
                            tv_gross_amount.setText(menuResponse.getGross_amount());
                            tv_payment_received.setText(menuResponse.getPayment_received());
                            tv_tender_change.setText(menuResponse.getTender_change());
                            tv_net_amount.setText(menuResponse.getNet_amount());
                        tv_canceled_orders.setText(menuResponse.getTotal_cancelled_orders());
                        tv_takeaway_orders.setText(menuResponse.getTotal_take_away_orders());
                        tv_delivery_orders.setText(menuResponse.getTotal_delivery_orders());
                        tv_dine_order.setText(menuResponse.getTotal_dine_in_orders());
                        tv_total_orders.setText(menuResponse.getOrders_count());
                        tv_bycash_value.setText(menuResponse.getCash_collected_as_cash());
                        tv_bycard_value.setText(menuResponse.getCash_collected_by_card());
                            tv_by_talabat_value.setText(menuResponse.getCash_collected_by_talabat());
                            tv_by_makan_value.setText(menuResponse.getCash_collected_by_makan());
                            tv_by_gift_value.setText(menuResponse.getCash_collected_by_giftcoupon());
                            tv_net_discount.setText(menuResponse.getNet_discount());

                        final String jsonstrin = response;


                        try {


                            // Create the root JSONObject from the JSON string.
                            JSONObject jsonRootObject = new JSONObject(jsonstrin);

                            //Get the instance of JSONArray that contains JSONObjects
                            JSONArray jsonArray = jsonRootObject.optJSONArray("orders");

                            //Iterate the jsonArray and print the info of JSONObjects
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String orderid = jsonObject.getString("order_id");
                                String orderdate = jsonObject.getString("order_date");
                                String order_total = jsonObject.getString("total_paid_amnt");
                                /* String order_total = jsonObject.getString("order_total");*/
                                String subtotal = jsonObject.getString("subtotal");
                                String menustatusname = jsonObject.getString("menu_status_name");
                                String menustatusid = jsonObject.getString("menu_status_id");
                                String coupon_discount = jsonObject.getString("coupon_discount");
                                String other_discount = jsonObject.getString("other_discount");
                                double tot_disc = Double.valueOf(coupon_discount) + Double.valueOf(other_discount);
                                String total_discount = String.valueOf(tot_disc);
                                String tax1_collected = jsonObject.getString("tax1_collected");
                                String tax2_collected = jsonObject.getString("tax2_collected");
                                double tot_tax = Double.valueOf(tax1_collected) + Double.valueOf(tax2_collected);
                                String total_tax = String.valueOf(tot_tax);
                                ArrayList<ItemList> childList = new ArrayList<ItemList>();
                                OrderList oderitem = new OrderList();
                                oderitem.setOrder_id(orderid);
                                oderitem.setOrder_total(order_total);
                                oderitem.setOrder_date(orderdate);
                                oderitem.setSubtotal(subtotal);
                                oderitem.setMenu_status_name(menustatusname);
                                oderitem.setMenu_status_id(menustatusid);
                                oderitem.setTotal_discount(total_discount);
                                oderitem.setTotal_tax(total_tax);
                                headerList.add(oderitem);

                                JSONArray jsonArrayy = jsonObject.optJSONArray("items");
                                for (int j = 0; j < jsonArrayy.length(); j++) {
                                    JSONObject jsonObjectt = jsonArrayy.getJSONObject(j);


                                    String rowid = jsonObjectt.getString("rowid");
                                    String name = jsonObjectt.getString("name");
                                    String price = jsonObjectt.getString("price");
                                    String quantity = jsonObjectt.getString("qty");


                                    //get the children for the group
                                    /*ArrayList<ResponseOrderProductsitems> childList  =oderitem.getOrderProductsitems();*/


                                    ItemList productitms = new ItemList();
                                    productitms.setRowid(rowid);
                                    productitms.setName(name);
                                    productitms.setPrice(price);
                                    productitms.setQty(quantity);
                                    childList.add(productitms);
                                    oderitem.setItems(childList);


                                    /* Toast.makeText(MyOrderActivity.this, productnamee+"...."+name, Toast.LENGTH_SHORT).show();*/
                                    /*Toast.makeText(MyOrderActivity.this,productitms.getProductname()+productitms.getQuantity()+productitms.getPrice(), Toast.LENGTH_SHORT).show();*/
                                            /*  Toast.makeText(MyOrderActivity.this,  oderitem.getStoreName(), Toast.LENGTH_SHORT).show();
                                              Toast.makeText(MyOrderActivity.this,  childList.toString(), Toast.LENGTH_SHORT).show();*/

                                    /*Toast.makeText(MyOrderActivity.this,productitms.getQuantity(), Toast.LENGTH_SHORT).show();*/



                                    /*listAdapter.notifyDataSetChanged();*/

                                }


                            }

                            listAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*  Log.e(TAG, "Json parsing error: " + e.getMessage());*/


                        }


                    } else {
                        Toast.makeText(getActivity(), menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                }
                if (tag.equals(FROM_CLOSE)) {
                    Gson gson = new Gson();
                    CloseRestrauntResponse menuResponse = gson.fromJson(response, CloseRestrauntResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        if (getActivity()!=null) {
                            categoryList.clear();
                            categoryList2.clear();
                            categoryList_payment.clear();
                          //  menuResponse.getData().getClosed_by();
                           // tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                            alertDialog.hide();

                           // Toast.makeText(mContext, menuResponse.getData().getClosed_by(), Toast.LENGTH_SHORT).show();


                            SharedPreferences.Editor editor1 = sharedpreferencesCloseRestraunt.edit();
                            editor1.putString("collection", menuResponse.getCollection());
                            editor1.putString("data_collection", menuResponse.getData().getCollection());
                            editor1.putString("data_gross_amount", menuResponse.getData().getGross_amount());
                            editor1.putString("data_net_discount", menuResponse.getData().getNet_discount());
                            editor1.putString("data_closed_by", menuResponse.getData().getClosed_by());
                            editor1.putString("data_tender_cash", menuResponse.getData().getTender_cash());
                            editor1.putString("data_tender_change",  menuResponse.getData().getTender_change());

                            editor1.putString("data_payment_received",  menuResponse.getData().getPayment_received());


                            editor1.putString("data_cash_short", menuResponse.getData().getCash_short());
                            editor1.putString("data_total_sales", menuResponse.getData().getTotal_sales());
                            editor1.putString("data_net_amount", menuResponse.getData().getNet_amount());


                            editor1.putString("data_orders_count", menuResponse.getData().getOrders_count());
                            editor1.putString("data_CASH", menuResponse.getData().getCASH());
                            editor1.putString("data_CARD", menuResponse.getData().getCARD());
                            editor1.putString("data_BOTH",  menuResponse.getData().getBOTH());
                            editor1.putString("data_TALABAT",  menuResponse.getData().getTALABAT());
                            editor1.putString("data_GIFTCOUPON",  menuResponse.getData().getGIFTCOUPON());

                            editor1.putString("report_date", menuResponse.getReport_date());
                            editor1.putString("closing_time", menuResponse.getClosing_time());

                            if(menuResponse.getData().getWaiters_orders()!=null&&menuResponse.getData().getWaiters_orders().size()>0)
                            {
                                for (int i = 0; i < menuResponse.getData().getWaiters_orders().size(); i++) {
                                    map1 = new HashMap<String, String>();

                                    map1.put("staff_name", menuResponse.getData().getWaiters_orders().get(i).getStaff_name());
                                    map1.put("waiter_orders_count", menuResponse.getData().getWaiters_orders().get(i).getWaiter_orders_count());
                                    map1.put("cash_collected", menuResponse.getData().getWaiters_orders().get(i).getCash_collected());
                                    map1.put("gross_amount", menuResponse.getData().getWaiters_orders().get(i).getGross_amount());
                                    map1.put("net_discount", menuResponse.getData().getWaiters_orders().get(i).getNet_discount());


                                    map1.put("CASH", menuResponse.getData().getWaiters_orders().get(i).getCASH());
                                    map1.put("CARD", menuResponse.getData().getWaiters_orders().get(i).getCARD());
                                    map1.put("BOTH", menuResponse.getData().getWaiters_orders().get(i).getBOTH());
                                    map1.put("TALABAT", menuResponse.getData().getWaiters_orders().get(i).getTALABAT());
                                    map1.put("GIFTCOUPON", menuResponse.getData().getWaiters_orders().get(i).getGIFTCOUPON());
                                    map1.put("MAKAN", menuResponse.getData().getWaiters_orders().get(i).getMAKAN());
                                    if(menuResponse.getData().getWaiters_orders().get(i).getOrder_list()!=null
                                            &&menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size()>0)
                                    {
                                        for(int j=0;j<menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size();j++)
                                        {
                                            map2 = new HashMap<String, String>();
                                            map2.put("invoice_no", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getInvoice_no());
                                            map2.put("invoice_date",menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getInvoice_date());
                                            map2.put("sale_no", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getSale_no());
                                            map2.put("order_type", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getOrder_type());
                                            map2.put("coupon_discount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getCoupon_discount());
                                            map2.put("other_discount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getOther_discount());
                                            map2.put("final_total", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getFinal_total());
                                            map2.put("total_items", String.valueOf(menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size()));
                                            if(menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type()!=null
                                                    &&menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().size()>0)
                                            {
                                                for(int k=0;k<menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().size();k++)
                                                {
                                                    map5_payment = new HashMap<String, String>();
                                                    map5_payment.put("payment", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getPayment());
                                                    map5_payment.put("amount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getAmount());
                                                    map5_payment.put("card_type", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getCard_type());

                                                    categoryList_payment.add(map5_payment);
                                                }
                                            }

                                            map2.put("payment_type", ""/*menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type()*/);


                                            categoryList2.add(map2);
                                        }
                                       /* for(int j=0;j<menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().size();j++)
                                        {
                                            map4_cancelled = new HashMap<String, String>();
                                            map4_cancelled = new HashMap<String, String>();
                                            map4_cancelled.put("order_id", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getOrder_id());
                                            map4_cancelled.put("menu_id",menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getMenu_id());
                                            map4_cancelled.put("name", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getName());
                                            map4_cancelled.put("price", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getPrice());
                                            map4_cancelled.put("quantity", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getQuantity());
                                            map4_cancelled.put("subtotal", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getSubtotal());

                                            categoryList4_cancelled.add(map4_cancelled);
                                        }*/
                                    }


                                    categoryList.add(map1);
                                }

                            }
                            if(menuResponse.getData().getCancelled_orders()!=null
                                    &&menuResponse.getData().getCancelled_orders().size()>0)
                            {
                                for(int j=0;j<menuResponse.getData().getCancelled_orders().size();j++)
                                {
                                    map4_cancelled = new HashMap<String, String>();
                                    map4_cancelled.put("order_id", menuResponse.getData().getCancelled_orders().get(j).getOrder_id());
                                    map4_cancelled.put("menu_id",menuResponse.getData().getCancelled_orders().get(j).getMenu_id());
                                    map4_cancelled.put("name", menuResponse.getData().getCancelled_orders().get(j).getName());
                                    map4_cancelled.put("price", menuResponse.getData().getCancelled_orders().get(j).getPrice());
                                    map4_cancelled.put("quantity", menuResponse.getData().getCancelled_orders().get(j).getQuantity());
                                    map4_cancelled.put("subtotal", menuResponse.getData().getCancelled_orders().get(j).getSubtotal());

                                    categoryList4_cancelled.add(map4_cancelled);
                                }
                            }

                        /*    final String jsonstrin = response;


                            try {


                                // Create the root JSONObject from the JSON string.
                                JSONObject jsonRootObject = new JSONObject(jsonstrin);

                                //Get the instance of JSONArray that contains JSONObjects
                                //   JSONArray jsonArray = jsonRootObject.optJSONArray("orders");

                                //Iterate the jsonArray and print the info of JSONObjects

                                JSONObject jsonObject = jsonRootObject.getJSONObject("data");

                                JSONArray jsonArray = jsonObject.optJSONArray("waiters_orders");

                                //Iterate the jsonArray and print the info of JSONObjects
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    String staff_name = jsonObject2.getString("staff_name");
                                    String waiter_orders_count = jsonObject2.getString("waiter_orders_count");
                                    String cash_collected =jsonObject2.getString("cash_collected");

                                    map1 = new HashMap<String, String>();

                                    map1.put("staff_name", staff_name);
                                    map1.put("waiter_orders_count", waiter_orders_count);
                                    map1.put("cash_collected", cash_collected);


                                    categoryList.add(map1);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Json parsing error: " + e.getMessage());


                            }*/
                            String json = gson.toJson(categoryList);
                            editor1.putString("WaiterOrders", json);

                            saveArrayList(categoryList2,"WaiterOrders_orderlist");
                            saveArrayList(categoryList,"WaiterOrders2");
                            saveArrayList(categoryList4_cancelled,"Cancelled_orders");
                            saveArrayList(categoryList_payment,"Payment_types");
                            editor1.commit();



                            Intent i= new Intent(mContext,Main_ActivityPrinterLongin.class);
                            AppSession.getInstance().setcompletecart("124");
                            startActivity(i);

                        }

                    }
                }

                if (tag.equals(FROM_PRINT_IF_CLOSED)) {
                    Gson gson = new Gson();
                    CloseRestrauntResponse menuResponse = gson.fromJson(response, CloseRestrauntResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        if (getActivity()!=null) {
                            categoryList.clear();
                            categoryList2.clear();
                            categoryList4_cancelled.clear();
                            categoryList_payment.clear();
                            //  menuResponse.getData().getClosed_by();
                            // tv_close_sale.setBackground(getResources().getDrawable(R.drawable.report_rectangle));
                          /*  alertDialog.hide();*/

                            // Toast.makeText(mContext, menuResponse.getData().getClosed_by(), Toast.LENGTH_SHORT).show();


                            SharedPreferences.Editor editor1 = sharedpreferencesCloseRestraunt.edit();
                            editor1.putString("collection", menuResponse.getCollection());
                            editor1.putString("data_collection", menuResponse.getData().getCollection());
                            editor1.putString("data_gross_amount", menuResponse.getData().getGross_amount());
                            editor1.putString("data_net_discount", menuResponse.getData().getNet_discount());

                            editor1.putString("data_cash_short", menuResponse.getData().getCash_short());
                            editor1.putString("data_total_sales", menuResponse.getData().getTotal_sales());
                            editor1.putString("data_net_amount", menuResponse.getData().getNet_amount());

                            editor1.putString("data_closed_by", menuResponse.getData().getClosed_by());
                            editor1.putString("data_tender_cash", menuResponse.getData().getTender_cash());
                            editor1.putString("data_tender_change",  menuResponse.getData().getTender_change());

                            editor1.putString("data_payment_received",  menuResponse.getData().getPayment_received());


                            editor1.putString("data_orders_count", menuResponse.getData().getOrders_count());
                            editor1.putString("data_CASH", menuResponse.getData().getCASH());
                            editor1.putString("data_CARD", menuResponse.getData().getCARD());
                            editor1.putString("data_BOTH",  menuResponse.getData().getBOTH());
                            editor1.putString("data_TALABAT",  menuResponse.getData().getTALABAT());
                            editor1.putString("data_GIFTCOUPON",  menuResponse.getData().getGIFTCOUPON());
                            editor1.putString("data_MAKAN",  menuResponse.getData().getMAKAN());

                            editor1.putString("report_date", menuResponse.getReport_date());
                            editor1.putString("closing_time", menuResponse.getClosing_time());

                            if(menuResponse.getData().getWaiters_orders()!=null&&menuResponse.getData().getWaiters_orders().size()>0)
                            {
                                for (int i = 0; i < menuResponse.getData().getWaiters_orders().size(); i++) {
                                    map1 = new HashMap<String, String>();

                                    map1.put("staff_name", menuResponse.getData().getWaiters_orders().get(i).getStaff_name());
                                    map1.put("waiter_orders_count", menuResponse.getData().getWaiters_orders().get(i).getWaiter_orders_count());
                                    map1.put("cash_collected", menuResponse.getData().getWaiters_orders().get(i).getCash_collected());
                                    map1.put("gross_amount", menuResponse.getData().getWaiters_orders().get(i).getGross_amount());
                                    map1.put("net_discount", menuResponse.getData().getWaiters_orders().get(i).getNet_discount());

                                    map1.put("CASH", menuResponse.getData().getWaiters_orders().get(i).getCASH());
                                    map1.put("CARD", menuResponse.getData().getWaiters_orders().get(i).getCARD());
                                    map1.put("BOTH", menuResponse.getData().getWaiters_orders().get(i).getBOTH());
                                    map1.put("TALABAT", menuResponse.getData().getWaiters_orders().get(i).getTALABAT());
                                    map1.put("GIFTCOUPON", menuResponse.getData().getWaiters_orders().get(i).getGIFTCOUPON());
                                    map1.put("MAKAN", menuResponse.getData().getWaiters_orders().get(i).getMAKAN());
                                 if(menuResponse.getData().getWaiters_orders().get(i).getOrder_list()!=null
                                         &&menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size()>0)
                                 {
                                     for(int j=0;j<menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size();j++)
                                     {
                                         map2 = new HashMap<String, String>();
                                         map2.put("invoice_no", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getInvoice_no());
                                         map2.put("invoice_date",menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getInvoice_date());
                                         map2.put("sale_no", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getSale_no());
                                         map2.put("order_type", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getOrder_type());
                                         map2.put("payment_type", ""/*menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type()*/);
                                         map2.put("coupon_discount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getCoupon_discount());
                                         map2.put("other_discount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getOther_discount());
                                         map2.put("final_total", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getFinal_total());
                                         map2.put("total_items", String.valueOf(menuResponse.getData().getWaiters_orders().get(i).getOrder_list().size()));
                                         if(menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type()!=null
                                                 &&menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().size()>0)
                                         {
                                             for(int k=0;k<menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().size();k++)
                                             {
                                                 map5_payment = new HashMap<String, String>();
                                                 map5_payment.put("payment", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getPayment());
                                                 map5_payment.put("amount", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getAmount());
                                                 map5_payment.put("card_type", menuResponse.getData().getWaiters_orders().get(i).getOrder_list().get(j).getPayment_type().get(k).getCard_type());

                                                 categoryList_payment.add(map5_payment);
                                             }
                                         }
                                         categoryList2.add(map2);
                                     }
                                  /*   for(int j=0;j<menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().size();j++)
                                     {
                                         map4_cancelled = new HashMap<String, String>();
                                         map4_cancelled = new HashMap<String, String>();
                                         map4_cancelled.put("order_id", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getOrder_id());
                                         map4_cancelled.put("menu_id",menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getMenu_id());
                                         map4_cancelled.put("name", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getName());
                                         map4_cancelled.put("price", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getPrice());
                                         map4_cancelled.put("quantity", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getQuantity());
                                         map4_cancelled.put("subtotal", menuResponse.getData().getWaiters_orders().get(i).getCancelled_orders().get(j).getSubtotal());

                                         categoryList4_cancelled.add(map4_cancelled);
                                     }*/
                                 }


                                    categoryList.add(map1);
                                }
                            }

                            if(menuResponse.getData().getCancelled_orders()!=null
                                    &&menuResponse.getData().getCancelled_orders().size()>0)
                            {
                                for(int j=0;j<menuResponse.getData().getCancelled_orders().size();j++)
                                {
                                    map4_cancelled = new HashMap<String, String>();
                                    map4_cancelled.put("order_id", menuResponse.getData().getCancelled_orders().get(j).getOrder_id());
                                    map4_cancelled.put("menu_id",menuResponse.getData().getCancelled_orders().get(j).getMenu_id());
                                    map4_cancelled.put("name", menuResponse.getData().getCancelled_orders().get(j).getName());
                                    map4_cancelled.put("price", menuResponse.getData().getCancelled_orders().get(j).getPrice());
                                    map4_cancelled.put("quantity", menuResponse.getData().getCancelled_orders().get(j).getQuantity());
                                    map4_cancelled.put("subtotal", menuResponse.getData().getCancelled_orders().get(j).getSubtotal());

                                    categoryList4_cancelled.add(map4_cancelled);
                                }
                            }



                       /*     final String jsonstrin = response;


                            try {


                                // Create the root JSONObject from the JSON string.
                                JSONObject jsonRootObject = new JSONObject(jsonstrin);

                                //Get the instance of JSONArray that contains JSONObjects
                                //   JSONArray jsonArray = jsonRootObject.optJSONArray("orders");

                                //Iterate the jsonArray and print the info of JSONObjects

                                JSONObject jsonObject = jsonRootObject.getJSONObject("data");

                                JSONArray jsonArray = jsonObject.optJSONArray("waiters_orders");

                                //Iterate the jsonArray and print the info of JSONObjects
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    String staff_name = jsonObject2.getString("staff_name");
                                    String waiter_orders_count = jsonObject2.getString("waiter_orders_count");
                                    String cash_collected =jsonObject2.getString("cash_collected");

                                    String CASH =jsonObject2.getString("CASH");
                                    String CARD =jsonObject2.getString("CARD");
                                    String BOTH =jsonObject2.getString("BOTH");
                                    String TALABAT =jsonObject2.getString("TALABAT");

                                    String MAKAN =jsonObject2.getString("MAKAN");
                                    String GIFTCOUPON =jsonObject2.getString("GIFTCOUPON");


                                    map1 = new HashMap<String, String>();

                                    map1.put("staff_name", staff_name);
                                    map1.put("waiter_orders_count", waiter_orders_count);
                                    map1.put("cash_collected", cash_collected);


                                    categoryList.add(map1);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Json parsing error: " + e.getMessage());


                            }*/
                            String json = gson.toJson(categoryList);
                            editor1.putString("WaiterOrders", json);

                            saveArrayList(categoryList2,"WaiterOrders_orderlist");
                            saveArrayList(categoryList,"WaiterOrders2");
                            saveArrayList(categoryList4_cancelled,"Cancelled_orders");
                            saveArrayList(categoryList_payment,"Payment_types");
                            editor1.commit();
                            Intent i= new Intent(mContext,Main_ActivityPrinterLongin.class);
                            AppSession.getInstance().setcompletecart("124");
                            startActivity(i);
                        }

                    }
                }
                if (tag.equals(FROM_ORDERBILL_AGAIN)) {
                    Gson gson = new Gson();
                    AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
                    if (menuResponse.getStatus().equals("success")) {
                        AppSession.getInstance().setInvoice_No(menuResponse.getOrder_details().getInvoice_no());
                        AppSession.getInstance().setOrder_status("22");
                        AppSession.getInstance().setOrderNumber(order_number);
                        categoryList3.clear();
                        categoryList_payment.clear();
                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("order_id", menuResponse.getOrder_details().getOrder_id());
                        editor1.putString("sale_no", menuResponse.getOrder_details().getSale_no());
                        editor1.putString("invoice_no",  menuResponse.getOrder_details().getInvoice_no());
                        editor1.putString("invoice_date",  menuResponse.getOrder_details().getInvoice_date());
                        editor1.putString("coupon_discount", menuResponse.getOrder_details().getCoupon_discount());
                        editor1.putString("other_discount", menuResponse.getOrder_details().getOther_discount());
                        editor1.putString("subtotal",   menuResponse.getOrder_details().getSubtotal());

                        editor1.putString("tender_cash", menuResponse.getOrder_details().getTender_cash());
                        editor1.putString("tender_change", menuResponse.getOrder_details().getTender_change());
                        editor1.putString("vat", menuResponse.getOrder_details().getVat());
                        editor1.putString("vat_percentage", menuResponse.getOrder_details().getVat_percentage());
                        editor1.putString("service_charge_percentage", menuResponse.getOrder_details().getService_charge_percentage());
                        editor1.putString("payment_type", ""/*menuResponse.getOrder_details().getPayment_type()*/);
                        editor1.putString("card_type", menuResponse.getOrder_details().getCard_type());

                        editor1.putString("tax1_applicable_sale", menuResponse.getOrder_details().getTax1_applicable_sale());
                        editor1.putString("tax2_applicable_sale", menuResponse.getOrder_details().getTax2_applicable_sale());
                        editor1.putString("tax1_non_applicable_sale", menuResponse.getOrder_details().getTax1_non_applicable_sale());
                        editor1.putString("tax2_non_applicable_sale", menuResponse.getOrder_details().getTax2_non_applicable_sale());

                        editor1.putString("service_charge", menuResponse.getOrder_details().getService_charge());
                        editor1.putString("tax1_exempted", menuResponse.getOrder_details().getTax1_exempted());
                        editor1.putString("tax2_exempted",menuResponse.getOrder_details().getTax2_exempted());
                        editor1.putString("final_total", menuResponse.getOrder_details().getTotal());

                        editor1.putString("qk_name", menuResponse.getOrder_details().getQk_name());
                        editor1.putString("mobile_number", menuResponse.getOrder_details().getMobile_number());
                        editor1.putString("no_of_pax", menuResponse.getOrder_details().getNo_of_pax());
                        editor1.putString("disabled", menuResponse.getOrder_details().getDisabled());
                        editor1.putString("solo_parent",  menuResponse.getOrder_details().getSolo_parent());
                        editor1.putString("senior_citizen",menuResponse.getOrder_details().getSenior_citizen());

                        editor1.putString("order_firstname",menuResponse.getOrder_details().getOrder_firstname());
                        editor1.putString("order_telephone", menuResponse.getOrder_details().getOrder_telephone());
                        editor1.putString("order_type",menuResponse.getOrder_details().getOrder_type());
                        editor1.putString("address1", menuResponse.getOrder_details().getAddress1());

                        editor1.putString("address2",menuResponse.getOrder_details().getAddress2());
                        editor1.putString("location", menuResponse.getOrder_details().getLocation());
                        editor1.putString("area", menuResponse.getOrder_details().getArea());
                        editor1.putString("city", menuResponse.getOrder_details().getCity());

                        editor1.putString("coupon_type", menuResponse.getOrder_details().getCoupon_type());
                        editor1.putString("coupon_code", menuResponse.getOrder_details().getCoupon_code());
                        editor1.putString("table_name", menuResponse.getOrder_details().getTable_info().getTable_name());
                        editor1.putString("table_id", menuResponse.getOrder_details().getTable_info().getTable_id());


                        if(menuResponse.getOrder_details().getOrder_menus()!=null&&menuResponse.getOrder_details().getOrder_menus().size()>0)
                        {
                            for (int i = 0; i < menuResponse.getOrder_details().getOrder_menus().size(); i++) {
                                map3 = new HashMap<String, String>();

                                map3.put("name", menuResponse.getOrder_details().getOrder_menus().get(i).getName());
                                map3.put("quantity", menuResponse.getOrder_details().getOrder_menus().get(i).getQuantity());
                                map3.put("price", menuResponse.getOrder_details().getOrder_menus().get(i).getPrice());

                                map3.put("subtotal", menuResponse.getOrder_details().getOrder_menus().get(i).getName());
                                map3.put("discount", menuResponse.getOrder_details().getOrder_menus().get(i).getDiscount());
                                map3.put("discount_value", menuResponse.getOrder_details().getOrder_menus().get(i).getDiscount_value());


                                categoryList3.add(map3);
                            }
                        }
                        if(menuResponse.getOrder_details().getPayment_type()!=null
                                &&menuResponse.getOrder_details().getPayment_type().size()>0)
                        {
                            for(int k=0;k<menuResponse.getOrder_details().getPayment_type().size();k++)
                            {
                                map5_payment = new HashMap<String, String>();
                                map5_payment.put("payment", menuResponse.getOrder_details().getPayment_type().get(k).getPayment());
                                map5_payment.put("amount", menuResponse.getOrder_details().getPayment_type().get(k).getAmount());
                                map5_payment.put("card_type", menuResponse.getOrder_details().getPayment_type().get(k).getCard_type());

                                categoryList_payment.add(map5_payment);
                            }
                        }


              /*  String json = gson.toJson(categoryList3);
                editor1.putString("ItemOrders", json);*/

                        saveArrayList2(categoryList3,"ItemOrders");
                        saveArrayList2(categoryList_payment,"Payment_types");
                        /*categoryList_payment.clear();
                        categoryList_payment= getArrayList2("Payment_types");
                        if (categoryList_payment != null && categoryList_payment.size() > 0) {
                            for (int i = 0; i < categoryList_payment.size(); i++) {
                                String paymentdetailss = "";
                                paymentdetailss="Payment Type: "+categoryList_payment.get(i).get("payment")+ "         " + "\n" +
                                        "Payment Recieved : " + categoryList_payment.get(i).get("amount") + "        " + "\n" +
                                        "Card Type : " + categoryList_payment.get(i).get("card_type") + "        " + "\n";
                                Toast.makeText(mContext,paymentdetailss, Toast.LENGTH_SHORT).show();

                            }
                        }*/
                        editor1.commit();


                        AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+order_number);

                        Intent i= new Intent(mContext,Main_ActivityPrinterLongin.class);
                        AppSession.getInstance().setcompletecart("127");startActivity(i);
                        /* checkcompletebipass();*/
                    }
                    else
                    {
                        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailed(String tag, String response) {
                Gson gson = new Gson();
                ReportResponse menuResponse = gson.fromJson(response, ReportResponse.class);
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onJsonError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServerError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }

        };}


    public void saveArrayList( ArrayList<HashMap<String, String>> list, String key){
        SharedPreferences.Editor editor1 = sharedpreferencesCloseRestraunt.edit();

        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor1.putString(key, json);
        editor1.apply();     // This line is IMPORTANT !!!
    }

    public  ArrayList<HashMap<String, String>> getArrayList(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesCloseRestraunt.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }
    public void saveArrayList2( ArrayList<HashMap<String, String>> list2, String key2){
        SharedPreferences.Editor editor2 = sharedpreferencesData.edit();

        Gson gson2 = new Gson();
        String json2 = gson2.toJson(list2);
        editor2.putString(key2, json2);
        editor2.apply();     // This line is IMPORTANT !!!
    }
    public  ArrayList<HashMap<String, String>> getArrayList2(String key){

        Gson gson = new Gson();
        String json = sharedpreferencesData.getString(key, null);
        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        return gson.fromJson(json, type);
    }

    private void DateDialog() {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(DatePicker view, int year1string, int month1string, int day1string) {

                int day = day1string;
                int month = month1string + 1;
                /* int year = year1string ;*/
                int year = year1string;

                /*ageCalculation.setDateOfBirth(day, month, year);*/
                tv_date.setText(year + "-" + month + "-" + day);
                birthYear= Integer.parseInt(String.valueOf(year));

                /*calculateAge();*/
                DateValue=String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(day);
                simpleProgressBar.setVisibility(View.VISIBLE);
                tv_from_to.setText(" On "+tv_date.getText().toString());
                loadReportData();
            }
        };
       /* long now = System.currentTimeMillis() - 1000;*/
        long now = System.currentTimeMillis()+24*60*60*1000;
        DatePickerDialog dpDialog = new DatePickerDialog(mContext, listener, year1, month1, day1);
        dpDialog.getDatePicker().setMaxDate(now);
        dpDialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();
      /*  if(tv_date.getText().equals("Date"))
        {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateValue = df.format(c);
            tv_date.setText(DateValue);
            tv_from_to.setText(" On "+tv_date.getText().toString());
        }



        if (getActivity() != null) {
            simpleProgressBar.setVisibility(View.VISIBLE);
            loadReportData();
        }*/


    }

    private void loadReportData() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.REPORT_MENU_ORDER;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();



        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
       /* values.put("action", "orderReports");*/
        values.put("action", Constants.API.ACTION_REPORT_MENU_ORDER);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("date",DateValue);
        values.put("report_type", sort);
        //values.put("report_type", "DAILY");
        if(AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            values.put("supervisor_login", "1");
        }


        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_REPORT, "POST");
    }

    private void close_Restraunt() {
        String URL = AppSession.getInstance().getRegUrl()+ API.CLOSE_RESTAURANT;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();



        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /* values.put("action", "orderReports");*/
        values.put("action", API.ACTION_CLOSE_RESTAURANT);
        values.put("in_drawer",ed_total.getText().toString());
        values.put("location_id", AppSession.getInstance().getLocation_id());
       // values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("report_date",DateValue2);
        values.put("closed_by", AppSession.getInstance().getStaff_ID());
        //values.put("report_type", "DAILY");



        /* String token= AppSession.getInstance(getApplicationContext()).getTabToken().toString();*/

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_CLOSE, "POST");
    }

    private void print_if_closed() {
        String URL = AppSession.getInstance().getRegUrl()+ API.PRINT_IF_CLOSED;
        URL = URL ;

        HashMap<String, String> values = new HashMap<>();

        values.put("action", API.ACTION_PRINT_IF_CLOSED);
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("report_date",DateValue);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());



        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL,FROM_PRINT_IF_CLOSED, "POST");
    }
    private void OrderBillAgain() {
        String URL = AppSession.getInstance().getRegUrl()+ API.ORDERBILL_AGAIN_API;
        HashMap values = new HashMap<>();
        values.put("action", API.ACTION_ORDER_AGAIN_BILL);
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("order_id",order_number);
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_ORDERBILL_AGAIN, "POST");
    }
}
