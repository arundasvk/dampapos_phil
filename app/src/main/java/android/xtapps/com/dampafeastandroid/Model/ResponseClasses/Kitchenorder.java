package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public  class Kitchenorder
{
    @SerializedName("order_menu_id")
    @Expose
    private String order_menu_id;

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("waiter_name")
    @Expose
    private String waiter_name;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("option_values")
    @Expose
    private ArrayList<OptionValues> optionValuesLists;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("menu_status_id")
    @Expose
    private String menu_status_id;

    @SerializedName("menu_status_name")
    @Expose
    private String menu_status_name;


    @SerializedName("table_name")
    @Expose
    private String table_name;

    @SerializedName("menu_status_comment")
    @Expose
    private String menu_status_comment;

    @SerializedName("order_date")
    @Expose
    private String order_date;

    @SerializedName("order_time")
    @Expose
    private String order_time;

    @SerializedName("order_date_time")
    @Expose
    private String order_date_time;

    @SerializedName("order_type")
    @Expose
    private String order_type;

    @SerializedName("customer_name")
    @Expose
    private String customer_name;

    @SerializedName("telephone")
    @Expose
    private String telephone;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("payment_type")
    @Expose
    private String payment_type;

    @SerializedName("order_total")
    @Expose
    private String order_total;

    @SerializedName("order_status")
    @Expose
    private String order_status;

    @SerializedName("table_info")
    @Expose
    private OptionList table_info;

    @SerializedName("pending_items_count")
    @Expose
    private String pending_items_count;

    @SerializedName("preparation_items_count")
    @Expose
    private String preparation_items_count;


    @SerializedName("completed_items_count")
    @Expose
    private String completed_items_count;

    @SerializedName("updated")
    @Expose
    private String updated;

    @SerializedName("items")
    @Expose
    private ArrayList<MenuItemsList> menuItemsLists;

    int state;

    public OptionList getTable_info() {
        return table_info;
    }

    public void setTable_info(OptionList table_info) {
        this.table_info = table_info;
    }

    public String getOrder_menu_id() {
        return order_menu_id;
    }

    public void setOrder_menu_id(String order_menu_id) {
        this.order_menu_id = order_menu_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public ArrayList<OptionValues> getOptionValuesLists() {
        return optionValuesLists;
    }

    public void setOptionValuesLists(ArrayList<OptionValues> optionValuesLists) {
        this.optionValuesLists = optionValuesLists;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMenu_status_id() {
        return menu_status_id;
    }

    public void setMenu_status_id(String menu_status_id) {
        this.menu_status_id = menu_status_id;
    }

    public String getMenu_status_name() {
        return menu_status_name;
    }

    public void setMenu_status_name(String menu_status_name) {
        this.menu_status_name = menu_status_name;
    }


    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getMenu_status_comment() {
        return menu_status_comment;
    }

    public void setMenu_status_comment(String menu_status_comment) {
        this.menu_status_comment = menu_status_comment;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }


    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_date_time() {
        return order_date_time;
    }

    public void setOrder_date_time(String order_date_time) {
        this.order_date_time = order_date_time;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getOrder_total() {
        return order_total;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public ArrayList<MenuItemsList> getMenuItemsLists() {
        return menuItemsLists;
    }

    public void setMenuItemsLists(ArrayList<MenuItemsList> menuItemsLists) {
        this.menuItemsLists = menuItemsLists;
    }

    public String getPending_items_count() {
        return pending_items_count;
    }

    public void setPending_items_count(String pending_items_count) {
        this.pending_items_count = pending_items_count;
    }

    public String getPreparation_items_count() {
        return preparation_items_count;
    }

    public void setPreparation_items_count(String preparation_items_count) {
        this.preparation_items_count = preparation_items_count;
    }

    public String getCompleted_items_count() {
        return completed_items_count;
    }

    public void setCompleted_items_count(String completed_items_count) {
        this.completed_items_count = completed_items_count;
    }

    public String getWaiter_name() {
        return waiter_name;
    }

    public void setWaiter_name(String waiter_name) {
        this.waiter_name = waiter_name;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public  class OptionValues
    {
        @SerializedName("menu_option_value_id")
        @Expose
        private String menu_option_value_id;

        @SerializedName("option_value_id")
        @Expose
        private String option_value_id;

        @SerializedName("option_id")
        @Expose
        private String option_id;

        @SerializedName("option_name")
        @Expose
        private String option_name;

        @SerializedName("value")
        @Expose
        private String value;

        @SerializedName("price")
        @Expose
        private String price;

        public String getMenu_option_value_id() {
            return menu_option_value_id;
        }

        public void setMenu_option_value_id(String menu_option_value_id) {
            this.menu_option_value_id = menu_option_value_id;
        }

        public String getOption_value_id() {
            return option_value_id;
        }

        public void setOption_value_id(String option_value_id) {
            this.option_value_id = option_value_id;
        }

        public String getOption_id() {
            return option_id;
        }

        public void setOption_id(String option_id) {
            this.option_id = option_id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getOption_name() {
            return option_name;
        }

        public void setOption_name(String option_name) {
            this.option_name = option_name;
        }
    }
    public class OptionList {

        @SerializedName("staff_id")
        @Expose
        private String staff_id;

        @SerializedName("table_id")
        @Expose
        private String table_id;

        @SerializedName("table_name")
        @Expose
        private String table_name;

        @SerializedName("waiter_name")
        @Expose
        private String waiter_name;

        @SerializedName("updated_at")
        @Expose
        private String updated_at;

        public String getStaff_id() {
            return staff_id;
        }

        public void setStaff_id(String staff_id) {
            this.staff_id = staff_id;
        }

        public String getTable_id() {
            return table_id;
        }

        public void setTable_id(String table_id) {
            this.table_id = table_id;
        }

        public String getWaiter_name() {
            return waiter_name;
        }

        public void setWaiter_name(String waiter_name) {
            this.waiter_name = waiter_name;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getTable_name() {
            return table_name;
        }

        public void setTable_name(String table_name) {
            this.table_name = table_name;
        }
    }

}
