package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by USER on 7/24/2018.
 */

public class AddCartResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("menu_relations")
    @Expose
    private ArrayList<MenuRelations> menu_relations;

    @SerializedName("order")
    @Expose
    private String order_id;

    @SerializedName("menu")
    @Expose
    private String[] menu;

    @SerializedName("order_menu_ids")
    @Expose
    private String[] order_menu_ids;

    @SerializedName("tax_details")
    @Expose
    private Taxdetails tax_details;

    @SerializedName("order_details")
    @Expose
    private Orderdetails order_details;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("prev_subtotal")
    @Expose
    private String prev_subtotal;

    @SerializedName("disc_per")
    @Expose
    private String dis_percent;


    @SerializedName("disc_amount")
    @Expose
    private String dis_amount;

    @SerializedName("inv_number")
    @Expose
    private String inv_number;

    @SerializedName("payable_amount")
    @Expose
    private String payable_amount;

    @SerializedName("payment_types")
    @Expose
    private String[] payment_types;



    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMenu() {
        return menu;
    }

    public void setMenu(String[] menu) {
        this.menu = menu;
    }

    public String[] getOrder_menu_ids() {
        return order_menu_ids;
    }

    public void setOrder_menu_ids(String[] order_menu_ids) {
        this.order_menu_ids = order_menu_ids;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getPrev_subtotal() {
        return prev_subtotal;
    }

    public void setPrev_subtotal(String prev_subtotal) {
        this.prev_subtotal = prev_subtotal;
    }

    public String getDis_percent() {
        return dis_percent;
    }

    public void setDis_percent(String dis_percent) {
        this.dis_percent = dis_percent;
    }

    public String getDis_amount() {
        return dis_amount;
    }

    public void setDis_amount(String dis_amount) {
        this.dis_amount = dis_amount;
    }

    public Taxdetails getTax_details() {
        return tax_details;
    }

    public void setTax_details(Taxdetails tax_details) {
        this.tax_details = tax_details;
    }

    public String getInv_number() {
        return inv_number;
    }

    public void setInv_number(String inv_number) {
        this.inv_number = inv_number;
    }

    public ArrayList<MenuRelations> getMenu_relations() {
        return menu_relations;
    }

    public void setMenu_relations(ArrayList<MenuRelations> menu_relations) {
        this.menu_relations = menu_relations;
    }

    public String getPayable_amount() {
        return payable_amount;
    }

    public void setPayable_amount(String payable_amount) {
        this.payable_amount = payable_amount;
    }


    public Orderdetails getOrder_details() {
        return order_details;
    }

    public void setOrder_details(Orderdetails order_details) {
        this.order_details = order_details;
    }

    public String[] getPayment_types() {
        return payment_types;
    }

    public void setPayment_types(String[] payment_types) {
        this.payment_types = payment_types;
    }

    private class Taxdetails {

        @SerializedName("vat_amount")
        @Expose
        private String vat_amount;

        @SerializedName("tax_exemption")
        @Expose
        private String tax_exemption;

        @SerializedName("discount_amount")
        @Expose
        private String discount_amount;

        @SerializedName("service_charge")
        @Expose
        private String service_charge;

        @SerializedName("payable_amount")
        @Expose
        private String payable_amount;

        public String getVat_amount() {
            return vat_amount;
        }

        public void setVat_amount(String vat_amount) {
            this.vat_amount = vat_amount;
        }

        public String getTax_exemption() {
            return tax_exemption;
        }

        public void setTax_exemption(String tax_exemption) {
            this.tax_exemption = tax_exemption;
        }

        public String getDiscount_amount() {
            return discount_amount;
        }

        public void setDiscount_amount(String discount_amount) {
            this.discount_amount = discount_amount;
        }

        public String getService_charge() {
            return service_charge;
        }

        public void setService_charge(String service_charge) {
            this.service_charge = service_charge;
        }

        public String getPayable_amount() {
            return payable_amount;
        }

        public void setPayable_amount(String payable_amount) {
            this.payable_amount = payable_amount;
        }
    }
}
