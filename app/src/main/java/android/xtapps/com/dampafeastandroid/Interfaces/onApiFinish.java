package android.xtapps.com.dampafeastandroid.Interfaces;

/**
 * Created by USER on 7/17/2018.
 */

public interface onApiFinish {
    void onSuccess(String tag, String response);
    void onFailed(String tag, String response);
    void onJsonError(String tag, String response);
    void onServerError(String tag, String response);

}

