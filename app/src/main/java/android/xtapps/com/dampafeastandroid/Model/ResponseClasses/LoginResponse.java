package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by USER on 7/20/2018.
 */

public class LoginResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("delivery_charge")
    @Expose
    private String delivery_charge;

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    @SerializedName("data")
    @Expose
    private ArrayList<SupervisorDetails> data;


    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public ArrayList<SupervisorDetails> getData() {
        return data;
    }

    public void setData(ArrayList<SupervisorDetails> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    @SerializedName("staff_info")
    @Expose
    public staffInfo staffInfo;

    //Rest

    @SerializedName("location_info")
    @Expose
    public  locationInfo locationinfo;

    public LoginResponse.staffInfo getStaffInfo() {
        return staffInfo;
    }
    public void setStaffInfo(LoginResponse.staffInfo staffInfo) {
        this.staffInfo = staffInfo;
    }

    public LoginResponse.locationInfo getLocationinfo() {
        return locationinfo;
    }
    public void setLocationinfo(LoginResponse.locationInfo locationinfo) {
        this.locationinfo = locationinfo;
    }






    public  class locationInfo{

        @SerializedName("location_id")
        @Expose
        private String locationId;

        @SerializedName("location_name")
        @Expose
        private String locationName;

        @SerializedName("address_1")
        @Expose
        private String address1;

        @SerializedName("address_2")
        @Expose
        private String address2;

        @SerializedName("city")
        @Expose
        private String City;

        @SerializedName("state")
        @Expose
        private String State;

        @SerializedName("country")
        @Expose
        private String Country;

        @SerializedName("currency_name")
        @Expose
        private String currencyName;

        @SerializedName("currency_code")
        @Expose
        private String currencyCode;

        @SerializedName("currency_symbol")
        @Expose
        private String currencySymbol;

        @SerializedName("symbol_position")
        @Expose
        private String symbolPosition;

        @SerializedName("decimal_position")
        @Expose
        private String decimalPosition;

        @SerializedName("tax_name")
        @Expose
        private String taxName;

        @SerializedName("tax_value")
        @Expose
        private String taxValue;

        @SerializedName("tax_number")
        @Expose
        private String taxNumber;

        @SerializedName("tax_type")
        @Expose
        private String taxType;

        @SerializedName("tax_name2")
        @Expose
        private String taxName2;

        @SerializedName("tax_value2")
        @Expose
        private String taxValue2;

        @SerializedName("tax_number2")
        @Expose
        private String taxNumber2;

        @SerializedName("tax_type2")
        @Expose
        private String taxType2;

        @SerializedName("location_telephone")
        @Expose
        private String locationTelephone;

        @SerializedName("senior_discount")
        @Expose
        private String seniorDiscount;

        @SerializedName("disabled_discount")
        @Expose
        private String disabledDiscount;

        @SerializedName("solo_discount")
        @Expose
        private String soloDiscount;

        public String getLocationId() { return locationId;  }
        public void setLocationId(String locationId) {
            this.locationId = locationId;
        }

        public String getLocationName() { return locationName;  }
        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        public String getAddress1() { return address1;  }
        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() { return address2;  }
        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() { return City;  }
        public void setCity(String City) {
            this.City = City;
        }

        public String getCountry() { return Country;  }
        public void setCountry(String Country) {
            this.Country = Country;
        }

        public String getcurrencyName() { return currencyName;  }
        public void setcurrencyName(String currencyName) {
            this.currencyName = currencyName;
        }

        public String getcurrencyCode() { return currencyCode;  }
        public void setcurrencyCode(String currencyCode) {this.currencyCode = currencyCode;
        }

        public String getCurrencySymbol() { return currencySymbol;  }
        public void setcurrencySymbol(String currencySymbol) { this.currencySymbol = currencySymbol; }

        public String getDecimalPosition() { return decimalPosition;  }
        public void setDecimalPosition(String decimalPosition) { this.decimalPosition = decimalPosition; }

        public String getTaxName() { return taxName;  }
        public void setTaxName(String taxName) { this.taxName = taxName; }

        public String getTaxValue() { return taxValue;  }
        public void setTaxValue(String taxValue) { this.taxValue = taxValue; }

        public String getTaxNumber() { return taxNumber;  }
        public void setTaxNumber(String taxNumber) { this.taxNumber = taxNumber; }

        public String getTaxType() { return taxType;  }
        public void setTaxType(String taxType) { this.taxType = taxType; }

        public String getTaxName2() { return taxName2;  }
        public void setTaxName2(String taxName2) { this.taxName2 = taxName2; }

        public String getTaxValue2() { return taxValue2;  }
        public void setTaxValue2(String taxValue2) { this.taxValue2 = taxValue2; }

        public String getTaxNumber2() { return taxNumber2;  }
        public void setTaxNumber2(String taxNumber2) { this.taxNumber2 = taxNumber2; }

        public String getTaxType2() { return taxType2;  }
        public void setTaxType2(String  taxType2) { this.taxType2 = taxType2; }

        public String getseniorDiscount() { return seniorDiscount;  }
        public void setseniorDiscount(String  seniorDiscount) { this.seniorDiscount = seniorDiscount; }

        public String getdisabledDiscount() { return disabledDiscount;  }
        public void setdisabledDiscount(String  disabledDiscount) { this.disabledDiscount = disabledDiscount; }

        public String getsoloDiscount() { return soloDiscount;  }
        public void setsoloDiscount(String  soloDiscount) { this.soloDiscount = soloDiscount; }


        public String getLocationTelephone() { return locationTelephone;  }
        public void setLocationTelephone(String  locationTelephone) { this.locationTelephone = locationTelephone; }


    }

    public  class staffInfo{

        @SerializedName("staff_id")
        @Expose
        private String staffId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("location")
        @Expose
        private String location;

        public String getStaffId() {
            return staffId;
        }

        public void setStaffId(String staffId) {
            this.staffId = staffId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserType() {
            return userType;
        }


        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }
}
