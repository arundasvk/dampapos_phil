package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Takeway extends Fragment {

    public static Takeway newInstance() {
        Takeway fragment = new Takeway();
        return fragment;
    }
    Context mContext;
    private TextView tv_take_away;
    private EditText edt_guest_name,edt_number;
    private String guest_name,guest_number;
    public static final String MyPREFERENCES_TAKEAWAY= "MyPrefsTakeaway";
    private SharedPreferences sharedpreferencesTakeaway;

    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    Spinner spin;
    String no_guest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.takeawayfragment, container, false);
        mContext = getActivity();
        spin = view.findViewById(R.id.spinner);


        tv_take_away = view.findViewById(R.id.tv_take_away);
        edt_guest_name = view.findViewById(R.id.edt_guest_name);
        edt_number = view.findViewById(R.id.edt_guest_number);

        sharedpreferencesTakeaway = this.getActivity().getSharedPreferences(MyPREFERENCES_TAKEAWAY,Context.MODE_PRIVATE);

        guest_name = edt_guest_name.getText().toString().trim();
        guest_number = edt_number.getText().toString().trim();

        sharedpreferencesData = this.getActivity().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


        setTextPreference();

        String[] types = getResources().getStringArray(R.array.guestCount);
        final List<String> nationalityType = new ArrayList<>(Arrays.asList(types));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_text, nationalityType) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spin.setAdapter(spinnerArrayAdapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                no_guest = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text


                }
                String spinnerValue = spin.getSelectedItem().toString();
                AppSession.getInstance().setGuest_Count(spinnerValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*SharedPreferences.Editor editor1 = sharedpreferencesTakeaway.edit();
        editor1.putString("GuestName", guest_name);
        editor1.putString("GuestNumber", guest_number);
        editor1.commit();*/

        return view;
    }
    private void setTextPreference() {
        if( !sharedpreferencesData.getString("GuestName","").equals(""))
        {
            edt_guest_name.setText( sharedpreferencesData.getString("GuestName",""));
        }
        if( !sharedpreferencesData.getString("GuestNumber","").equals(""))
        {
            edt_number.setText( sharedpreferencesData.getString("GuestNumber",""));
        }
    }

    private void setPreference() {
        guest_name = edt_guest_name.getText().toString().trim();
        guest_number = edt_number.getText().toString().trim();
        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        editor1.putString("GuestName", guest_name);
        editor1.putString("GuestNumber", guest_number);

        editor1.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setPreference();
    }

}
