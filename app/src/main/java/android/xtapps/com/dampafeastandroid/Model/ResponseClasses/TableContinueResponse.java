package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TableContinueResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("order_info")
    @Expose
    private ArrayList<OrderInfo> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderInfo> getData() {
        return data;
    }

    public void setData(ArrayList<OrderInfo> data) {
        this.data = data;
    }
}
