package android.xtapps.com.dampafeastandroid.View.Activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.DB.ModifierItems;
import android.xtapps.com.dampafeastandroid.Interfaces.ItemClickListener;
import android.xtapps.com.dampafeastandroid.Interfaces.LogOutTimerUtil;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.AddCartResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.LoginResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ModifierDataList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ModifierInfoResponce;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.ModifierMainListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.ModifierSubListAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.NotificationAdapter;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.xtapps.com.dampafeastandroid.Controller.Constants.API.ACTION_LOGIN;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ARRAY;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_CREATED_AT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_MENU_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ORDER_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STAFF_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STATUS;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_NAME;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TEXT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TYPE;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_UPDATED_AT;


public class ItemDetailsActivity extends AppCompatActivity implements  onApiFinish, LogOutTimerUtil.LogOutListener{
    private final String FROM_LOGIN = "login";
    String item_discount_percent="";
    EditText ed_discount_percent;
    Double item_discount=0.00;
    Spinner spinner1;
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    // popup
    ImageView  img_right ;
    ImageView  img_wrong;
    LinearLayout linear_two ;
    LinearLayout linear_user_pass ;
    LinearLayout linear_comment,ll_apply_coupon ;
    TextView tv_one,tv_two,tv_three,tv_subtotal ;
    TextView tv_login;
    TextView tv_submit ;
    EditText ed_coupon_code;
    String discount_comment="";
    EditText edt_username,edt_password,edt_comment;
    Spinner spinner,spinner_card;
    private final String FROM_SUPERVISOR_LIST = "supervisor_list";
    private ArrayList<String> SupervisorListArray = new ArrayList<String>();
    String select,selected_id;
    ImageView img_success,img_print;
    private String TAG = ItemDetailsActivity.class.getSimpleName();
    private  AlertDialog.Builder dialogBuilder;
    private AlertDialog alertDialog;
    private String[] mNavigationDrawerItemTitles;
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerList;
    Toolbar toolbar;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private TextView tv_update_item;
    GridView grid1,grid2;
    TextView addmodifier,tv_item_name,tv_price_value,tv_value,tv_menu_name,tv_update,tv_item_discount_apply,tv_item_discount;
    private EditText edt_remark;
    ImageView img_add,img_minus,nav_back;
    LinearLayout ll_dine,ll_take_away,ll_delivery,ll_reports,ll_kitchen,ll_logout,ll_discount,ll_disc_btn,ll_apply_btn;
    String name;
    public int count ;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    RecyclerView recyclerView;
    GridView rv_submenu;
    Context mContext;
    onApiFinish mOnApiFinish;
    private String FROM_MENU = "restaurant_menu";
    List<ModifierDataList> mProductLists;
    List<OptionValuesList> mProductLists2;
    ModifierMainListAdapter lasthrAdapter;
    ModifierSubListAdapter lasthrAdapter2;
    FrameLayout frag1,frag2;
    String strName=null;
    String strPrice=null;
    String strtotal=null;
    String strquantity=null;
    String strMenuId=null;
    String strOrderMenuId=null;
    String strcategory_name=null;
    String strorder_id=null;
    int position=0;
    String total;
    RelativeLayout rl_right_container;
    View vw;
    DecimalFormat df2;
    ProgressBar simpleProgressBar;
    int Service_type=0;
    List<CartItem> new_orders;
    List<CartItem> old_orders;
    int cartcount;
    ListView listview2;
    private BroadcastReceiver broadcastReceiver;
    NotificationAdapter adapter2;
    private int SPLASH_TIME_OUT = 5000;
    HashMap<String, String> map1;
    private  String menu_option_id="",menu_option_name="",menu_option_value_id="",option_value_id="",option_id="",value,new_price,price,option_name="",display_type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        mContext=this;
        df2 = new DecimalFormat(".##");
        sharedpreferencesData = ItemDetailsActivity.this.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

        //  flContainerfull.setVisibility(View.VISIBLE);
        // tv_update_item = (TextView) view.findViewById(R.id.tv_update_item);
        grid1 = (GridView) findViewById(R.id.left_drawerr);
        grid2 = (GridView) findViewById(R.id.left_drawerrr);
        // addmodifier = (TextView) view.findViewById(R.id.addmodifier);
        tv_item_name = (TextView) findViewById(R.id.tv_item_name);
        tv_price_value = (TextView) findViewById(R.id.tv_price_value);
        tv_value = (TextView) findViewById(R.id.tv_value);
        tv_menu_name = (TextView) findViewById(R.id.tv_menu_name);
        tv_update= (TextView) findViewById(R.id.tv_update);
        tv_item_discount_apply = (TextView) findViewById(R.id.tv_item_discount_apply);
        tv_item_discount = (TextView) findViewById(R.id.tv_item_discount);

        ll_dine = (LinearLayout) findViewById(R.id.ll_dine);
        ll_take_away = (LinearLayout) findViewById(R.id.ll_take_away);
        ll_delivery = (LinearLayout) findViewById(R.id.ll_delivery);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_kitchen = (LinearLayout) findViewById(R.id.ll_kitchen);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        ll_discount = (LinearLayout) findViewById(R.id.ll_discount);
        ll_disc_btn = (LinearLayout) findViewById(R.id.ll_disc_btn);
        ll_apply_btn = (LinearLayout) findViewById(R.id.ll_apply_btn);


        rl_right_container= (RelativeLayout) findViewById(R.id.rl_right_container);
        vw= (View) findViewById(R.id.vw);

        nav_back = (ImageView) findViewById(R.id.nav_back);
        img_add = (ImageView) findViewById(R.id.img_add);
        img_minus = (ImageView) findViewById(R.id.img_minus);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        edt_remark= (EditText) findViewById(R.id.edt_remark);
        ed_discount_percent = findViewById(R.id.ed_discount_percent);

        recyclerView = findViewById(R.id.rvAnimals);
        rv_submenu = findViewById(R.id.rv_submenu);

        listview2 = (ListView)findViewById(R.id.listview2);
        listview2.setVisibility(View.GONE);

        if(AppSession.getInstance().getModifierFlag().equals(""))
        {
            rl_right_container.setVisibility(View.GONE);
            vw.setVisibility(View.GONE);
        }


        strName=getIntent().getStringExtra("name");
        strcategory_name=getIntent().getStringExtra("category_name");
        strPrice=getIntent().getStringExtra("price");
        strtotal=getIntent().getStringExtra("total");
        strquantity=getIntent().getStringExtra("quantity");
        strMenuId=getIntent().getStringExtra("menu_id");
        strOrderMenuId=getIntent().getStringExtra("order_menu_id");
        strorder_id=getIntent().getStringExtra("order_id");
        position=getIntent().getIntExtra("position",0);
        //Toast.makeText(ItemDetailsActivity.this,"...."+strName+"...."+strPrice+"...."+strtotal+"...."+strquantity+"...."+strMenuId,Toast.LENGTH_LONG).show();
        // strtotal=getArguments().getString("price");
        tv_menu_name.setText(strName);
        tv_item_name.setText(strcategory_name);
        double f = Double.valueOf(strtotal);
        /*String ft =df2.format(f);*/
        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
        tv_price_value.setText(ft);
        //tv_price_value.setText(strtotal);
        tv_value.setText(strquantity);
        count=Integer.valueOf(tv_value.getText().toString());
        simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setScaleX(3f);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ItemDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        initCallBackListner();
        old_orders = DBMethodsCart.newInstance(ItemDetailsActivity.this).getCartItems();
        if(!old_orders.get(position).getDiscount_comment().equals(""))
        {
            ll_discount.setVisibility(View.VISIBLE);
            ed_discount_percent.setText(old_orders.get(position).getDiscount_percent().toString()+" %");
            ed_discount_percent.setFocusable(false);
            ll_apply_btn.setVisibility(View.GONE);
            ll_disc_btn.setVisibility(View.GONE);
        }
        else
        {

        }

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener( ItemDetailsActivity.this,
                recyclerView, new ItemClickListener() {

            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
               /* Toast.makeText(ItemDetailsActivity.this, "Single Click on position        :"+position,
                        Toast.LENGTH_SHORT).show();*/

                mProductLists2 = mProductLists.get(position).getOptionValues();
                if(mProductLists2.size()>0)
                {
                    if(!isFinishing()&&mContext!=null)
                    {
                        menu_option_id= mProductLists.get(position).getmenu_option_id();
                        menu_option_name= mProductLists.get(position).getoption_name();
                        lasthrAdapter2 = new ModifierSubListAdapter(ItemDetailsActivity.this, mProductLists2);
                        rv_submenu.setAdapter(lasthrAdapter2);
                        lasthrAdapter2.notifyDataSetChanged();

                    }

                    //    Toast.makeText(ItemDetailsActivity.this,"rescycle"+menu_option_id,Toast.LENGTH_LONG).show();



                    rv_submenu.setVisibility(View.VISIBLE);

                }
                else
                {
                    rv_submenu.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
               /* Toast.makeText( getActivity(), "Long press on position :"+position,
                        Toast.LENGTH_LONG).show();*/
            }

            @Override
            public void onItemSelected(String item, int position, Object response) {

            }

        }));
        rv_submenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String selectedItem = adapterView.getItemAtPosition(i).toString();
               /* Toast.makeText(ItemDetailsActivity.this, "Single Click on position        :"+selectedItem,
                        Toast.LENGTH_SHORT).show();*/
                if(!isFinishing()&&mContext!=null){


                    if(mProductLists2.get(i).getState()==1)
                    {

                        mProductLists2.get(i).setState(0);
                        DBMethodsCart.newInstance(mContext).removeFromModifier2(strMenuId,mProductLists2.get(i).getOption_value_id());

                    }
                    else
                    {

                        mProductLists2.get(i).setState(1);


                        menu_option_value_id= mProductLists2.get(i).getMenu_option_value_id();
                        option_value_id= mProductLists2.get(i).getOption_value_id();
                        option_id= mProductLists2.get(i).getOption_id();
                        option_name=mProductLists2.get(i).getOption_name();
                        display_type=mProductLists2.get(i).getDisplay_type();
                        value= mProductLists2.get(i).getValue();
                        new_price= mProductLists2.get(i).getNew_price();
                        price= mProductLists2.get(i).getPrice();
                        // Toast.makeText(ItemDetailsActivity.this,"grid"+menu_option_id,Toast.LENGTH_LONG).show();
                        List<ModifierItems> modifiers = DBMethodsCart.newInstance(getApplicationContext()).getModifierById(menu_option_id,option_value_id);
                        if(modifiers.size()>0)
                        {

                        }
                        else
                        {
                            DBMethodsCart.newInstance(ItemDetailsActivity.this).insertModifierItems(
                                    strMenuId,
                                    menu_option_name,
                                    menu_option_id,
                                    menu_option_value_id,
                                    option_value_id,
                                    option_id,
                                    option_name,
                                    display_type,
                                    value,
                                    new_price,
                                    price);
                            test();

                        }


                    }



                    lasthrAdapter2.notifyDataSetChanged();




                }

            }
        });
        /*tv_update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CartFragment fragment2 = new CartFragment();

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.flContainerfull, fragment2);
                fragmentTransaction.commit();
               *//* Intent intent=new Intent(getActivity(),ViewCartList_Fragment.class);
                startActivity(intent);*//*
            }
        });*/


        nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                // data.get(position).setMinimum_qty(Integer.parseInt(String.valueOf(count)));

                tv_value.setText(String.valueOf(count));
                double f = Double.valueOf(strPrice)*count;
               /* String ft =df2.format(f);*/
                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                // tv_price_value.setText(String.valueOf(Double.valueOf(strPrice)*count));
                tv_price_value.setText(ft);
            }
        });

        img_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                count--;
                // data.get(position).setMinimum_qty(Integer.parseInt(String.valueOf(count)));

                if (count < 1) {
                    count = 1;
                }

                tv_value.setText(String.valueOf(count));
                double f = Double.valueOf(strPrice)*count;
               /* String ft =df2.format(f);*/
                String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
                tv_price_value.setText(ft);
                //tv_price_value.setText(String.valueOf(Double.valueOf(strPrice)*count));
            }
        });
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBMethodsCart.newInstance(ItemDetailsActivity.this).updateCart( strOrderMenuId,count,
                        Double.valueOf(tv_price_value.getText().toString()),
                        menu_option_id,
                        menu_option_value_id,option_value_id,option_id,value,new_price,price,
                        edt_remark.getText().toString(),"");


                 Toast.makeText(ItemDetailsActivity.this,getResources().getString(R.string.update_modofiers),Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
                        AppSession.getInstance().setTabP("1");
                        startActivity(i);
                    }
                }, 2000);


            }
        });
        ll_disc_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                supervier_list();
            }
        });
        ll_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( ed_discount_percent.getText().length()==0)
                {
                    item_discount_percent= "0";
                    Toast.makeText(ItemDetailsActivity.this,getResources().getString(R.string.disc_val),Toast.LENGTH_LONG).show();
                }
                else if(Double.valueOf(ed_discount_percent.getText().toString())>100)
                {
                    item_discount_percent= "0";

                    Toast.makeText(ItemDetailsActivity.this,getResources().getString(R.string.disc_val_100),Toast.LENGTH_LONG).show();

                }
                else
                {
                    item_discount_percent= ed_discount_percent.getText().toString();
                    item_discount=(Double.valueOf(item_discount_percent)*(Double.parseDouble(strPrice)*
                            Integer.parseInt(String.valueOf(tv_value.getText().toString()))))/100;
                    DBMethodsCart.newInstance(mContext).updateCartDiscount(strOrderMenuId,item_discount,Double.valueOf(item_discount_percent),strorder_id,discount_comment);
                    ll_apply_btn.setVisibility(View.GONE);
                    ed_discount_percent.setFocusable(false);
                }


            }
        });
        ll_dine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusDinein();


            }
        });
        ll_take_away.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusTakeAway();

            }
        });
        ll_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStatusDelivery();

            }
        });
        ll_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setTick("reports");
                Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
                startActivity(i);
                finish();


            }
        });
        ll_kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setTick("kitchen");
                Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
                startActivity(i);
                finish();

            }
        });
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();


            }
        });

    }

    private void logout() {
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_id_delivery("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setTick("logout");
        Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }

    private void test() {
        List<ModifierItems> modifiers = DBMethodsCart.newInstance(getApplicationContext()).getModifierById(menu_option_id,menu_option_value_id);
        for (ModifierItems item : modifiers) {
          /*  Toast.makeText(ItemDetailsActivity.this,"Menu_option_id "+item.getMenu_option_id()
                    +" option_value_id "+item.getMenu_option_value_id()+"option_id"
                    +item.getOption_id(),Toast.LENGTH_LONG).show();*/
        }


    }

    private void checkStatusDelivery() {
        if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            finish();
        }
        else
        {
            Service_type=3;
           /* cartcount = DBMethodsCart.newInstance(ItemDetailsActivity.this).getCartItemsCount();*/

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("delivery"))
            {
                new_orders = DBMethodsCart.newInstance(ItemDetailsActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();
                }
                else
                {
                    // continue with delete
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearCart();
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearModifier();
                    delivery();
                }
                //  alert();
            }
            else
            {
                delivery();
            }


        }
    }

    private void checkStatusTakeAway() {
        if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            finish();
        }
        else
        {
            Service_type=2;

           /* cartcount = DBMethodsCart.newInstance(ItemDetailsActivity.this).getCartItemsCount();*/

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                new_orders = DBMethodsCart.newInstance(ItemDetailsActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearCart();
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearModifier();
                    take_away();
                }
                // alert();
            }
            else
            {
                take_away();
            }
        }
    }



    private void checkStatusDinein() {
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            finish();
        }
        else
        {
            Service_type=1;

         /*   cartcount = DBMethodsCart.newInstance(ItemDetailsActivity.this).getCartItemsCount();*/
            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("dine_in"))
            {
                new_orders = DBMethodsCart.newInstance(ItemDetailsActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setOrderTotal("0");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearCart();
                    DBMethodsCart.newInstance(ItemDetailsActivity.this).clearModifier();
                    dine_in();
                }
                //alert();
            }
            else
            {
                dine_in();
            }
        }
    }
    private void alert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(ItemDetailsActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(ItemDetailsActivity.this);
        }
        builder.setTitle("You have unfinished orders")
                .setMessage("Are you sure you want to continue?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("order_comment","");
                        editor1.commit();
                        AppSession.getInstance().setTable_ID("");
                        AppSession.getInstance().setTable_Name("");
                        AppSession.getInstance().setOrderNumber("");
                        AppSession.getInstance().setOrderTotal("0");
                        AppSession.getInstance().setOrder_id_delivery("");
                        AppSession.getInstance().setOrder_idTake_delivery("");
                        DBMethodsCart.newInstance(ItemDetailsActivity.this).clearCart();
                        DBMethodsCart.newInstance(ItemDetailsActivity.this).clearModifier();
                        if(Service_type==1)
                        {
                            dine_in();
                        }
                        else
                        if(Service_type==2)
                        {
                            take_away();
                        }
                        else if(Service_type==3)
                        {
                            delivery();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void delivery() {
        AppSession.getInstance().setServiceType("delivery");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_idTake_delivery("");
        Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }

    private void dine_in() {
        AppSession.getInstance().setServiceType("dine_in");
        AppSession.getInstance().setOrder_id_delivery("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }
    private void take_away() {
        AppSession.getInstance().setServiceType("takeaway");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_id_delivery("");
        AppSession.getInstance().setTable_Name("");
        Intent i=new Intent(ItemDetailsActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }

    /*   @Override
       public void onResume() {
           super.onResume();

           if( AppSession.getInstance().getModifierFlag().equals("1")){
               if(!isFinishing()&&mContext!=null){
                   simpleProgressBar.setVisibility(View.VISIBLE);
                   loadData();
               }


           }else{

           }
       }*/
    private void initCallBackListner() {
        mOnApiFinish = new onApiFinish() {
            @Override
            public void onSuccess(String tag, String response) {


                if (tag.equals(FROM_MENU)) {


                    Gson gson = new Gson();
                    ModifierInfoResponce menuResponse = gson.fromJson(response, ModifierInfoResponce.class);
                    if (menuResponse.getStatus().equals("success")) {
                        simpleProgressBar.setVisibility(View.GONE);


                        if(!isFinishing()&&mContext!=null){

                            mProductLists = menuResponse.getData();
                            lasthrAdapter = new ModifierMainListAdapter(ItemDetailsActivity.this, mProductLists);
                            recyclerView.setAdapter(lasthrAdapter);
                            lasthrAdapter.notifyDataSetChanged();
                            menu_option_id= mProductLists.get(0).getmenu_option_id();
                            menu_option_name= mProductLists.get(0).getoption_name();
                                /* for (int i = 0; i < menuResponse.getData().size(); i++) {*/
                            mProductLists2 = menuResponse.getData().get(0).getOptionValues();
                            if (mProductLists2.size() > 0) {
                                for(int i=0;i<mProductLists2.size();i++)
                                {
                                    mProductLists2.get(i).setMenu_id(strMenuId);
                                }

                                if(!isFinishing()&&mContext!=null){
                                    lasthrAdapter2 = new ModifierSubListAdapter(ItemDetailsActivity.this, mProductLists2);
                                    rv_submenu.setAdapter(lasthrAdapter2);
                                    lasthrAdapter2.notifyDataSetChanged();
                                    //   Toast.makeText(mContext,mProductLists2.get(0).getOption_id().toString(), Toast.LENGTH_SHORT).show();
                                }



                                //}
                            }
                        }






                    }

                    else {
                        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

            }

            @Override
            public void onFailed(String tag, String response) {
                Gson gson = new Gson();
                ModifierInfoResponce menuResponse = gson.fromJson(response, ModifierInfoResponce.class);
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onJsonError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServerError(String tag, String response) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();

            }

        };

    }
    private void loadData() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.MODIFIERSAPI ;
        /*current_page= page++;*/
        HashMap<String, String> values = new HashMap<>();
     /*   values.put("action", "menuOptionsInfo");*/
        values.put("action", Constants.API.ACTION_MODIFIERSAPI);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("menu_id", strMenuId);
        //values.put("staff_id", "11");
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(mContext,mOnApiFinish, values);
        objCallWebServiceTask.execute(URL, FROM_MENU , "POST");
    }
    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ItemClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ItemClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
    @Override public void onBackPressed() {

        super.onBackPressed();

    }
    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if(view instanceof EditText){
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    /*    @Override public void onStart() {
            super.onStart();
            registerReceiver();
        }*/
    @Override
    protected void onStop() {
        super.onStop();
        /*
         * Step 4: Ensure to unregister the receiver when the activity is destroyed so that
         * you don't face any memory leak issues in the app
         */
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
    private void registerReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(AppSession.getInstance().getUser_Type().equals("WAITER")){
                    String otpCode = intent.getStringExtra(NOTI_ARRAY);
              /*  final ArrayList<HashMap<String, String>> map1 =(ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
                for (int i=0;i<map1.size();i++) {
                  String  id = map1.get(i).get(SplashScreenActivity.LOCAION_ID);
                    String location_name = map1.get(i).get(SplashScreenActivity.LOCAION_NAME);
                    *//* Build the StringWithTag List using these keys and values. *//*
                    Toast.makeText(NavigationMainActivity.this, id+" "+location_name, Toast.LENGTH_SHORT).show();
                }*/


                    vibrate();
                    JSONArray jArray = null;
                    try {
                        jArray = new JSONArray(otpCode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (jArray.length()==0)
                    {

                    }
                    else
                    {
                        categoryList.clear();
                        for(int i=0;i<jArray.length();i++){

                            JSONObject json_data = null;
                            try {
                                json_data = jArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String text1 = null;
                            String table_id = null;
                            String staff_id = null;
                            String menu_id = null;
                            String order_id = null;
                            String status = null;
                            String created_at = null;
                            String updated_at = null;
                            String table_name = null;
                            String order_type = null;
                            try {
                                text1 = json_data.getString("text");
                                table_id = json_data.getString("id");
                                table_name = json_data.getString("table_name");
                                order_type = json_data.getString("order_type");
                                staff_id = json_data.getString("staff_id");
                                menu_id = json_data.getString("menu_id");
                                order_id = json_data.getString("order_id");
                                status = json_data.getString("status");
                                created_at = json_data.getString("created_at");
                                updated_at = json_data.getString("updated_at");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            map1 = new HashMap<String, String>();

                            map1.put(NOTI_TABLE_ID, table_id);
                            map1.put(NOTI_TEXT, text1);
                            map1.put(NOTI_TYPE, order_type);
                            map1.put(NOTI_TABLE_NAME, table_name);
                            map1.put(NOTI_STAFF_ID, staff_id);
                            map1.put(NOTI_MENU_ID, menu_id);
                            map1.put(NOTI_ORDER_ID, order_id);
                            map1.put(NOTI_STATUS, status);
                            map1.put(NOTI_CREATED_AT, created_at);
                            map1.put(NOTI_UPDATED_AT, updated_at);
                            categoryList.add(map1);

                        }

                    }
                    /*
                     * Step 3: We can update the UI of the activity here
                     * */


                    listview2.setVisibility(View.VISIBLE);
                    adapter2 = new NotificationAdapter(ItemDetailsActivity.this,categoryList);
                    listview2.setAdapter(adapter2);
                    adapter2.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {



                        @Override
                        public void run() {

                            listview2.setVisibility(View.GONE);

                        }
                    }, SPLASH_TIME_OUT);

                }else{

                }

            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("notification_update_UI"));

    }

    private void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void popup() {
        dialogBuilder = new AlertDialog.Builder(ItemDetailsActivity.this, R.style.NewDialog);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.prepration_popup, null);

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();

        /* ImageView*/  img_right = (ImageView) dialogView.findViewById(R.id.img_right);
        /* final ImageView */ img_wrong = (ImageView) dialogView.findViewById(R.id.img_wrong);

        /*final LinearLayout*/ linear_two = (LinearLayout) dialogView.findViewById(R.id.linear_two);
        /* final LinearLayout*/ linear_user_pass = (LinearLayout) dialogView.findViewById(R.id.linear_user_pass);
        /* final LinearLayout*/ linear_comment = (LinearLayout) dialogView.findViewById(R.id.linear_comment);

        tv_one = (TextView)dialogView.findViewById(R.id.tv_one);
        tv_two = (TextView)dialogView.findViewById(R.id.tv_two);
        /* final TextView*/ tv_three = (TextView)dialogView.findViewById(R.id.tv_three);
        /* final TextView*/ tv_login = (TextView)dialogView.findViewById(R.id.tv_login);
        /* final TextView*/ tv_submit = (TextView)dialogView.findViewById(R.id.tv_submit);

        edt_username = (EditText)dialogView.findViewById(R.id.edt_username);
        edt_password = (EditText)dialogView.findViewById(R.id.edt_password);
        edt_comment= (EditText)dialogView.findViewById(R.id.edt_comment);

        //final SearchableSpinner spinner1 = dialogView.findViewById(R.id.spinner);
        spinner1 = dialogView.findViewById(R.id.spinner);
       // spinner1.setTitle(getResources().getString(R.string.spinner_title));
        String[] types1 = getResources().getStringArray(R.array.select_service);
        final List<String> genderType1 = new ArrayList<>(Arrays.asList(types1));

        /* Set your ArrayAdapter with the StringWithTag, and when each entry is shown in the Spinner, .toString() is called. */
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SupervisorListArray);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position >= 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);




                    /*  Toast.makeText(getApplicationContext(),select, Toast.LENGTH_LONG).show();*/

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        alertDialog.setCancelable(true);
        alertDialog.show();


            tv_one.setText("Enter Your Discount Comment");
            tv_two.setText("");
            linear_two.setVisibility(View.VISIBLE);
            img_wrong.setVisibility(View.GONE);


        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linear_two.setVisibility(View.VISIBLE);
                img_wrong.setVisibility(View.GONE);
            }
        });

        img_wrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFinishing()&&mContext!=null)
                {
                    superviser_login();
                }

               /* linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.comment));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);*/
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                    // AppSession.getInstance().setDiscountFlag(true);

                    discount_comment=edt_comment.getText().toString();

                    if(discount_comment.equals("")||discount_comment==null)
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.discount_comment_valid), Toast.LENGTH_LONG).show();

                    }
                    else
                    {

                        ll_discount.setVisibility(View.VISIBLE);
                        ll_apply_btn.setVisibility(View.VISIBLE);
                        ll_disc_btn.setVisibility(View.GONE);
                        alertDialog.hide();
                    }




                /*DBMethodsCart.newInstance(mContext).removeFromCart(MenuId);
                mCartItems =DBMethodsCart.newInstance(getApplicationContext()).getCartItems( );
                adapter = new CartListAdapter(CartActivity.this, mCartItems, mItemClickListner);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                bottomBarSettext();
                alertDialog.hide();*/
            }


        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    // To dismiss the fragment when the back-button is pressed.
                    alertDialog.hide();
                    return true;
                }


                // Otherwise, do nothing else
                else return false;
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){


                /****cleanup code****/
            }});
    }
    private void superviser_login() {
        String URL = AppSession.getInstance().getRegUrl()+ API.LOGIN;
        HashMap<String, String> values = new HashMap<>();
        // values.put("username", edt_username.getText().toString());
        values.put("username", select);
        values.put("password", edt_password.getText().toString());
        /* values.put("action","StaffLogin");*/
        values.put("action",ACTION_LOGIN);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(ItemDetailsActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_LOGIN, "POST");
    }
    private void supervier_list() {
        String URL = AppSession.getInstance().getRegUrl()+ API.SUPERVISOR_LIST;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","verifyCoupon");*/
        values.put("action", API.ACTION_SUPERVISOR_LIST);
        values.put("location_id", AppSession.getInstance().getLocation_id());

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(ItemDetailsActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_SUPERVISOR_LIST, "POST");
    }


    @Override
    public void onSuccess(String tag, String response) {
        if (tag.equals(FROM_LOGIN)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {

               /* AppSession.getInstance().setisLoginStatus(true);
                AppSession.getInstance().setStaff_ID(loginResponse.getStaffInfo().getStaffId().toString());
                AppSession.getInstance().setStaff_Name(loginResponse.getStaffInfo().getName().toString());
                AppSession.getInstance().setStaff_Email(loginResponse.getStaffInfo().getEmail().toString());
                AppSession.getInstance().setUser_Type(loginResponse.getStaffInfo().getUserType().toString());
                AppSession.getInstance().setLocation_id(loginResponse.getStaffInfo().getLocation().toString());*/

                // Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.comment));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);
            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_SUPERVISOR_LIST)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {
                SupervisorListArray.clear();
                for(int i=0;i<loginResponse.getData().size();i++)
                {
                    SupervisorListArray.add(loginResponse.getData().get(i).getUsername());

                }
                if(!isFinishing()&&mContext!=null)
                {
                    popup();
                }

            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "OnStart () &&& Starting timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "User interacting with screen");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if( AppSession.getInstance().getModifierFlag().equals("1")){
            if(!isFinishing()&&mContext!=null){
                simpleProgressBar.setVisibility(View.VISIBLE);
                loadData();
            }


        }
        Log.e(TAG, "onResume()");
    }

    /**
     * Performing idle time logout
     */
    @Override
    public void doLogout() {
        if(AppSession.getInstance().getUser_Type().equals("WAITER")
                ||AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            logout();
        }
        else
        {

        }
        // write your stuff here
    }
}