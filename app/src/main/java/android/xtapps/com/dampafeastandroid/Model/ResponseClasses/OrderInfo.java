package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderInfo {



    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("order_status_name")
    @Expose
    private String order_status_name;

    @SerializedName("order_status")
    @Expose
    private String order_status;

    @SerializedName("sale_no")
    @Expose
    private String sale_no;

    @SerializedName("staff")
    @Expose
    private String staff;

    @SerializedName("order_total")
    @Expose
    private String order_total;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("cart")
    @Expose
    private ArrayList<CartItem> cart;

    @SerializedName("items")
    @Expose
    private ArrayList<CartItem> items;

    @SerializedName("customer")
    @Expose
    private CustomerDetails customer;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public ArrayList<CartItem> getCart() {
        return cart;
    }

    public void setCart(ArrayList<CartItem> cart) {
        this.cart = cart;
    }

    public ArrayList<CartItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<CartItem> items) {
        this.items = items;
    }

    public CustomerDetails getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetails customer) {
        this.customer = customer;
    }

    public String getOrder_status_name() {
        return order_status_name;
    }

    public void setOrder_status_name(String order_status_name) {
        this.order_status_name = order_status_name;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOrder_total() {
        return order_total;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public String getSale_no() {
        return sale_no;
    }

    public void setSale_no(String sale_no) {
        this.sale_no = sale_no;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }
}
