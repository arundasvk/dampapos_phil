package android.xtapps.com.dampafeastandroid.SideMenuModules.interfacesSideMenu;

import android.graphics.Bitmap;

/**
 * Created by Konstantin on 12.01.2015.
 */
public interface ScreenShotable {
    void takeScreenShot();

    Bitmap getBitmap();
}
