package android.xtapps.com.dampafeastandroid.DB;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModifierItems {
    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("menu_name")
    @Expose
    private String menu_name;

    @SerializedName("menu_option_id")
    @Expose
    private String menu_option_id;

    @SerializedName("menu_option_value_id")
    @Expose
    private String menu_option_value_id;

    @SerializedName("option_value_id")
    @Expose
    private String option_value_id;

    @SerializedName("option_id")
    @Expose
    private String option_id;

    @SerializedName("option_name")
    @Expose
    private String option_name;

    @SerializedName("display_type")
    @Expose
    private String display_type;

    @SerializedName("new_price")
    @Expose
    private String new_price;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("price")
    @Expose
    private String price;

    private String index;

    public String getMenu_option_value_id() {
        return menu_option_value_id;
    }

    public void setMenu_option_value_id(String menu_option_value_id) {
        this.menu_option_value_id = menu_option_value_id;
    }

    public String getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(String option_value_id) {
        this.option_value_id = option_value_id;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(String display_type) {
        this.display_type = display_type;
    }

    public String getNew_price() {
        return new_price;
    }

    public void setNew_price(String new_price) {
        this.new_price = new_price;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getMenu_option_id() {
        return menu_option_id;
    }

    public void setMenu_option_id(String menu_option_id) {
        this.menu_option_id = menu_option_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public ModifierItems(String  index, String  menu_id,String  menu_name, String menu_option_id, String menu_option_value_id, String option_value_id, String option_id, String option_name, String display_type, String value, String new_price, String price) {

        this.index=index;
        this.menu_id=menu_id;
        this.menu_name=menu_name;
        this.menu_option_id=menu_option_id;
        this.menu_option_value_id= menu_option_value_id;
        this.option_value_id=option_value_id;
        this.option_id= option_id;
        this.option_name= option_name;
        this.display_type= display_type;
        this.value= value;
        this.new_price= new_price;
        this.price= price;


    }
}