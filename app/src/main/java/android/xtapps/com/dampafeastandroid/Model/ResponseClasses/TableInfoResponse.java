package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 7/24/2018.
 */

public class TableInfoResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<DataList> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataList> getData() {
        return data;
    }

    public void setData(ArrayList<DataList> data) {
        this.data = data;
    }


    public class DataList {

        @SerializedName("table_id")
        @Expose
        private String tableId;

        @SerializedName("table_name")
        @Expose
        private String tableName;

        @SerializedName("min_capacity")
        @Expose
        private String minCapacity;

        @SerializedName("max_capacity")
        @Expose
        private String maxCapacity;

        @SerializedName("table_position")
        @Expose
        private String tablePosition;

        @SerializedName("table_status")
        @Expose
        private String tableStatus;

        @SerializedName("table_reserved_staff")
        @Expose
        private String tableReservedStaff;

        @SerializedName("staff_name")
        @Expose
        private String staffName;


        public String getTableId() {
            return tableId;
        }

        public void setTableId(String tableId) {
            this.tableId = tableId;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }

        public String getMinCapacity() {
            return minCapacity;
        }

        public void setMinCapacity(String minCapacity) {
            this.minCapacity = minCapacity;
        }

        public String getMaxCapacity() {
            return maxCapacity;
        }

        public void setMaxCapacity(String maxCapacity) {
            this.maxCapacity = maxCapacity;
        }

        public String getTablePosition() {
            return tablePosition;
        }

        public void setTablePosition(String tablePosition) {
            this.tablePosition = tablePosition;
        }

        public String getTableStatus() {
            return tableStatus;
        }

        public void setTableStatus(String tableStatus) {
            this.tableStatus = tableStatus;
        }

        public String getTableReservedStaff() {
            return tableReservedStaff;
        }

        public void setTableReservedStaff(String tableReservedStaff) {
            this.tableReservedStaff = tableReservedStaff;
        }

        public String getStaffName() {
            return staffName;
        }

        public void setStaffName(String staffName) {
            this.staffName = staffName;
        }
    }

}
