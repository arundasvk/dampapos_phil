package android.xtapps.com.dampafeastandroid.DB;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartItem {

    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("order_menu_id")
    @Expose
    private String order_menu_id;

    @SerializedName("menu_name")
    @Expose
    private String menu_name;

    @SerializedName("category_name")
    @Expose
    private String category_name;

    @SerializedName("secondary_name")
    @Expose
    private String menu_name2;

    @SerializedName("menu_description")
    @Expose
    private String menu_description;

    @SerializedName("location_name")
    @Expose
    private String location_name;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("minimum_qty")
    @Expose
    private int minimum_qty;

    @SerializedName("menu_priority")
    @Expose
    private String menu_priority;

    @SerializedName("mealtime_name")
    @Expose
    private String mealtime_name;

    @SerializedName("menu_price")
    @Expose
    private String menu_price;

    @SerializedName("order")
    @Expose
    private String order_id;

    @SerializedName("menu_qty")
    @Expose
    private int menu_qty;

    @SerializedName("prev_quantity")
    @Expose
    private int prev_quantity;

    @SerializedName("menu_qty_X_menu_price")
    @Expose
    private double menu_qty_X_menu_price;

    @SerializedName("menu_option_id")
    @Expose
    private String menu_option_id;


    @SerializedName("required")
    @Expose
    private String required;

    @SerializedName("option_name")
    @Expose
    private String option_name;
    @SerializedName("menu_option_value_id")
    @Expose
    private String menu_option_value_id;

    @SerializedName("option_value_id")
    @Expose
    private String option_value_id;

    @SerializedName("option_id")
    @Expose
    private String option_id;


    @SerializedName("display_type")
    @Expose
    private String display_type;

    @SerializedName("new_price")
    @Expose
    private String new_price;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("default_value_id")
    @Expose
    private String default_value_id;

    @SerializedName("priority")
    @Expose
    private String priority;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("discount")
    @Expose
    private Double discount;

    @SerializedName("discount_percent")
    @Expose
    private Double discount_percent;

    @SerializedName("discount_comment")
    @Expose
    private String discount_comment;

    private int setCannot_decrease;

    public int getSetCannot_decrease() {
        return setCannot_decrease;
    }

    public void setSetCannot_decrease(int setCannot_decrease) {
        this.setCannot_decrease = setCannot_decrease;
    }

    private String editTextValue;

    public String getEditTextValue() {
        return editTextValue;
    }

    public void setEditTextValue(String editTextValue) {
        this.editTextValue = editTextValue;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    private String index;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String comment;


    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }


    public String getMenu_name2() {
        return menu_name2;
    }

    public void setMenu_name2(String menu_name2) {
        this.menu_name2 = menu_name2;
    }

    public String getMenu_description() {
        return menu_description;
    }

    public void setMenu_description(String menu_description) {
        this.menu_description = menu_description;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }



    public String getMenu_priority() {
        return menu_priority;
    }

    public void setMenu_priority(String menu_priority) {
        this.menu_priority = menu_priority;
    }

    public String getMealtime_name() {
        return mealtime_name;
    }

    public void setMealtime_name(String mealtime_name) {
        this.mealtime_name = mealtime_name;
    }

    public String getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(String menu_price) {
        this.menu_price = menu_price;
    }

    public int getMinimum_qty() {
        return minimum_qty;
    }

    public void setMinimum_qty(int minimum_qty) {
        this.minimum_qty = minimum_qty;
    }

    public int getMenu_qty() {
        return menu_qty;
    }

    public void setMenu_qty(int menu_qty) {
        this.menu_qty = menu_qty;
    }

    public double getMenu_qty_X_menu_price() {
        return menu_qty_X_menu_price;
    }

    public void setMenu_qty_X_menu_price(double menu_qty_X_menu_price) {
        this.menu_qty_X_menu_price = menu_qty_X_menu_price;
    }


    public String getDiscount_comment() {
        return discount_comment;
    }

    public void setDiscount_comment(String discount_comment) {
        this.discount_comment = discount_comment;
    }

    public Double getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(Double discount_percent) {
        this.discount_percent = discount_percent;
    }

    public String getMenu_option_id() {
        return menu_option_id;
    }

    public void setMenu_option_id(String menu_option_id) {
        this.menu_option_id = menu_option_id;
    }



    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getMenu_option_value_id() {
        return menu_option_value_id;
    }

    public void setMenu_option_value_id(String menu_option_value_id) {
        this.menu_option_value_id = menu_option_value_id;
    }

    public String getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(String option_value_id) {
        this.option_value_id = option_value_id;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }


    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(String display_type) {
        this.display_type = display_type;
    }

    public String getNew_price() {
        return new_price;
    }

    public void setNew_price(String new_price) {
        this.new_price = new_price;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDefault_value_id() {
        return default_value_id;
    }

    public void setDefault_value_id(String default_value_id) {
        this.default_value_id = default_value_id;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getPrev_quantity() {
        return prev_quantity;
    }

    public void setPrev_quantity(int prev_quantity) {
        this.prev_quantity = prev_quantity;
    }

    public String getOrder_menu_id() {
        return order_menu_id;
    }

    public void setOrder_menu_id(String order_menu_id) {
        this.order_menu_id = order_menu_id;
    }

    public CartItem(String  index, String menu_id,String order_menu_id, String category_name, String menu_name, String menu_name2, String order_id, String menu_description, String location_name, String location, String menu_priority, String mealtime_name, String menu_price,
                    int minimum_qty, int menu_qty, int prev_quantity, double menu_qty_X_menu_price, String required, String comment, String menu_option_id, String menu_option_value_id, String option_value_id, String option_id, String value, String new_price, String price, String status,
                    Double discount, Double discount_percent, String discount_comment) {
        this.menu_id = menu_id;
        this.order_menu_id = order_menu_id;
        this.category_name = category_name;
        this.menu_name = menu_name;
        this.menu_name2 = menu_name2;
        this.order_id = order_id;
        this.menu_description = menu_description;
        this.location_name = location_name;
        this.location = location;
        this.menu_priority = menu_priority;
        this.mealtime_name = mealtime_name;
        this.menu_price = menu_price;
        this.minimum_qty = minimum_qty;
        this.menu_qty = menu_qty;
        this.prev_quantity = prev_quantity;
        this.menu_qty_X_menu_price = menu_qty_X_menu_price;

        this.required = required;

        this.comment= comment;
        this.menu_option_id= menu_option_id;
        this.menu_option_value_id= menu_option_value_id;
        this.option_value_id=option_value_id;
        this.option_id= option_id;
        this.value= value;
        this.new_price= new_price;
        this.price= price;
        this.index=index;

        this.status = status;
        this.discount = discount;
        this.discount_percent = discount_percent;
        this.discount_comment = discount_comment;



    }






    public CartItem(String menu_id,String order_menu_id,String menu_name,String menu_name2,
                    int minimum_qty,int menu_qty,double menu_qty_X_menu_price) {
        this.menu_id = menu_id;
        this.order_menu_id = order_menu_id;
        this.menu_name = menu_name;
        this.menu_name2 = menu_name2;
        this.minimum_qty = minimum_qty;
        this.menu_qty = menu_qty;
        this.menu_qty_X_menu_price = menu_qty_X_menu_price;


    }
}
