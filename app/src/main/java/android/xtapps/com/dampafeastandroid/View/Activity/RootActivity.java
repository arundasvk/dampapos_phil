package android.xtapps.com.dampafeastandroid.View.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Created by USER on 7/17/2018.
 */

public class RootActivity extends AppCompatActivity implements DialogInterface.OnCancelListener {

    private int screenHeightInPixels = 0, screenWidhtInPixels = 0;

    private static RootActivity instance;
    private ProgressDialog progress;
    public static boolean requestFlag=false;
    public static boolean rateusFlag=false;
    public static boolean pushFlag=false;
    public static boolean commitFlag=false;


    public RootActivity() {

        instance = this;
        // TODO Auto-generated constructor stub
    }
    public static Context getContext() {
        return instance;
    }

    //ProgressHUD mProgressHUD;


    public void showToastMessage(final String message){
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void onCreate(Bundle savedInstanceState, int resLayout) {
        super.onCreate(savedInstanceState);
        setContentView(resLayout);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeightInPixels = displaymetrics.heightPixels;
        screenWidhtInPixels = displaymetrics.widthPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeightInPixels = displaymetrics.heightPixels;
        screenWidhtInPixels = displaymetrics.widthPixels;
    }

    public int getScreenHeightInPixels() {
        return screenHeightInPixels;
    }

    public int getScreenWidhtInPixels() {
        return screenWidhtInPixels;
    }


    private void progressDialog ()
    {
        progress=new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        // progress.setProgress(0);
        progress.show();
    }



    public void showBusyAnimation(){

        progress=new ProgressDialog(RootActivity.this);
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        // progress.setProgress(0);
        progress.show();


    }

    public void hideBusyAnimation() {

        new Thread(new Runnable() {
            @Override
            public void run()
            {
                // do the thing that takes a long time

                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        progress.dismiss();
                    }
                });
            }
        }).start();


    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub

    }



    protected void onResume() {
        super.onResume();
    }
    protected void onPause() {
        super.onPause();
    }
    protected void onDestroy() {

        //PersitentUtil.saveCommonData();
        super.onDestroy();
    }


    public void hideSoftKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showSoftKeyboard(View focusView) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(focusView,
                    InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {
            // TODO Auto-generated catch block
        }
    }
}

