package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants.API;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.AddCartResponse;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.LoginResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.xtapps.com.dampafeastandroid.Controller.Constants.API.ACTION_LOGIN;

public class CompletePopupActivity extends AppCompatActivity implements onApiFinish {
    TextView tv_price_term,tv_tax_subtotal,tv_vat_amount,tv_vatable_sale,tv_nonvatable_sale,tv_service_tax,tv_del_charge,
            tv_discount_amount,tv_coupon_disc,tv_payable_amount,tv_order_id,tv_guestname,tv_guestcount,tv_solo_count,tv_senior_count,tv_disabled_count;
    EditText ed_coupon_code;
    Spinner spinner1;

    LinearLayout ll_apply_coupon,ll_coupon_disc,ll_solo_parent,ll_del_charge;
    ImageView img_success,img_print;
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    private final int CANCEL_POPUP = 3;
    private final int COMPLETED_POPUP = 2;

    String order_number,settotal,coupon_applied;
    private Context mContext;
    private final String FROM_LOGIN = "login";
    private  AlertDialog.Builder dialogBuilder;
    private AlertDialog alertDialog;
    ImageView  img_right ;
    ImageView  img_wrong;
    LinearLayout linear_two ;
    LinearLayout linear_user_pass ;
    LinearLayout linear_comment ;
    TextView tv_one,tv_two,tv_three,tv_subtotal ;
    TextView tv_login;
    TextView tv_submit ;
    private final String FROM_COUPON = "coupon";
    private final String FROM_SUPERVISOR_LIST = "supervisor_list";
    private ArrayList<String> SupervisorListArray = new ArrayList<String>();
    EditText edt_username,edt_password,edt_comment;
    Spinner spinner,spinner_card;
    String select,selected_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_complete_popup);
        mContext=this;
        sharedpreferencesData = CompletePopupActivity.this.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


        tv_vat_amount = (TextView) findViewById(R.id.tv_vat_amount);
        tv_price_term = (TextView) findViewById(R.id.tv_price_term);
        tv_vatable_sale = (TextView) findViewById(R.id.tv_vatable_amount);
        tv_nonvatable_sale = (TextView) findViewById(R.id.tv_nonvatable_amount);
        tv_service_tax= (TextView) findViewById(R.id.tv_service_tax);
        tv_del_charge= (TextView) findViewById(R.id.tv_del_charge);
        tv_discount_amount = (TextView) findViewById(R.id.tv_discount_amount);
        tv_payable_amount = (TextView) findViewById(R.id.tv_payable_amount);
        tv_order_id = (TextView) findViewById(R.id.tv_order_id);
        tv_guestname = (TextView) findViewById(R.id.tv_guestname);
        tv_guestcount = (TextView) findViewById(R.id.tv_guestcount);
        tv_senior_count = (TextView) findViewById(R.id.tv_senior_count);
        tv_solo_count = (TextView) findViewById(R.id.tv_solo_count);
        tv_disabled_count = (TextView) findViewById(R.id.tv_disabled_count);
        tv_coupon_disc= (TextView) findViewById(R.id.tv_coupon_disc);
        tv_tax_subtotal = (TextView) findViewById(R.id.tv_tax__subtotal);
        ed_coupon_code = (EditText) findViewById(R.id.ed_coupon_code);

        img_success = (ImageView) findViewById(R.id.img_success);
        ll_apply_coupon = (LinearLayout) findViewById(R.id.ll_apply_coupon);
        ll_coupon_disc= (LinearLayout) findViewById(R.id.ll_coupon_disc);
        ll_solo_parent= (LinearLayout) findViewById(R.id.ll_solo_parent);
        ll_del_charge= (LinearLayout) findViewById(R.id.ll_del_charge);

        order_number =getIntent().getStringExtra("order_number");
        settotal =getIntent().getStringExtra("settotal");
        coupon_applied=getIntent().getStringExtra("coupon_applied");

        if(coupon_applied.equals("1"))
        {
            ll_apply_coupon.setVisibility(View.GONE);
            ll_coupon_disc.setVisibility(View.VISIBLE);

        }
        if(AppSession.getInstance().getServiceType().equals("delivery")&&Double.valueOf(AppSession.getInstance().getRestraunt_DeliveryCharge())>0)
        {
            ll_del_charge.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_del_charge.setVisibility(View.GONE);
        }


       /* Float taxforSubtotal=Float.parseFloat(AppSession.getInstance().getSubTotal())*Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())/100;
        Float taxedSubtotal=taxforSubtotal+Float.parseFloat(AppSession.getInstance().getSubTotal());
        tv_tax_subtotal.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", taxedSubtotal));
        Float serviceTax=Float.parseFloat(AppSession.getInstance().getSubTotal())*Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value2())/100;
        tv_service_tax.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", serviceTax));
        tv_discount_amount.setText(AppSession.getInstance().getTax_discount_amount());
        tv_order_id.setText(order_number);*/


        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        editor1.putString("Coupon_code", ed_coupon_code.getText().toString().trim());
        editor1.commit();

        String GuestName=  sharedpreferencesData.getString("GuestName","");
        String no_of_pax=  sharedpreferencesData.getString("dinein_guest_count","");
        String senior= sharedpreferencesData.getString("dinein_edt_senior_guest","");
        String solo=  sharedpreferencesData.getString("dinein_edt_solo_guest","");
        String disabled= sharedpreferencesData.getString("dinein_edt_disabled","");
/*
        Float vatableSales=Float.parseFloat(AppSession.getInstance().getSubTotal())*(Float.parseFloat(no_of_pax)-(Float.parseFloat(senior)+Float.parseFloat(disabled)))/Float.parseFloat(no_of_pax);
*/
        /*Float vatableSales=Float.parseFloat(AppSession.getInstance().getSubTotal());

        Float nonvatableSales= Float.parseFloat(AppSession.getInstance().getSubTotal())-vatableSales;

        tv_vatable_sale.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f",vatableSales));
        tv_nonvatable_sale.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", nonvatableSales));
        Float discount_amount=Float.parseFloat(AppSession.getInstance().getRestraunt_seniorDiscount())*nonvatableSales/100;
        tv_discount_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", discount_amount));
        Float vatAmount=Float.parseFloat(AppSession.getInstance().getRestraunt_tax_value())*vatableSales/100;
        tv_vat_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", vatAmount));
        Float payableTotal=Float.parseFloat(AppSession.getInstance().getSubTotal())-discount_amount+vatAmount+serviceTax;
        tv_payable_amount.setText(String.format("%."+AppSession.getInstance().getRestraunt_DecimalPosition()+"f", payableTotal));
      */
        tv_guestname.setText(GuestName);
        tv_guestcount.setText(no_of_pax);
        tv_senior_count.setText(senior);
        tv_disabled_count.setText(disabled);
        tv_solo_count.setText(solo);
        tv_price_term.setText(AppSession.getInstance().getRestraunt_CurrencySymbol());
        tv_order_id.setText(order_number);

        tv_tax_subtotal.setText(AppSession.getInstance().getSubTotal());
        tv_vatable_sale.setText(AppSession.getInstance().getTax_vatable_sales());
        tv_nonvatable_sale.setText(AppSession.getInstance().getTax_non_vatable_sales());
        tv_discount_amount.setText(AppSession.getInstance().getTax_discount_amount());
        tv_vat_amount.setText(AppSession.getInstance().getTax_vat_amount());
        tv_service_tax.setText(AppSession.getInstance().getTax_service_charge());
        tv_del_charge.setText(AppSession.getInstance().getRestraunt_DeliveryCharge());
        tv_coupon_disc.setText(AppSession.getInstance().getTax_coupon_discount());
        tv_payable_amount.setText(AppSession.getInstance().getTax_payable_amount());




        AppSession.getInstance().settotal(settotal);
        img_success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFinishing()&&mContext!=null)
                {
                    /*Intent i= new Intent(CompletePopupActivity.this,Main_ActivityPrinterLongin.class);
                    AppSession.getInstance().setcompletecart("123");
                    startActivity(i);
                    OrderBill();*/
                    Intent intent=new Intent();
                    setResult(COMPLETED_POPUP,intent);
                    finish();//finishing activity


                }

                        /*Intent i=new Intent(CartActivity.this,Main_Activity.class);
                        startActivity(i);*/

            }
        });

        ll_apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFinishing()&&mContext!=null)
                {
                    supervier_list();

                }

            }
        });




    }

    private void popup() {
        dialogBuilder = new AlertDialog.Builder(CompletePopupActivity.this, R.style.NewDialog);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.prepration_popup, null);

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();

        /* ImageView*/  img_right = (ImageView) dialogView.findViewById(R.id.img_right);
        /* final ImageView */ img_wrong = (ImageView) dialogView.findViewById(R.id.img_wrong);

        /*final LinearLayout*/ linear_two = (LinearLayout) dialogView.findViewById(R.id.linear_two);
        /* final LinearLayout*/ linear_user_pass = (LinearLayout) dialogView.findViewById(R.id.linear_user_pass);
        /* final LinearLayout*/ linear_comment = (LinearLayout) dialogView.findViewById(R.id.linear_comment);

        tv_one = (TextView)dialogView.findViewById(R.id.tv_one);
        tv_two = (TextView)dialogView.findViewById(R.id.tv_two);
        /* final TextView*/ tv_three = (TextView)dialogView.findViewById(R.id.tv_three);
        /* final TextView*/ tv_login = (TextView)dialogView.findViewById(R.id.tv_login);
        /* final TextView*/ tv_submit = (TextView)dialogView.findViewById(R.id.tv_submit);

        edt_username = (EditText)dialogView.findViewById(R.id.edt_username);
        edt_password = (EditText)dialogView.findViewById(R.id.edt_password);
        edt_comment= (EditText)dialogView.findViewById(R.id.edt_comment);

      //  final SearchableSpinner spinner1 = dialogView.findViewById(R.id.spinner);
        spinner1 = dialogView.findViewById(R.id.spinner);
    //    spinner1.setTitle(getResources().getString(R.string.spinner_title));
        String[] types1 = getResources().getStringArray(R.array.select_service);
        final List<String> genderType1 = new ArrayList<>(Arrays.asList(types1));

        /* Set your ArrayAdapter with the StringWithTag, and when each entry is shown in the Spinner, .toString() is called. */
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SupervisorListArray);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position >= 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);




                    /*  Toast.makeText(getApplicationContext(),select, Toast.LENGTH_LONG).show();*/

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        alertDialog.setCancelable(true);
        alertDialog.show();


            tv_one.setText(getResources().getString(R.string.apply_coup_text));
            tv_two.setText("");
            linear_two.setVisibility(View.VISIBLE);
            img_wrong.setVisibility(View.GONE);

        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linear_two.setVisibility(View.VISIBLE);
                img_wrong.setVisibility(View.GONE);
            }
        });

        img_wrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFinishing()&&mContext!=null)
                {
                    superviser_login();
                }

               /* linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.comment));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);*/
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                applyCoupon();


            }


        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    // To dismiss the fragment when the back-button is pressed.
                    alertDialog.hide();
                    return true;
                }


                // Otherwise, do nothing else
                else return false;
            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){


                /****cleanup code****/
            }});
    }

    private void supervier_list() {
        String URL = AppSession.getInstance().getRegUrl()+ API.SUPERVISOR_LIST;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","verifyCoupon");*/
        values.put("action", API.ACTION_SUPERVISOR_LIST);
        values.put("location_id", AppSession.getInstance().getLocation_id());

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_SUPERVISOR_LIST, "POST");
    }


    private void applyCoupon() {

        String URL = AppSession.getInstance().getRegUrl()+ API.APPLY_COUPON;
        HashMap<String, String> values = new HashMap<>();
        /* values.put("username", edt_username.getText().toString());*/
        /* values.put("action","verifyCoupon");*/
        values.put("action", API.ACTION_APPLY_COUPON);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("coupon", edt_comment.getText().toString());
        values.put("subtotal",AppSession.getInstance().getSubTotal());
        values.put("payable_amount",AppSession.getInstance().getTax_payable_amount());
        values.put("order_id",String.valueOf(order_number));

        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_COUPON, "POST");
    }

    private void superviser_login() {
        String URL = AppSession.getInstance().getRegUrl()+ API.LOGIN;
        HashMap<String, String> values = new HashMap<>();
        //values.put("username", edt_username.getText().toString());
         values.put("username", select);
        values.put("password", edt_password.getText().toString());
        /* values.put("action","StaffLogin");*/
        values.put("action",ACTION_LOGIN);
        values.put("tab_token", AppSession.getInstance().getTabToken());
        values.put("location_id", AppSession.getInstance().getLocation_id());
        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(CompletePopupActivity.this, values);
        objCallWebServiceTask.execute(URL, FROM_LOGIN, "POST");
    }

    @Override public void onBackPressed() {
        Intent intent=new Intent();
        setResult(CANCEL_POPUP,intent);
        finish();
       /* super.onBackPressed();*/

    }

    @Override
    public void onSuccess(String tag, String response) {
        if (tag.equals(FROM_LOGIN)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {

               /* AppSession.getInstance().setisLoginStatus(true);
                AppSession.getInstance().setStaff_ID(loginResponse.getStaffInfo().getStaffId().toString());
                AppSession.getInstance().setStaff_Name(loginResponse.getStaffInfo().getName().toString());
                AppSession.getInstance().setStaff_Email(loginResponse.getStaffInfo().getEmail().toString());
                AppSession.getInstance().setUser_Type(loginResponse.getStaffInfo().getUserType().toString());
                AppSession.getInstance().setLocation_id(loginResponse.getStaffInfo().getLocation().toString());*/

                // Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                linear_user_pass.setVisibility(View.GONE);
                linear_comment.setVisibility(View.VISIBLE);
                tv_three.setText(getResources().getString(R.string.apply_coup));
                tv_login.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);
            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag.equals(FROM_COUPON)) {


            Gson gson = new Gson();
            AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);
            if (menuResponse.getStatus().equals("success")) {
                alertDialog.hide();
                Toast.makeText(mContext,menuResponse.getMessage()+menuResponse.getPrev_subtotal(), Toast.LENGTH_SHORT).show();
                // tv_subtotal.setText(menuResponse.getPrev_subtotal());
                ll_coupon_disc.setVisibility(View.VISIBLE);
                tv_coupon_disc.setText(menuResponse.getDis_amount());
                tv_payable_amount.setText(menuResponse.getPayable_amount());
                AppSession.getInstance().setSubTotal(menuResponse.getSubtotal());
                AppSession.getInstance().setTax_payable_amount(menuResponse.getPayable_amount());
               /* DecimalFormat df2 = new DecimalFormat("###.###");
                double f = Double.parseDouble(menuResponse.getSubtotal().toString());
                String ft =df2.format(f);
                Toast.makeText(mContext,ft, Toast.LENGTH_SHORT).show();*/
                AppSession.getInstance().setApplyCoupon("true");
                AppSession.getInstance().setOrderTotal(menuResponse.getPayable_amount());
                AppSession.getInstance().settotal(menuResponse.getPrev_subtotal());
                AppSession.getInstance().setCouponTotal(menuResponse.getDis_amount());
                AppSession.getInstance().setCouponper(menuResponse.getDis_percent());
                //  Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }

            else {
                Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
        if (tag.equals(FROM_SUPERVISOR_LIST)) {

            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if (loginResponse.getStatus().equals("success")) {
                SupervisorListArray.clear();
                for(int i=0;i<loginResponse.getData().size();i++)
                {
                    SupervisorListArray.add(loginResponse.getData().get(i).getUsername());

                }
                if(!isFinishing()&&mContext!=null)
                {
                    popup();
                }

            }

            else {
                Toast.makeText(mContext,loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        AddCartResponse menuResponse = gson.fromJson(response, AddCartResponse.class);

        Toast.makeText(mContext,menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onJsonError(String tag, String response) {

        Toast.makeText(mContext,getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {

        Toast.makeText(mContext,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }
}
