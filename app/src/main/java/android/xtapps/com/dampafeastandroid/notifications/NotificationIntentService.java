package android.xtapps.com.dampafeastandroid.notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity;
import android.xtapps.com.dampafeastandroid.broadcast_receivers.NotificationEventReceiver;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ARRAY;


/**
 * Created by klogi
 *
 *
 */
public class NotificationIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";
    public onApiFinish mOnApiFinish;
    public   Context mContext;
    public String text;
    public String FROM_NOTIFICATION = "notification";
    private String result = "";
    private String messageResult;
    private String message;

    public NotificationIntentService() {
        super(NotificationIntentService.class.getSimpleName());

    }

    public static Intent createIntentStartNotificationService(Context context) {

        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(getClass().getSimpleName(), "onHandleIntent, started handling a notification event");
        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {

               /* loadOrderData();
                initi();*/
                new loadOrderData().execute("0");
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void processDeleteNotification(Intent intent) {
        // Log something?
    }

    public   void processStartNotification(String text) {
        // Do something. For example, fetch fresh data from backend to create a rich notification?

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.dampafeast_logo);
            builder.setColor(getResources().getColor(R.color.trans));
        } else {
            builder.setSmallIcon(R.drawable.dampafeast_logo);
        }
        builder.setContentTitle("Scheduled Notification")
                .setAutoCancel(true)
                .setContentText(text)
                /*.setSmallIcon(R.drawable.dampafeast_logo)*/;
        //Vibration
        builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

        //LED
        builder.setLights(Color.RED, 3000, 3000);

        //Ton
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);

        Intent mainIntent = new Intent(this, NavigationMainActivity.class);
       /* AppSession.getInstance().setnoti("123");*/
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID,
                mainIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(NotificationEventReceiver.getDeleteIntent(this));

        final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, builder.build());
    }

    class loadOrderData extends AsyncTask<String, String, Void> {

        InputStream is = null;


        @Override
        protected Void doInBackground(String... params) {

            //Log.i("-------------user name-------------password-------------",Username+"----------"+Password);
            String url_select = AppSession.getInstance().getRegUrl()+ Constants.API.NOTIFICATIONAPI;;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);
            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();


           /* param.add(new BasicNameValuePair("action", "TableNotification"));*/
            param.add(new BasicNameValuePair("action", Constants.API.ACTION_NOTIFICATIONAPI));
            param.add(new BasicNameValuePair("tab_token",AppSession.getInstance().getTabToken()));
            param.add(new BasicNameValuePair("location_id",AppSession.getInstance().getLocation_id()));
            param.add(new BasicNameValuePair("staff_id", AppSession.getInstance().getStaff_ID()));

            HttpResponse response = null;
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (Exception e) {


            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.i("-------result-----1--", result);

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("log_tag", "Error converting result " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(Void v) {
            // ambil data dari Json database
            try {




                JSONObject myJson = new JSONObject(result);
                messageResult=myJson.getString("status");
                message=myJson.getString("message");



                if(messageResult.equals("success")&&message.equals("Table Notification")){
                    text = myJson.getString("notification");
                 /*   JSONArray jArray = new JSONArray(text);
                    if (jArray.length()==0)
                    {

                    }
                    else
                    {
                        for(int i=0;i<jArray.length();i++){

                            JSONObject json_data = jArray.getJSONObject(i);

                            String text1 = json_data.getString("text");
                            String table_id = json_data.getString("id");
                            String menu_id = json_data.getString("menu_id");
                            String order_id = json_data.getString("order_id");
                            String updated_at = json_data.getString("updated_at");
                            map1 = new HashMap<String, String>();

                            map1.put(LOCAION_ID, table_id);
                            map1.put(LOCAION_NAME, text1);
                            map1.put(LOCAION_ADD1, menu_id);
                            map1.put(LOCAION_ADD2, order_id);
                            map1.put(LOCAION_IMAGE, updated_at);

                            categoryList.add(map1);
                        }
                    }*/


                    //  processStartNotification(text);
                    Intent in = new Intent(NavigationMainActivity.notification_update_UI);
                    Bundle extras = new Bundle();
                    extras.putString(NOTI_ARRAY, text);
                    in.putExtras(extras);


                    // in.putExtra(LOCAION_ARRAY, categoryList);
                    getApplicationContext().sendBroadcast(in);
                }
                else{





                }





            } catch (Exception e) {
                // TODO: handle exception


                // Signed out, show unauthenticated UI.

                //Toast.makeText(AccessCodeActivity.this, "Please check your internet connection",
                //  Toast.LENGTH_LONG).show();
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
        }
    }


}