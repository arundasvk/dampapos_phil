package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetails {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mobile_number")
    @Expose
    private String mobile_number;

    @SerializedName("no_of_pax")
    @Expose
    private String no_of_pax;

    @SerializedName("senior_citizen")
    @Expose
    private String senior_citizen;

    @SerializedName("solo_parent")
    @Expose
    private String solo_parent;

    @SerializedName("disabled")
    @Expose
    private String disabled;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("address1")
    @Expose
    private String address1;

    @SerializedName("address2")
    @Expose
    private String address2;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("area")
    @Expose
    private String area;

    @SerializedName("city")
    @Expose
    private String city;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getNo_of_pax() {
        return no_of_pax;
    }

    public void setNo_of_pax(String no_of_pax) {
        this.no_of_pax = no_of_pax;
    }

    public String getSenior_citizen() {
        return senior_citizen;
    }

    public void setSenior_citizen(String senior_citizen) {
        this.senior_citizen = senior_citizen;
    }

    public String getSolo_parent() {
        return solo_parent;
    }

    public void setSolo_parent(String solo_parent) {
        this.solo_parent = solo_parent;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}