package android.xtapps.com.dampafeastandroid.Controller;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

/**
 * Created by USER on 7/17/2018.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();



    private static AppController mInstance;
    public static volatile Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        mInstance = this;
        AppSession.initializeInstance(this);
        //appEnvironment = AppEnvironment.SANDBOX;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }




}
