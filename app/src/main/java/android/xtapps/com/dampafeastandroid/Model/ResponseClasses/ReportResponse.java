package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("collection")
    @Expose
    private String collection;

    @SerializedName("report_type")
    @Expose
    private String report_type;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("order_from")
    @Expose
    private String order_from;

    @SerializedName("orders_count")
    @Expose
    private String orders_count;

    @SerializedName("cash_short")
    @Expose
    private String cash_short;

    @SerializedName("total_sales")
    @Expose
    private String total_sales;

    @SerializedName("gross_amount")
    @Expose
    private String gross_amount;

    @SerializedName("net_discount")
    @Expose
    private String net_discount;

    @SerializedName("payment_received")
    @Expose
    private String payment_received;

    @SerializedName("tender_change")
    @Expose
    private String tender_change;

    @SerializedName("net_amount")
    @Expose
    private String net_amount;

    @SerializedName("cash_collected_by_card")
    @Expose
    private String cash_collected_by_card;

    @SerializedName("cash_collected_as_cash")
    @Expose
    private String cash_collected_as_cash;

    @SerializedName("cash_collected_by_talabat")
    @Expose
    private String cash_collected_by_talabat;

    @SerializedName("cash_collected_by_makan")
    @Expose
    private String cash_collected_by_makan;

    @SerializedName("cash_collected_by_giftcoupon")
    @Expose
    private String cash_collected_by_giftcoupon;

    @SerializedName("total_dine_in_orders")
    @Expose
    private String total_dine_in_orders;

    @SerializedName("total_delivery_orders")
    @Expose
    private String total_delivery_orders;

    @SerializedName("total_take_away_orders")
    @Expose
    private String total_take_away_orders;

    @SerializedName("total_cancelled_orders")
    @Expose
    private String total_cancelled_orders;

    @SerializedName("order")
    @Expose
    private ArrayList<OrderList> order;

    @SerializedName("waiters_orders")
    @Expose
    private ArrayList<waiters_ordersList> waiters_orders;

    @SerializedName("data")
    @Expose
    public data data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrder_from() {
        return order_from;
    }

    public void setOrder_from(String order_from) {
        this.order_from = order_from;
    }

    public String getOrders_count() {
        return orders_count;
    }

    public void setOrders_count(String orders_count) {
        this.orders_count = orders_count;
    }

    public String getCash_collected_by_card() {
        return cash_collected_by_card;
    }

    public void setCash_collected_by_card(String cash_collected_by_card) {
        this.cash_collected_by_card = cash_collected_by_card;
    }

    public String getCash_collected_as_cash() {
        return cash_collected_as_cash;
    }

    public void setCash_collected_as_cash(String cash_collected_as_cash) {
        this.cash_collected_as_cash = cash_collected_as_cash;
    }

    public String getTotal_dine_in_orders() {
        return total_dine_in_orders;
    }

    public void setTotal_dine_in_orders(String total_dine_in_orders) {
        this.total_dine_in_orders = total_dine_in_orders;
    }

    public String getTotal_delivery_orders() {
        return total_delivery_orders;
    }

    public void setTotal_delivery_orders(String total_delivery_orders) {
        this.total_delivery_orders = total_delivery_orders;
    }

    public String getTotal_take_away_orders() {
        return total_take_away_orders;
    }

    public void setTotal_take_away_orders(String total_take_away_orders) {
        this.total_take_away_orders = total_take_away_orders;
    }

    public String getTotal_cancelled_orders() {
        return total_cancelled_orders;
    }

    public void setTotal_cancelled_orders(String total_cancelled_orders) {
        this.total_cancelled_orders = total_cancelled_orders;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public ArrayList<OrderList> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<OrderList> order) {
        this.order = order;
    }

    public ReportResponse.data getData() {
        return data;
    }

    public void setData(ReportResponse.data data) {
        this.data = data;
    }

    public ArrayList<waiters_ordersList> getWaiters_orders() {
        return waiters_orders;
    }

    public void setWaiters_orders(ArrayList<waiters_ordersList> waiters_orders) {
        this.waiters_orders = waiters_orders;
    }

    public String getCash_short() {
        return cash_short;
    }

    public void setCash_short(String cash_short) {
        this.cash_short = cash_short;
    }

    public String getTotal_sales() {
        return total_sales;
    }

    public void setTotal_sales(String total_sales) {
        this.total_sales = total_sales;
    }

    public String getGross_amount() {
        return gross_amount;
    }

    public void setGross_amount(String gross_amount) {
        this.gross_amount = gross_amount;
    }

    public String getNet_discount() {
        return net_discount;
    }

    public void setNet_discount(String net_discount) {
        this.net_discount = net_discount;
    }

    public String getPayment_received() {
        return payment_received;
    }

    public void setPayment_received(String payment_received) {
        this.payment_received = payment_received;
    }

    public String getTender_change() {
        return tender_change;
    }

    public void setTender_change(String tender_change) {
        this.tender_change = tender_change;
    }

    public String getNet_amount() {
        return net_amount;
    }

    public void setNet_amount(String net_amount) {
        this.net_amount = net_amount;
    }

    public String getCash_collected_by_talabat() {
        return cash_collected_by_talabat;
    }

    public void setCash_collected_by_talabat(String cash_collected_by_talabat) {
        this.cash_collected_by_talabat = cash_collected_by_talabat;
    }

    public String getCash_collected_by_makan() {
        return cash_collected_by_makan;
    }

    public void setCash_collected_by_makan(String cash_collected_by_makan) {
        this.cash_collected_by_makan = cash_collected_by_makan;
    }

    public String getCash_collected_by_giftcoupon() {
        return cash_collected_by_giftcoupon;
    }

    public void setCash_collected_by_giftcoupon(String cash_collected_by_giftcoupon) {
        this.cash_collected_by_giftcoupon = cash_collected_by_giftcoupon;
    }

    public   class data
    {
        @SerializedName("collection")
        @Expose
        private String collection;


        @SerializedName("closed_by")
        @Expose
        private String closed_by;

        @SerializedName("tender_cash")
        @Expose
        private String tender_cash;


        @SerializedName("tender_change")
        @Expose
        private String tender_change;

        @SerializedName("orders_count")
        @Expose
        private String orders_count;


        @SerializedName("CASH")
        @Expose
        private String CASH;

        @SerializedName("CARD")
        @Expose
        private String CARD;


        @SerializedName("BOTH")
        @Expose
        private String BOTH;

        @SerializedName("TALABAT")
        @Expose
        private String TALABAT;

        @SerializedName("GIFTCOUPON")
        @Expose
        private String GIFTCOUPON;



        public String getCollection() {
            return collection;
        }

        public void setCollection(String collection) {
            this.collection = collection;
        }

        public String getClosed_by() {
            return closed_by;
        }

        public void setClosed_by(String closed_by) {
            this.closed_by = closed_by;
        }

        public String getTender_cash() {
            return tender_cash;
        }

        public void setTender_cash(String tender_cash) {
            this.tender_cash = tender_cash;
        }

        public String getTender_change() {
            return tender_change;
        }

        public void setTender_change(String tender_change) {
            this.tender_change = tender_change;
        }

        public String getOrders_count() {
            return orders_count;
        }

        public void setOrders_count(String orders_count) {
            this.orders_count = orders_count;
        }

        public String getCASH() {
            return CASH;
        }

        public void setCASH(String CASH) {
            this.CASH = CASH;
        }

        public String getCARD() {
            return CARD;
        }

        public void setCARD(String CARD) {
            this.CARD = CARD;
        }

        public String getBOTH() {
            return BOTH;
        }

        public void setBOTH(String BOTH) {
            this.BOTH = BOTH;
        }

        public String getTALABAT() {
            return TALABAT;
        }

        public void setTALABAT(String TALABAT) {
            this.TALABAT = TALABAT;
        }

        public String getGIFTCOUPON() {
            return GIFTCOUPON;
        }

        public void setGIFTCOUPON(String GIFTCOUPON) {
            this.GIFTCOUPON = GIFTCOUPON;
        }
    }
    public class waiters_ordersList {

        @SerializedName("staff_name")
        @Expose
        private String staff_name;

        @SerializedName("waiter_orders_count")
        @Expose
        private String waiter_orders_count;

        @SerializedName("cash_collected")
        @Expose
        private String cash_collected;

        public String getStaff_name() {
            return staff_name;
        }

        public void setStaff_name(String staff_name) {
            this.staff_name = staff_name;
        }

        public String getWaiter_orders_count() {
            return waiter_orders_count;
        }

        public void setWaiter_orders_count(String waiter_orders_count) {
            this.waiter_orders_count = waiter_orders_count;
        }

        public String getCash_collected() {
            return cash_collected;
        }

        public void setCash_collected(String cash_collected) {
            this.cash_collected = cash_collected;
        }
    }



}
