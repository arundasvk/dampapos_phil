package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class KitchenOrderListResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("current_page")
    @Expose
    private String current_page;

    @SerializedName("total_pages")
    @Expose
    private String total_pages;

    @SerializedName("orders")
    @Expose
    private ArrayList<Kitchenorder> orderItemsLists;

    @SerializedName("completed_orders")
    @Expose
    private ArrayList<Kitchenorder> CompletedOrders;


    @SerializedName("completed_menuitems_count")
    @Expose
    private String completed_menuitems_count;

    @SerializedName("pending_menuitems_count")
    @Expose
    private String pending_menuitems_count;


    @SerializedName("pending_menu_count")
    @Expose
    private String pending_menu_count;

    @SerializedName("pending_menu_type")
    @Expose
    private String pending_menu_type;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("orders_count")
    @Expose
    private String orders_count;




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Kitchenorder> getOrderItemsLists() {
        return orderItemsLists;
    }

    public void setOrderItemsLists(ArrayList<Kitchenorder> orderItemsLists) {
        this.orderItemsLists = orderItemsLists;
    }

    public String getCompleted_menuitems_count() {
        return completed_menuitems_count;
    }

    public void setCompleted_menuitems_count(String completed_menuitems_count) {
        this.completed_menuitems_count = completed_menuitems_count;
    }

    public String getPending_menuitems_count() {
        return pending_menuitems_count;
    }

    public void setPending_menuitems_count(String pending_menuitems_count) {
        this.pending_menuitems_count = pending_menuitems_count;
    }

    public String getPending_menu_count() {
        return pending_menu_count;
    }

    public void setPending_menu_count(String pending_menu_count) {
        this.pending_menu_count = pending_menu_count;
    }

    public String getPending_menu_type() {
        return pending_menu_type;
    }

    public void setPending_menu_type(String pending_menu_type) {
        this.pending_menu_type = pending_menu_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrders_count() {
        return orders_count;
    }

    public void setOrders_count(String orders_count) {
        this.orders_count = orders_count;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(String total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<Kitchenorder> getCompletedOrders() {
        return CompletedOrders;
    }

    public void setCompletedOrders(ArrayList<Kitchenorder> completedOrders) {
        CompletedOrders = completedOrders;
    }
}
