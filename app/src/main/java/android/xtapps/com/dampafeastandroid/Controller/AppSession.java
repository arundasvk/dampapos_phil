package android.xtapps.com.dampafeastandroid.Controller;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by USER on 7/17/2018.
 */

public class AppSession {

    private static final String PREF_NAME = "pumpkin";
    private static AppSession sInstance;
    private final SharedPreferences mPref;
    public static String FIRST_TIME_LOADER = "first_time_loader";
    public static String FIRST_TIME_USER = "first_time_user";
    public static String TAB_TOKEN="tab_token";
    public static String Android_id="android_id";
    public static String Location_id="location_id";
    public static String Location_name="location_name";
    public static String Location_add1="location_add1";
    public static String Location_add2="location_add2";
    public static String Location_image="location_image";

    public static String TAB_PROFILE="tab_profile";

    public static String TAB_P="tab_p";

    public static String REG_URL="reg_url";

    public static String BitmapFlag="bitmap_flag";

    public static String Restraunt_Name="restaurant_name";
    public static String Restraunt_Address="restaurant_address";
    public static String Restraunt_City="restaurant_city";
    public static String Restraunt_Telephone="restaurant_telephone";
    public static String Restraunt_Country="restaurant_country";
    public static String Restraunt_Currency="restaurant_currency";
    public static String Restraunt_CurrencyDecimal="currency_decimal";
    public static String Restraunt_Currency_Symbol="restaurant_currency_symbol";

    public static String Restraunt_DeliveryCharge="restaurant_delivery_charge";

    public static String Restraunt_Decimal_Format="restaurant_decimal_format";
    public static String Restraunt_Decimal_Zero="restaurant_decimal_zero";

    public static String Restraunt_tax_number="restaurant_tax_number";
    public static String Restraunt_tax_name="restaurant_tax_name";
    public static String Restraunt_tax_type="restaurant_tax_type";
    public static String Restraunt_tax_value="restaurant_tax_value";

    public static String Restraunt_tax_number2="restaurant_tax_number2";
    public static String Restraunt_tax_name2="restaurant_tax_name2";
    public static String Restraunt_tax_type2="restaurant_tax_type2";
    public static String Restraunt_tax_value2="restaurant_tax_value2";


    public static String Senior_discount="senior_discount";
    public static String Disabled_discount="disabled_discount";
    public static String Solo_discount="solo_discount";

    public static String Staff_ID="staff_id";
    public static String Staff_Name="staff_name";
    public static String User_Name="user_name";
    public static String Staff_Email="staff_email";
    public static String User_Type="user_type";
    public static String Expired_Flag ="expired_flag";

    public static String TABLE_ID ="table_flag";
    public static String TABLE_ChangeFLAG="table_change_flag";
    public static String TABLE_CONTINUE_ORDER="continue_order";
    public static String SERVICE_TYPE="service_type";
    public static String ORDER_NUMBER ="order_no";
    public static String Guest_Count ="guest_count";
    public static String ModifierFlag ="modifier_flag";
    public static String DiscountFlag ="discount_flag";

    public static String CartPlusFlag ="cart_plus_flag";

    public static String Tick ="tick";

    public static String menuname ="menuname";
    public static String menuname2 ="menuname2";
    public static String menuprice ="menuprice";
    public static String Menuprinter ="Menuprinter";
    public static String menuquantity ="menuquantity";
    public static String menuid ="menuid";
    public static String ordermenuid ="ordermenuid";

    public static String Total="total";
    public static String SubTotal="subtotal";
    public static String CouponTotal="cupontotal";
    public static String Couponper="cuponper";
    public static String Cancelcart="cancelcart";
    public static String completecart="completecart";
    public static String bluetoothdevicename="bluetoothdevicename";

    public static String tablename="tablename";

    public static String order_idTake_delivery="order/take_delivey";
    public static String order_id_delivery="order_delivery";

    public static String ORDER_TYPE="order_type";
    public static String Printericon="Printericon";
    public static String ApplyCoupon="coupon";
    public static String COMPLETEBILLDETAILS="completebilldetails";
    public static String order_status="order_status";
    public static String OrderTotal="orderTotal";

    public static String Tax_vat_amount="vat_amount";
    public static String Tax_tax_exemption="tax_exemption";
    public static String Tax_discount_amount="discount_amount";
    public static String Tax_service_charge="service_charge";
    public static String Tax_payable_amount="payable_amount";
    public static String payable_amount_afterCoupon="after_coupon";
    public static String Tax_coupon_discount="coupon_discount";
    public static String Tax_vatable_sales="vatable_sales";
    public static String Tax_non_vatable_sales="non_vatable_sales";

    public static String Invoice_No="invoice_no";
    public static String Floor="floor";

    public static String Prev_size="prev_size";
    public static String kitchen_newflag="kitchen_newflag";
    public static String continue_position="position";

    public static String completeOrderList="complete_order_list";

    public static String UpdatedFlag_sssss="updatedflgssss";




    public AppSession(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppSession(context);
        }
    }

    public static synchronized AppSession getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(
                    AppSession.class.getSimpleName()
                            + " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }
    public void setisFirstTimeLoader(boolean firstTimeUser) {
        mPref.edit().putBoolean(FIRST_TIME_LOADER, firstTimeUser).commit();
    }



    public boolean getisFirstTimeLoader() {
        return mPref.getBoolean(FIRST_TIME_LOADER, false);
    }

    public void setisLoginStatus(boolean firstTimeUser) {
        mPref.edit().putBoolean(FIRST_TIME_USER, firstTimeUser).commit();
    }

    public boolean getisLoginStatus() {
        return mPref.getBoolean(FIRST_TIME_USER, false);
    }

    public void setBitmapFlag(boolean firstTimeUser) {
        mPref.edit().putBoolean(BitmapFlag, firstTimeUser).commit();
    }

    public boolean getBitmapFlag() {
        return mPref.getBoolean(BitmapFlag, false);
    }


    public void setTabToken(String userId) {
        mPref.edit().putString(TAB_TOKEN, userId).commit();

    }
    public String getTabToken() {
        return mPref.getString(TAB_TOKEN, "");
    }

    public void setStaff_ID(String userId) {
        mPref.edit().putString(Staff_ID, userId).commit();

    }
    public String getStaff_ID() {
        return mPref.getString(Staff_ID, "");
    }

    public void setTable_ID(String tableflag) {
        mPref.edit().putString(TABLE_ID, tableflag).commit();

    }
    public String getTable_ID() {
        return mPref.getString(TABLE_ID, "");
    }



    public void setTable_Name(String tableflag) {
        mPref.edit().putString(tablename, tableflag).commit();

    }
    public String getTable_Name() {
        return mPref.getString(tablename, "");
    }




    public void setServiceType(String service_type) {
        mPref.edit().putString(SERVICE_TYPE, service_type).commit();

    }
    public String getServiceType() {
        return mPref.getString(SERVICE_TYPE, "");
    }
    public void setOrderType(String service_type) {
        mPref.edit().putString(ORDER_TYPE, service_type).commit();

    }
    public String getOrderType() {
        return mPref.getString(ORDER_TYPE, "");
    }

    public void setTABLE_ChangeFLAG(String table_change) {
        mPref.edit().putString(TABLE_ChangeFLAG, table_change).commit();

    }
    public String getTABLE_ChangeFLAG() {
        return mPref.getString(TABLE_ChangeFLAG, "");
    }

    public void setAndroid_id(String android_id) {
        mPref.edit().putString(Android_id, android_id).commit();

    }
    public String getAndroid_id() {
        return mPref.getString(Android_id, "");
    }

    public void setLocation_id(String location_id) {
        mPref.edit().putString(Location_id, location_id).commit();

    }
    public String getLocation_id() {
        return mPref.getString(Location_id, "");
    }

    public void setStaff_Name(String staff_name) {
        mPref.edit().putString(Staff_Name, staff_name).commit();

    }
    public String getStaff_Name() {
        return mPref.getString(Staff_Name, "");
    }

    public void setUser_Name(String staff_name) {
        mPref.edit().putString(User_Name, staff_name).commit();

    }
    public String getUser_Name() {
        return mPref.getString(User_Name, "");
    }

    public void setStaff_Email(String staff_email) {
        mPref.edit().putString(Staff_Email, staff_email).commit();

    }
    public String getStaff_Email() {
        return mPref.getString(Staff_Email, "");
    }

    public void setUser_Type(String user_type) {
        mPref.edit().putString(User_Type, user_type).commit();

    }
    public String getUser_Type() {
        return mPref.getString(User_Type, "");
    }

    public void setRestraunt_Name(String restraunt_name) {
        mPref.edit().putString(Restraunt_Name, restraunt_name).commit();

    }
    public String getRestraunt_Name() {
        return mPref.getString(Restraunt_Name, "");
    }

    public void setRestraunt_Address(String restraunt_address) {
        mPref.edit().putString(Restraunt_Address, restraunt_address).commit();

    }
    public String getRestraunt_Address() {
        return mPref.getString(Restraunt_Address, "");
    }

    public void setRestraunt_City(String restraunt_city) {
        mPref.edit().putString(Restraunt_City, restraunt_city).commit();

    }
    public String getRestraunt_City() {
        return mPref.getString(Restraunt_City, "");
    }

    public void setRestraunt_Telephone(String restraunt_telephone) {
        mPref.edit().putString(Restraunt_Telephone, restraunt_telephone).commit();

    }
    public String getRestraunt_Telephone() {
        return mPref.getString(Restraunt_Telephone, "");
    }

    public void setRestraunt_Country(String restraunt_country) {
        mPref.edit().putString(Restraunt_Country, restraunt_country).commit();

    }
    public String getRestraunt_Country() {
        return mPref.getString(Restraunt_Country, "");
    }

    public void setRestraunt_CurrencySymbol(String restraunt_currencySymbol) {
        mPref.edit().putString(Restraunt_Currency_Symbol, restraunt_currencySymbol).commit();

    }
    public String getRestraunt_CurrencySymbol() {
        return mPref.getString(Restraunt_Currency, "");
    }

    public void setRestraunt_Decimal_Format(String restraunt_currencySymbol) {
        mPref.edit().putString(Restraunt_Decimal_Format, restraunt_currencySymbol).commit();

    }
    public String getRestraunt_Decimal_Format() {
        return mPref.getString(Restraunt_Decimal_Format, "");
    }

    public void setRestraunt_Decimal_Zero(String restraunt_currencySymbol) {
        mPref.edit().putString(Restraunt_Decimal_Zero, restraunt_currencySymbol).commit();

    }
    public String getRestraunt_Decimal_Zero() {
        return mPref.getString(Restraunt_Decimal_Zero, "");
    }

    public String getRestraunt_DecimalPosition() {
        return mPref.getString(Restraunt_CurrencyDecimal, "");
    }

    public void setRestraunt_DecimalPosition(String restraunt_currencydecimal) {
        mPref.edit().putString(Restraunt_CurrencyDecimal, restraunt_currencydecimal).commit();

    }

    public void setRestraunt_Currency(String restraunt_currency) {
        mPref.edit().putString(Restraunt_Currency, restraunt_currency).commit();

    }

    public String getRestraunt_Currency() {
        return mPref.getString(Restraunt_Currency, "");
    }

    public void setRestraunt_tax_number(String restraunt_tax_number) {
        mPref.edit().putString(Restraunt_tax_number, restraunt_tax_number).commit();

    }
    public String getRestraunt_tax_number() {
        return mPref.getString(Restraunt_tax_number, "");
    }

    public void setRestraunt_tax_name(String restraunt_tax_name) {
        mPref.edit().putString(Restraunt_tax_name, restraunt_tax_name).commit();

    }
    public String getRestraunt_tax_name() {
        return mPref.getString(Restraunt_tax_name, "");
    }

    public void setRestraunt_tax_type(String restraunt_tax_type) {
        mPref.edit().putString(Restraunt_tax_type, restraunt_tax_type).commit();

    }
    public String getRestraunt_tax_type() {
        return mPref.getString(Restraunt_tax_type, "");
    }

    public void setRestraunt_tax_value(String restraunt_tax_value) {
        mPref.edit().putString(Restraunt_tax_value, restraunt_tax_value).commit();

    }
    public String getRestraunt_tax_value() {
        return mPref.getString(Restraunt_tax_value, "");
    }



    public void setRestraunt_tax_number2(String restraunt_tax_number2) {
        mPref.edit().putString(Restraunt_tax_number2, restraunt_tax_number2).commit();

    }
    public String getRestraunt_tax_number2() {
        return mPref.getString(Restraunt_tax_number2, "");
    }

    public void setRestraunt_tax_name2(String restraunt_tax_name2) {
        mPref.edit().putString(Restraunt_tax_name2, restraunt_tax_name2).commit();

    }
    public String getRestraunt_tax_name2() {
        return mPref.getString(Restraunt_tax_name2, "");
    }

    public void setRestraunt_tax_type2(String restraunt_tax_type2) {
        mPref.edit().putString(Restraunt_tax_type2, restraunt_tax_type2).commit();

    }
    public String getRestraunt_tax_type2() {
        return mPref.getString(Restraunt_tax_type2, "");
    }

    public void setRestraunt_tax_value2(String restraunt_tax_value2) {
        mPref.edit().putString(Restraunt_tax_value2, restraunt_tax_value2).commit();

    }
    public String getRestraunt_tax_value2() {
        return mPref.getString(Restraunt_tax_value2, "");
    }

    public void setRestraunt_seniorDiscount(String senior_discount) {
        mPref.edit().putString(Senior_discount, senior_discount).commit();

    }
    public String getRestraunt_seniorDiscount() {
        return mPref.getString(Senior_discount, "");
    }
    public void setRestraunt_soloDiscount(String solo_discount) {
        mPref.edit().putString(Solo_discount, solo_discount).commit();

    }
    public String getRestraunt_soloDiscount() {
        return mPref.getString(Solo_discount, "");
    }
    public void setRestraunt_disabledDiscount(String disabled_discount) {
        mPref.edit().putString(Disabled_discount, disabled_discount).commit();

    }
    public String getRestraunt_disabledDiscount() {
        return mPref.getString(Disabled_discount, "");
    }



    public void setTabProfile(String tabProfile) {
        mPref.edit().putString(TAB_PROFILE, tabProfile).commit();

    }
    public String getTabProfile() {
        return mPref.getString(TAB_PROFILE, "");
    }

    public void setOrderNumber(String orderNumber) {
        mPref.edit().putString(ORDER_NUMBER, orderNumber).commit();

    }
    public String getOrderNumber() {
        return mPref.getString(ORDER_NUMBER, "");
    }

    public void setGuest_Count(String guest_count) {
        mPref.edit().putString(Guest_Count, guest_count).commit();

    }
    public String getGuest_Count() {
        return mPref.getString(Guest_Count, "");
    }

    public void setRegUrl(String guest_count) {
        mPref.edit().putString(REG_URL, guest_count).commit();

    }
    public String getRegUrl() {
        return mPref.getString(REG_URL, "");
    }

    public void setTabP(String guest_count) {
        mPref.edit().putString(TAB_P, guest_count).commit();

    }
    public String getTabP() {
        return mPref.getString(TAB_P, "");
    }

    public void setModifierFlag(String guest_count) {
        mPref.edit().putString(ModifierFlag, guest_count).commit();

    }
    public String getModifierFlag() {
        return mPref.getString(ModifierFlag, "");
    }

    public void setDiscountFlag(boolean guest_count) {
        mPref.edit().putBoolean(DiscountFlag, guest_count).commit();

    }
    public boolean getDiscountFlag() {
        return mPref.getBoolean(DiscountFlag, false);
    }

    public void setTableContinueOrder(String continueOrder) {
        mPref.edit().putString(TABLE_CONTINUE_ORDER, continueOrder).commit();

    }
    public String getTableContinueOrder() {
        return mPref.getString(TABLE_CONTINUE_ORDER, "");
    }
    public void setTick(String tick) {
        mPref.edit().putString(Tick, tick).commit();

    }
    public String getTick() {
        return mPref.getString(Tick, "");
    }

    public void settotal(String continueOrder) {
        mPref.edit().putString(Total,continueOrder).commit();

    }
    public String gettotal() {
        return mPref.getString(Total, "");
    }

    public void setSubTotal(String continueOrder) {
        mPref.edit().putString(SubTotal,continueOrder).commit();

    }
    public String getSubTotal() {
        return mPref.getString(SubTotal, "");
    }

    public void setCouponTotal(String continueOrder) {
        mPref.edit().putString(CouponTotal,continueOrder).commit();

    }
    public String getCouponTotal() {
        return mPref.getString(CouponTotal, "");
    }



    public void setCouponper(String continueOrder) {
        mPref.edit().putString(Couponper,continueOrder).commit();

    }
    public String getCouponper() {
        return mPref.getString(Couponper, "");
    }





    public void setCancelcart(String continueOrder) {
        mPref.edit().putString(Cancelcart,continueOrder).commit();

    }
    public String getCancelcart() {
        return mPref.getString(Cancelcart, "");
    }


    public void setcompletecart(String continueOrder) {
        mPref.edit().putString(completecart,continueOrder).commit();

    }
    public String getcompletecart() {
        return mPref.getString(completecart, "");
    }




    public void setLocationbill_Name(String location_id) {
        mPref.edit().putString(Location_name, location_id).commit();

    }
    public String getLocationbill_Name() {
        return mPref.getString(Location_name, "");
    }

    public void setLocationbill_adress1(String location_id) {
        mPref.edit().putString(Location_add1, location_id).commit();

    }
    public String getLocationbill_adress1() {
        return mPref.getString(Location_add1, "");
    }

    public void setLocationbill_adress2(String location_id) {
        mPref.edit().putString(Location_add2, location_id).commit();

    }
    public String getLocationbill_adress2() {
        return mPref.getString(Location_add2, "");
    }


    public void setLocationbill_image(String location_id) {
        mPref.edit().putString(Location_image, location_id).commit();

    }
    public String getLocationbill_image() {
        return mPref.getString(Location_image, "");
    }






    public void setMenuquantity(String location_id) {
        mPref.edit().putString(menuquantity, location_id).commit();

    }
    public String getMenuquantity() {
        return mPref.getString(menuquantity, "");
    }

    public void setMenuPrice(String location_id) {
        mPref.edit().putString(menuprice, location_id).commit();

    }
    public String getMenuPrice() {
        return mPref.getString(menuprice, "");
    }

    public void setMenuname(String location_id) {
        mPref.edit().putString(menuname, location_id).commit();

    }
    public String getMenuname() {
        return mPref.getString(menuname, "");
    }

    public void setMenuid(String location_id) {
        mPref.edit().putString(menuid, location_id).commit();

    }
    public String getMenuid() {
        return mPref.getString(menuid, "");
    }
    public void setOrdermenuid(String location_id) {
        mPref.edit().putString(ordermenuid, location_id).commit();

    }
    public String getOrdermenuid() {
        return mPref.getString(ordermenuid, "");
    }

    public void setMenuname2(String location_id) {
        mPref.edit().putString(menuname2, location_id).commit();

    }
    public String getMenuname2() {
        return mPref.getString(menuname2, "");
    }



    public void setMenuprinter(String location_id) {
        mPref.edit().putString(Menuprinter, location_id).commit();

    }
    public String getMenuprinter() {
        return mPref.getString(Menuprinter, "");
    }


    public void setBluetoothdevicename(String location_id) {
        mPref.edit().putString(bluetoothdevicename, location_id).commit();

    }
    public String getBluetoothdevicename() {
        return mPref.getString(bluetoothdevicename, "");
    }

    public void setPrintericon(String location_id) {
        mPref.edit().putString(Printericon, location_id).commit();

    }
    public String getPrintericon() {
        return mPref.getString(Printericon, "");
    }

    public void setApplyCoupon(String coupon) {
        mPref.edit().putString(ApplyCoupon, coupon).commit();

    }
    public String getApplyCoupon() {
        return mPref.getString(ApplyCoupon, "");
    }

    public void setOrder_idTake_delivery(String order) {
        mPref.edit().putString(order_idTake_delivery, order).commit();

    }
    public String getOrder_idTake_delivery() {
        return mPref.getString(order_idTake_delivery, "");
    }

    public void setExpired_Flag(String guest_count) {
        mPref.edit().putString(Expired_Flag, guest_count).commit();

    }
    public String getExpired_Flag() {
        return mPref.getString(Expired_Flag, "");
    }


    public void setComplteBillDetails(String guest_count) {
        mPref.edit().putString(COMPLETEBILLDETAILS, guest_count).commit();

    }
    public String getComplteBillDetails() {
        return mPref.getString(COMPLETEBILLDETAILS, "");
    }

    public void setOrder_id_delivery(String guest_count) {
        mPref.edit().putString(order_id_delivery, guest_count).commit();

    }
    public String getOrder_id_delivery() {
        return mPref.getString(order_id_delivery, "");
    }

    public void setOrder_status(String order_statuss) {
        mPref.edit().putString(order_status, order_statuss).commit();
    }
    public String getOrder_status() {return mPref.getString(order_status, "");}


    public void setTax_vat_amount(String guest_count) {
        mPref.edit().putString(Tax_vat_amount, guest_count).commit();

    }
    public String getTax_vat_amount() {
        return mPref.getString(Tax_vat_amount, "");
    }

    public void setTax_tax_exemption(String guest_count) {
        mPref.edit().putString(Tax_tax_exemption, guest_count).commit();

    }
    public String getTax_tax_exemption() {
        return mPref.getString(Tax_tax_exemption, "");
    }

    public void setTax_discount_amount(String guest_count) {
        mPref.edit().putString(Tax_discount_amount, guest_count).commit();

    }
    public String getTax_discount_amount() {
        return mPref.getString(Tax_discount_amount, "");
    }

    public void setTax_service_charge(String guest_count) {
        mPref.edit().putString(Tax_service_charge, guest_count).commit();

    }
    public String getTax_service_charge() {
        return mPref.getString(Tax_service_charge, "");
    }

    public void setTax_payable_amount(String guest_count) {
        mPref.edit().putString(Tax_payable_amount, guest_count).commit();

    }
    public String getTax_payable_amount() {
        return mPref.getString(Tax_payable_amount, "");
    }

    public void setInvoice_No(String guest_count) {
        mPref.edit().putString(Invoice_No, guest_count).commit();

    }
    public String getInvoice_No() {
        return mPref.getString(Invoice_No, "");
    }

    public void setPayable_amount_afterCoupon(String guest_count) {
        mPref.edit().putString(payable_amount_afterCoupon, guest_count).commit();

    }
    public String getPayable_amount_afterCoupon() {
        return mPref.getString(payable_amount_afterCoupon, "");

    }
    public void setFloor(int guest_count) {
        mPref.edit().putInt(Floor, guest_count).commit();

    }
    public int getFloor() {
        return mPref.getInt(Floor, 0);
    }

    public void setPrev_size(int guest_count) {
        mPref.edit().putInt(Prev_size, guest_count).commit();

    }
    public int getPrev_size() {
        return mPref.getInt(Prev_size, 0);
    }

    public void setKitchen_newflag(int guest_count) {
        mPref.edit().putInt(kitchen_newflag, guest_count).commit();

    }
    public int getKitchen_newflag() {
        return mPref.getInt(kitchen_newflag, 0);
    }

    public void setTax_coupon_discount(String guest_count) {
        mPref.edit().putString(Tax_coupon_discount, guest_count).commit();

    }
    public String getTax_coupon_discount() {
        return mPref.getString(Tax_coupon_discount, "");

    }

    public void setTax_vatable_sales(String guest_count) {
        mPref.edit().putString(Tax_vatable_sales, guest_count).commit();

    }
    public String getTax_vatable_sales() {
        return mPref.getString(Tax_vatable_sales, "");

    }

    public void setTax_non_vatable_sales(String guest_count) {
        mPref.edit().putString(Tax_non_vatable_sales, guest_count).commit();

    }
    public String getTax_non_vatable_sales() {
        return mPref.getString(Tax_non_vatable_sales, "");

    }
    public void setContinue_position(int guest_count) {
        mPref.edit().putInt(continue_position, guest_count).commit();

    }
    public int getContinue_position() {
        return mPref.getInt(continue_position, 0);

    }

    public void setCompleteOrderList(int guest_count) {
        mPref.edit().putInt(completeOrderList, guest_count).commit();

    }
    public int getCompleteOrderList() {
        return mPref.getInt(completeOrderList, 0);

    }

    public void setCartPlusFlag(String guest_count) {
        mPref.edit().putString(CartPlusFlag, guest_count).commit();

    }
    public String getCartPlusFlag() {
        return mPref.getString(CartPlusFlag, "");
    }

    public void setOrderTotal(String guest_count) {
        mPref.edit().putString(OrderTotal, guest_count).commit();

    }
    public String getOrderTotal() {
        return mPref.getString(OrderTotal, "");
    }

    public void setRestraunt_DeliveryCharge(String guest_count) {
        mPref.edit().putString(Restraunt_DeliveryCharge, guest_count).commit();

    }
    public String getRestraunt_DeliveryCharge() {
        return mPref.getString(Restraunt_DeliveryCharge, "");
    }

    public void setUpdatedFlag_sssss(String guest_count) {
        mPref.edit().putString(UpdatedFlag_sssss, guest_count).commit();

    }
    public String getUpdatedFlag_sssss() {
        return mPref.getString(UpdatedFlag_sssss, "");
    }
}
