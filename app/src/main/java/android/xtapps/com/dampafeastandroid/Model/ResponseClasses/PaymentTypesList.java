package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentTypesList {

    @SerializedName("payment")
    @Expose
    private String payment;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("card_type")
    @Expose
    private String card_type;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }
}
