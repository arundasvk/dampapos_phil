package android.xtapps.com.dampafeastandroid.DB;

import android.net.Uri;

public class DBConstants {

    public static final String AUTHORITY = "android.xtapps.com.dampafeastandroid.DB.DbBaseProvider";

    public static final String DB_NAME = "Dampa_feast_demo.db";
    public static final int DB_VERSION = 2;


    /*********
     * start of CART related constants
     *******/
    public static final int CART = 1;

    public static final String CART_TABLE = "CART_TABLE";

    public static final String CART_INDEX = "_id";
    public static final String CART_CATEGORY_NAME = "category_name";
    public static final String CART_ORDER_ID = "order_id";
    public static final String CART_MENU_ID = "menu_id";
    public static final String CART_ORDER_MENU_ID = "order_menu_id";
    public static final String CART_MENU_NAME = "menu_name";
    public static final String CART_MENU_NAME2 = "menu_name2";
    public static final String CART_MENU_DESC = "menu_description";
    public static final String CART_MENU_LOCATION_NAME = "location_name";
    public static final String CART_MENU_LOCATION = "location";
    public static final String CART_MENU_MIN_QTY = "minimum_qty";
    public static final String CART_MENU_PRIORITY= "menu_priority";
    public static final String CART_MENU_MEALTIME_NAME= "mealtime_name";
    public static final String CART_MENU_PRICE= "menu_price";
    public static final String CART_MENU_QTY = "menu_qty";
    public static final String CART_MENU_prev_quantity= "prev_quantity";
    public static final String CART_MENU_QTY_X_MENU_PRICE= "menu_qty_X_menu_price";

    public static final String CART_MENU_option_Required= "required";

    public static final String CART_MENU_option_row_id= "row_id";
    public static final String CART_MENU_option_menu_id= "menu_id";

    public static final String CART_MENU_option_comment= "comment";
    public static final String CART_MENU_option_menu_option_id= "menu_option_id";
    public static final String CART_MENU_option_menu_option_value_id= "menu_option_value_id";
    public static final String CART_MENU_option_option_value_id= "option_value_id";
    public static final String CART_MENU_option_menu_menu_id= "menu_id";
    public static final String CART_MENU_option_option_id= "option_id";
    public static final String CART_MENU_option_value= "value";
    public static final String CART_MENU_option_new_price= "new_price";
    public static final String CART_MENU_option_price= "price";

    public static final String CART_MENU_ORDER_STATUS= "status";
    public static final String CART_MENU_DISCOUNT= "discount";
    public static final String CART_MENU_DISCOUNT_PERCENT= "discount_percent";
    public static final String CART_MENU_DISCOUNT_COMMENT= "discount_comment";





/*    public static final String CART_PRODUCT_SIZE= "product_size";
    public static final String CART_PRODUCT_SIZE_LENGTH= "product_size_length";
    public static final String CART_PRODUCT_SIZE_OPTIONS= "product_size_options";
    public static final String CART_PRODUCT_PREFIX= "product_prefix";
    public static final String CART_PRODUCT_OPTION_PRICE= "product_option_prices";
    public static final String CART_PRODUCT_OPTION_VALUE_ITEMS= "product_option_value_items";*/


    public static final Uri CART_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + CART_TABLE);

    public static final String SQL_CREATE_CART_TABLE = String
            .format("create table %s"
                            + "(%s integer primary key autoincrement, %s text, %s text ,"
                            + "%s text,%s text,%s text,%s text,"
                            +"%s text,%s text,%s text,%s text,%s text,%s text,"
                            +"%s text,%s text,%s text,%s text,%s text,%s text,"
                            +"%s text,%s text,%s text,"
                            +"%s int,%s int,%s int,%s double,%s text,%s double,%s double,%s text)",
                    CART_TABLE, CART_INDEX,CART_MENU_ID,CART_ORDER_MENU_ID,
                    CART_CATEGORY_NAME,CART_MENU_NAME,CART_MENU_NAME2,CART_ORDER_ID,
                    CART_MENU_DESC, CART_MENU_LOCATION_NAME, CART_MENU_LOCATION,CART_MENU_PRIORITY,CART_MENU_MEALTIME_NAME,CART_MENU_PRICE,
                    CART_MENU_option_comment,CART_MENU_option_menu_option_id,CART_MENU_option_menu_option_value_id,CART_MENU_option_option_value_id,CART_MENU_option_option_id,CART_MENU_option_value,
                    CART_MENU_option_new_price,CART_MENU_option_price,CART_MENU_option_Required,
                    CART_MENU_MIN_QTY,CART_MENU_QTY,CART_MENU_prev_quantity,CART_MENU_QTY_X_MENU_PRICE,CART_MENU_ORDER_STATUS,CART_MENU_DISCOUNT,
                    CART_MENU_DISCOUNT_PERCENT,CART_MENU_DISCOUNT_COMMENT);
    /********* end of CART_TABLE related constants *******/

    /********************************************************Table formodifies************************************/

    public static final int MODIFIER = 2;
    public static final String MODIFIER_TABLE = "MODIFIER_TABLE";
    public static final String MODIFIER_INDEX = "_id";
    public static final String MODIFIER_MENU_ID = "menu_id";
    public static final String MODIFIER_MENU_NAME = "menu_name";
    public static final String MODIFIER_MENU_option_menu_option_id= "menu_option_id";
    public static final String MODIFIER_menu_option_value_id= "menu_option_value_id";
    public static final String MODIFIER_option_option_value_id= "option_value_id";
    public static final String MODIFIER_option_option_id= "option_id";
    public static final String MODIFIER_option_option_name= "option_name";
    public static final String MODIFIER_option_display_type= "display_type";
    public static final String MODIFIER_option_value= "value";
    public static final String MODIFIER_option_new_price= "new_price";
    public static final String MODIFIER_option_price= "price";

    public static final Uri CART_CONTENT_URI2 = Uri.parse("content://"
            + AUTHORITY + "/" + MODIFIER_TABLE);

    public static final String SQL_CREATE_MODIFIER_TABLE = String
            .format("create table %s"
                            + "(%s integer primary key autoincrement,%s text , %s text,%s text,%s text,"
                            +"%s text,%s text,%s text,"
                            +"%s text,%s text,"
                            +"%s text,%s text)",
                    MODIFIER_TABLE, MODIFIER_INDEX,MODIFIER_MENU_ID,MODIFIER_MENU_NAME,MODIFIER_menu_option_value_id,MODIFIER_MENU_option_menu_option_id,
                    MODIFIER_option_option_value_id,MODIFIER_option_option_id,MODIFIER_option_option_name,
                    MODIFIER_option_display_type,MODIFIER_option_value,
                    MODIFIER_option_new_price,MODIFIER_option_price
            );

    /********************************************************Table for taxdetails************************************/
    public static final int TAX = 3;
    public static final String TAX_TABLE = "TAX_TABLE";
    public static final String TAX_INDEX = "_id";
    public static final String TAX_ORDER_ID = "order_id";
    public static final String TAX_VAT_AMOUNT = "vat_amount";
    public static final String TAX_EXEMPTION= "tax_exemption";
    public static final String TAX_DISCOUNT_AMOUNT= "discount_amount";
    public static final String TAX_SERVICE_CAHARGE= "service_charge";
    public static final String TAX_PAYABLE_AMOUNT= "payable_amount";


    public static final Uri CART_CONTENT_URI3 = Uri.parse("content://"
            + AUTHORITY + "/" + TAX_TABLE);

    public static final String SQL_CREATE_TAX_TABLE = String
            .format("create table %s"
                            + "(%s integer primary key autoincrement,"
                            +"%s text,%s text,%s text,"
                            +"%s text,%s text,%s text)",
                    TAX_TABLE, TAX_INDEX,
                    TAX_ORDER_ID,TAX_VAT_AMOUNT,TAX_EXEMPTION,
                    TAX_DISCOUNT_AMOUNT,TAX_SERVICE_CAHARGE, TAX_PAYABLE_AMOUNT
            );


}