package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModifierDataList {
    @SerializedName("menu_option_id")
    @Expose
    private String menu_option_id;

    @SerializedName("default_value_id")
    @Expose
    private String default_value_id;
    @SerializedName("required")
    @Expose
    private String required;

    @SerializedName("option_name")
    @Expose
    private String option_name;


    @SerializedName("option_values")
    @Expose
    private ArrayList<OptionValuesList> optionValues;

    public ArrayList<OptionValuesList> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(ArrayList<OptionValuesList> optionValues) {
        this.optionValues =optionValues;
    }




    public String getmenu_option_id() {
        return menu_option_id;
    }

    public void setmenu_option_id(String menu_option_id) {
        this.menu_option_id = menu_option_id;
    }



    public String getdefault_value_id() {
        return default_value_id;
    }

    public void setdefault_value_id(String default_value_id) {
        this.default_value_id = default_value_id;
    }



    public String getrequired() {
        return required;
    }

    public void setrequired(String categoryId) {
        this.required = required;
    }



    public String getoption_name() {
        return option_name;
    }

    public void setoption_name(String option_name) {
        this.option_name = option_name;
    }
}
