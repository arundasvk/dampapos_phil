package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Orderdetails {

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("invoice_no")
    @Expose
    private String invoice_no;

    @SerializedName("invoice_date")
    @Expose
    private String invoice_date;

    @SerializedName("sale_no")
    @Expose
    private String sale_no;

    @SerializedName("extra_item")
    @Expose
    private String extra_item;

    @SerializedName("other_extra")
    @Expose
    private String other_extra;

    @SerializedName("table_info")
    @Expose
    private OptionList table_info;

    @SerializedName("coupon_discount")
    @Expose
    private String coupon_discount;

    @SerializedName("other_discount")
    @Expose
    private String other_discount;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("tender_cash")
    @Expose
    private String tender_cash;

    @SerializedName("tender_change")
    @Expose
    private String tender_change;

    @SerializedName("vat")
    @Expose
    private String vat;

    @SerializedName("tax1_applicable_sale")
    @Expose
    private String tax1_applicable_sale;

    @SerializedName("tax2_applicable_sale")
    @Expose
    private String tax2_applicable_sale;


    @SerializedName("tax1_non_applicable_sale")
    @Expose
    private String tax1_non_applicable_sale;

    @SerializedName("tax2_non_applicable_sale")
    @Expose
    private String tax2_non_applicable_sale;

    @SerializedName("vat_percentage")
    @Expose
    private String vat_percentage;

    @SerializedName("service_charge_percentage")
    @Expose
    private String service_charge_percentage;

    @SerializedName("service_charge")
    @Expose
    private String service_charge;

    @SerializedName("tax1_exempted")
    @Expose
    private String tax1_exempted;

    @SerializedName("tax2_exempted")
    @Expose
    private String tax2_exempted;

   /* @SerializedName("payment_type")
    @Expose
    private String payment_type;*/



    @SerializedName("card_type")
    @Expose
    private String card_type;

    @SerializedName("final_total")
    @Expose
    private String final_total;

    @SerializedName("qk_name")
    @Expose
    private String qk_name;

    @SerializedName("mobile_number")
    @Expose
    private String mobile_number;

    @SerializedName("no_of_pax")
    @Expose
    private String no_of_pax;

    @SerializedName("disabled")
    @Expose
    private String disabled;

    @SerializedName("solo_parent")
    @Expose
    private String solo_parent;

    @SerializedName("senior_citizen")
    @Expose
    private String senior_citizen;

    @SerializedName("order_firstname")
    @Expose
    private String order_firstname;

    @SerializedName("order_telephone")
    @Expose
    private String order_telephone;

    @SerializedName("order_type")
    @Expose
    private String order_type;

    @SerializedName("address1")
    @Expose
    private String address1;

    @SerializedName("address2")
    @Expose
    private String address2;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("area")
    @Expose
    private String area;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("order_menus")
    @Expose
    private ArrayList<OrderMenus> order_menus;

    @SerializedName("payment_type")
    @Expose
    private ArrayList<PaymentTypesList> payment_type;

    @SerializedName("coupon_type")
    @Expose
    private String coupon_type;

    @SerializedName("coupon_code")
    @Expose
    private String coupon_code;

    @SerializedName("card_number")
    @Expose
    private String card_number;

    @SerializedName("card_name")
    @Expose
    private String card_name;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getOther_discount() {
        return other_discount;
    }

    public void setOther_discount(String other_discount) {
        this.other_discount = other_discount;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTender_cash() {
        return tender_cash;
    }

    public void setTender_cash(String tender_cash) {
        this.tender_cash = tender_cash;
    }

    public String getTender_change() {
        return tender_change;
    }

    public void setTender_change(String tender_change) {
        this.tender_change = tender_change;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }



    public String getTax1_applicable_sale() {
        return tax1_applicable_sale;
    }

    public void setTax1_applicable_sale(String vat) {
        this.tax1_applicable_sale = tax1_applicable_sale;
    }

    public String getTax2_applicable_sale() {
        return tax2_applicable_sale;
    }

    public void setTax2_applicable_sale(String tax1_applicable_sale) {
        this.tax1_applicable_sale = tax1_applicable_sale;
    }

    public String getTax1_non_applicable_sale() {
        return tax1_non_applicable_sale;
    }

    public void setTax1_non_applicable_sale(String tax1_non_applicable_sale) {
        this.tax1_non_applicable_sale = tax1_non_applicable_sale;
    }

    public String getTax2_non_applicable_sale() {
        return tax2_non_applicable_sale;
    }

    public void setTax2_non_applicable_sale(String tax2_non_applicable_sale) {
        this.tax2_non_applicable_sale = tax2_non_applicable_sale;
    }



    public String getVat_percentage() {
        return vat_percentage;
    }

    public void setVat_percentage(String vat_percentage) {
        this.vat_percentage = vat_percentage;
    }

    public String getService_charge_percentage() {
        return service_charge_percentage;
    }

    public void setService_charge_percentage(String service_charge_percentage) {
        this.service_charge_percentage = service_charge_percentage;
    }

    public String getService_charge() {
        return service_charge;
    }

    public void setService_charge(String service_charge) {
        this.service_charge = service_charge;
    }

    public String getTax1_exempted() {
        return tax1_exempted;
    }

    public void setTax1_exempted(String tax1_exempted) {
        this.tax1_exempted = tax1_exempted;
    }

    public String getTax2_exempted() {
        return tax2_exempted;
    }

    public void setTax2_exempted(String tax2_exempted) {
        this.tax2_exempted = tax2_exempted;
    }


    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    /*public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }*/

    public String getExtra_item() {
        return extra_item;
    }

    public void setExtra_item(String extra_item) {
        this.extra_item = extra_item;
    }

    public String getOther_extra() {
        return other_extra;
    }

    public void setOther_extra(String other_extra) {
        this.other_extra = other_extra;
    }

    public String getTotal() {
        return final_total;
    }

    public void setTotal(String final_total) {
        this.final_total = final_total;
    }

    public String getQk_name() {
        return qk_name;
    }

    public void setQk_name(String qk_name) {
        this.qk_name = qk_name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getNo_of_pax() {
        return no_of_pax;
    }

    public void setNo_of_pax(String no_of_pax) {
        this.no_of_pax = no_of_pax;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getSolo_parent() {
        return solo_parent;
    }

    public void setSolo_parent(String solo_parent) {
        this.solo_parent = solo_parent;
    }

    public String getSenior_citizen() {
        return senior_citizen;
    }

    public void setSenior_citizen(String senior_citizen) {
        this.senior_citizen = senior_citizen;
    }

    public String getOrder_firstname() {
        return order_firstname;
    }

    public void setOrder_firstname(String order_firstname) {
        this.order_firstname = order_firstname;
    }

    public String getOrder_telephone() {
        return order_telephone;
    }

    public void setOrder_telephone(String order_telephone) {
        this.order_telephone = order_telephone;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<OrderMenus> getOrder_menus() {
        return order_menus;
    }

    public void setOrder_menus(ArrayList<OrderMenus> order_menus) {
        this.order_menus = order_menus;
    }

    public ArrayList<PaymentTypesList> getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(ArrayList<PaymentTypesList> payment_type) {
        this.payment_type = payment_type;
    }

    public String getFinal_total() {
        return final_total;
    }

    public void setFinal_total(String final_total) {
        this.final_total = final_total;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getSale_no() {
        return sale_no;
    }

    public void setSale_no(String sale_no) {
        this.sale_no = sale_no;
    }

    public OptionList getTable_info() {
        return table_info;
    }

    public void setTable_info(OptionList table_info) {
        this.table_info = table_info;
    }

    public class OptionList {

        @SerializedName("staff_id")
        @Expose
        private String staff_id;

        @SerializedName("table_id")
        @Expose
        private String table_id;

        @SerializedName("table_name")
        @Expose
        private String table_name;

        @SerializedName("waiter_name")
        @Expose
        private String waiter_name;

        @SerializedName("updated_at")
        @Expose
        private String updated_at;

        public String getStaff_id() {
            return staff_id;
        }

        public void setStaff_id(String staff_id) {
            this.staff_id = staff_id;
        }

        public String getTable_id() {
            return table_id;
        }

        public void setTable_id(String table_id) {
            this.table_id = table_id;
        }

        public String getWaiter_name() {
            return waiter_name;
        }

        public void setWaiter_name(String waiter_name) {
            this.waiter_name = waiter_name;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getTable_name() {
            return table_name;
        }

        public void setTable_name(String table_name) {
            this.table_name = table_name;
        }
    }


}
