package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.LogOutTimerUtil;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.Kitchenorder;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.NotificationAdapter;
import android.xtapps.com.dampafeastandroid.View.Fragments.AllInOneFragment;
import android.xtapps.com.dampafeastandroid.View.Fragments.KitchenFragment;
import android.xtapps.com.dampafeastandroid.View.Fragments.ReportFragment;
import android.xtapps.com.dampafeastandroid.broadcast_receivers.NotificationEventReceiver;
import android.xtapps.com.dampafeastandroid.broadcast_receivers.SampleAlarmReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.xtapps.com.dampafeastandroid.Controller.Constants.API.LOGO_IN_PRINTER;

public class NavigationMainActivity extends RootActivity implements LogOutTimerUtil.LogOutListener {
    private String TAG = NavigationMainActivity.class.getSimpleName();
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    HashMap<String, String> map1;
    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerList;
    Toolbar toolbar;
    TextView tv_toolbar_title,nav_name;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private TextView tv_logout,tv_item_name,tv_add_cart,nav_dine_in,nav_get_token,nav_kitchen_order,nav_my_order,nav_Notification,nav_CompletedOrder;
    Context mContext;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String FROM_TABLE = "restaurant_table";
    private String FROM_MENU = "restaurant_menu";
    private String select;
    LinearLayout ll_dine,ll_take_away,ll_delivery,ll_reports,ll_kitchen,ll_logout;
    LinearLayout ll_add_cart,ll_frag2,sidemenu;

    FrameLayout frag1,frag2,contentt_frame;
    List<CartItem> mCartItems;
    List<CartItem> new_orders;
    int cartcount;
    int Service_type=0;
    ProgressBar simpleProgressBar;
    private final SampleAlarmReceiver alarm = new SampleAlarmReceiver();
    private BroadcastReceiver broadcastReceiver;
    private ListView listview;
    NotificationAdapter adapter;
    ArrayList<Kitchenorder> Orderlist = new ArrayList<Kitchenorder>();
    private int SPLASH_TIME_OUT = 5000;
    public static final String notification_update_UI = "notification_update_UI"; // parent node
    public static final String NOTI_ARRAY = "NOTI_ARRAY"; // parent node
    public static final String NOTI_TABLE_ID = "NOTI_TABLE_ID"; // parent node
    public static final String NOTI_TABLE_NAME = "NOTI_TABLE_NAME"; // parent node
    public static final String NOTI_TEXT = "NOTI_TEXT";
    public static final String NOTI_TYPE = "NOTI_TYPE";
    public static final String NOTI_STAFF_ID = "NOTI_STAFF_ID";
    public static final String NOTI_MENU_ID = "NOTI_MENU_ID";
    public static final String NOTI_ORDER_ID = "NOTI_ORDER_ID";// parent node
    public static final String NOTI_STATUS = "NOTI_STATUS";
    public static final String NOTI_CREATED_AT = "NOTI_CREATED_AT";
    public static final String NOTI_UPDATED_AT = "NOTI_UPDATED_AT";// parent node
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigationactivity_main);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = this;

        sharedpreferencesData = NavigationMainActivity.this.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);
        alarm.setAlarm(this);
        mTitle = mDrawerTitle = getTitle();
        mNavigationDrawerItemTitles= getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (LinearLayout) findViewById(R.id.left_drawer);
        tv_logout = (TextView) findViewById(R.id.tv_logout);

        tv_item_name = (TextView) findViewById(R.id.tv_item_names);
        tv_add_cart=(TextView) findViewById(R.id.tv_add_carte);
        contentt_frame=(FrameLayout)findViewById(R.id.contentt_frame);
        frag1=(FrameLayout)findViewById(R.id.flContainer);
        frag2=(FrameLayout)findViewById(R.id.flContainer2);
        sidemenu =(LinearLayout) findViewById(R.id.sidemenu);
        //flContainerfull=(FrameLayout)findViewById(R.id.flContainerfull);

        listview = (ListView)findViewById(R.id.listview);
        listview.setVisibility(View.GONE);
        ll_add_cart =(LinearLayout)findViewById(R.id.ll_add_cart);
        ll_frag2 =(LinearLayout)findViewById(R.id.ll_frag2);

        toolbar =(Toolbar) findViewById(R.id.toolbar);
        tv_toolbar_title =(TextView) findViewById(R.id.tv_toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //tv_toolbar_title.setText("Dash Board");

        nav_name=(TextView) findViewById(R.id.nav_name);
        nav_dine_in=(TextView) findViewById(R.id.nav_dine_in);
        nav_get_token=(TextView) findViewById(R.id.nav_get_token);
        nav_kitchen_order=(TextView) findViewById(R.id.nav_kitchen_order);
        nav_my_order=(TextView) findViewById(R.id.nav_my_order);
        nav_Notification=(TextView) findViewById(R.id.nav_Notification);
        nav_CompletedOrder=(TextView) findViewById(R.id.nav_CompletedOrder);
        nav_name.setText(AppSession.getInstance().getStaff_Name());

        String usertype=AppSession.getInstance().getUser_Type();

        NotificationEventReceiver.setupAlarm(getApplicationContext());


        ll_dine = (LinearLayout) findViewById(R.id.ll_dine);
        ll_take_away = (LinearLayout) findViewById(R.id.ll_take_away);
        ll_delivery = (LinearLayout) findViewById(R.id.ll_delivery);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_kitchen = (LinearLayout) findViewById(R.id.ll_kitchen);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);

        if(AppSession.getInstance().getBitmapFlag())
        {
            AppSession.getInstance().setBitmapFlag(false);
            bitmapImagePrinter();
        }


        ll_dine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkStatusDinein();


            }
        });
        ll_take_away.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkStatusTakeAway();



            }
        });
        ll_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkStatusDelivery();



            }
        });
        ll_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reports();


            }
        });

        ll_kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                kitchen();


            }
        });
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logout();


            }
        });

        Log.d("DEBUG", getResources().getConfiguration().orientation + "");

        if(AppSession.getInstance().getTabP().equals("1"))
        {
            AppSession.getInstance().setTabP("");
            ll_frag2.setVisibility(View.GONE);
            /*CartFragment firstFragment = new CartFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();//
            ft.replace(R.id.flContainer, firstFragment);
            ft.addToBackStack(null);// add    Fragment
            ft.commit();*/
            Intent i = new Intent(NavigationMainActivity.this, CartActivity.class);
            startActivity(i);
        }
        else if(usertype.equals("KITCHEN")||usertype.equals("BAR")){
            DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
            DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
            ll_frag2.setVisibility(View.GONE);
            nav_dine_in.setVisibility(View.GONE);
            nav_CompletedOrder.setVisibility(View.VISIBLE);
            nav_get_token.setVisibility(View.GONE);
            nav_kitchen_order.setVisibility(View.GONE);
            nav_my_order.setVisibility(View.GONE);
            nav_Notification.setVisibility(View.GONE);
            KitchenFragment firstFragment = new KitchenFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();//
            ft.replace(R.id.contentt_frame, firstFragment);
            ft.addToBackStack(null);// add    Fragment
            ft.commit();
        }
        else if(usertype.equals("SUPERVISOR")){
            ll_frag2.setVisibility(View.GONE);
            nav_dine_in.setVisibility(View.GONE);
            nav_CompletedOrder.setVisibility(View.GONE);
            nav_get_token.setVisibility(View.GONE);
            nav_kitchen_order.setVisibility(View.GONE);
            nav_my_order.setVisibility(View.GONE);
            nav_Notification.setVisibility(View.GONE);
            ReportFragment firstFragment = new ReportFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();//
            ft.replace(R.id.contentt_frame, firstFragment);
            ft.addToBackStack(null);// add    Fragment
            ft.commit();
        }
        else if(usertype.equals("WAITER")){
            nav_CompletedOrder.setVisibility(View.GONE);
            ll_frag2.setVisibility(View.GONE);
            if(AppSession.getInstance().getMenuprinter().equals("123")){
                Intent i = new Intent(NavigationMainActivity.this, Main_ActivityPrinterLongin.class);
                startActivity(i);

            }else if(AppSession.getInstance().getMenuprinter().equals(" ")){

            }
            if(  AppSession.getInstance().getTick().equals("reports"))
            {
                AppSession.getInstance().setTick("");
                reports();
            }
            else
            if(  AppSession.getInstance().getTick().equals("kitchen"))
            {
                AppSession.getInstance().setTick("");
                kitchen();
            }
            else
            if(  AppSession.getInstance().getTick().equals("logout"))
            {
                AppSession.getInstance().setTick("");
                logout();
            }
            else
            {
                if(AppSession.getInstance().getServiceType().equals(""))
                {
                    AppSession.getInstance().setServiceType("dine_in");
                    checkStatusDinein();
                }
                else if(AppSession.getInstance().getServiceType().equals("dine_in"))
                {
                    checkStatusDinein();
                }
                else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                {
                    checkStatusTakeAway();
                }
                else if(AppSession.getInstance().getServiceType().equals("delivery"))
                {
                    checkStatusDelivery();
                }
                else
                {
                    AppSession.getInstance().setServiceType("dine_in");
                    checkStatusDinein();
                }



            }

        }
        else
        {
            if (savedInstanceState == null) {
                // Instance of first fragment


                if(  AppSession.getInstance().getTick().equals("reports"))
                {
                    AppSession.getInstance().setTick("");
                    reports();
                }
                else
                if(  AppSession.getInstance().getTick().equals("kitchen"))
                {
                    AppSession.getInstance().setTick("");
                    kitchen();
                }
                else
                if(  AppSession.getInstance().getTick().equals("logout"))
                {
                    AppSession.getInstance().setTick("");
                    logout();
                }
                else
                {
                    if(AppSession.getInstance().getServiceType().equals(""))
                    {
                        AppSession.getInstance().setServiceType("dine_in");
                        checkStatusDinein();
                    }
                    else if(AppSession.getInstance().getServiceType().equals("dine_in"))
                    {
                        checkStatusDinein();
                    }
                    else if(AppSession.getInstance().getServiceType().equals("takeaway"))
                    {
                        checkStatusTakeAway();
                    }
                    else if(AppSession.getInstance().getServiceType().equals("delivery"))
                    {
                        checkStatusDelivery();
                    }
                    else
                    {
                        AppSession.getInstance().setServiceType("dine_in");
                        checkStatusDinein();
                    }



                }

               /* AppSession.getInstance().setServiceType("dine_in");
                AllInOneFragment firstFragment = new AllInOneFragment();

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.contentt_frame, firstFragment);
                ft.addToBackStack(null);
                ft.commit(); */
            }

            /*if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                ll_frag2.setVisibility(View.VISIBLE);
                HomeScreenTwo secondFragment = new HomeScreenTwo();
                Bundle args = new Bundle();
                args.putInt("position", 0);
                secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
                FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
                ft2.add(R.id.flContainer2, secondFragment);
                ft2.addToBackStack(null);// add    Fragment
                ft2.commit();                                                            // commit FragmentTransaction
            }*/
        }


        String[] types1 = getResources().getStringArray(R.array.select_service);

        final Spinner spinner1 = (Spinner) findViewById(R.id.spinner_prod);
        final Spinner spinner2 = (Spinner) findViewById(R.id.spinner_service);
        final List<String> genderType1 = new ArrayList<>(Arrays.asList(types1));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                this,R.layout.support_simple_spinner_dropdown_item,genderType1){
            @Override
            public boolean isEnabled(int position){
               /* if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {*/

                return true;
                //}
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

               /* if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {*/
                tv.setTextColor(Color.BLACK);
                // }
                return view;
            }
        };

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(spinnerArrayAdapter1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        spinner2.setAdapter(spinnerArrayAdapter1);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select = (String) parent.getItemAtPosition(position);
                //((TextView) parent.getChildAt(0)).setText(getResources().getString(R.string.select_prod));
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){
                    // Notify the selected item text
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setText(select);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });



        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
               /* SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.clear();
                editor1.commit();
                AppSession.getInstance().setTable_ID("");
                AppSession.getInstance().setTable_Name("");
                DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
                DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
                AppSession.getInstance().setGuest_Count("");
                AppSession.getInstance().setOrderNumber("");
                AppSession.getInstance().setisLoginStatus(false);
                Intent intent=new Intent(NavigationMainActivity.this,LoginActivity.class);
                startActivity(intent);*/
            }
        });

        nav_dine_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sidemenu.setVisibility(View.VISIBLE);
                checkStatusDinein();
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        nav_get_token.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        nav_kitchen_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        nav_my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                Intent i = new Intent(NavigationMainActivity.this, MyOrderActivity.class);
                startActivity(i);
            }
        });
        nav_Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                Intent i = new Intent(NavigationMainActivity.this, NotificationActivity.class);
                startActivity(i);
            }
        });
        nav_CompletedOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSession.getInstance().setCompleteOrderList(1);
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                Intent i = new Intent(NavigationMainActivity.this, CompletedOrdersList.class);
                startActivity(i);
            }
        });
    }

    private void bitmapImagePrinter() {
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                   /* URL url = new URL("http://18.220.183.40/dampafeast_cloud/assets/images/data/Dampa_Logo_line_work.jpg");*/
                    URL url = new URL(LOGO_IN_PRINTER);
                    //Bitmap mBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = calculateInSampleSize(options, 500,500);
                    options.inJustDecodeBounds = false;
                    Bitmap mBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream(),null,options);
                    String abc=BitMapToString(mBitmap);
                    AppSession.getInstance().setPrintericon(abc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public String BitMapToString(Bitmap bitmap){
        try{
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG,50, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    } catch (NullPointerException e) {
        return null;
    } catch (OutOfMemoryError e) {
        return null;
    }
    }


    private void registerReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(AppSession.getInstance().getUser_Type().equals("WAITER")){
                    String otpCode = intent.getStringExtra(NOTI_ARRAY);
              /*  final ArrayList<HashMap<String, String>> map1 =(ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
                for (int i=0;i<map1.size();i++) {
                  String  id = map1.get(i).get(SplashScreenActivity.LOCAION_ID);
                    String location_name = map1.get(i).get(SplashScreenActivity.LOCAION_NAME);
                    *//* Build the StringWithTag List using these keys and values. *//*
                    Toast.makeText(NavigationMainActivity.this, id+" "+location_name, Toast.LENGTH_SHORT).show();
                }*/


                    vibrate();
                    JSONArray jArray = null;
                    try {
                        jArray = new JSONArray(otpCode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (jArray.length()==0)
                    {

                    }
                    else
                    {
                        categoryList.clear();
                        for(int i=0;i<jArray.length();i++){

                            JSONObject json_data = null;
                            try {
                                json_data = jArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String text1 = null;
                            String table_id = null;
                            String staff_id = null;
                            String menu_id = null;
                            String order_id = null;
                            String status = null;
                            String created_at = null;
                            String updated_at = null;
                            String table_name = null;
                            String order_type = null;
                            try {
                                text1 = json_data.getString("text");
                                table_id = json_data.getString("id");
                                table_name = json_data.getString("table_name");
                                order_type = json_data.getString("order_type");
                                staff_id = json_data.getString("staff_id");
                                menu_id = json_data.getString("menu_id");
                                order_id = json_data.getString("order_id");
                                status = json_data.getString("status");
                                created_at = json_data.getString("created_at");
                                updated_at = json_data.getString("updated_at");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            map1 = new HashMap<String, String>();

                            map1.put(NOTI_TABLE_ID, table_id);
                            map1.put(NOTI_TABLE_NAME, table_name);
                            map1.put(NOTI_TEXT, text1);
                            map1.put(NOTI_TYPE, order_type);
                            map1.put(NOTI_STAFF_ID, staff_id);
                            map1.put(NOTI_MENU_ID, menu_id);
                            map1.put(NOTI_ORDER_ID, order_id);
                            map1.put(NOTI_STATUS, status);
                            map1.put(NOTI_CREATED_AT, created_at);
                            map1.put(NOTI_UPDATED_AT, updated_at);
                            categoryList.add(map1);
                            /*Log.i("#########################################",categoryList.toString());*/

                        }

                    }
                    /*
                     * Step 3: We can update the UI of the activity here
                     * */


                    listview.setVisibility(View.VISIBLE);
                    adapter = new NotificationAdapter(NavigationMainActivity.this,categoryList);
                    listview.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {



                        @Override
                        public void run() {

                            listview.setVisibility(View.GONE);

                        }
                    }, SPLASH_TIME_OUT);

                }else{

                }

            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("notification_update_UI"));

    }
    private void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void reports() {
        ll_delivery.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_dine.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_take_away.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.color.colorreddd);
        ll_kitchen.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_logout.setBackgroundResource(R.drawable.sidemenuboredr);

        ReportFragment firstFragment = new ReportFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();//
        ft.replace(R.id.contentt_frame, firstFragment);
        ft.addToBackStack(null);// add    Fragment
        ft.commit();
    }

    private void kitchen() {
        ll_delivery.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_dine.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_take_away.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_kitchen.setBackgroundResource(R.color.colorreddd);
        ll_logout.setBackgroundResource(R.drawable.sidemenuboredr);

        // ll_frag2.setVisibility(View.GONE);


        KitchenFragment firstFragment = new KitchenFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();//
        ft.replace(R.id.contentt_frame, firstFragment);
        ft.addToBackStack(null);// add    Fragment
        ft.commit();
    }

    private void logout() {
        ll_delivery.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_dine.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_take_away.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_kitchen.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_logout.setBackgroundResource(R.color.colorreddd);
        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        editor1.clear();
        editor1.commit();
        AppSession.getInstance().setServiceType("");
        AppSession.getInstance().setGuest_Count("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrderTotal("0");
        AppSession.getInstance().setOrder_id_delivery("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setFloor(0);
        DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
        DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
        AppSession.getInstance().setisLoginStatus(false);
        Intent intent=new Intent(NavigationMainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkStatusDelivery() {
        if(AppSession.getInstance().getServiceType().equals("delivery"))
        {
            delivery();
        }
        else
        {
            Service_type=3;
           /* cartcount = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItemsCount();*/

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("delivery"))
            {
                new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getByOrderStatus("");
                // new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItems();
                if(new_orders.size()>0)
                {
                    alert();
                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
                    delivery();
                }
                // alert();
            }
            else
            {
                delivery();
            }


        }
    }

    private void checkStatusTakeAway() {
        if(AppSession.getInstance().getServiceType().equals("takeaway"))
        {
            take_away();
        }
        else
        {
            Service_type=2;

         /*   cartcount = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItemsCount();*/

            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                // new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItems();
                new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
                    take_away();
                }
                // alert();
            }
            else
            {
                take_away();
            }
        }
    }

    private void checkStatusDinein() {
        if(AppSession.getInstance().getServiceType().equals("dine_in"))
        {
            dine_in();
        }
        else
        {
            Service_type=1;

          /*  cartcount = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItemsCount();*/
            if(/*cartcount>0&&*/!AppSession.getInstance().getServiceType().equals("dine_in"))
            {
                //new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getCartItems();
                new_orders = DBMethodsCart.newInstance(NavigationMainActivity.this).getByOrderStatus( "");
                if(new_orders.size()>0)
                {
                    alert();

                }
                else
                {
                    // continue with delete
                    SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                    editor1.putString("order_comment","");
                    editor1.commit();
                    AppSession.getInstance().setTable_ID("");
                    AppSession.getInstance().setOrderNumber("");
                    AppSession.getInstance().setTable_Name("");
                    AppSession.getInstance().setOrder_idTake_delivery("");
                    AppSession.getInstance().setOrder_id_delivery("");
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
                    DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
                    dine_in();
                }
                // alert();
            }
            else
            {
                dine_in();
            }

        }

    }

    private void take_away() {

        //ll_frag2.setVisibility(View.VISIBLE);
        AppSession.getInstance().setServiceType("takeaway");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrder_id_delivery("");
        ll_take_away.setBackgroundResource(R.color.colorreddd);
        ll_dine.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_delivery.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_kitchen.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_logout.setBackgroundResource(R.drawable.sidemenuboredr);
        AllInOneFragment firstFragment = new AllInOneFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
        ft.replace(R.id.contentt_frame, firstFragment);
        ft.addToBackStack(null);// add    Fragment
        ft.commit();
        /*if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            ll_frag2.setVisibility(View.VISIBLE);
            HomeScreenTwo secondFragment = new HomeScreenTwo();
            Bundle args = new Bundle();
            args.putInt("position", 0);
            secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
            FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
            ft2.add(R.id.flContainer2, secondFragment);
            ft2.addToBackStack(null);// add    Fragment
            ft2.commit();                                                            // commit FragmentTransaction
        }*/
    }

    private void dine_in() {
        /* ll_frag2.setVisibility(View.VISIBLE);*/

        ll_dine.setBackgroundResource(R.color.colorreddd);
        ll_take_away.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_delivery.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_kitchen.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_logout.setBackgroundResource(R.drawable.sidemenuboredr);
        //  ll_dine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        AppSession.getInstance().setServiceType("dine_in");

        AllInOneFragment firstFragment = new AllInOneFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
        ft.replace(R.id.contentt_frame, firstFragment);
        ft.addToBackStack(null);// add    Fragment
        ft.commit();
       /* if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            ll_frag2.setVisibility(View.VISIBLE);
            HomeScreenTwo secondFragment = new HomeScreenTwo();
            Bundle args = new Bundle();
            args.putInt("position", 0);
            secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
            FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
            ft2.add(R.id.flContainer2, secondFragment);
            ft2.addToBackStack(null);// add    Fragment
            ft2.commit();                                                            // commit FragmentTransaction
        }*/
    }

    private void delivery() {

        AppSession.getInstance().setServiceType("delivery");
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        ll_delivery.setBackgroundResource(R.color.colorreddd);
        ll_dine.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_take_away.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_reports.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_kitchen.setBackgroundResource(R.drawable.sidemenuboredr);
        ll_logout.setBackgroundResource(R.drawable.sidemenuboredr);
        //ll_frag2.setVisibility(View.VISIBLE);
        AllInOneFragment firstFragment = new AllInOneFragment ();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
        ft.replace(R.id.contentt_frame, firstFragment);
        ft.addToBackStack(null);// add    Fragment
        ft.commit();
       /* if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            ll_frag2.setVisibility(View.VISIBLE);
            HomeScreenTwo secondFragment = new HomeScreenTwo();
            Bundle args = new Bundle();
            args.putInt("position", 0);
            secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
            FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
            ft2.add(R.id.flContainer2, secondFragment);
            ft2.addToBackStack(null);// add    Fragment
            ft2.commit();                                                            // commit FragmentTransaction
        }*/
    }

    private void alert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(NavigationMainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(NavigationMainActivity.this);
        }
        builder.setTitle("You have unfinished orders")
                .setMessage("Are you sure you want to continue?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("order_comment","");
                        editor1.commit();
                        AppSession.getInstance().setTable_ID("");
                        AppSession.getInstance().setOrderNumber("");
                        AppSession.getInstance().setOrder_id_delivery("");
                        AppSession.getInstance().setOrder_idTake_delivery("");
                        AppSession.getInstance().setTable_Name("");
                        DBMethodsCart.newInstance(NavigationMainActivity.this).clearCart();
                        DBMethodsCart.newInstance(NavigationMainActivity.this).clearModifier();
                        tv_item_name.setText("");
                        tv_add_cart.setText(AppSession.getInstance().getRestraunt_Decimal_Format()+" "+AppSession.getInstance().getRestraunt_Currency());
                        if(Service_type==1)
                        {
                            dine_in();
                        }
                        else
                        if(Service_type==2)
                        {
                            take_away();
                        }
                        else if(Service_type==3)
                        {
                            delivery();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }






    /* @Override
     public void onResume() {
         super.onResume();

         //loadData();
     }*/
    @Override
    protected void onStop() {
        super.onStop();
        /*
         * Step 4: Ensure to unregister the receiver when the activity is destroyed so that
         * you don't face any memory leak issues in the app
         */
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
    boolean doubleBackToExitPressedOnce = false;
    @Override public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(this);super.onBackPressed();
            return;
        }this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);}

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activi:mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /* @Override public void onStart() {
         super.onStart();
         registerReceiver();
     }//loadData(); }
 */
    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if(view instanceof EditText){
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
//********************************************logout session in  10 min inactivity

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "OnStart () &&& Starting timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "User interacting with screen");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "onResume()");
    }

    /**
     * Performing idle time logout
     */
    @Override
    public void doLogout() {
        AppSession.getInstance().setExpired_Flag("true");
        if(AppSession.getInstance().getUser_Type().equals("WAITER")
                ||AppSession.getInstance().getUser_Type().equals("SUPERVISOR"))
        {
            logout();
        }
        else
        {

        }
       // logout();
    }
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }


}