package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderMenus {

    @SerializedName("order_menu_id")
    @Expose
    private String order_menu_id;

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("menu_type")
    @Expose
    private String menu_type;

    @SerializedName("secondary_name")
    @Expose
    private String secondary_name;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @SerializedName("discount")
    @Expose
    private String discount;

    @SerializedName("discount_value")
    @Expose
    private String discount_value;


    @SerializedName("discount_comment")
    @Expose
    private String discount_comment;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("menu_status_id")
    @Expose
    private String menu_status_id;

    @SerializedName("status_name")
    @Expose
    private String status_name;

    @SerializedName("status_comment")
    @Expose
    private String status_comment;

    @SerializedName("options")
    @Expose
    private ArrayList<OrderMenusOptions> options;

    public ArrayList<OrderMenusOptions> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OrderMenusOptions> options) {
        this.options = options;
    }

    public String getOrder_menu_id() {
        return order_menu_id;
    }

    public void setOrder_menu_id(String order_menu_id) {
        this.order_menu_id = order_menu_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public String getSecondary_name() {
        return secondary_name;
    }

    public void setSecondary_name(String secondary_name) {
        this.secondary_name = secondary_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    public String getDiscount_comment() {
        return discount_comment;
    }

    public void setDiscount_comment(String discount_comment) {
        this.discount_comment = discount_comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMenu_status_id() {
        return menu_status_id;
    }

    public void setMenu_status_id(String menu_status_id) {
        this.menu_status_id = menu_status_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getStatus_comment() {
        return status_comment;
    }

    public void setStatus_comment(String status_comment) {
        this.status_comment = status_comment;
    }
}
