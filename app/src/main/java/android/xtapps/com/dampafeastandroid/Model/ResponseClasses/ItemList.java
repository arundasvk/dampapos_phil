package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ItemList {

    @SerializedName("rowid")
    @Expose
    private String rowid;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("qty")
    @Expose
    private String qty;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("options")
    @Expose
    private ArrayList<OptionList> options;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<OptionList> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionList> options) {
        this.options = options;
    }

    public class OptionList {

        @SerializedName("menu_option_id")
        @Expose
        private String menu_option_id;

        @SerializedName("default_value_id")
        @Expose
        private String default_value_id;

        @SerializedName("required")
        @Expose
        private String required;

        @SerializedName("option_name")
        @Expose
        private String option_name;

        @SerializedName("option_values")
        @Expose
        private ArrayList<OptionValuesList> optionValues;

        public String getMenu_option_id() {
            return menu_option_id;
        }

        public void setMenu_option_id(String menu_option_id) {
            this.menu_option_id = menu_option_id;
        }

        public String getDefault_value_id() {
            return default_value_id;
        }

        public void setDefault_value_id(String default_value_id) {
            this.default_value_id = default_value_id;
        }

        public String getRequired() {
            return required;
        }

        public void setRequired(String required) {
            this.required = required;
        }

        public String getOption_name() {
            return option_name;
        }

        public void setOption_name(String option_name) {
            this.option_name = option_name;
        }

        public ArrayList<OptionValuesList> getOptionValues() {
            return optionValues;
        }

        public void setOptionValues(ArrayList<OptionValuesList> optionValues) {
            this.optionValues = optionValues;
        }

        public  class OptionValuesList {

            @SerializedName("menu_option_value_id")
            @Expose
            private String menu_option_value_id;

            @SerializedName("option_value_id")
            @Expose
            private String option_value_id;

            @SerializedName("option_id")
            @Expose
            private String option_id;

            @SerializedName("option_name")
            @Expose
            private String option_name;

            @SerializedName("display_type")
            @Expose
            private String display_type;

            @SerializedName("new_price")
            @Expose
            private String new_price;

            @SerializedName("value")
            @Expose
            private String value;

            @SerializedName("price")
            @Expose
            private String price;

            @SerializedName("default_value_id")
            @Expose
            private String default_value_id;

            @SerializedName("priority")
            @Expose
            private String priority;

            int state;

            public String getMenu_option_value_id() {
                return menu_option_value_id;
            }

            public void setMenu_option_value_id(String menu_option_value_id) {
                this.menu_option_value_id = menu_option_value_id;
            }

            public String getOption_value_id() {
                return option_value_id;
            }

            public void setOption_value_id(String option_value_id) {
                this.option_value_id = option_value_id;
            }

            public String getOption_id() {
                return option_id;
            }

            public void setOption_id(String option_id) {
                this.option_id = option_id;
            }

            public String getOption_name() {
                return option_name;
            }

            public void setOption_name(String option_name) {
                this.option_name = option_name;
            }

            public String getDisplay_type() {
                return display_type;
            }

            public void setDisplay_type(String display_type) {
                this.display_type = display_type;
            }

            public String getNew_price() {
                return new_price;
            }

            public void setNew_price(String new_price) {
                this.new_price = new_price;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDefault_value_id() {
                return default_value_id;
            }

            public void setDefault_value_id(String default_value_id) {
                this.default_value_id = default_value_id;
            }

            public String getPriority() {
                return priority;
            }

            public void setPriority(String priority) {
                this.priority = priority;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }
    }

}
