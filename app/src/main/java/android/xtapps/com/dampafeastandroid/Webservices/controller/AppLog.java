package android.xtapps.com.dampafeastandroid.Webservices.controller;

import android.util.Log;

/**
 * Created by USER on 7/17/2018.
 */

public class AppLog {

    private static final String TAG = "UserApp";

    public static void logString(String message) {
        // (AppConstant.IS_LOG_ENABLED)
        Log.d(TAG, message);
    }
}
