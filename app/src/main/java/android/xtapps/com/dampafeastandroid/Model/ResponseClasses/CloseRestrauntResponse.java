package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CloseRestrauntResponse {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("report_date")
    @Expose
    private String report_date;

    @SerializedName("closing_time")
    @Expose
    private String closing_time;

    @SerializedName("collection")
    @Expose
    private String collection;

    @SerializedName("waiters_orders")
    @Expose
    private ArrayList<WaitersOrdersList> waiters_orders;


  /*  @SerializedName("cancelled_orders")
    @Expose
    private ArrayList<OrderMenus> cancelled_orders;*/

    @SerializedName("data")
    @Expose
    public data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public CloseRestrauntResponse.data getData() {
        return data;
    }

    public void setData(CloseRestrauntResponse.data data) {
        this.data = data;
    }


    public String getReport_date() {
        return report_date;
    }

    public void setReport_date(String report_date) {
        this.report_date = report_date;
    }

    public ArrayList<WaitersOrdersList> getWaiters_orders() {
        return waiters_orders;
    }

    public void setWaiters_orders(ArrayList<WaitersOrdersList> waiters_orders) {
        this.waiters_orders = waiters_orders;
    }
/*

    public ArrayList<OrderMenus> getCancelled_orders() {
        return cancelled_orders;
    }

    public void setCancelled_orders(ArrayList<OrderMenus> cancelled_orders) {
        this.cancelled_orders = cancelled_orders;
    }
*/

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public   class data
    {
        @SerializedName("collection")
        @Expose
        private String collection;

        @SerializedName("gross_amount")
        @Expose
        private String gross_amount;

        @SerializedName("net_discount")
        @Expose
        private String net_discount;

        @SerializedName("cash_short")
        @Expose
        private String cash_short;

        @SerializedName("total_sales")
        @Expose
        private String total_sales;

        @SerializedName("net_amount")
        @Expose
        private String net_amount;

        @SerializedName("closed_by")
        @Expose
        private String closed_by;

        @SerializedName("tender_cash")
        @Expose
        private String tender_cash;


        @SerializedName("tender_change")
        @Expose
        private String tender_change;

        @SerializedName("payment_received")
        @Expose
        private String payment_received;

        @SerializedName("orders_count")
        @Expose
        private String orders_count;


        @SerializedName("CASH")
        @Expose
        private String CASH;


        @SerializedName("MAKAN")
        @Expose
        private String MAKAN;


        @SerializedName("CARD")
        @Expose
        private String CARD;


        @SerializedName("BOTH")
        @Expose
        private String BOTH;

        @SerializedName("TALABAT")
        @Expose
        private String TALABAT;

        @SerializedName("GIFTCOUPON")
        @Expose
        private String GIFTCOUPON;

        @SerializedName("waiters_orders")
        @Expose
        private ArrayList<WaitersOrdersList> waiters_orders;


        @SerializedName("cancelled_orders")
        @Expose
        private ArrayList<OrderMenus> cancelled_orders;

        public String getCollection() {
            return collection;
        }

        public void setCollection(String collection) {
            this.collection = collection;
        }

        public String getClosed_by() {
            return closed_by;
        }

        public void setClosed_by(String closed_by) {
            this.closed_by = closed_by;
        }

        public String getTender_cash() {
            return tender_cash;
        }

        public void setTender_cash(String tender_cash) {
            this.tender_cash = tender_cash;
        }

        public String getTender_change() {
            return tender_change;
        }

        public void setTender_change(String tender_change) {
            this.tender_change = tender_change;
        }

        public String getOrders_count() {
            return orders_count;
        }

        public void setOrders_count(String orders_count) {
            this.orders_count = orders_count;
        }

        public String getCASH() {
            return CASH;
        }

        public void setCASH(String CASH) {
            this.CASH = CASH;
        }

        public String getCARD() {
            return CARD;
        }

        public void setCARD(String CARD) {
            this.CARD = CARD;
        }

        public String getBOTH() {
            return BOTH;
        }

        public void setBOTH(String BOTH) {
            this.BOTH = BOTH;
        }

        public String getTALABAT() {
            return TALABAT;
        }

        public void setTALABAT(String TALABAT) {
            this.TALABAT = TALABAT;
        }

        public String getGIFTCOUPON() {
            return GIFTCOUPON;
        }

        public void setGIFTCOUPON(String GIFTCOUPON) {
            this.GIFTCOUPON = GIFTCOUPON;
        }


        public String getMAKAN() {
            return MAKAN;
        }

        public void setMAKAN(String MAKAN) {
            this.MAKAN = MAKAN;
        }

        public ArrayList<WaitersOrdersList> getWaiters_orders() {
            return waiters_orders;
        }

        public void setWaiters_orders(ArrayList<WaitersOrdersList> waiters_orders) {
            this.waiters_orders = waiters_orders;
        }

        public ArrayList<OrderMenus> getCancelled_orders() {
            return cancelled_orders;
        }

        public void setCancelled_orders(ArrayList<OrderMenus> cancelled_orders) {
            this.cancelled_orders = cancelled_orders;
        }

        public String getGross_amount() {
            return gross_amount;
        }

        public void setGross_amount(String gross_amount) {
            this.gross_amount = gross_amount;
        }

        public String getNet_discount() {
            return net_discount;
        }

        public void setNet_discount(String net_discount) {
            this.net_discount = net_discount;
        }

        public String getCash_short() {
            return cash_short;
        }

        public void setCash_short(String cash_short) {
            this.cash_short = cash_short;
        }

        public String getTotal_sales() {
            return total_sales;
        }

        public void setTotal_sales(String total_sales) {
            this.total_sales = total_sales;
        }

        public String getNet_amount() {
            return net_amount;
        }

        public void setNet_amount(String net_amount) {
            this.net_amount = net_amount;
        }

        public String getPayment_received() {
            return payment_received;
        }

        public void setPayment_received(String payment_received) {
            this.payment_received = payment_received;
        }
    }




}
