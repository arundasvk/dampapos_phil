package android.xtapps.com.dampafeastandroid.Model.ResponseClasses;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CartItem {
    @SerializedName("order_menu_id")
    @Expose
    private String order_menu_id;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("menu_id")
    @Expose
    private String menu_id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("secondary_name")
    @Expose
    private String name2;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("menu_status_id")
    @Expose
    private String menu_status_id;

    @SerializedName("menu_status_name")
    @Expose
    private String menu_status_name;

    @SerializedName("menu_status_comment")
    @Expose
    private String menu_status_comment;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("discount")
    @Expose
    private String discount;

    @SerializedName("discount_comment")
    @Expose
    private String discount_comment;

    @SerializedName("discount_value")
    @Expose
    private String discount_value;

   @SerializedName("options")
    @Expose
    private ArrayList<OptionValuesList> optionValues;

    public String getOrder_menu_id() {
        return order_menu_id;
    }

    public void setOrder_menu_id(String order_menu_id) {
        this.order_menu_id = order_menu_id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMenu_status_id() {
        return menu_status_id;
    }

    public void setMenu_status_id(String menu_status_id) {
        this.menu_status_id = menu_status_id;
    }

    public String getMenu_status_name() {
        return menu_status_name;
    }

    public void setMenu_status_name(String menu_status_name) {
        this.menu_status_name = menu_status_name;
    }

    public String getMenu_status_comment() {
        return menu_status_comment;
    }

    public void setMenu_status_comment(String menu_status_comment) {
        this.menu_status_comment = menu_status_comment;
    }

   public ArrayList<OptionValuesList> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(ArrayList<OptionValuesList> optionValues) {
        this.optionValues = optionValues;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_comment() {
        return discount_comment;
    }

    public void setDiscount_comment(String discount_comment) {
        this.discount_comment = discount_comment;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }
}

