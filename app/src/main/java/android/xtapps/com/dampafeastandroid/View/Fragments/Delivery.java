package android.xtapps.com.dampafeastandroid.View.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Delivery extends Fragment {

    public static Delivery newInstance() {
        Delivery fragment = new Delivery();
        return fragment;
    }
    Context mContext;
    private TextView tv_delivery;
    private EditText edt_guest_name,edt_number,edt_one,edt_two,edt_three,edt_four,edt_five;
    private String guest_name,guest_number,detail_one,detail_two,detail_three,detail_four,detail_five;
    public static final String MyPREFERENCES_DELIVERY= "MyPrefsDelivery";
    private SharedPreferences sharedpreferencesDelivery;

    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    Spinner spin;
    String no_guest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.deliveryfragment, container, false);
        mContext = getActivity();

        spin = view.findViewById(R.id.spinner);


        tv_delivery = view.findViewById(R.id.tv_delivery);
        edt_guest_name = view.findViewById(R.id.edt_guest_name);
        edt_number = view.findViewById(R.id.edt_number);
        edt_one = view.findViewById(R.id.edt_one);
        edt_two = view.findViewById(R.id.edt_two);
        edt_three = view.findViewById(R.id.edt_three);
        edt_four = view.findViewById(R.id.edt_four);
        edt_five = view.findViewById(R.id.edt_five);


        sharedpreferencesDelivery = this.getActivity().getSharedPreferences(MyPREFERENCES_DELIVERY,Context.MODE_PRIVATE);
        sharedpreferencesData = this.getActivity().getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);

        setTextPreference();

        String[] types = getResources().getStringArray(R.array.guestCount);
        final List<String> nationalityType = new ArrayList<>(Arrays.asList(types));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_text, nationalityType) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spin.setAdapter(spinnerArrayAdapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                no_guest = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text


                }
                String spinnerValue = spin.getSelectedItem().toString();
                AppSession.getInstance().setGuest_Count(spinnerValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void setTextPreference() {

        if( !sharedpreferencesData.getString("GuestName","").equals(""))
        {
            edt_guest_name.setText( sharedpreferencesData.getString("GuestName",""));
        }
        if( !sharedpreferencesData.getString("GuestNumber","").equals(""))
        {
            edt_number.setText( sharedpreferencesData.getString("GuestNumber",""));
        }

        if( !sharedpreferencesData.getString("DetailOne","").equals(""))
        {
            edt_one.setText( sharedpreferencesData.getString("DetailOne",""));
        }
        if( !sharedpreferencesData.getString("Detailtwo","").equals(""))
        {
            edt_two.setText( sharedpreferencesData.getString("Detailtwo",""));
        }
        if( !sharedpreferencesData.getString("DetailThree","").equals(""))
        {
            edt_three.setText( sharedpreferencesData.getString("DetailThree",""));
        }
        if( !sharedpreferencesData.getString("DetailFour","").equals(""))
        {
            edt_four.setText( sharedpreferencesData.getString("DetailFour",""));
        }
        if( !sharedpreferencesData.getString("DeatilFive","").equals(""))
        {
            edt_five.setText( sharedpreferencesData.getString("DeatilFive",""));
        }
    }
    private void setPreference() {
        guest_name = edt_guest_name.getText().toString().trim();
        guest_number = edt_number.getText().toString().trim();
        detail_one = edt_one.getText().toString().trim();
        detail_two = edt_two.getText().toString().trim();
        detail_three = edt_three.getText().toString().trim();
        detail_four = edt_four.getText().toString().trim();
        detail_five = edt_five.getText().toString().trim();

        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
        editor1.putString("GuestName", guest_name);
        editor1.putString("GuestNumber", guest_number);
        editor1.putString("DetailOne", detail_one);
        editor1.putString("Detailtwo", detail_two);
        editor1.putString("DetailThree", detail_three);
        editor1.putString("DetailFour", detail_four);
        editor1.putString("DeatilFive", detail_five);
        editor1.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setPreference();
    }


}
