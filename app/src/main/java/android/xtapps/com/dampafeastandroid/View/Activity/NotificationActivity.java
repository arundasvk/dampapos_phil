package android.xtapps.com.dampafeastandroid.View.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.Controller.Constants;
import android.xtapps.com.dampafeastandroid.Interfaces.LogOutTimerUtil;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.ReportResponse;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Adapters.MyOrderListViewNewAdapter;
import android.xtapps.com.dampafeastandroid.View.Adapters.NotifictionAlladapterlist;
import android.xtapps.com.dampafeastandroid.Webservices.backgroundtask.CallWebServiceTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_CREATED_AT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_MENU_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_ORDER_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STAFF_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_STATUS;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_ID;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TABLE_NAME;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TEXT;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_TYPE;
import static android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity.NOTI_UPDATED_AT;

public class NotificationActivity extends AppCompatActivity implements onApiFinish, LogOutTimerUtil.LogOutListener {
    ProgressBar simpleProgressBar;
    private Context mContext;
    private String TAG = NotificationActivity.class.getSimpleName();

    private String  FROM_MyNotification="mynotification";
    private ExpandableListView expandableListView;
    private ArrayList<OrderList> headerList = new ArrayList<OrderList>();
    MyOrderListViewNewAdapter listAdapter;
    ImageView print;
    String date;
    private String messageResult;
    ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();
    HashMap<String, String> map1;
    private ListView listview;
    NotifictionAlladapterlist adapter2;
    ImageView nav_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifications);

        loadmyNotificationData();


        simpleProgressBar = (ProgressBar)findViewById(R.id.progressSpinner);

        listview = (ListView)findViewById(R.id.List);
        nav_back = (ImageView) findViewById(R.id.nav_back);

        nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }

    @Override
    public void onSuccess(String tag, String response) {


        if (tag.equals(FROM_MyNotification)) {
            simpleProgressBar.setVisibility(View.GONE);



            try {




                JSONObject myJson = new JSONObject(response);
                messageResult=myJson.getString("status");




                if(messageResult.equals("success")){

                    String text = myJson.getString("notification");





                    String otpCode =text ;
              /*  final ArrayList<HashMap<String, String>> map1 =(ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra(SplashScreenActivity.LOCAION_ARRAY);
                for (int i=0;i<map1.size();i++) {
                  String  id = map1.get(i).get(SplashScreenActivity.LOCAION_ID);
                    String location_name = map1.get(i).get(SplashScreenActivity.LOCAION_NAME);
                    *//* Build the StringWithTag List using these keys and values. *//*
                    Toast.makeText(NavigationMainActivity.this, id+" "+location_name, Toast.LENGTH_SHORT).show();
                }*/



                    JSONArray jArray = null;
                    try {
                        if(otpCode!=null&&!otpCode.equals(""))
                            jArray = new JSONArray(otpCode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (jArray.length()==0)
                    {
                        Toast.makeText(getApplicationContext(),myJson.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        categoryList.clear();
                        for(int i=0;i<jArray.length();i++){

                            JSONObject json_data = null;
                            try {
                                json_data = jArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String text1 = null;
                            String table_id = null;
                            String staff_id = null;
                            String menu_id = null;
                            String order_id = null;
                            String status = null;
                            String created_at = null;
                            String updated_at = null;
                            String table_name = null;
                            String order_type = null;
                            try {
                                text1 = json_data.getString("text");
                                table_id = json_data.getString("id");
                                order_type = json_data.getString("order_type");
                                table_name = json_data.getString("table_name");
                                staff_id = json_data.getString("staff_id");
                                menu_id = json_data.getString("menu_id");
                                order_id = json_data.getString("order_id");
                                status = json_data.getString("status");
                                created_at = json_data.getString("created_at");
                                updated_at = json_data.getString("updated_at");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            map1 = new HashMap<String, String>();

                            map1.put(NOTI_TABLE_ID, table_id);
                            map1.put(NOTI_TABLE_NAME, table_name);
                            map1.put(NOTI_TYPE, order_type);
                            map1.put(NOTI_TEXT, text1);
                            map1.put(NOTI_STAFF_ID, staff_id);
                            map1.put(NOTI_MENU_ID, menu_id);
                            map1.put(NOTI_ORDER_ID, order_id);
                            map1.put(NOTI_STATUS, status);
                            map1.put(NOTI_CREATED_AT, created_at);
                            map1.put(NOTI_UPDATED_AT, updated_at);
                            categoryList.add(map1);


                        }

                    }
                    /*
                     * Step 3: We can update the UI of the activity here
                     * */


                    listview.setVisibility(View.VISIBLE);
                    adapter2 = new NotifictionAlladapterlist(NotificationActivity.this,categoryList);
                    listview.setAdapter(adapter2);
                    adapter2.notifyDataSetChanged();


                }
                else{





                }





            } catch (Exception e) {
                // TODO: handle exception


                // Signed out, show unauthenticated UI.
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();

                //Toast.makeText(AccessCodeActivity.this, "Please check your internet connection",
                //  Toast.LENGTH_LONG).show();
                Log.e("log_tag", "Error parsing data " + e.toString());
            }





        }




    }

    @Override
    public void onFailed(String tag, String response) {
        Gson gson = new Gson();
        ReportResponse menuResponse = gson.fromJson(response, ReportResponse.class);
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),menuResponse.getMessage(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onJsonError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.json_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String tag, String response) {
        simpleProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }


    private void loadmyNotificationData() {
        String URL = AppSession.getInstance().getRegUrl()+ Constants.API.NOTIFICATIONAPI;;


        HashMap<String, String> values = new HashMap<>();

        /*values.put("token", AppSession.getInstance(getApplicationContext()).getTabToken());*/
        /* values.put("action", "orderReports");*/
        values.put("tab_token",AppSession.getInstance().getTabToken());
        values.put("action", Constants.API.ACTION_NOTIFICATIONAPI);
        values.put("location_id", AppSession.getInstance().getLocation_id());
        values.put("staff_id", AppSession.getInstance().getStaff_ID());
        values.put("all","1");


        CallWebServiceTask objCallWebServiceTask = new CallWebServiceTask(NotificationActivity.this, values);
        objCallWebServiceTask.execute(URL,FROM_MyNotification, "POST");
    }
    @Override
    protected void onStart() {
        super.onStart();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "OnStart () &&& Starting timer");
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
        Log.e(TAG, "User interacting with screen");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "onResume()");
    }

    /**
     * Performing idle time logout
     */
    @Override
    public void doLogout() {
        logout();
        // write your stuff here
    }
    private void logout() {
        AppSession.getInstance().setTable_ID("");
        AppSession.getInstance().setTable_Name("");
        AppSession.getInstance().setOrderNumber("");
        AppSession.getInstance().setOrder_idTake_delivery("");
        AppSession.getInstance().setTick("logout");
        Intent i=new Intent(NotificationActivity.this,NavigationMainActivity.class);
        startActivity(i);
        finish();
    }
}

