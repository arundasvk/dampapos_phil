package android.xtapps.com.dampafeastandroid.View.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.xtapps.com.dampafeastandroid.Controller.AppSession;
import android.xtapps.com.dampafeastandroid.DB.CartItem;
import android.xtapps.com.dampafeastandroid.DB.DBMethodsCart;
import android.xtapps.com.dampafeastandroid.Interfaces.onApiFinish;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OptionValuesList;
import android.xtapps.com.dampafeastandroid.Model.ResponseClasses.OrderInfo;
import android.xtapps.com.dampafeastandroid.R;
import android.xtapps.com.dampafeastandroid.View.Activity.NavigationMainActivity;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

public class TakeAwayGridviewAdapter extends BaseAdapter {

    private Activity activity;
    //private ArrayList<ImageModel> data;
    private List<OrderInfo> mProductLists;
    private static LayoutInflater inflater=null;
    private TakeAwayGridviewAdapter.ViewHolder holder = null;
    HashMap<String, String> song;
    int row_index=-1;
    private String FROM_CONTINUE = "continue_order";
    public static final String MyPREFERENCES_DATA = "MyPrefsData";
    private SharedPreferences sharedpreferencesData;
    OrderInfo object;
    List<OptionValuesList> mContinueLists2;
    boolean changed=false;
    onApiFinish mOnApiFinish;
    int cartcount;
    TextView tv_item_name,tv_add_cart_nav;
    LinearLayout ll_rightside_frame;
    List<OrderInfo> mContinueLists;
    private String selected_items;
    Double Total=0.0;
    DecimalFormat df2;
    int POSITION;
    public TakeAwayGridviewAdapter(Activity a, List<OrderInfo> d) {
        activity = a;
        mProductLists=d;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mProductLists.size();
        //return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder {

        TextView tv_tablename,tv_staff_name;
        LinearLayout l_layout_table_item;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        object = mProductLists.get(position);
        POSITION=position;
        View vi=convertView;
        if(convertView==null) {
            // vi = inflater.inflate(R.layout.horizontal_list_item, null);
            vi = inflater.inflate(R.layout.sub_screen_grid_row, null);
            holder = new TakeAwayGridviewAdapter.ViewHolder();

            holder.tv_tablename = (TextView) vi.findViewById(R.id.tv_tablename);
            holder.tv_staff_name = (TextView) vi.findViewById(R.id.tv_staff_name);
            holder.tv_staff_name.setVisibility(View.VISIBLE);

            holder.l_layout_table_item = (LinearLayout) vi.findViewById(R.id.l_layout_table_item);

            tv_item_name =(TextView)activity.findViewById(R.id.tv_item_name);
            tv_add_cart_nav =(TextView)activity.findViewById(R.id.tv_add_cart);
            ll_rightside_frame=(LinearLayout)activity.findViewById(R.id.ll_rightside_frame);
            // holder.grid_container = (LinearLayout) vi.findViewById(R.id.grid_container);

            sharedpreferencesData = activity.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


            cartcount = DBMethodsCart.newInstance(activity).getCartItemsCount();
            df2 = new DecimalFormat(".##");


            vi.setTag(holder);
        } else {
            holder = (TakeAwayGridviewAdapter.ViewHolder) vi.getTag();
        }




        final OrderInfo item = mProductLists.get(position);

       /* holder.tv_tablename.setText(item.getOrder_id());*/
        holder.tv_tablename.setText(item.getSale_no());
        holder.tv_staff_name.setText(item.getStaff());


        holder.l_layout_table_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;

               /* if (! activity.isFinishing()) {
                    loadData();

                }*/
                if(cartcount>0&&!AppSession.getInstance().getTABLE_ChangeFLAG().equals(""))

                {
                    List<CartItem> new_orders;
                    new_orders = DBMethodsCart.newInstance(activity).getByOrderStatus( "");
                    if(new_orders.size()>0)
                    {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(activity);
                        }
                        builder.setTitle("Changing Order Will Clear Your Cart Items ")
                                .setMessage("Are you sure you want to change the selected Order?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        AppSession.getInstance().setTABLE_ChangeFLAG("");
                                        DBMethodsCart.newInstance(activity).clearCart();
                                        DBMethodsCart.newInstance(activity).clearModifier();
                                        tv_item_name.setText("");
                                        tv_add_cart_nav.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
                                        row_index=position;

                                        //AppSession.getInstance().setTable_ID(item.getOrder_id());
                                       /* AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());*/
                                        if(AppSession.getInstance().getServiceType().equals("delivery"))
                                        {
                                            AppSession.getInstance().setOrder_id_delivery(item.getOrder_id());
                                        }
                                        else  if(AppSession.getInstance().getServiceType().equals("takeaway"))
                                        {

                                            AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());
                                        }
                                        sharedpreferencesData = activity.getSharedPreferences(MyPREFERENCES_DATA, Context.MODE_PRIVATE);


                                        notifyDataSetChanged();
                                        Intent i = new Intent(activity, NavigationMainActivity.class);
                                        activity.startActivity(i);

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }
                    else
                    {
                        AppSession.getInstance().setTABLE_ChangeFLAG("");
                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();
                        tv_item_name.setText("");
                        tv_add_cart_nav.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
                        row_index=position;

                        // AppSession.getInstance().setTable_ID(item.getOrder_id());
                        /*AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());*/
                        if(AppSession.getInstance().getServiceType().equals("delivery"))
                        {
                            AppSession.getInstance().setOrder_id_delivery(item.getOrder_id());
                        }
                        else  if(AppSession.getInstance().getServiceType().equals("takeaway"))
                        {

                            AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());
                        }


                        notifyDataSetChanged();
                        if(AppSession.getInstance().getOrder_idTake_delivery().equals(item.getOrder_id())
                                ||AppSession.getInstance().getOrder_id_delivery().equals(item.getOrder_id()))
                        {
                            AppSession.getInstance().setTableContinueOrder("true");

                            DBMethodsCart.newInstance(activity).clearCart();
                            DBMethodsCart.newInstance(activity).clearModifier();

                            if (! activity.isFinishing()) {
                                loadData();
                            }

                        }
                        else
                        {

                            if(DBMethodsCart.newInstance(activity).getByOrderStatus( "").size()<=0)
                            {
                                DBMethodsCart.newInstance(activity).clearCart();
                                DBMethodsCart.newInstance(activity).clearModifier();
                            }
                            // AppSession.getInstance().setTableContinueOrder("");
                            SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                            editor1.putString("order_comment","");
                            editor1.commit();
                            AppSession.getInstance().setOrderNumber("");
                            AppSession.getInstance().setOrderTotal("0");

                            Intent myIntent = new Intent(activity, NavigationMainActivity.class);
                            activity.startActivity(myIntent);
                        }
                    }
                    //alert();

                }
                else
                {


                    row_index=position;

                    //AppSession.getInstance().setTable_ID(item.getOrder_id());
                   /* AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());*/
                    if(AppSession.getInstance().getServiceType().equals("delivery"))
                    {
                        AppSession.getInstance().setOrder_id_delivery(item.getOrder_id());
                    }
                    else  if(AppSession.getInstance().getServiceType().equals("takeaway"))
                    {

                        AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());
                    }
                    notifyDataSetChanged();
                    if(AppSession.getInstance().getOrder_idTake_delivery().equals(item.getOrder_id())
                            ||AppSession.getInstance().getOrder_id_delivery().equals(item.getOrder_id()))
                    {
                        AppSession.getInstance().setTableContinueOrder("true");

                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();

                        if (! activity.isFinishing()) {
                            loadData();
                        }

                    }
                    else
                    {

                        if(DBMethodsCart.newInstance(activity).getByOrderStatus( "").size()<=0)
                        {
                            DBMethodsCart.newInstance(activity).clearCart();
                            DBMethodsCart.newInstance(activity).clearModifier();
                        }
                        // AppSession.getInstance().setTableContinueOrder("");
                        AppSession.getInstance().setOrderNumber("");
                        AppSession.getInstance().setOrderTotal("0");

                        SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                        editor1.putString("order_comment","");
                        editor1.commit();
                        Intent myIntent = new Intent(activity, NavigationMainActivity.class);
                        activity.startActivity(myIntent);
                    }
                }
                // if(AppSession.getInstance().getTable_ID().equals(item.getOrder_id()))
                /*if(AppSession.getInstance().getOrder_idTake_delivery().equals(item.getOrder_id())
                        ||AppSession.getInstance().getOrder_id_delivery().equals(item.getOrder_id()))
                {
                    AppSession.getInstance().setTableContinueOrder("true");

                    DBMethodsCart.newInstance(activity).clearCart();
                    DBMethodsCart.newInstance(activity).clearModifier();

                    if (! activity.isFinishing()) {
                        loadData();
                    }

                }
                else
                {

                    if(DBMethodsCart.newInstance(activity).getByOrderStatus( "").size()<=0)
                    {
                        DBMethodsCart.newInstance(activity).clearCart();
                        DBMethodsCart.newInstance(activity).clearModifier();
                    }
                    // AppSession.getInstance().setTableContinueOrder("");
                    AppSession.getInstance().setOrderNumber("");
                    Intent myIntent = new Intent(activity, NavigationMainActivity.class);
                    activity.startActivity(myIntent);
                }*/

            }
        });
        if(row_index==position){
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
            // AppSession.getInstance().setTable_ID(item.getOrder_id());
           /* AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());*/
            if(AppSession.getInstance().getServiceType().equals("delivery"))
            {
                AppSession.getInstance().setOrder_id_delivery(item.getOrder_id());
            }
            else  if(AppSession.getInstance().getServiceType().equals("takeaway"))
            {

                AppSession.getInstance().setOrder_idTake_delivery(item.getOrder_id());
            }

        }
        else
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.gridview_item_shape);

        }
      /*  if(AppSession.getInstance().getTable_ID().equals(item.getOrder_id()))*/
        if(AppSession.getInstance().getOrder_idTake_delivery().equals(item.getOrder_id())||
                AppSession.getInstance().getOrder_id_delivery().equals(item.getOrder_id()) )
        {
            holder.l_layout_table_item.setBackgroundResource(R.drawable.table_green_bg);
           /* holder.tv_staff_name.setVisibility(View.VISIBLE);*/
            holder.l_layout_table_item.setEnabled(true);
            holder.l_layout_table_item.setClickable(true);


        }

        if(cartcount<=0)
        {
            tv_item_name.setText("");
            tv_add_cart_nav.setText(AppSession.getInstance().getRestraunt_Decimal_Zero()+" "+AppSession.getInstance().getRestraunt_Currency());
        }
        else
        {
            if (! activity.isFinishing()) {
                bottomBarSettext();
            }
           /* Intent i= new Intent(activity, NavigationMainActivity.class);
            activity.startActivity(i);*/

        }


        return vi;
    }

    private void loadData() {
        AppSession.getInstance().setOrderTotal(mProductLists.get(row_index).getOrder_total().toString());
        AppSession.getInstance().setOrderNumber(mProductLists.get(row_index).getOrder_id().toString());
        AppSession.getInstance().setOrder_status(mProductLists.get(row_index).getOrder_status().toString());
        AppSession.getInstance().setComplteBillDetails(AppSession.getInstance().getOrder_status()+AppSession.getInstance().getOrderNumber());

        AppSession.getInstance().setContinue_position(row_index);
        for(int j=0;j<mProductLists.get(row_index).getItems().size();j++)
        {
            // DecimalFormat df2 = new DecimalFormat("###.###");
            double f = Double.parseDouble( mProductLists.get(row_index).getItems().get(j).getPrice().toString());
            // String ft =df2.format(f);
            /*String ft = String.format("%.3f", f);*/
            String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), f);
            String order_m_id=  mProductLists.get(row_index).getItems().get(j).getOrder_menu_id();
            if(order_m_id==null||order_m_id.equals(""))
            {
                order_m_id=  mProductLists.get(row_index).getItems().get(j).getMenu_id().toString();
            }
            DBMethodsCart.newInstance(activity).insertCartItems(
                    mProductLists.get(row_index).getItems().get(j).getMenu_id().toString(),
                    mProductLists.get(row_index).getItems().get(j).getOrder_menu_id().toString(),
                    "",
                    mProductLists.get(row_index).getItems().get(j).getName().toString(),
                    mProductLists.get(row_index).getItems().get(j).getName2().toString(),
                    mProductLists.get(row_index).getOrder_id().toString(),
                    "",
                    "",
                    "",
                    "",
                    "",
                    ft,
                    1,
                    Integer.valueOf(mProductLists.get(row_index).getItems().get(j).getQuantity().toString()),
                    Integer.valueOf(mProductLists.get(row_index).getItems().get(j).getQuantity().toString()),
                    (Double.valueOf(ft)*
                            Double.valueOf(mProductLists.get(row_index).getItems().get(j).getQuantity().toString())),
                    "2",
                    mProductLists.get(row_index).getItems().get(j).getComment().toString(),
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "order_sent",
                    Double.valueOf( mProductLists.get(row_index).getItems().get(j).getDiscount_value().toString()),
                    Double.valueOf( mProductLists.get(row_index).getItems().get(j).getDiscount().toString()),
                    mProductLists.get(row_index).getItems().get(j).getDiscount_comment().toString());


            mContinueLists2=mProductLists.get(row_index).getItems().get(j).getOptionValues();
            if(mContinueLists2.size()>0 && mContinueLists2 !=null)
            { for(int k =0;k<mContinueLists2.size();k++)
            { DBMethodsCart.newInstance(activity).insertModifierItems( mProductLists.get(row_index).getItems().get(j).getMenu_id().toString(),
                    mContinueLists2.get(k).getOption_name(),
                    mProductLists.get(row_index).getItems().get(j).getOrder_menu_id().toString(),
                    mContinueLists2.get(k).getMenu_option_value_id(),
                    mContinueLists2.get(k).getOption_value_id(),
                    mContinueLists2.get(k).getOption_id(),
                    mContinueLists2.get(k).getOption_name(),
                    mContinueLists2.get(k).getDisplay_type(),
                    mContinueLists2.get(k).getValue(),
                    mContinueLists2.get(k).getNew_price(),
                    mContinueLists2.get(k).getPrice()); } }
            if(AppSession.getInstance().getServiceType().equals("dine_in"))
            {
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.putString("order_comment", mProductLists.get(row_index).getComment().toString());

                editor1.putString("dinein_GuestName", mProductLists.get(row_index).getCustomer().getName());
                editor1.putString("dinein_GuestNumber", mProductLists.get(row_index).getCustomer().getMobile_number());
                editor1.putString("dinein_guest_count", mProductLists.get(row_index).getCustomer().getNo_of_pax());
                editor1.putString("dinein_edt_senior_guest", mProductLists.get(row_index).getCustomer().getSenior_citizen());
                editor1.putString("dinein_edt_solo_guest", mProductLists.get(row_index).getCustomer().getSolo_parent());
                editor1.putString("dinein_edt_disabled", mProductLists.get(row_index).getCustomer().getDisabled());
                editor1.commit();
            }
            else if(AppSession.getInstance().getServiceType().equals("takeaway"))
            {
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.putString("order_comment", mProductLists.get(row_index).getComment().toString());


                editor1.putString("takeaway_GuestName", mProductLists.get(row_index).getCustomer().getName());
                editor1.putString("takeaway_GuestNumber", mProductLists.get(row_index).getCustomer().getMobile_number());
                editor1.commit();
            }
            else if(AppSession.getInstance().getServiceType().equals("delivery"))
            {
                SharedPreferences.Editor editor1 = sharedpreferencesData.edit();
                editor1.putString("order_comment", mProductLists.get(row_index).getComment().toString());


                editor1.putString("delivery_GuestName", mProductLists.get(row_index).getCustomer().getName());
                editor1.putString("delivery_GuestNumber", mProductLists.get(row_index).getCustomer().getMobile_number());
                editor1.putString("DetailOne", mProductLists.get(row_index).getCustomer().getAddress1());
                editor1.putString("Detailtwo", mProductLists.get(row_index).getCustomer().getAddress2());
                editor1.putString("DetailThree",  mProductLists.get(row_index).getCustomer().getLocation());
                editor1.putString("DetailFour", mProductLists.get(row_index).getCustomer().getArea());
                editor1.putString("DeatilFive", mProductLists.get(row_index).getCustomer().getCity());
                editor1.commit();
            }
        }


        AppSession.getInstance().setTABLE_ChangeFLAG("added_cart");
        notifyDataSetChanged();
        //  Toast.makeText(activity,tableResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (! activity.isFinishing()) {
            Intent myIntent = new Intent(activity, NavigationMainActivity.class);
            activity.startActivity(myIntent);
            //bottomBarSettext();
        }
    }
    private void bottomBarSettext() {
        selected_items="";
        Total=0.0;
        List<CartItem> cartItems;
        cartItems =DBMethodsCart.newInstance(activity).getItemname_qty( );
        for(int i=0;i<cartItems.size();i++)
        {
            selected_items=selected_items+cartItems.get(i).getMenu_name()+" X "+
                    cartItems.get(i).getMenu_qty()+", ";
            Total=Total+cartItems.get(i).getMenu_qty_X_menu_price();
        }
        tv_item_name.setText(selected_items);
       /* String ft =df2.format(Total);*/
        String ft = String.format(AppSession.getInstance().getRestraunt_Decimal_Format(), Total);
        tv_add_cart_nav.setText(String.valueOf(ft)+" "+AppSession.getInstance().getRestraunt_Currency());
    }


}